# Semantic segmentation model inference on torchscripts only.
#
from typing import List, Optional, Union

import numpy as np
import PIL
import torch
import torchvision.transforms as transforms
from PIL import Image

from sl_camera_sensor.utils import logger

from ..model_base import ModelBase

log = logger.getLogger(__name__)


class SegmentationModel(ModelBase):
    """This is an inference class that accepts only torch scripts (.pt files)
    and TensorRT optimized torch scripts (.ts files). If you have
    .pth files, export them with `torch.jit.trace`
    """

    def __init__(
        self,
        target_size: Optional[tuple] = (576, 1024),
        batch_size: Optional[int] = 8,
    ) -> None:
        """
        Any .ts files should have been compiled on the GPU you are currently
        running on. If you have TRT compilation errors, the .pt file should work
        anywhere.
        :target_size: this is the input tensor shape of the model!
        :param model_name: file name of the model you want to run, that lives in
        $GRAPHS_DIR/segmentation/{model_name}
        """
        super().__init__()
        self.model, self.label_map = self.load_model('segmentation')
        self.target_size = target_size
        self.batch_size = batch_size

        # Below are a function of training data - what did you train your model with?
        self.means = [0.5125, 0.5363, 0.5526]
        self.stds = [0.1679, 0.1527, 0.1653]

    @staticmethod
    def get_label_map():
        return ModelBase._load_label_map("segmentation")

    def predict(
        self,
        frames: Union[
            np.ndarray,
            str,
            PIL.Image.Image,
            List[np.ndarray],
            List[str],
            List[PIL.Image.Image],
            torch.Tensor,
        ],
        logits: Optional[bool] = False,
        check_input: Optional[bool] = True,
        output_size: Optional[tuple] = None,
    ) -> np.ndarray:
        """
        Inference on a single frame/tensor or stack of frames/tensors.
        :param frames: Some representation of unit8 frames.
        :param logits: bool to return logits or not. Default is to return
        argmax of logits.
        :returns: np.array of logits or most probable class integer.
        """
        tensors = self.check_input_torch(frames)
        if tensors == [] or len(tensors) == 0:
            return []
        # Pad with zeros if we're given the wrong batch size. TRT will throw an error
        # otherwise.
        dbatch = 0
        if tensors.shape[0] != self.batch_size:
            dbatch = self.batch_size - tensors.shape[0]
            zeros = torch.zeros(
                dbatch,
                *list(tensors.shape[1:]),
                dtype=tensors.dtype,
                device=tensors.device,
            )
            tensors = torch.cat((tensors, zeros), dim=0)
        masks = self.model(tensors).cpu().numpy()
        # If someone requested a specific output size we have no option but to argmax.
        if output_size is not None:
            if logits:
                log.info(
                    'Requested both logits and output '
                    'reshape - suppressing logits request'
                )
            logits = False
        if not logits:
            if masks.ndim == 4:
                masks = np.argmax(masks, axis=1).astype(np.uint8)
            elif masks.ndim == 3:
                masks = np.argmax(masks, axis=0).astype(np.uint8)
            else:
                raise ValueError(
                    "I'm trying to argmax the segmentation masks, but "
                    f"I don't know on which axis to do that - masks: {masks.shape}"
                )
        if dbatch > 0:
            # Remove zero padded entries TODO move to base class postprocessing?
            masks = masks[: self.batch_size - dbatch]

        if output_size is not None and masks[0].shape != output_size:
            new_masks = []
            for mask in masks:
                new_masks.append(
                    np.asarray(
                        Image.fromarray(mask.astype(np.uint8)).resize(
                            output_size[::-1], Image.NEAREST
                        )
                    )
                )
            masks = new_masks
        return masks

    def preprocess(
        self, frame: Union[torch.Tensor, PIL.Image.Image]
    ) -> torch.Tensor:
        """Called from base class, which only passes PIL or torch.Tensor
        types.
        """
        if isinstance(frame, PIL.Image.Image):
            transf = transforms.Compose(
                [
                    transforms.Resize(self.target_size),
                    transforms.ToTensor(),
                    transforms.Normalize(mean=self.means, std=self.stds),
                ]
            )(frame)
        else:
            transf = transforms.Normalize(mean=self.means, std=self.stds)(
                frame
            )
        # Assume trtorch models are float16
        return transf.half() if self.is_trtorch else transf
