# Loads label_map.pbtxt files into dicts.
#
import ast
import os

from sl_camera_sensor.utils import logger

log = logger.getLogger(__name__)


def load_label_map(label_map_path):
    """It's either this or a wacky protobuf circle to get this
    into a dict, or a tensorflow object detection API dependency in here.
    """
    if not isinstance(label_map_path, str):
        raise ValueError("Input label_map should be a string path.")
    if not os.path.isfile(label_map_path):
        raise FileNotFoundError(f"File: {label_map_path} doesn't exist.")
    if not label_map_path.endswith(".pbtxt"):
        raise ValueError("Input label map path should be a .pbtxt file.")
    try:
        with open(label_map_path) as f:
            ltxt = f.read()
    except IOError:
        raise IOError(f"Cannot read {label_map_path}.")
    label_map = {}

    for txt in ltxt.split("item")[1:]:
        pert = (
            txt.replace("\n", "")
            .replace("  id:", "")
            .replace("  name:", ":")
            .replace(" {", "{")
        )
        try:
            lm = ast.literal_eval(pert)
        except Exception:
            raise ValueError(f"Unable to evaluate {pert} to literal dict")

        k, v = zip(*lm.items())
        label_map[v[0]] = k[0]  # ivert this, so it's {string: int, ...}
    return label_map
