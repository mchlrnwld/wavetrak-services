# -*- coding: utf-8 -*-
import logging
import os


def getLogger(name):
    try:
        os.environ["LOG_LEVEL"]
    except KeyError:
        pass
    logging.basicConfig(
        format="%(asctime)s %(levelname)-8s %(message)s",
        level=os.environ["LOG_LEVEL"],
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    return logging.getLogger(name)
