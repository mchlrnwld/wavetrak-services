provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/highlights-batch-job/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cam_highlights_job_definition" {
  source           = "../../"
  environment      = "sandbox"
  image            = "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/cam-highlights/cam-highlights:sandbox"
  hosted_zone_name = "sandbox.surfline.com"
}
