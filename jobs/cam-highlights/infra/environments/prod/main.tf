provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/highlights-batch-job/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cam_highlights_job_definition" {
  source           = "../../"
  environment      = "prod"
  image            = "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/cam-highlights/cam-highlights:prod"
  hosted_zone_name = "cdn-surfline.com"
}
