provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "jobs/highlights-batch-job/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cam_highlights_job_definition" {
  source           = "../../"
  environment      = "staging"
  image            = "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/cam-highlights/cam-highlights:staging"
  hosted_zone_name = "staging.surfline.com"
}
