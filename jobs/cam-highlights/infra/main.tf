provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

locals {
  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Service     = "batch"
    Terraform   = "true"
    Source      = "Surfline/wavetrak-services/jobs/cam-highlights/infra/"
  }
  highlights_bucket = "wt-highlights-${var.environment}"
  wavetracks_bucket = "wt-wavetracks-${var.environment}"
  highlights_url    = "highlights.${var.hosted_zone_name}"
  comment           = "${var.environment} Highlight Clips CDN wth S3"
  container_properties = {
    "command" : [
      "python3.7", "/usr/src/app/entrypoint_highlights.py",
      "--recording_id", "Ref::recording_id"
    ]
    "image" : var.image
    # all resources on a g4dn.xlarge instance as min gpu we can request is 1
    "resourceRequirements" : [
      {
        "type" : "GPU"
        "value" : "1"
      },
      {
        "type" : "VCPU"
        "value": "2"
      },
      {
        "type" : "MEMORY"
        "value": "8000"
      }
    ]
    "retry_strategy" : {
      "attempts" : 1
    }
    "timeout" : {
      "attempt_duration_seconds" : 600
    }
    "name" : "${var.company}-highlights-job-${var.environment}"
    "type" : "container"
    "environment" : [
      { "name" : "ENV", "value" : "${var.environment}" },
      { "name" : "DESTINATION_BUCKET", "value" : local.highlights_bucket },
      { "name" : "CAMERAS_API", "value" : "http://cameras-service.${var.environment}.surfline.com" },
      { "name" : "HIGHLIGHTS_URL", "value" : "https://${local.highlights_url}" },
      { "name" : "REWIND_BUCKET", "value" : "sl-live-cam-archive-${var.environment}" },
      { "name" : "LOG_LEVEL", "value" : "INFO" },
      { "name" : "WAVETRACK_BUCKET", "value" : local.wavetracks_bucket },
      { "name" : "NEW_RELIC_LICENSE_KEY", "value" : "${data.aws_ssm_parameter.new_relic_license_key.value}" },
    ]
  }
}

data "aws_ssm_parameter" "new_relic_license_key" {
  name = "/${var.environment}/common/NEW_RELIC_LICENSE_KEY"
}

module "cam_highlights_job_definition" {
  source               = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"
  company              = var.company
  environment          = var.environment
  application          = var.application
  container_properties = local.container_properties
}

resource "aws_iam_policy" "highlights_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-highlights-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resources/batch-iam-access-policy.json", {
    bucket_name = local.highlights_bucket
  })
}

resource "aws_iam_role_policy_attachment" "highlights_bucket_mgmt_policy_attachement" {
  role       = module.cam_highlights_job_definition.batch_job_iam_role_name
  policy_arn = aws_iam_policy.highlights_bucket_mgmt_policy.arn
}

resource "aws_iam_policy" "wavetracks_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-wavetracks-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resources/batch-iam-access-policy.json", {
    bucket_name = local.wavetracks_bucket
  })
}

resource "aws_iam_role_policy_attachment" "wavetracks_bucket_mgmt_policy_attachement" {
  role       = module.cam_highlights_job_definition.batch_job_iam_role_name
  policy_arn = aws_iam_policy.wavetracks_bucket_mgmt_policy.arn
}

resource "aws_s3_bucket" "wavetracks_bucket" {
  bucket = local.wavetracks_bucket
  acl    = "private"
  tags   = local.tags
}

resource "aws_s3_bucket" "highlights_bucket" {
  bucket = local.highlights_bucket
  acl    = "private"
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 86400
  }

  policy = templatefile("${path.module}/resources/S3-policy.json", {
    bucket_name    = local.highlights_bucket
    cloudfront_arn = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  })
  tags = local.tags
}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = local.comment
}

data "aws_acm_certificate" "cdn_acm" {
  provider    = aws.us-east-1
  domain      = "*.${var.hosted_zone_name}"
  most_recent = true
  statuses    = ["ISSUED"]
  tags        = local.tags
}

# CloudFront distribution
resource "aws_cloudfront_distribution" "highlights_cdn" {
  origin {
    domain_name = aws_s3_bucket.highlights_bucket.bucket_domain_name
    origin_id   = "S3-${local.highlights_bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  comment         = local.comment
  aliases         = [local.highlights_url]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["HEAD", "GET", "OPTIONS"]
    target_origin_id = "S3-${local.highlights_bucket}"
    compress         = false

    forwarded_values {
      headers      = ["Origin"]
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    default_ttl            = 3600
    max_ttl                = 3600
  }
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = local.tags
  viewer_certificate {
    acm_certificate_arn      = data.aws_acm_certificate.cdn_acm.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2019"
  }
}

data "aws_route53_zone" "cdn" {
  name = var.hosted_zone_name
}

resource "aws_route53_record" "highlights_cdn_record" {
  zone_id = data.aws_route53_zone.cdn.zone_id
  name    = local.highlights_url
  type    = "CNAME"
  ttl     = 300
  records = [aws_cloudfront_distribution.highlights_cdn.domain_name]
}


