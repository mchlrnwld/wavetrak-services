variable "environment" {
  type = string
}

variable "company" {
  type    = string
  default = "wt"
}

variable "application" {
  type    = string
  default = "cam-highlights-job"
}

variable "image" {
  type = string
}

variable "hosted_zone_name" {
  type = string
}
