import logging

import requests

import lib.config as config

logger = logging.getLogger('sync-forecast-data-to-fauna')


def sync_spot(fauna_helper, spot_id):
    """
    Syncs all the spot data we care about to Spots Collection.
    Pulls the spot data from spots-api
    """
    r = requests.get(f'{config.SPOTS_API}{spot_id}')
    r.raise_for_status()
    response = r.json()

    spot_api_data = {
        'id': spot_id,
        'pointOfInterestId': response['pointOfInterestId'],
        'name': response['name'],
        'lon': response['location']['coordinates'][0],
        'lat': response['location']['coordinates'][1],
        'timezone': response['timezone'],
    }

    # Grab spot document by id
    spot_doc = fauna_helper.get_spot_document(spot_id)

    if spot_api_data != spot_doc['data']:
        logger.info(f'Syncing POI Id for Spot: {spot_id}')
        fauna_helper.update_document(spot_doc, spot_api_data)


def query_surf_spot_forecasts(poi_id):
    """
    Returns surf, swell, wind, and weather data for given POI
    """
    query = f'''
    query {{
        surfSpotForecasts(pointOfInterestId: "{poi_id}") {{
            surf {{
                data {{
                    timestamp
                    surf {{
                      breakingWaveHeightMin
                      breakingWaveHeightMax
                    }}
                }}
            }}
            swells {{
                data {{
                    timestamp
                    swells {{
                        combined {{
                            height
                            period
                            direction
                        }}
                        components {{
                            height
                            period
                            direction
                        }}
                    }}
                }}
            }}
            weather(agency:"Wavetrak",model:"Blended",grid:"Blended") {{
                data {{
                    timestamp
                    weather {{
                        conditions
                    }}
                }}
            }}
            wind(agency:"Wavetrak",model:"Blended",grid:"Blended") {{
                data {{
                    timestamp
                    wind {{
                        speed
                        direction
                    }}
                }}
            }}
        }}
    }}
    '''
    resp = requests.post(config.GRAPHQL_URL, json={'query': query})
    resp.raise_for_status()
    return resp.json()
