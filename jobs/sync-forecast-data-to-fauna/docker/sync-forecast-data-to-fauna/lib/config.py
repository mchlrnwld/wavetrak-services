import os

from job_secrets_helper import get_secret

FAUNA_DB = os.environ['FAUNA_DB']
FAUNA_SECRET = get_secret('sandbox/jobs/fauna/', FAUNA_DB)
GRAPHQL_URL = os.environ['GRAPHQL_URL']
SPOTS_API = os.environ['SPOTS_API']

FORECAST_TYPES = ['surf', 'swells', 'weather', 'wind']
