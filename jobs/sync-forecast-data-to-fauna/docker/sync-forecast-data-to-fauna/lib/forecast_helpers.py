import logging

import lib.config as config
import lib.utils as utils
from lib.fauna_helper import FaunaHelper

logger = logging.getLogger('sync-forecast-data-to-fauna')


def sync_forecast_data_to_fauna():
    fauna_helper = FaunaHelper(config.FAUNA_SECRET)

    logger.info(f'Syncing forecast data to Fauna: {config.FAUNA_DB}')

    # get spot ids from Spots collection
    logger.info('Pulling spot ids.')
    spot_ids = fauna_helper.get_all_spot_ids()

    # Loop through each spot id
    for _id in spot_ids:
        # Sync Spot data from spots-api to Spot Collection
        utils.sync_spot(fauna_helper, _id)

        # Grab spot document by id
        spot_doc = fauna_helper.get_spot_document(_id)

        logger.info(
            f'Pulling forecast data for {_id} '
            f"w/ POI Id: {spot_doc['data']['pointOfInterestId']}"
        )

        # query forecast data from science data service
        forecast_data = utils.query_surf_spot_forecasts(
            spot_doc['data']['pointOfInterestId']
        )

        # Loop through each type of surf forecast data
        for forecast_type in config.FORECAST_TYPES:
            logger.info(
                f'Adding {forecast_type} data to '
                f'{forecast_type.capitalize()} collection.'
            )

            # Add spot id to each dict for forecast_type
            try:
                data = [
                    dict(item, spot_id=_id)
                    for item in forecast_data['data']['surfSpotForecasts'][
                        forecast_type
                    ]['data']
                ]
            except KeyError:
                raise KeyError(
                    f'Forecast Data for {forecast_type} '
                    'missing from graph pull.'
                )

            # Get latest timestamp for current Collection
            latest_timestamp = fauna_helper.get_latest_document_ts_for_spot(
                f'latest_{forecast_type}_timestamps_by_spot_id', _id
            )

            # Splice data by timestamps
            if latest_timestamp:
                entries_to_create = list(
                    filter(
                        lambda entry: entry['timestamp'] > latest_timestamp,
                        data,
                    )
                )

                entries_to_update = list(
                    filter(
                        lambda entry: entry['timestamp'] <= latest_timestamp,
                        data,
                    )
                )
            else:
                entries_to_create = data
                entries_to_update = []

            # Add new docs to collection
            num_updated = 0

            # Check for documents to update or create
            for entry in entries_to_update:
                old_doc = fauna_helper.get_document_by_spot_and_ts(
                    f'search_{forecast_type}_collection_by_spot_and_timestamp',
                    entry['spot_id'],
                    entry['timestamp'],
                )

                # Create doc if it doesn't exsit
                if not old_doc:
                    entries_to_create.append(entry)
                # Update if new entry differs from old
                elif entry != old_doc['data']:
                    fauna_helper.update_document(old_doc, entry)
                    num_updated += 1

            # Add new docs to Collection
            fauna_helper.create_documents(
                forecast_type.capitalize(), entries_to_create
            )

            logger.info(
                f'{len(entries_to_create)} documents added to '
                f'{forecast_type.capitalize()} collection.'
            )

            logger.info(
                f'{num_updated} documents updated in '
                f'{forecast_type.capitalize()} collection.'
            )
