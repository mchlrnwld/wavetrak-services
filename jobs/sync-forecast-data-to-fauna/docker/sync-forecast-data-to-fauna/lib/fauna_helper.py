import logging

from faunadb import query as q  # type: ignore
from faunadb.client import FaunaClient  # type: ignore
from faunadb.errors import NotFound  # type: ignore

logger = logging.getLogger('sync-forecast-data-to-fauna')


class FaunaHelper:
    def __init__(self, db_secret):
        self.client = FaunaClient(secret=db_secret)

    def get_all_spot_ids(self):
        """
        Returns an array of all the spot ids in Spot Collection.
        """
        try:
            return [
                self.client.query(q.get(q.ref(ref)))['data']['id']
                for ref in self.client.query(
                    q.paginate(q.match(q.index('all_spots')))
                )['data']
            ]
        except Exception as e:
            logger.error(f'Failed to pull spot_ids with all_spots index.')
            raise (e)

    def get_spot_document(self, spot_id):
        """
        Returns a single spot document using the spot id.
        """
        try:
            return self.client.query(
                q.get(
                    q.match(
                        q.index('search_spot_collection_by_spot_id'), spot_id
                    )
                )
            )
        except NotFound:
            return {}
        except Exception as e:
            logger.error(f'Failed to fetch spot document with id: {spot_id}.')
            raise (e)

    def create_documents(self, collection, data_array):
        """
        Creates new documents in the passed collection.
        """
        try:
            self.client.query(
                q.map_(
                    lambda entry: q.create(
                        q.collection(collection), {"data": entry}
                    ),
                    data_array,
                )
            )
        except Exception as e:
            logger.error(
                f'Failed to create document in {collection} with '
                f'data: {data_array}'
            )
            raise (e)

    def update_document(self, doc, updated_data):
        """
        Update a document with new data.
        """
        try:
            self.client.query(
                q.update(q.ref(doc['ref']), {'data': updated_data})
            )
        except Exception as e:
            logger.error(
                f'Failed to update doc: {doc}, with data: {updated_data}'
            )
            raise (e)

    def get_document_by_spot_and_ts(self, index_name, spot_id, ts):
        """
        Returns document that contains spot id and timestamp passed.
        """
        try:
            return self.client.query(
                q.get(q.match(q.index(index_name), spot_id, ts))
            )
        except NotFound:
            return {}
        except Exception as e:
            logger.error(
                f'Failed to get document by spot: {spot_id} and '
                f'timestamp: {ts} with index: {index_name}'
            )
            raise (e)

    def get_latest_document_ts_for_spot(self, index_name, spot_id):
        """
        Returns the timestamp for the latest doc in the index's collection.
        """
        try:
            result = self.client.query(
                q.paginate(q.match(q.index(index_name), spot_id), size=1,)
            )
        except Exception as e:
            logger.error(
                f'Failed to get latest timestamp for spot: {spot_id} '
                f'with index: {index_name}'
            )
            raise (e)

        if not result['data']:
            return None

        return result['data'][0]
