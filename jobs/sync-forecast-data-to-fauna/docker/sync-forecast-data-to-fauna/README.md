# Sync Forecast Data To Fauna Job

Docker image to sync hourly forecast data to FaunaDB.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate sync-forecast-data-to-fauna
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

### Build and Run

```sh
$ docker build -t sync-forecast-data-to-fauna/sync-forecast-data-to-fauna .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env sync-forecast-data-to-fauna/sync-forecast-data-to-fauna
```
