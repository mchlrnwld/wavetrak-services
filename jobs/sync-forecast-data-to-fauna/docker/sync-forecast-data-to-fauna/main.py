import logging
from timeit import default_timer

import lib.forecast_helpers as forecast_helpers

logger = logging.getLogger('sync-forecast-data-to-fauna')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info('Starting sync-forecast-data-to-fauna Job')
    job_start_time = default_timer()

    # Pull forecast data and do post processing here
    forecast_helpers.sync_forecast_data_to_fauna()

    elapsed = default_timer() - job_start_time
    logger.info('Finished sync-forecast-data-to-fauna Job')
    logger.info(f'Downloaded forecast data in {elapsed}s')


if __name__ == '__main__':
    main()
