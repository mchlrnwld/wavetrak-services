from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from dag_helpers.docker import docker_image

AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')

JOB_NAME = 'sync-forecast-data-to-fauna'
APP_ENVIRONMENT = 'sandbox'
FAUNA_DB_TO_USE = 'labs-latest-db-dev'
SPOTS_API = 'http://spots-api.prod.surfline.com/admin/spots/'
GRAPHQL_URL = 'http://science-data-service.prod.surfline.com/graphql'

# set environment variables here
environment_values = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'JOB_NAME': 'sl-{0}-{1}'.format(JOB_NAME, APP_ENVIRONMENT),
    'JOB_DEFINITION': 'sl-labs-{0}-{1}'.format(JOB_NAME, APP_ENVIRONMENT),
    'JOB_QUEUE': 'sl-labs-jobs-common-job-queue-sandbox',
    'TIMEOUT': str(1 * 60 * 60),
    'ENVIRONMENT': ';'.join(
        [
            'AWS_DEFAULT_REGION={}'.format(AWS_DEFAULT_REGION),
            'FAUNA_DB={}'.format(FAUNA_DB_TO_USE),
            'SPOTS_API={}'.format(SPOTS_API),
            'GRAPHQL_URL={}'.format(GRAPHQL_URL)
        ]
    )
}

# DAG definition
default_args = {
    'owner': 'surfline-labs',
    'start_date': datetime(2020, 10, 20),
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30)
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(hours=1),
    default_args=default_args
)

sync_forecast_data_to_fauna = WTDockerOperator(
    task_id='sync-forecast-data-to-fauna',
    image=docker_image('execute-batch-job'),
    environment=environment_values,
    force_pull=True,
    dag=dag
)
