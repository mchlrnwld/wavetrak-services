from datetime import datetime, timedelta

from airflow.models import Variable
from airflow import DAG
from airflow.operators import WTDockerOperator

from dag_helpers.docker import docker_image

JOB_NAME = 'delete-outdated-model-run-forecasts'
ENVIRONMENT = Variable.get('ENVIRONMENT')
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT


delete_outdated_model_run_forecasts_environment = {
    'ENVIRONMENT': APP_ENVIRONMENT,
    'COMMIT_CHANGES': 'true',
}


# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2019, 10, 3, 8),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval='@daily',
    default_args=default_args,
)

WTDockerOperator(
    task_id='delete-outdated-model-run-forecasts',
    image=docker_image('delete-outdated-model-run-forecasts', JOB_NAME),
    environment=delete_outdated_model_run_forecasts_environment,
    force_pull=True,
    depends_on_past=True,
    dag=dag,
)
