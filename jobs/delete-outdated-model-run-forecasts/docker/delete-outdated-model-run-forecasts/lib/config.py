import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENVIRONMENT']
SECRETS_PREFIX = f'{ENV}/common/'
SCIENCE_PLATFORM_PSQL_JOB = get_secret(
    SECRETS_PREFIX, 'SCIENCE_PLATFORM_PSQL_JOB'
)
COMMIT_CHANGES = os.getenv('COMMIT_CHANGES', 'false').lower() == 'true'
