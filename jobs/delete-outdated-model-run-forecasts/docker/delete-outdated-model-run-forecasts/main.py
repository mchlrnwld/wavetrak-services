import logging

import lib.config as config
from lib.psql import (
    delete_outdated_model_runs_forecasts,
    delete_outdated_forecast_hours,
)

logger = logging.getLogger('delete-outdated-model-run-forecasts')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    delete_outdated_model_runs_forecasts(
        ['swell_probabilities', 'swell_ensemble_members'],
        config.SCIENCE_PLATFORM_PSQL_JOB,
        commit=config.COMMIT_CHANGES,
    )

    delete_outdated_forecast_hours(
        ['surf', 'swells', 'weather', 'wind'],
        config.SCIENCE_PLATFORM_PSQL_JOB,
        commit=config.COMMIT_CHANGES,
    )


if __name__ == '__main__':
    main()
