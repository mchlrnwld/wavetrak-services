import logging
from functools import partial
from typing import Dict

from check_model_ready import check_sqs_messages, set_pending_model_run

from lib.check_gefs_wave import check_gefs_wave_ready
from lib.config import config as getConfig
from lib.helpers import model_run_from_key

logger = logging.getLogger('check-model-ready')


def poll_for_latest_model_run(config: Dict[str, str]):
    result = None
    total_messages = 0
    while True:
        model_run, messages_found = check_sqs_messages(  # type: ignore
            config['GEFS_BUCKET'],
            'gefs.',
            config['SQS_QUEUE_NAME'],
            partial(
                check_gefs_wave_ready,
                config['GEFS_BUCKET'],
                config['SCIENCE_DATA_SERVICE'],
                config['AGENCY'],
                config['MODEL'],
            ),
            model_run_from_key,
        )
        result = model_run
        total_messages += messages_found
        if result is not None or messages_found == 0:
            return result, total_messages


def main():
    # TODO: Uncomment below to stream info level logging to stderr once
    #       WTDockerOperator can distinguish between stdout and stderr streams.
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler())
    config = getConfig()

    logger.info(f'Polling queue: {config["SQS_QUEUE_NAME"]}...')

    result, total_messages = poll_for_latest_model_run(config)

    logger.info(f'Checked {total_messages} messages.')

    # Write to stdout for xcoms_push
    if result is not None:
        set_pending_model_run(
            config['AGENCY'],
            config['MODEL'],
            result,
            'POINT_OF_INTEREST',
            config['SCIENCE_DATA_SERVICE'],
        )
        print(result)
    else:
        print('')


if __name__ == '__main__':
    main()
