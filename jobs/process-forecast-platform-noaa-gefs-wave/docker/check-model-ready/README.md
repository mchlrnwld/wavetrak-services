# Check Model Ready

Docker image to check if the latest NOAA GEFS-Wave model is ready for
processing.  This image checks SQS for the latest model runs.  If the model run
is fully downloaded to S3 for all grids then we execute
`begin_processing_model` for the model run.  Otherwise we execute
`model_not_ready`.

## Setup

```sh
$ conda devenv
$ source activate process-forecast-platform-noaa-gefs-wave-check-model-ready
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-forecast-platform-noaa-gefs-wave/check-model-ready .
$ cp .env.sample .env
$ docker run --env-file=.env --rm process-forecast-platform-noaa-gefs-wave/check-model-ready
```
