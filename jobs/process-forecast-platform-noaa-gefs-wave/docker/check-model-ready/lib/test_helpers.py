from lib.helpers import get_file_data, model_run_from_key


def test_get_file_data():
    filenames = [
        (
            'gefs.20210224/00/wave/gridded/'
            'gefs.wave.t00z.p01.global.0p25.f009.grib2'
        ),
        (
            'gefs.20210224/00/wave/gridded/'
            'gefs.wave.t00z.p30.global.0p25.f260.grib2'
        ),
        (
            'gefs.20210224/00/wave/gridded/'
            'gefs.wave.t00z.c00.global.0p25.f000.grib2'
        ),
    ]
    assert get_file_data(filenames[0]) == (1, 9)
    assert get_file_data(filenames[1]) == (30, 260)
    assert get_file_data(filenames[2]) is None


def test_model_run_from_key():
    assert (
        model_run_from_key(
            'gefs.20210224/00/wave/gridded/'
            'gefs.wave.t00z.p01.global.0p25.f009.grib2'
        )
        == 2021022400
    )
    assert (
        model_run_from_key(
            'gefs.20210224/00/wave/gridded/'
            'gefs.wave.t00z.p01.global.0p25.f009.grib2.idx'
        )
        == 2021022400
    )
