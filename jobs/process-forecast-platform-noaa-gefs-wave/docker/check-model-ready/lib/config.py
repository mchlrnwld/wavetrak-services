import os

from job_secrets_helper import get_secret  # type: ignore


def config():
    ENV = os.environ['ENV']
    SECRETS_PREFIX = f'{ENV}/common/'
    return {
        'ENV': ENV,
        'SECRETS_PREFIX': SECRETS_PREFIX,
        'AGENCY': os.environ['MODEL_AGENCY'],
        'MODEL': os.environ['MODEL_NAME'],
        'SQS_QUEUE_NAME': os.environ['SQS_QUEUE_NAME'],
        'GEFS_BUCKET': get_secret(SECRETS_PREFIX, 'GEFS_BUCKET'),
        'SCIENCE_DATA_SERVICE': get_secret(
            SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE'
        ),
    }
