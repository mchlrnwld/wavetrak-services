import re
from typing import Optional, Tuple

ENSEMBLE_MEMBER = re.compile(
    r'gefs\.wave\.t\d{2}z\.p(\d{2})\..+\.f(\d{3})\.grib2'
)
GEFS_WAVE_PREFIX_REGEX = re.compile(
    r'gefs\.(\d{8})/(00|06|12|18)/wave/gridded'
)


def get_file_data(filename: str) -> Optional[Tuple[int, int]]:
    """
    Extracts the ensemble member number (ex. 1, 30) and forecast hour from the
    given filename.
    Args:
        filename: The GEFS-Wave filename to check.
    Returns:
        A tuple of the ensemble member number as an int and the forecast hour
        as an int if the filename matches,
        NoneType otherwise.
        Example: (18, 9)
                 from filename:
                 gefs.wave.t00z.p18.global.0p25.f009.grib2
    """
    result = ENSEMBLE_MEMBER.search(filename)
    if not result:
        return None

    return (int(result.group(1)), int(result.group(2)))


# TODO: Extract these helper functions into modules.
def model_run_from_key(key: str) -> Optional[int]:
    """
    Extracts a model run from the given key string

    Args:
        prefix: The prefix to replace (ex. noaa/gefs-wave).
        key: The key string to extract data from.

    Returns:
        A string representing the model run.
    """
    match = GEFS_WAVE_PREFIX_REGEX.match(key)
    if not match:
        return None

    run_date = match.group(1)
    run_hour = match.group(2)
    return int(f'{run_date}{run_hour}')
