from typing import List

import boto3  # type: ignore
import check_model_ready  # type: ignore

from lib.helpers import get_file_data

JOB_NAME = 'process-forecast-platform-noaa-gefs-wave'


def check_gefs_wave_ready(
    bucket: str, sds_host: str, agency: str, model: str, run: int
) -> bool:
    """
    Checks if the given run is ready for processing. This is accomplished by
    checking that the run has all of the files needed in the given S3 bucket.

    Args:
        bucket: S3 bucket to check for file objects.
        sds_host: Science Data Service host URI.
        agency: The model agency to query for (ex. NOAA).
        model: The name of the model to query for (ex. GEFS-Wave).
        run: Model run (YYYYMMDDHH) to check.

    Returns:
        True if all files exist in the S3 bucket for the run. False if the run
        was already processed, if the some files exist in S3, but not all, or
        lastly if there are no files in S3.
    """
    if check_model_ready.check_model_run_already_exists(
        agency, model, run, 'POINT_OF_INTEREST', sds_host
    ):
        return False

    expected_ensemble_members = list(range(1, 31))
    expected_hours = list(range(0, 241, 3))
    expected_file_data = {
        (m, h) for m in expected_ensemble_members for h in expected_hours
    }
    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')
    run_date = str(run)[:8]
    run_hour = str(run)[8:]
    pages = paginator.paginate(
        Bucket=bucket, Prefix=f'gefs.{run_date}/{run_hour}/wave/gridded'
    )
    objects: List[dict] = []
    for page in pages:
        if 'Contents' in page:
            objects += page['Contents']
    if not len(objects):
        return False
    file_data = {
        file_data
        for file_data in [get_file_data(object['Key']) for object in objects]
        if file_data is not None
    }
    return file_data.issuperset(expected_file_data)
