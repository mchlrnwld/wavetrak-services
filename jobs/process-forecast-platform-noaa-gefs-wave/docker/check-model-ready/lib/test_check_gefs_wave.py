import json
from unittest.mock import patch

import boto3  # type: ignore
import pytest  # type: ignore
from boto3_type_annotations.s3 import Client  # type: ignore
from moto import mock_s3  # type: ignore

from lib.check_gefs_wave import check_gefs_wave_ready

GRID = 'global.0p25'
AGENCY = 'NOAA'
MODEL = 'GEFS-Wave'
RUN_DATE = '20190502'
RUN_HOUR = '00'
RUN = int(f'{RUN_DATE}{RUN_HOUR}')
SCIENCE_BUCKET = 'surfline-science-s3-dev'
SCIENCE_KEY_PREFIX = 'noaa/gefs-wave'
SCIENCE_DATA_SERVICE = 'surfline-data-service'


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3', region_name='us-west-1')
        s3_client.create_bucket(Bucket=SCIENCE_BUCKET)
        yield s3_client


def create_s3_message_body(object_keys: list):
    return json.dumps(
        {
            'Message': json.dumps(
                {
                    'Records': [
                        {'s3': {'object': {'key': key}}} for key in object_keys
                    ]
                }
            )
        }
    )


def put_items_in_bucket(
    s3_client: Client,
    ensemble_members: list,
    timestamps: list,
    grid: str,
    model_key_prefix: str = (f'{SCIENCE_KEY_PREFIX}/' f'{RUN}'),
):
    for time in timestamps:
        for member in ensemble_members:
            s3_client.put_object(
                Bucket=SCIENCE_BUCKET,
                Key=(
                    f'{model_key_prefix}/gefs.wave.t00z.'
                    f'p{str(member).zfill(2)}.'
                    f'{grid}.f{str(time).zfill(3)}.grib2'
                ),
            )


def test_check_gefs_wave_ready(create_s3_bucket):
    with patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        s3_client = create_s3_bucket
        ensemble_members = list(range(1, 31))
        hours = list(range(0, 241, 3))
        put_items_in_bucket(
            s3_client,
            ensemble_members,
            hours,
            GRID,
            f'gefs.{RUN_DATE}/{RUN_HOUR}/wave/gridded',
        )
        s3_client.put_object(
            Bucket=SCIENCE_BUCKET,
            Key=(
                f'gefs.{RUN_DATE}/{RUN_HOUR}/wave/gridded/'
                f'invalid.key.should.be.ignored'
            ),
        )

        assert check_gefs_wave_ready(
            SCIENCE_BUCKET, SCIENCE_DATA_SERVICE, AGENCY, MODEL, RUN
        )


def test_check_gefs_wave_ready_not_ready(create_s3_bucket):
    s3_client = create_s3_bucket
    with patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        ensemble_members = list(range(1, 20))  # Not complete
        hours = list(range(0, 240, 3))  # Not complete
        put_items_in_bucket(
            s3_client,
            ensemble_members,
            hours,
            GRID,
            f'gefs.{RUN_DATE}/{RUN_HOUR}/wave/gridded',
        )

        assert not check_gefs_wave_ready(
            SCIENCE_BUCKET, SCIENCE_DATA_SERVICE, AGENCY, MODEL, RUN
        )
