import json
import os
from unittest.mock import patch

import boto3  # type: ignore
import pytest  # type: ignore
from boto3_type_annotations.s3 import Client as boto_client  # type: ignore
from moto import mock_s3, mock_sqs  # type: ignore

from lib.config import config
from main import poll_for_latest_model_run

GRID = 'global.0p25'
AGENCY = 'NOAA'
MODEL = 'GEFS-Wave'
RUN_DATE = '20190502'
RUN_HOUR = '00'
RUN = int(f'{RUN_DATE}{RUN_HOUR}')
GEFS_QUEUE = 'noaa-gefs-pds'
GEFS_BUCKET = 'noaa-gefs-pds'
SCIENCE_DATA_SERVICE = 'surfline-data-service'


@pytest.fixture
def create_sqs_queue():
    with mock_sqs():
        sqs_client = boto3.client('sqs', region_name='us-west-1')
        sqs_client.create_queue(QueueName=GEFS_QUEUE)
        queue_url = sqs_client.get_queue_url(QueueName=GEFS_QUEUE)['QueueUrl']
        yield sqs_client, queue_url


def send_messages_to_queue(
    sqs_client: boto_client,
    queue_url: str,
    ensemble_members: list,
    timestamps: list,
    model_key_prefix: str,
):
    keys = [
        (
            f'{model_key_prefix}/gefs.wave.t00z.'
            f'p{str(member).zfill(2)}.'
            f'{GRID}.f{str(time).zfill(3)}.grib2'
        )
        for time in timestamps
        for member in ensemble_members
    ]
    sqs_client.send_message_batch(
        QueueUrl=queue_url,
        Entries=[
            {
                'Id': key,
                'MessageBody': json.dumps(
                    {
                        'Message': json.dumps(
                            {'Records': [{'s3': {'object': {'key': key}}}]}
                        )
                    }
                ),
            }
            for key in keys
        ],
    )


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3', region_name='us-west-1')
        s3_client.create_bucket(Bucket=GEFS_BUCKET)
        yield s3_client


def put_items_in_bucket(
    s3_client: boto_client,
    ensemble_members: list,
    timestamps: list,
    model_key_prefix: str,
):
    for time in timestamps:
        for member in ensemble_members:
            s3_client.put_object(
                Bucket=GEFS_BUCKET,
                Key=(
                    f'{model_key_prefix}/gefs.wave.t00z.'
                    f'p{str(member).zfill(2)}.'
                    f'{GRID}.f{str(time).zfill(3)}.grib2'
                ),
            )


# TODO: Remove unnecessary variables once cutover to AWS.
@patch.dict(
    os.environ,
    {
        'ENV': 'test',
        'MODEL_AGENCY': AGENCY,
        'MODEL_NAME': MODEL,
        'SQS_QUEUE_NAME': GEFS_QUEUE,
        'GEFS_BUCKET': GEFS_BUCKET,
        'SCIENCE_DATA_SERVICE': SCIENCE_DATA_SERVICE,
    },
)
def test_main(create_s3_bucket, create_sqs_queue):
    s3_client = create_s3_bucket
    sqs_client, queue_url = create_sqs_queue
    with patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        ensemble_members = list(range(1, 31))
        hours = list(range(0, 385, 3))
        put_items_in_bucket(
            s3_client,
            ensemble_members,
            hours,
            f'gefs.{RUN_DATE}/{RUN_HOUR}/wave/gridded',
        )
        send_messages_to_queue(
            sqs_client,
            queue_url,
            ensemble_members,
            hours,
            f'gefs.{RUN_DATE}/{RUN_HOUR}/wave/gridded',
        )

        assert poll_for_latest_model_run(config()) == (RUN, 10)
