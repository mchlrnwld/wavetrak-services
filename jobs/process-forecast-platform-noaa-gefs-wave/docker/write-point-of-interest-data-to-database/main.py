import asyncio
import logging
from timeit import default_timer

import lib.config as config
from lib.psql import PSQL
from lib.s3_client import S3Client

logger = logging.getLogger('write-point-of-interest-data-to-database')


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info('Downloading CSV files from S3...')
    start = default_timer()
    s3_client = S3Client()
    file_paths = await s3_client.download_all_files(
        config.JOBS_BUCKET,
        f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/{config.RUN}',
        config.DATA_DIR,
    )
    elapsed = default_timer() - start
    logger.info(f'Downloaded CSV files from S3 in {elapsed}s.')

    ensemble_file_paths = [
        file_path for file_path in file_paths if 'ensemble' in file_path
    ]
    probability_file_paths = [
        file_path for file_path in file_paths if 'probabilities' in file_path
    ]

    logger.info(
        'Copying ensemble and probability data from CSV to Postgres...'
    )

    start = default_timer()
    with PSQL(config.SCIENCE_PLATFORM_PSQL_JOB) as psql:
        for file_path in ensemble_file_paths:
            psql.copy_csv(file_path, 'swell_ensemble_members')
            logger.info(f'Copied {file_path} to swell_ensemble_members.')

        for file_path in probability_file_paths:
            psql.copy_csv(file_path, 'swell_probabilities')
            logger.info(f'Copied {file_path} to swell_probabilities.')

        if config.COMMIT:
            logger.info('Committing data to Postgres...')
            psql.commit()

    elapsed = default_timer() - start
    logger.info(
        f'Copied swell ensemble and probability data to Postgres in '
        f'{elapsed}s.'
    )


if __name__ == '__main__':
    asyncio.run(main())
