# Insert Point of Interest Data

Docker image to parse GEFS-Wave data files for point of interest data, calculate the swell probability for each point of interest,
and then insert the ensemble members and probabilities into the Science Platform Postgres database.

## Setup

```sh
$ conda devenv
$ conda activate process-forecast-platform-noaa-gefs-wave-insert-point-of-interest-data
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-forecast-platform-noaa-gefs-wave/insert-point-of-interest-data .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data process-forecast-platform-noaa-gefs-wave/insert-point-of-interest-data
```
