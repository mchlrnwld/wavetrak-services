import asyncio
import logging
import os
from datetime import datetime, timezone
from tempfile import TemporaryDirectory
from timeit import default_timer
from typing import Dict, List, Set

from science_algorithms import (  # type: ignore
    SwellPartition,
    get_swell_probability,
)
from slack_helper import send_invalid_points_of_interest_notification

import lib.config as config
from lib.csv import write_ensemble_members_csv, write_probabilities_csv
from lib.grib import (
    get_file,
    get_poi_indexes,
    get_swell_ensemble_members_from_grib,
)
from lib.models import PointOfInterestGridPoint, SwellEnsembleMemberHour
from lib.s3_client import S3Client
from lib.sds import SDS

logger = logging.getLogger('insert-point-of-interest-data')

ENSEMBLE_MEMBERS_CSV_FILE_NAME = 'ensemble.csv'
PROBABILITIES_CSV_FILE_NAME = 'probabilities.csv'


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info(f'Starting job for model run: {config.RUN}')

    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    sds = SDS(config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL)
    pois_by_grid = sds.query_points_of_interest_grid_points()
    all_grid_pois = [
        grid_poi
        for grid_pois in pois_by_grid.values()
        for grid_poi in grid_pois
    ]
    logger.info(f'Found {len(all_grid_pois)} points of interest grid points.')
    if not all_grid_pois:
        logger.info('No points of interest grid points found. Exiting.')
        return

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir:
        logger.info('Indexing S3 files for model run ' f'{config.RUN}...')

        s3_client = S3Client()

        ensemble_members = list(range(1, 31))
        hours = [
            int(forecast_hour)
            for forecast_hour in config.FORECAST_HOURS.split(',')
        ]
        grids = pois_by_grid.keys()
        model_run_files = {
            hour: {
                grid: [
                    (
                        os.path.join(
                            temp_dir, get_file(config.RUN, hour, grid, member)
                        ),
                        get_file(config.RUN, hour, grid, member),
                        member,
                    )
                    for member in ensemble_members
                ]
                for grid in grids
            }
            for hour in hours
        }
        files_to_download = [
            (local_file, s3_file)
            for hour, grid_files in model_run_files.items()
            for grid, files in grid_files.items()
            for local_file, s3_file, _ in files
        ]

        logger.info(f'Downloading {len(files_to_download)} S3 files...')
        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.download_file(
                    config.GEFS_BUCKET, s3_file, local_file
                )
                for local_file, s3_file in files_to_download
            ]
        )
        elapsed = default_timer() - start
        logger.info(f'Downloaded files in: {elapsed}s')

        logger.info('Parsing GRIB files...')

        poi_indexes_by_grid, invalid_poi_ids_by_grid = get_poi_indexes(
            model_run_files[hours[0]], pois_by_grid
        )

        ensemble_members_csv_path = os.path.join(
            temp_dir, 'ensemble_members.csv'
        )
        probabilities_csv_path = os.path.join(temp_dir, 'probabilities.csv')

        for hour, grid_files in model_run_files.items():
            for grid, files in grid_files.items():
                poi_members: Dict[
                    PointOfInterestGridPoint, List[SwellPartition]
                ] = {}
                swell_ensemble_member_hours: List[SwellEnsembleMemberHour] = []
                invalid_location_poi_ids: Set[str] = set()

                for local_file, _, member in files:
                    new_invalid_pois = get_swell_ensemble_members_from_grib(
                        local_file,
                        member,
                        hour,
                        poi_indexes_by_grid[grid],
                        swell_ensemble_member_hours,
                        poi_members,
                    )
                    invalid_location_poi_ids = invalid_location_poi_ids.union(
                        new_invalid_pois
                    )
                swell_probabilities = {
                    poi: get_swell_probability(members)
                    for poi, members in poi_members.items()
                    if poi.id not in invalid_location_poi_ids
                }
                if grid not in invalid_poi_ids_by_grid:
                    invalid_poi_ids_by_grid[grid] = []
                invalid_poi_ids_by_grid[grid] = list(
                    invalid_location_poi_ids.union(
                        invalid_poi_ids_by_grid[grid]
                    )
                )
                elapsed = default_timer() - start
                logger.info(f'Parsed files in: {elapsed}s')

                logger.info(
                    f'Creating CSV data to {ensemble_members_csv_path} and '
                    f'{probabilities_csv_path}...'
                )
                start = default_timer()
                run_dt = datetime.strptime(config.RUN, '%Y%m%d%H').replace(
                    tzinfo=timezone.utc
                )
                await asyncio.gather(
                    write_ensemble_members_csv(
                        ensemble_members_csv_path,
                        config.AGENCY,
                        config.MODEL,
                        run_dt,
                        swell_ensemble_member_hours,
                    ),
                    write_probabilities_csv(
                        probabilities_csv_path,
                        config.AGENCY,
                        config.MODEL,
                        run_dt,
                        hour,
                        swell_probabilities,
                    ),
                )

        logger.info('Uploading CSVs to S3...')
        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.upload_file(
                    local_file_path,
                    config.JOBS_BUCKET,
                    (
                        f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/'
                        f'{config.RUN}/{config.TASK_PARTITION}/{file_name}'
                    ),
                )
                for local_file_path, file_name in [
                    (
                        ensemble_members_csv_path,
                        ENSEMBLE_MEMBERS_CSV_FILE_NAME,
                    ),
                    (probabilities_csv_path, PROBABILITIES_CSV_FILE_NAME),
                ]
            ]
        )
        elapsed = default_timer() - start
        logger.info(f'Finished uploading CSVs to S3 in {elapsed}s.')

        all_invalid_pois = [
            grid_poi
            for grid_pois in invalid_poi_ids_by_grid.values()
            for grid_poi in grid_pois
        ]

        logger.info(f'{len(all_invalid_pois)} total invalid POIs found.')

        if (
            all_invalid_pois
            and config.SLACK_API_TOKEN
            and config.SLACK_CHANNEL
            # Only alert from the job that includes the 0-hour to avoid
            # generating N matching alerts (where N=number of partitions for
            # the all forecast hours in # ../../dag.py).
            and 0 in hours
        ):
            send_invalid_points_of_interest_notification(
                config.JOB_NAME,
                invalid_poi_ids_by_grid,
                config.SLACK_API_TOKEN,
                config.SLACK_CHANNEL,
                logger.error,
            )


if __name__ == '__main__':
    asyncio.run(main())
