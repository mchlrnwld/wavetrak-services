from lib.grib import get_file


def test_get_file():
    assert get_file('2021031600', 1, 'global.0p25', 2) == (
        'gefs.20210316/00/wave/gridded/'
        'gefs.wave.t00z.p02.global.0p25.f001.grib2'
    )
