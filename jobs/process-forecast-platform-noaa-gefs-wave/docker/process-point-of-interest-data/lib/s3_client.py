import asyncio
import logging
import os
from functools import partial

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type:ignore

import file_transfer

logger = logging.getLogger('process-point-of-interest-data')


class S3Client:
    """
    S3Client to interface with S3, download files asynchronously, and
    upload files asynchronously.
    """

    def __init__(self):
        self.s3_client = file_transfer.S3Client()
        self.boto_s3_client = boto3.client('s3')

    async def download_file(self, bucket: str, key: str, local_file_path: str):
        """
        Downloads a file asynchronously from the given S3 Bucket.

        Args:
            bucket: S3 bucket to download from.
            key: The key object in the S3 Bucket to download.
            local_file_path: The local file path to download the S3 object to.
        """
        os.makedirs(os.path.dirname(local_file_path), exist_ok=True)
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None,
            self.boto_s3_client.download_file,
            bucket,
            key,
            local_file_path,
        )

    async def upload_file(self, local_file_path: str, bucket: str, key: str):
        """
        Upload file to S3.

        Args:
            local_file_path: Local path to upload file from.
            bucket: S3 bucket to upload to.
            key: S3 object key to upload to.
        """
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None, partial(self._upload_file, local_file_path, bucket, key)
        )

    def _upload_file(self, local_file_path: str, bucket: str, key: str):
        try:
            self.boto_s3_client.upload_file(local_file_path, bucket, key)
            logger.info(f'Uploaded {local_file_path} to s3://{bucket}/{key}.')
        except ClientError as e:
            logger.error(
                f'Failed to upload {local_file_path} to s3://{bucket}/{key}!'
            )
            raise e
