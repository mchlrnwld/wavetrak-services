import logging
from typing import Dict, List, Tuple

from grib_helpers import get_lat_lon_index, parse_grib
from science_algorithms import SwellPartition  # type: ignore

from lib.models import PointOfInterestGridPoint, SwellEnsembleMemberHour

logger = logging.getLogger('insert-point-of-interest-data')

GRIB_VARIABLES = {
    'windU': ['U component of wind'],
    'windV': ['V component of wind'],
    'windWaveDirection': ['Direction of wind waves'],
    'windWaveHeight': ['Significant height of wind waves'],
    'windWavePeriod': ['Mean period of wind waves'],
    'combinedWaveHeight': [
        'Significant height of combined wind waves and swell'
    ],
    'primaryWaveDirection': ['Primary wave direction'],
    'primaryWavePeriod': ['Primary wave mean period'],
    'swellWave1Direction': ['Direction of swell waves', 'level 1'],
    'swellWave1Height': ['Significant height of swell waves', 'level 1'],
    'swellWave1Period': ['Mean period of swell waves', 'level 1'],
    'swellWave2Height': ['Significant height of swell waves', 'level 2'],
    'swellWave2Period': ['Mean period of swell waves', 'level 2'],
    'swellWave2Direction': ['Direction of swell waves', 'level 2'],
}


def get_file(run: str, hour: int, grid: str, member: int) -> str:
    """
    Gets a NOAA GEFS-Wave file path from a run, hour, grid, and member.

    Args:
        run: Model run time.
        hour: Forecasted hour or hours from analysis hour.
        grid: Grid associated with the NOAA GEFS-Wave file.
        member: Ensemble member associated with the file.

    Returns:
        Path to the NOAA GEFS file.
    """
    run_date = str(run)[:8]
    run_hour = str(run)[8:]
    return (
        f'gefs.{run_date}/{run_hour}/wave/gridded/'
        f'gefs.wave.t{run_hour}z.p{member:02d}.{grid}.f{hour:03d}.grib2'
    )


def get_poi_indexes(
    grid_files: Dict[str, List[Tuple[str, str, int]]],
    point_of_interest_grid_points: Dict[str, List[PointOfInterestGridPoint]],
) -> Tuple[
    Dict[str, List[Tuple[PointOfInterestGridPoint, Tuple[int, int]]]],
    Dict[str, List[str]],
]:
    """
    Gets indexes for POI grid points from the grib file.

    Args:
        files: Dictionary of grids and files to parse.
        point_of_interest_grid_points: POI grid points to get the indexes for.

    Returns:
       POI grid point indexes and invalid POI grid points.
    """
    lat_lons_by_grid = {}
    for grid, files in grid_files.items():
        lats, lons, _, _ = parse_grib(files[0][0], GRIB_VARIABLES)
        lat_list = [value[0] for value in lats.tolist()]
        lon_list = lons.tolist()[0]
        lat_lons_by_grid[grid] = (lat_list, lon_list)

    pois_with_indexes_by_grid: Dict[
        str, List[Tuple[PointOfInterestGridPoint, Tuple[int, int]]]
    ] = {}
    invalid_point_of_interest_ids_by_grid: Dict[str, List[str]] = {}
    for grid, pois in point_of_interest_grid_points.items():
        if grid not in pois_with_indexes_by_grid:
            pois_with_indexes_by_grid[grid] = []
        for poi in pois:
            try:
                pois_with_indexes_by_grid[grid].append(
                    (
                        poi,
                        get_lat_lon_index(
                            (poi.lat, poi.lon),
                            lat_lons_by_grid[grid][0],
                            lat_lons_by_grid[grid][1],
                        ),
                    )
                )
            except ValueError:
                if grid not in invalid_point_of_interest_ids_by_grid:
                    invalid_point_of_interest_ids_by_grid[grid] = []

                invalid_point_of_interest_ids_by_grid[grid].append(poi.id)

    return pois_with_indexes_by_grid, invalid_point_of_interest_ids_by_grid


def get_swell_ensemble_members_from_grib(
    file: str,
    member: int,
    hour: int,
    poi_indexes: List[Tuple[PointOfInterestGridPoint, Tuple[int, int]]],
    swell_ensemble_member_hours: List[SwellEnsembleMemberHour],
    poi_members: Dict[PointOfInterestGridPoint, List[SwellPartition]],
) -> List[str]:
    """
    Parses a GRIB file for ensemble member data for a specific points of
    interest grid points.

    Args:
        file: GRIB file to parse.
        member: The ensemble member associated with the file.
        hour: The forecast hour.
        poi_indexes: Indexes for each POI grid point
                     to extract data from grib file.
        swell_ensemble_member_hours: List of swell ensemble members to add to.
        poi_members: Dictionary mapping of POI grid point
                     and ensemble probability members.
    Return
        List of point-of-interest IDs with invalid latitude and longitude.
    """
    logger.info(f'Parsing {file}...')
    lats, lons, variable_values, variable_details = parse_grib(
        file, GRIB_VARIABLES
    )
    filled_variable_values = {
        variable: values.filled(0.0)
        for variable, values in variable_values.items()
    }
    invalid_poi_ids: List[str] = []

    for poi, latlon_indexes in poi_indexes:
        if variable_values['combinedWaveHeight'].mask[latlon_indexes[0]][
            latlon_indexes[1]
        ]:
            invalid_poi_ids.append(poi.id)
            continue

        swell_ensemble_member_hour = SwellEnsembleMemberHour(
            poi,
            member,
            hour,
            filled_variable_values['windU'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['windV'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['combinedWaveHeight'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['primaryWavePeriod'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['primaryWaveDirection'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['swellWave1Height'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['swellWave1Period'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['swellWave1Direction'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['swellWave2Height'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['swellWave2Period'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['swellWave2Direction'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['windWaveHeight'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['windWavePeriod'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
            filled_variable_values['windWaveDirection'][latlon_indexes[0]][
                latlon_indexes[1]
            ],
        )
        swell_ensemble_member_hours.append(swell_ensemble_member_hour)

        point_of_interest_grid_point = (
            swell_ensemble_member_hour.point_of_interest_grid_point
        )

        if point_of_interest_grid_point not in poi_members:
            poi_members[point_of_interest_grid_point] = []

        poi_members[point_of_interest_grid_point].append(
            SwellPartition(
                swell_ensemble_member_hour.combined_wave_height,
                swell_ensemble_member_hour.primary_wave_period,
                swell_ensemble_member_hour.primary_wave_direction,
            )
        )

    return invalid_poi_ids
