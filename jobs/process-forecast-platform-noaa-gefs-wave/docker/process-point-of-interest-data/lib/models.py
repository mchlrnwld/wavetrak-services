class PointOfInterestGridPoint:
    """
    Point of interest and its grid point.
    """

    def __init__(self, id: str, grid: str, lon: float, lat: float):
        self.id = id
        self.grid = grid
        self.lat = lat
        if lon < 0:
            self.lon = lon + 360
        else:
            self.lon = lon

    def __eq__(self, other):
        return (
            self.id == other.id
            and self.grid == other.grid
            and self.lon == other.lon
            and self.lat == other.lat
        )

    def __hash__(self):
        return round(abs(100 * self.lon) + abs(self.lat))

    def __repr__(self):
        return (
            f'PointOfInterestGridPoint: '
            f'{self.id}, '
            f'{self.grid}, '
            f'{self.lon}, '
            f'{self.lat}'
        )


class SwellEnsembleMemberHour:
    """
    SwellEnsembleMemberHour contains wind and wave values for a single member
    and a single hour of a point of interest grid point.
    """

    def __init__(
        self,
        point_of_interest_grid_point: PointOfInterestGridPoint,
        member: int,
        hour: int,
        wind_u: float,
        wind_v: float,
        combined_wave_height: float,
        primary_wave_period: float,
        primary_wave_direction: float,
        swell_wave_1_height: float,
        swell_wave_1_period: float,
        swell_wave_1_direction: float,
        swell_wave_2_height: float,
        swell_wave_2_period: float,
        swell_wave_2_direction: float,
        wind_wave_height: float,
        wind_wave_period: float,
        wind_wave_direction: float,
    ):
        self.point_of_interest_grid_point = point_of_interest_grid_point
        self.member = member
        self.hour = hour
        self.wind_u = wind_u
        self.wind_v = wind_v
        self.combined_wave_height = combined_wave_height
        self.primary_wave_period = primary_wave_period
        self.primary_wave_direction = primary_wave_direction
        self.swell_wave_1_height = swell_wave_1_height
        self.swell_wave_1_period = swell_wave_1_period
        self.swell_wave_1_direction = swell_wave_1_direction
        self.swell_wave_2_height = swell_wave_2_height
        self.swell_wave_2_period = swell_wave_2_period
        self.swell_wave_2_direction = swell_wave_2_direction
        self.wind_wave_height = wind_wave_height
        self.wind_wave_period = wind_wave_period
        self.wind_wave_direction = wind_wave_direction

    def __repr__(self):
        return (
            f'SwellEnsembleMemberHour: '
            f'({self.point_of_interest_grid_point}), '
            f'{self.member}, '
            f'{self.hour}, '
            f'{self.wind_u}, '
            f'{self.wind_v}, '
            f'{self.combined_wave_height}, '
            f'{self.primary_wave_period}, '
            f'{self.primary_wave_direction}, '
            f'{self.swell_wave_1_height}, '
            f'{self.swell_wave_1_period}, '
            f'{self.swell_wave_1_direction}, '
            f'{self.swell_wave_2_height}, '
            f'{self.swell_wave_2_period}, '
            f'{self.swell_wave_2_direction}, '
            f'{self.wind_wave_height}, '
            f'{self.wind_wave_period}, '
            f'{self.wind_wave_direction}'
        )
