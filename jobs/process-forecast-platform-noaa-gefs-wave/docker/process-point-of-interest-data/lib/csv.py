import asyncio
import csv
import os
from datetime import datetime, timedelta
from functools import partial
from typing import Dict, List

from lib.models import PointOfInterestGridPoint, SwellEnsembleMemberHour


async def _async_write_csv(csvpath: str, rows: List[Dict[str, object]]):
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, partial(_write_csv, csvpath, rows))


def _write_csv(csvpath: str, rows: List[Dict[str, object]]):
    write_header = True
    if os.path.exists(csvpath):
        write_header = False

    with open(csvpath, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(rows[0].keys()))

        if write_header:
            writer.writeheader()

        writer.writerows(rows)


async def write_ensemble_members_csv(
    csvpath: str,
    agency: str,
    model: str,
    run_dt: datetime,
    swells_ensemble_member_hours: List[SwellEnsembleMemberHour],
):
    """
    Creates a CSV of swell ensemble members for each hour.

    Args:
        csvpath: Location of CSV file to write to.
        agency: Model agency to include in each CSV line.
        model: Model name to include in each CSV line.
        run_dt: Model run to include in each CSV line as a datetime
                representation.
        swells_ensemble_member_hours: List of ensemble members by hour. Each
                                      item will be a single row in the CSV.
    """
    await _async_write_csv(
        csvpath,
        [
            {
                'point_of_interest_id': (
                    member_hour.point_of_interest_grid_point.id
                ),
                'agency': agency,
                'model': model,
                'grid': member_hour.point_of_interest_grid_point.grid,
                'latitude': member_hour.point_of_interest_grid_point.lat,
                'longitude': member_hour.point_of_interest_grid_point.lon,
                'run': run_dt,
                'forecast_time': run_dt + timedelta(hours=member_hour.hour),
                'member': member_hour.member,
                'wind_u': member_hour.wind_u,
                'wind_v': member_hour.wind_v,
                'combined_wave_height': member_hour.combined_wave_height,
                'primary_wave_period': member_hour.primary_wave_period,
                'primary_wave_direction': (member_hour.primary_wave_direction),
                'swell_wave_1_height': member_hour.swell_wave_1_height,
                'swell_wave_1_period': member_hour.swell_wave_1_period,
                'swell_wave_1_direction': (member_hour.swell_wave_1_direction),
                'swell_wave_2_height': member_hour.swell_wave_2_height,
                'swell_wave_2_period': member_hour.swell_wave_2_period,
                'swell_wave_2_direction': (member_hour.swell_wave_2_direction),
                'wind_wave_height': member_hour.wind_wave_height,
                'wind_wave_period': member_hour.wind_wave_period,
                'wind_wave_direction': member_hour.wind_wave_direction,
            }
            for member_hour in swells_ensemble_member_hours
        ],
    )


async def write_probabilities_csv(
    csvpath: str,
    agency: str,
    model: str,
    run_dt: datetime,
    hour: int,
    swell_probabilities: Dict[PointOfInterestGridPoint, float],
):
    """
    Creates a CSV of swell probabilities for each hour.

    Args:
        csvpath: Location of CSV file to write to.
        agency: Model agency to include in each CSV line.
        model: Model name to include in each CSV line.
        run_dt: Model run to include in each CSV line as a datetime
                representation.
        swells_probabilities: Swell probabilities by point of interest grid
                              point and forecast hour.
    """
    await _async_write_csv(
        csvpath,
        [
            {
                'point_of_interest_id': poi.id,
                'agency': agency,
                'model': model,
                'grid': poi.grid,
                'latitude': poi.lat,
                'longitude': poi.lon,
                'run': run_dt,
                'forecast_time': run_dt + timedelta(hours=hour),
                'probability': probability,
            }
            for poi, probability in swell_probabilities.items()
        ],
    )
