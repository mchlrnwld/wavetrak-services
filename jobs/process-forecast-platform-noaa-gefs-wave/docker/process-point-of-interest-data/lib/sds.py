from typing import Dict, List

import requests

from lib.models import PointOfInterestGridPoint


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        url: URL for Science Data Service host.
        agency: The model agency to query.
        name: The model name to query.

    Attributes:
        url: URL for managing model runs.
        agency: The model agency to query.
    """

    def __init__(self, url: str, agency: str, model: str):
        self.url = url
        self.agency = agency
        self.model = model

    def query_points_of_interest_grid_points(
        self
    ) -> Dict[str, List[PointOfInterestGridPoint]]:
        """
        Queries the GraphQL endpoint for PointOfInterest data from the
        surf_spot_configurations table in Postgres.

        Returns:
            A list of PointOfInterest objects for
            the given agency, model, and grid.
        """
        query = f'''
        query {{
          models(agency: "{self.agency}", model: "{self.model}") {{
            grids {{
              grid
              pointsOfInterest {{
                id
                lon
                lat
              }}
            }}
          }}
        }}'''
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
        }
        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        return {
            grid_points_of_interest['grid']: [
                PointOfInterestGridPoint(
                    point_of_interest['id'],
                    grid_points_of_interest['grid'],
                    float(point_of_interest['lon']),
                    float(point_of_interest['lat']),
                )
                for point_of_interest in grid_points_of_interest[
                    'pointsOfInterest'
                ]
            ]
            for grid_points_of_interest in response.json()['data']['models'][
                0
            ]['grids']
        }
