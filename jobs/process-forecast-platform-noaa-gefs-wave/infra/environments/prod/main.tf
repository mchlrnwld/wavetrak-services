provider "aws" {
  region = "us-west-1"
}

provider "aws" {
  alias  = "noaa"
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/process-forecast-platform-noaa-gefs-wave/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_noaa_gefs_wave" {
  source = "../../"

  providers = {
    aws      = aws
    aws.noaa = aws.noaa
  }

  environment = "prod"
  jobs_bucket = "surfline-science-s3-prod"
}
