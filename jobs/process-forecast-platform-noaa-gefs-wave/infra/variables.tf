variable "environment" {
  type = string
}

variable "queue_name" {
  type    = string
  default = "s3-events"
}

variable "company" {
  type    = string
  default = "wt"
}

variable "application" {
  type    = string
  default = "process-forecast-platform-noaa-gefs-wave"
}

variable "jobs_bucket" {
  description = "S3 bucket for job state files."
}
