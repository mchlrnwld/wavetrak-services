import argparse
import itertools
from datetime import datetime

import boto3

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Count Batch jobs by status.')
    parser.add_argument('-a', '--created-after', type=datetime.fromisoformat, required=True)
    parser.add_argument('-b', '--created-before', type=datetime.fromisoformat)
    parser.add_argument('-q', '--job-queue', type=str, required=True)
    args = parser.parse_args()

    client = boto3.client('batch')
    paginator = client.get_paginator('list_jobs')
    statuses = [
        'SUBMITTED',
        'PENDING',
        'RUNNABLE',
        'STARTING',
        'RUNNING',
        'SUCCEEDED',
        'FAILED',
    ]
    jobs = [
        {
            'status': job['status'],
            'statusReason': (
                job['statusReason']
                if 'statusReason' in job
                else None
            ),
            'container': job['container'] if 'container' in job else None,
            'createdAt': job['createdAt'],
        }
        for job in itertools.chain.from_iterable(
            response['jobSummaryList']
            for status in statuses
            for response in paginator.paginate(
                jobQueue=args.job_queue,
                jobStatus=status,
            )
        )
        if job['createdAt'] >= args.created_after.timestamp() * 1000
        if not args.created_before or job['createdAt'] <= args.created_before.timestamp() * 1000
    ]

    jobs_by_status = {status: [] for status in statuses}
    for job in jobs:
        jobs_by_status[job['status']].append(job)

    for status in statuses:
        if status == 'FAILED':
            if not jobs_by_status[status]:
                print(f'{status}: 0')
                continue

            failed_reasons = {}
            print(f'{status}:')
            for job in jobs_by_status[status]:
                if 'container' not in job or 'reason' not in job['container']:
                    if 'None' not in failed_reasons:
                        failed_reasons['None'] = 1
                    else:
                        failed_reasons['None'] += 1
                else:
                    reason = job['container']['reason'].split(':')[0]
                    if reason not in failed_reasons:
                        failed_reasons[reason] = 1
                    else:
                        failed_reasons[reason] += 1

            for reason, count in failed_reasons.items():
                print(f'    {reason}: {count}')
        else:
            print(f'{status}: {len(jobs_by_status[status])}')

    print(f'\nTotal: {len(jobs)}')
