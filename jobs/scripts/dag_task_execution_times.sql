#!/usr/bin/env mysql

/*
Author: Jason Wang
Created: 2020-08-19
Description: Queries the most recent successful process-forecast-platform-* and process-full-grid-*
             DAG runs within the last 12 hours with overall execution times.
*/

SELECT
    task_instance.dag_id,
    task_instance.execution_date,
    SUM(duration) AS dag_duration_s
FROM (
    SELECT dag_id, MAX(execution_date) AS execution_date
    FROM (
        SELECT dag_id, execution_date
        FROM task_instance
        WHERE (
            dag_id LIKE 'process-forecast-platform-%'
            OR dag_id LIKE 'process-full-grid-%'
        )
        AND execution_date >= NOW() - INTERVAL 12 HOUR
        AND state = 'success'
        GROUP BY dag_id, execution_date
        HAVING count(task_id) > 3 -- check-model-ready, check-model-ready-branch, begin-processing-model
    ) AS successful_dag_runs
    GROUP BY dag_id
) AS latest_successful_dag_runs
INNER JOIN task_instance
ON latest_successful_dag_runs.dag_id = task_instance.dag_id
AND latest_successful_dag_runs.execution_date = task_instance.execution_date
GROUP BY task_instance.dag_id;
