from datetime import datetime


def create_grid_file_path(
    grid: str, prefix: str, forecast_time: datetime, latest_run: datetime
) -> str:
    """
    Creates the file path for the full grid analysis file.

    Args:
        grid:          The grid associated
                       with the Lotus-WW3 file (ex. AUS_E_3m, CAL_3m).
        prefix:        Prefix of full grid file location in S3 or locally.
        forecast_time: The forecast datetime associated with the file.
        latest_run:    The datetime associated with latest online Lotus run.

    Returns:
        Lotus grid file path.
        Examples:
        - Analysis data
          'UK_3m/202005/ww3_lotus_UK_3m.2020053118.nc'
        - Live forecast
          ''

    """
    if forecast_time < latest_run:
        month_folder = forecast_time.strftime('%Y%m')
        datetime_string = forecast_time.strftime('%Y%m%d%H')
        return (
            f'{prefix}/grids_analysis/{grid}/{month_folder}/'
            f'ww3_lotus_{grid}.{datetime_string}.nc'
        )
    else:
        run_string = latest_run.strftime('%Y%m%d%H')
        delta = forecast_time - latest_run
        hour = str(delta.days * 24 + int(delta.seconds / 3600)).zfill(3)
        return (
            f'{prefix}/grids/{grid}/{run_string}/'
            f'ww3_lotus_{grid}.{run_string}.{hour}.nc'
        )
