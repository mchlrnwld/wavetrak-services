from datetime import datetime
from typing import List

import requests

from lib.models import PointOfInterestGridPoint


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.
        grid: The model grid to query.
    """

    def __init__(self, url: str, agency: str, model: str, grid: str):
        self.url = url
        self.agency = agency
        self.model = model
        self.grid = grid

    def get_latest_run(self) -> datetime:
        """
        Queries the GraphQL endpoint for the latest online run for the agency
        and model.

        Returns:
            integer value in yyyymmddhh form.
        """
        query = f'''
        query ($agency: String!, $model: String) {{
          models(agency: $agency, model: $model) {{
            runs(statuses: ONLINE, types: POINT_OF_INTEREST) {{
                run
            }}
          }}
        }}'''
        headers = {'Content-Type': 'application/json'}
        response = requests.post(
            f'{self.url}/graphql',
            json=dict(
                query=query,
                variables=dict(agency=self.agency, model=self.model),
            ),
            headers=headers,
        )
        response.raise_for_status()
        runs = [d['run'] for d in response.json()['data']['models'][0]['runs']]
        return datetime.strptime(str(max(runs)) + ' +0000', '%Y%m%d%H %z')

    def query_points_of_interest(self) -> List[PointOfInterestGridPoint]:
        """
        Queries the GraphQL endpoint for PointOfInterest data
        from the surf_spot_configurations table in Postgres.

        Returns:
            A list of PointOfInterest objects for
            the given agency, model, and grid.
        """
        query = f'''
        query {{
          models(agency: "{self.agency}", model: "{self.model}") {{
            grids(grid: "{self.grid}")  {{
              pointsOfInterest {{
                id
                lon
                lat
                optimalSwellDirection
                breakingWaveHeightCoefficient
                breakingWaveHeightIntercept
                breakingWaveHeightAlgorithm
                spectralRefractionMatrix
              }}
            }}
          }}
        }}'''
        headers = {'Content-Type': 'application/json'}
        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        return [
            PointOfInterestGridPoint(
                point_of_interest['id'],
                float(point_of_interest['lon']),
                float(point_of_interest['lat']),
                point_of_interest['optimalSwellDirection'],
                point_of_interest['breakingWaveHeightCoefficient'],
                point_of_interest['breakingWaveHeightIntercept'],
                point_of_interest['breakingWaveHeightAlgorithm'],
                point_of_interest['spectralRefractionMatrix'],
            )
            for grid_points_of_interest in response.json()['data']['models'][
                0
            ]['grids']
            for point_of_interest in grid_points_of_interest[
                'pointsOfInterest'
            ]
        ]

    def query_point_of_interest(
        self, poi_id
    ) -> List[PointOfInterestGridPoint]:
        """
        Queries the GraphQL endpoint for PointOfInterest data
        from the surf_spot_configurations table in Postgres.

        Args:
            poi_id:     The id of the point of interest

        Returns:
            A list of PointOfInterest objects for
            the given agency, model, and grid.
        """
        query = f'''
        query {{
          pointOfInterest(pointOfInterestId: "{poi_id}") {{
            gridPoints  {{
                id
                lon
                lat
                optimalSwellDirection
                breakingWaveHeightCoefficient
                breakingWaveHeightIntercept
                breakingWaveHeightAlgorithm
                spectralRefractionMatrix
                grid {{ grid }}
            }}
          }}
        }}'''
        headers = {'Content-Type': 'application/json'}
        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        return [
            PointOfInterestGridPoint(
                grid_point_of_interest['id'],
                float(grid_point_of_interest['lon']),
                float(grid_point_of_interest['lat']),
                grid_point_of_interest['optimalSwellDirection'],
                grid_point_of_interest['breakingWaveHeightCoefficient'],
                grid_point_of_interest['breakingWaveHeightIntercept'],
                grid_point_of_interest['breakingWaveHeightAlgorithm'],
                grid_point_of_interest['spectralRefractionMatrix'],
            )
            for grid_point_of_interest in response.json()['data'][
                'pointOfInterest'
            ]["gridPoints"]
            if grid_point_of_interest['grid']['grid'] == self.grid
        ]
