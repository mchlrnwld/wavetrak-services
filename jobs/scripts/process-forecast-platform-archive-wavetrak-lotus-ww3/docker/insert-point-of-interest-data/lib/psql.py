from contextlib import ExitStack

import psycopg2  # type: ignore

import lib.config as config


def update_csv_to_postgres(table_name: str, csvpath: str, connection_uri: str):
    """
    Update table contents from a CSV, for the `surf` or `swells` tables.

    Note: Will only update existing rows. If there are rows in the CSV without
    a corresponding row in the table they will be ignored and not inserted.

    Arguments:
        table_name: The name of the table to update.
        csvpath: Path to surf CSV file, with field headers.
        connection_uri: URI to connect to the database.
    """
    primary_key = [
        'point_of_interest_id',
        'agency',
        'model',
        'grid',
        'forecast_time',
        'run',
    ]

    with ExitStack() as stack:
        csvfile = stack.enter_context(open(csvpath))
        columns = csvfile.readline()
        columns_list = columns.split(',')
        data_columns = set(columns_list).difference(primary_key)
        csvfile.seek(0)

        conn = stack.enter_context(psycopg2.connect(connection_uri))
        cur = stack.enter_context(conn.cursor())
        cur.execute(
            f'CREATE TEMPORARY TABLE {table_name}_temp (LIKE '
            f'{table_name} INCLUDING DEFAULTS) ON COMMIT DROP;'
        )
        cur.copy_expert(
            f'COPY {table_name}_temp ({columns}) FROM STDIN WITH CSV HEADER',
            csvfile,
        )
        select_run = (
            'SELECT MAX(model_runs_point_of_interest.run) '
            + 'FROM model_runs_point_of_interest '
            + f'WHERE agency=\'{config.AGENCY}\' '
            + f'AND model=\'{config.MODEL}\' '
            + f'AND model_runs_point_of_interest.run<={table_name}_temp.run'
        )
        cur.execute(
            f'INSERT INTO {table_name} ('
            + ', '.join(set(columns_list).difference(['run']))
            + ', run'
            + ')'
            + 'SELECT '
            + ', '.join(set(columns_list).difference(['run']))
            + f', ({select_run}) AS run'
            + f' FROM {table_name}_temp '
            + f'ON CONFLICT ON CONSTRAINT {table_name}_pkey DO UPDATE SET '
            + ', '.join([f'{f}=EXCLUDED.{f}' for f in data_columns])
            + ';'
        )
        conn.commit()


def update_surf_swells_csvs_to_postgres(
    surf_csvpath: str, swells_csvpath: str, connection_uri: str
):
    """
    Update the surf and swells tables with data in the two csv files.

    Arguments:
        surf_csvpath: Path to surf CSV file.
        swells_csvpath: Path to swells CSV file.
        connection_uri: URI to connect to the database.
    """
    update_csv_to_postgres('surf', surf_csvpath, connection_uri)
    update_csv_to_postgres('swells', swells_csvpath, connection_uri)
