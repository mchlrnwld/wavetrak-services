from datetime import datetime

from lib.file_helpers import create_grid_file_path

PREFIX = 'lotus/archive'


def test_create_grid_file_path_for_historical():
    grid = 'BALI_3m'
    forecast_hour = datetime(2020, 2, 28, 6)
    latest_run = datetime(2020, 2, 29, 6)
    assert create_grid_file_path(grid, PREFIX, forecast_hour, latest_run) == (
        'lotus/archive/grids_analysis/BALI_3m/202002/'
        'ww3_lotus_BALI_3m.2020022806.nc'
    )


def test_create_grid_file_path_for_live():
    grid = 'BALI_3m'
    forecast_hour = datetime(2020, 2, 28, 6)
    latest_run = datetime(2020, 2, 28, 0)
    assert create_grid_file_path(grid, PREFIX, forecast_hour, latest_run) == (
        'lotus/archive/grids/BALI_3m/2020022800/'
        'ww3_lotus_BALI_3m.2020022800.006.nc'
    )
