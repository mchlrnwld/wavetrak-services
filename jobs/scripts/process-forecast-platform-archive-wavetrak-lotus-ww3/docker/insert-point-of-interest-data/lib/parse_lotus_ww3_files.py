from typing import Dict, List, Tuple

from netCDF4 import Dataset  # type: ignore

FULL_GRID_VARIABLES = {
    'height': 'hs',
    'frequency': 'fp',
    'direction': 'dp',
    'swell_wave_1_height': 'phs0',
    'swell_wave_1_period': 'ptp0',
    'swell_wave_1_direction': 'pdir0',
    'swell_wave_1_spread': 'pspr0',
    'swell_wave_2_height': 'phs1',
    'swell_wave_2_period': 'ptp1',
    'swell_wave_2_direction': 'pdir1',
    'swell_wave_2_spread': 'pspr1',
    'swell_wave_3_height': 'phs2',
    'swell_wave_3_period': 'ptp2',
    'swell_wave_3_direction': 'pdir2',
    'swell_wave_3_spread': 'pspr2',
    'swell_wave_4_height': 'phs3',
    'swell_wave_4_period': 'ptp3',
    'swell_wave_4_direction': 'pdir3',
    'swell_wave_4_spread': 'pspr3',
    'swell_wave_5_height': 'phs4',
    'swell_wave_5_period': 'ptp4',
    'swell_wave_5_direction': 'pdir4',
    'swell_wave_5_spread': 'pspr4',
    'wind_wave_height': 'phs5',
    'wind_wave_period': 'ptp5',
    'wind_wave_direction': 'pdir5',
    'wind_wave_spread': 'pspr5',
}


def parse_lotus_full_grid_file(
    local_file_path: str,
) -> Tuple[List[float], List[float], Dict[str, List[List[float]]]]:
    """
    Parses a lotus_ww3 file from the local_file_path
    and creates a dictionary with the relevant variables.

    Args:
        local_file_path: Local path of lotus_ww3_file to parse.

    Returns:
        A list of latitude values and longitude values,
        along with a dictionary of the relevant variables
        from the Lotus-WW3 file.

        The index of the lats list serves as first index
        for the variables, and the index of the lons list
        serves as the second index for the variables.

        For example:

        lats: [-77.5  -77.25]
        lons: [1.0, 2.0, 3.0]
        data['height']: [[3.02, 3.03, 3.04],
                         [4.02, 4.03, 4.04]]

        For lat: -77.5, index 0. lon: 2.0, index 1.
        data['height'][0][1] == 3.03
    """
    with Dataset(local_file_path) as lotus_ww3_file:
        lats = lotus_ww3_file.variables['latitude'][:].tolist()
        lons = lotus_ww3_file.variables['longitude'][:].tolist()
        data = {
            key: lotus_ww3_file.variables[value][0][:].filled(0.0).tolist()
            for key, value in FULL_GRID_VARIABLES.items()
        }
        return lats, lons, data
