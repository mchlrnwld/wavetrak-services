from datetime import datetime
from math import floor


def guess_run_time(forecast_time: datetime) -> datetime:
    return forecast_time.replace(
        hour=floor(int(forecast_time.strftime('%H')) / 6) * 6
    )
