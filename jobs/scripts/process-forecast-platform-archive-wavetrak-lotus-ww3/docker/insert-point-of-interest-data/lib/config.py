import os
from datetime import datetime, timedelta

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
JOB_NAME = os.environ['JOB_NAME']
SECRETS_PREFIX = f'{ENV}/common/'
DATA_DIR = os.getenv('AIRFLOW_TMP_DIR', 'data')
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
GRID = os.environ['GRID']
POINT_OF_INTEREST_ID = os.getenv('POINT_OF_INTEREST_ID', None)

START = datetime.strptime(os.environ['START'] + ' +0000', '%Y%m%d%H %z')
END = datetime.strptime(os.environ['END'] + ' +0000', '%Y%m%d%H %z')
FORECAST_TIMES = [
    START + timedelta(hours=h)
    for h in range(
        0, 1 + 24 * (END - START).days + round((END - START).seconds / 3600)
    )
]

SCIENCE_DATA_PREFIX = os.environ['SCIENCE_DATA_PREFIX']

SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
SCIENCE_PLATFORM_PSQL_JOB = get_secret(
    SECRETS_PREFIX, 'SCIENCE_PLATFORM_PSQL_JOB'
)

SLACK_API_TOKEN = get_secret(SECRETS_PREFIX, 'SLACK_API_TOKEN', '')
SLACK_CHANNEL = os.getenv('SLACK_CHANNEL', '')
