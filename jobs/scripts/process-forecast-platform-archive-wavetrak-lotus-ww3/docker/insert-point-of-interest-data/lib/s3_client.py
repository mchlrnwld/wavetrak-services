import asyncio
import logging
import os
from functools import partial

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type:ignore

logger = logging.getLogger('insert-point-of-interest')


class S3Client:
    """
    S3Client to interface with S3 and download spot files
    asynchronously.

    Arguments:
        bucket: Name of S3 Bucket to download from.

    Attributes:
        bucket: Name of S3 Bucket to download from.
        client: boto3 client for S3.
    """

    def __init__(self, bucket):
        self.bucket = bucket
        self.client = boto3.client('s3')

    async def download_file(self, key: str, local_file_path: str):
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None, partial(self._download_file, key, local_file_path)
        )

    def _download_file(self, key: str, local_file_path: str):
        try:
            os.makedirs(os.path.dirname(local_file_path), exist_ok=True)
            self.client.download_file(self.bucket, key, local_file_path)
            logger.info(
                f'Downloaded s3://{self.bucket}/{key} to {local_file_path}'
            )
        except ClientError as e:
            logger.error(
                f'Failed to download s3://{self.bucket}/{key} to '
                f'{local_file_path}!'
            )
            raise e
