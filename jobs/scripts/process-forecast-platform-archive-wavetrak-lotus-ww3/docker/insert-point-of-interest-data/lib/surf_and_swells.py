from datetime import datetime
from typing import List, Tuple

import numpy as np  # type: ignore
from science_algorithms import (
    SurfHeight,
    SurfHeightInput,
    SwellPartition,
    approximate_spectra_from_swell_partitions,
    calculate_impacts,
)

from lib.constants import (
    DEFAULT_SPECTRA_DIRECTIONS,
    DEFAULT_SPECTRA_FREQUENCIES,
)
from lib.models import PointOfInterestGridPoint, SurfHour, SwellsHour

swell_partition_key_groups = [
    (
        'swell_wave_1_height',
        'swell_wave_1_period',
        'swell_wave_1_direction',
        'swell_wave_1_spread',
        'swell_wave_1_impact',
    ),
    (
        'swell_wave_2_height',
        'swell_wave_2_period',
        'swell_wave_2_direction',
        'swell_wave_2_spread',
        'swell_wave_2_impact',
    ),
    (
        'swell_wave_3_height',
        'swell_wave_3_period',
        'swell_wave_3_direction',
        'swell_wave_3_spread',
        'swell_wave_3_impact',
    ),
    (
        'swell_wave_4_height',
        'swell_wave_4_period',
        'swell_wave_4_direction',
        'swell_wave_4_spread',
        'swell_wave_4_impact',
    ),
    (
        'swell_wave_5_height',
        'swell_wave_5_period',
        'swell_wave_5_direction',
        'swell_wave_5_spread',
        'swell_wave_5_impact',
    ),
    (
        'wind_wave_height',
        'wind_wave_period',
        'wind_wave_direction',
        'wind_wave_spread',
        'wind_wave_impact',
    ),
]


class SurfAndSwellsBuilder:
    """
    Methods for building surf and swell hour forecast data from spot full
    grid analysis data.

    Args:
        data_dir: Path to a directory to write temporary files to. SurfHeight
                  uses this to compute spectral refraction surf heights.
    """

    def __init__(self, data_dir: str):
        self._surf_height = SurfHeight(data_dir)

    def build_from_full_grid_data(
        self,
        indexed_points_of_interest: List[
            Tuple[PointOfInterestGridPoint, Tuple[int, int]]
        ],
        forecast_time: datetime,
        run_time: datetime,
        full_grid_data: np.array,
    ) -> Tuple[List[SurfHour], List[SwellsHour], List[str]]:
        """
        Builds surf and swell hour forecast data from full grid files.

        Args:
            indexed_points_of_interest: List of points of interest and their
                                        lat/lon indexes to get data from the
                                        full grid arrays.
            forecast_time: Forecast time.
            full_grid_data: Array of full grid data used to build surf and
                            swell hours.

        Returns:
            Lists of surf hours, swells hours, and invalid points of interest
            IDs.
        """
        invalid_point_of_interest_ids: List[str] = []
        poi_surf_height_inputs = []
        poi_swell_partitions = []
        for poi, (lat_index, lon_index) in indexed_points_of_interest:
            swell_partitions = [
                SwellPartition(
                    full_grid_data[h][lat_index][lon_index],
                    full_grid_data[p][lat_index][lon_index],
                    full_grid_data[d][lat_index][lon_index],
                    full_grid_data[s][lat_index][lon_index],
                )
                for h, p, d, s, _ in swell_partition_key_groups
            ]

            spectra_wave_energy = (
                np.array(
                    approximate_spectra_from_swell_partitions(swell_partitions)
                )
                if poi.breaking_wave_height_algorithm == 'SPECTRAL_REFRACTION'
                else None
            )

            poi_surf_height_inputs.append(
                SurfHeightInput(
                    poi.breaking_wave_height_algorithm,
                    swell_partitions,
                    poi.optimal_swell_direction,
                    poi.breaking_wave_height_coefficient,
                    poi.breaking_wave_height_intercept,
                    DEFAULT_SPECTRA_FREQUENCIES,
                    DEFAULT_SPECTRA_DIRECTIONS,
                    np.array(spectra_wave_energy),
                    np.array(poi.spectral_refraction_matrix or []),
                )
            )
            poi_swell_partitions.append(swell_partitions)

        poi_surf_heights = self._surf_height.calculate(poi_surf_height_inputs)

        surf_hours: List[SurfHour] = []
        swells_hours: List[SwellsHour] = []
        for (
            (poi, (lat_index, lon_index)),
            (surf_heights, error),
            swell_partitions,
        ) in zip(
            indexed_points_of_interest, poi_surf_heights, poi_swell_partitions
        ):
            if error:
                invalid_point_of_interest_ids.append(poi.point_of_interest_id)
                continue

            if surf_heights is None:
                surf_min = surf_max = None
            else:
                surf_min, surf_max = surf_heights

            surf_hours.append(
                SurfHour(
                    poi,
                    forecast_time,
                    run_time,
                    surf_min,
                    surf_max,
                    poi.breaking_wave_height_algorithm,
                )
            )

            impacts = calculate_impacts(
                swell_partitions, poi.optimal_swell_direction
            )

            swells_hours.append(
                SwellsHour(
                    poi,
                    forecast_time,
                    run_time,
                    full_grid_data['height'][lat_index][lon_index],
                    1 / full_grid_data['frequency'][lat_index][lon_index]
                    if full_grid_data['frequency'][lat_index][lon_index] > 0.0
                    else 0.0,
                    full_grid_data['direction'][lat_index][lon_index],
                    swell_partitions[0].height,
                    swell_partitions[0].period,
                    swell_partitions[0].direction,
                    impacts[0],
                    swell_partitions[1].height,
                    swell_partitions[1].period,
                    swell_partitions[1].direction,
                    impacts[1],
                    swell_partitions[2].height,
                    swell_partitions[2].period,
                    swell_partitions[2].direction,
                    impacts[2],
                    swell_partitions[3].height,
                    swell_partitions[3].period,
                    swell_partitions[3].direction,
                    impacts[3],
                    swell_partitions[4].height,
                    swell_partitions[4].period,
                    swell_partitions[4].direction,
                    impacts[4],
                    swell_partitions[5].height,
                    swell_partitions[5].period,
                    swell_partitions[5].direction,
                    impacts[5],
                    None,
                    None,
                    None,
                )
            )

        return surf_hours, swells_hours, invalid_point_of_interest_ids
