from datetime import datetime

from lib.date_helpers import guess_run_time


def test_guess_run_time():
    dt1 = datetime(year=2020, month=8, day=23, hour=0)
    dt2 = datetime(year=2020, month=8, day=23, hour=4)

    assert guess_run_time(dt1) == dt1
    assert guess_run_time(dt2) == dt1
