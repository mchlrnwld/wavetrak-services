# Insert Point of Interest Data

Docker image to:

1. Query points of interest grid points for Lotus-WW3
1. Update point of interest data in the Science Platform Postgres Surf and Swells tables, using the Lotus archive

## Setup

```sh
$ conda env create -f environment.yml
$ source activate process-forecast-platform-archive-wavetrak-lotus-ww3-insert-point-of-interest-data
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-forecast-platform-archive-wavetrak-lotus-ww3/insert-point-of-interest-data .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data process-forecast-platform-archive-wavetrak-lotus-ww3/insert-point-of-interest-data
```
