import asyncio
import logging
import os
from tempfile import TemporaryDirectory
from timeit import default_timer
from typing import List, Tuple

from grib_helpers.process import get_lat_lon_index
from slack_helper import send_invalid_points_of_interest_notification

import lib.config as config
from lib.csv import append_surf_csv, append_swells_csv
from lib.date_helpers import guess_run_time
from lib.file_helpers import create_grid_file_path
from lib.models import PointOfInterestGridPoint
from lib.parse_lotus_ww3_files import parse_lotus_full_grid_file
from lib.psql import update_surf_swells_csvs_to_postgres
from lib.s3_client import S3Client
from lib.sds import SDS
from lib.surf_and_swells import SurfAndSwellsBuilder

logger = logging.getLogger('insert-point-of-interest')


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    logger.info(f'Starting update job')

    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir:
        surf_and_swells_builder = SurfAndSwellsBuilder(temp_dir)
        sds_client = SDS(
            config.SCIENCE_DATA_SERVICE,
            config.AGENCY,
            config.MODEL,
            config.GRID,
        )
        if config.POINT_OF_INTEREST_ID is not None:
            points_of_interest_grid_points = sds_client.query_point_of_interest(
                config.POINT_OF_INTEREST_ID
            )
        else:
            points_of_interest_grid_points = (
                sds_client.query_points_of_interest()
            )

        latest_run = sds_client.get_latest_run()
        s3_client = S3Client(config.SCIENCE_BUCKET)

        local_full_grid_files = [
            create_grid_file_path(
                config.GRID, temp_dir, forecast_time, latest_run
            )
            for forecast_time in config.FORECAST_TIMES
        ]

        s3_full_grid_files = [
            create_grid_file_path(
                config.GRID,
                config.SCIENCE_DATA_PREFIX,
                forecast_time,
                latest_run,
            )
            for forecast_time in config.FORECAST_TIMES
        ]

        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.download_file(s3_file_path, local_file_path)
                for s3_file_path, local_file_path in zip(
                    s3_full_grid_files, local_full_grid_files
                )
            ]
        )
        elapsed = default_timer() - start
        logger.info(
            f'Downloaded {len(s3_full_grid_files)} full grid files in '
            f'{elapsed}s.'
        )

        SWELLS_CSV_FILE_PATH = os.path.join(
            temp_dir, 'wavetrak-lotus-ww3-swells.csv'
        )

        SURF_CSV_FILE_PATH = os.path.join(
            temp_dir, 'wavetrak-lotus-ww3-surf.csv'
        )

        logger.info(f'Parsing full grid files for {config.GRID}...')
        start = default_timer()

        indexed_points_of_interest: List[
            Tuple[PointOfInterestGridPoint, Tuple[int, int]]
        ] = []
        invalid_point_of_interest_ids = []
        for i, file in enumerate(local_full_grid_files):
            forecast_time = config.FORECAST_TIMES[i]
            logger.info(
                f'Parsing full grid file {file} for '
                f'{len(points_of_interest_grid_points)} points of interest...'
            )
            lats, lons, data = parse_lotus_full_grid_file(file)
            if not indexed_points_of_interest:
                for poi in points_of_interest_grid_points:
                    try:
                        indexed_points_of_interest.append(
                            (
                                poi,
                                get_lat_lon_index(
                                    (poi.lat, poi.lon), lats, lons
                                ),
                            )
                        )
                    except ValueError:
                        invalid_point_of_interest_ids.append(
                            poi.point_of_interest_id
                        )

            if forecast_time >= latest_run:
                run_time = latest_run
            else:
                run_time = guess_run_time(forecast_time)

            (
                surf_hours,
                swells_hours,
                invalid_point_of_interest_ids,
            ) = surf_and_swells_builder.build_from_full_grid_data(
                indexed_points_of_interest, forecast_time, run_time, data
            )

            logger.info(
                f'Writing point of interest data to swells and surf CSV '
                f'for {file}...'
            )

            await asyncio.gather(
                append_surf_csv(
                    SURF_CSV_FILE_PATH,
                    config.AGENCY,
                    config.MODEL,
                    config.GRID,
                    surf_hours,
                ),
                append_swells_csv(
                    SWELLS_CSV_FILE_PATH,
                    config.AGENCY,
                    config.MODEL,
                    config.GRID,
                    swells_hours,
                ),
            )

        elapsed = default_timer() - start
        logger.info(
            f'Finished writing to csv for grid {config.GRID} in {elapsed}s.'
        )

        start = default_timer()
        logger.info('Copying swells and surf data from CSV to Postgres...')

        update_surf_swells_csvs_to_postgres(
            SURF_CSV_FILE_PATH,
            SWELLS_CSV_FILE_PATH,
            config.SCIENCE_PLATFORM_PSQL_JOB,
        )
        elapsed = default_timer() - start
        logger.info(
            f'Finished copying swells and surf data to Postgres in {elapsed}s.'
        )

        if (
            invalid_point_of_interest_ids
            and config.SLACK_API_TOKEN
            and config.SLACK_CHANNEL
        ):
            send_invalid_points_of_interest_notification(
                config.JOB_NAME,
                {config.GRID: invalid_point_of_interest_ids},
                config.SLACK_API_TOKEN,
                config.SLACK_CHANNEL,
                logger.error,
            )


if __name__ == '__main__':
    asyncio.run(main())
