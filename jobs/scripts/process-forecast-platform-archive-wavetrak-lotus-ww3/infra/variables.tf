variable "environment" {
  type = string
}

variable "company" {
  default = "wt"
}

variable "application" {
  default = "process-forecast-platform-archive-wavetrak-lotus-ww3"
}

variable "application_short" {
  default = "process-forecast-platform-archive-wt-lotus-ww3"
}

variable "s3_bucket" {
  type = string
}
