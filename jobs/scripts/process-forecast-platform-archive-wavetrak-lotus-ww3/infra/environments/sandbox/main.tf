provider "aws" {
  region  = "us-west-1"
  version = "~> 2.7.0"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/scripts/process-forecast-platform-archive-wavetrak-lotus-ww3/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_archive_wavetrak_lotus_ww3" {
  source      = "../../"
  environment = var.environment
  s3_bucket   = "surfline-science-s3-dev"
}
