locals {
  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/scripts/${var.application}/insert-point-of-interest-data:${var.environment}",
    "vcpus" : 1,
    "memory" : 4096,
    "volumes" : [
      {
        "host" : {
          "sourcePath" : "/mnt/ephemeral/airflow"
        },
        "name" : "tmp"
      }
    ],
    "mountPoints" : [
      {
        "sourceVolume" : "tmp",
        "containerPath" : "/mnt/ephemeral/airflow",
        "readOnly" : false
      }
    ]
  }
}

module "update_poi_batch_job_definition" {
  source  = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"
  company = var.company
  # Use shorter name because the aws_iam_role used in the module goes over
  # the 64 character limit when using the full application name.
  application          = "${var.application_short}"
  environment          = var.environment
  s3_bucket            = var.s3_bucket
  s3_key_prefix        = "lotus/archive"
  container_properties = local.container_properties
}

resource "aws_iam_role" "batch_restore_role" {
  name               = "${var.company}-${var.s3_bucket}-lotus-restore-${var.environment}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "batchoperations.s3.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "batch_restore_policy" {
  name   = "${var.company}-${var.s3_bucket}-lotus-restore-${var.environment}"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:RestoreObject"
            ],
            "Resource": "arn:aws:s3:::${var.s3_bucket}/lotus/*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion",
                "s3:GetBucketLocation"
            ],
            "Resource": [
                "arn:aws:s3:::${var.s3_bucket}/inventory/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.batch_restore_role.name
  policy_arn = aws_iam_policy.batch_restore_policy.arn
}

data "aws_s3_bucket" "science_bucket" {
  bucket = var.s3_bucket
}

resource "aws_s3_bucket_inventory" "lotus_archive_inventory" {
  bucket                   = var.s3_bucket
  name                     = "${var.company}-${var.s3_bucket}-lotus-archive-${var.environment}"
  included_object_versions = "All"
  schedule {
    frequency = "Daily"
  }
  filter {
    prefix = "lotus/archive/grids_analysis/"
  }
  destination {
    bucket {
      format     = "CSV"
      bucket_arn = data.aws_s3_bucket.science_bucket.arn
      prefix     = "inventory"
    }
  }
}
