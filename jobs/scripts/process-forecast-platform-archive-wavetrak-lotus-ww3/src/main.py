import signal
from curses import wrapper
from datetime import datetime, timedelta
from multiprocessing import Manager, Pool
from queue import Empty
from typing import Optional

import lib.config as config
from lib.batch import BatchJob
from lib.log_stream import LogStream
from lib.message import Message, MessageType
from lib.sds import SDS
from lib.ui import print_summary, render_status

days_per_grid = [
    ('AUS_E_3m', 8),
    ('AUS_SW_3m', 16),
    ('AUS_VT_3m', 16),
    ('BALI_3m', 16),
    ('CAL_3m', 8),
    ('FR_3m', 16),
    ('GLAKES_6m', 16),
    ('GLOB_15m', 1),
    ('GLOB_30m', 8),
    ('HW_3m', 16),
    ('PT_3m', 16),
    ('UK_3m', 16),
    ('US_E_3m', 16),
]

FORMAT = '%Y%m%d%H'


def exectute_batch_job(job_index, grid, start, end, log_queue):
    job_env = {
        'ENV': config.ENV,
        'AGENCY': 'Wavetrak',
        'MODEL': 'Lotus-WW3',
        'GRID': grid,
        'START': start,
        'END': end,
        'SCIENCE_DATA_PREFIX': config.SCIENCE_DATA_PREFIX,
        'JOB_NAME': config.JOB_NAME,
    }
    if config.POINT_OF_INTEREST_ID is not None:
        job_env['POINT_OF_INTEREST_ID'] = config.POINT_OF_INTEREST_ID
    batch_job = BatchJob(
        config.JOB_NAME,
        config.JOB_QUEUE,
        config.JOB_DEFINITION,
        2 * 60 * 60,
        lambda msg: log_queue.put((job_index, msg,)),
        **job_env,
    )
    batch_job.start()

    log_stream = LogStream(batch_job.log_stream)
    outcome: Optional[str] = None
    for status in batch_job.wait():
        outcome = status
        for timestamp, message in log_stream.more():
            log_queue.put(
                (
                    job_index,
                    Message(MessageType.LOG, f'[{timestamp}] - {message}'),
                )
            )

    log_queue.put(
        (
            job_index,
            Message(
                MessageType.STATUS,
                outcome if outcome is not None else 'UNKNOWN',
            ),
        )
    )


def main(screen):
    sds = SDS(config.SCIENCE_DATA_SERVICE, 'Wavetrak', 'Lotus-WW3')

    if config.START is None or config.END is None:
        latest_run = sds.get_latest_run()
        start = latest_run - timedelta(hours=24)
        end = latest_run + timedelta(hours=384)
    else:
        start = datetime.strptime(config.START, FORMAT)
        end = datetime.strptime(config.END, FORMAT)

    jobs = []

    if config.POINT_OF_INTEREST_ID is not None:
        poi_grids = sds.get_poi_grids(config.POINT_OF_INTEREST_ID)

    for grid, max_days in days_per_grid:
        if config.GRID is not None and config.GRID != grid:
            continue

        if config.POINT_OF_INTEREST_ID is not None and grid not in poi_grids:
            continue

        if config.POINT_OF_INTEREST_ID is not None:
            max_days = max(max_days, 4)

        end_partition = start + timedelta(hours=-1)
        while end_partition < end:
            start_partition = end_partition + timedelta(hours=1)
            end_partition = min(
                end, start_partition + timedelta(days=max_days, hours=-1)
            )
            jobs.append(
                (
                    grid,
                    start_partition.strftime(FORMAT),
                    end_partition.strftime(FORMAT),
                )
            )

    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)

    with Pool(config.POOL_SIZE) as pool:
        signal.signal(signal.SIGINT, original_sigint_handler)
        try:
            manager = Manager()
            log_queue = manager.Queue()

            status = pool.starmap_async(
                exectute_batch_job,
                [
                    (i, grid, start, end, log_queue)
                    for i, (grid, start, end) in enumerate(jobs)
                ],
            )

            job_status = [
                {
                    'status': 'PENDING',
                    'job': job,
                    'job_id': None,
                    'last_log': None,
                }
                for job in jobs
            ]

            while not status.ready():
                try:
                    i, message = log_queue.get(True, 1)
                    if message.message_type == MessageType.JOB_ID:
                        job_status[i]['job_id'] = message.value
                    elif message.message_type == MessageType.STATUS:
                        job_status[i]['status'] = message.value
                    elif message.message_type == MessageType.LOG:
                        job_status[i]['last_log'] = message.value

                    render_status(screen, job_status)

                except Empty:
                    pass

            return job_status
        except KeyboardInterrupt:
            pool.terminate()
            return job_status


if __name__ == '__main__':
    result = wrapper(main)
    print_summary(result)
