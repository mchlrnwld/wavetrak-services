import curses


def render_status(screen, job_status):
    counts = {
        'PENDING': 0,
        'SUBMITTED': 0,
        'RUNNABLE': 0,
        'STARTING': 0,
        'RUNNING': 0,
        'SUCCEEDED': 0,
        'FAILED': 0,
    }
    for job in job_status:
        if job['status'] not in counts:
            counts[job['status']] = 0
        counts[job['status']] += 1

    pending = counts['PENDING']
    submitted = counts['SUBMITTED'] + counts['RUNNABLE'] + counts['STARTING']
    running = counts['RUNNING']
    success = counts['SUCCEEDED']
    failed = counts['FAILED']

    screen.erase()
    screen.addstr(
        0,
        0,
        f'Pending: {pending}; submitted: {submitted}; running: {running}; '
        f'success: {success}; failed {failed}'[: (curses.COLS - 1)],
        curses.A_BOLD,
    )
    screen.addstr(2, 0, 'Submitted and Running jobs:'[: (curses.COLS - 1)])

    for i, job in enumerate(
        [
            job
            for job in job_status
            if job['job_id'] is not None
            and job['status']
            in ['RUNNING', 'SUBMITTED', 'RUNNABLE', 'STARTING']
        ]
    ):
        if i + 3 >= curses.LINES:
            break
        screen.addstr(
            i + 3,
            0,
            f'{job["job_id"]} - {job["last_log"]}'[: (curses.COLS - 1)],
        )

    screen.refresh()


def print_summary(job_status):
    counts = {
        'SUCCEEDED': 0,
        'FAILED': 0,
    }

    for job in job_status:
        if job['status'] not in counts:
            counts[job['status']] = 0
        counts[job['status']] += 1

    print('Batch jobs:')
    for status in counts:
        print(f'{status}: {counts[status]}')

    failures = [job for job in job_status if job['status'] != 'SUCCEEDED']

    if failures:
        print('\nFailed jobs:')
        for job in failures:
            print(
                f'{job["job_id"]} - {job["status"]} '
                f'- {job["job"]} - {job["last_log"]}'
            )
