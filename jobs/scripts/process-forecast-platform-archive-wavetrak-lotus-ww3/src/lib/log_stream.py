from datetime import datetime
from typing import Generator, Tuple

import backoff  # type: ignore
import boto3  # type: ignore
import botocore  # type: ignore


class LogStream:
    """
    LogStream allows streaming log events from AWS CloudWatch Logs.

    Args:
        log_stream: Name of Log Stream to stream log events from.
    """

    def __init__(self, log_stream: str):
        self.client = boto3.client('logs')
        self.log_stream = log_stream
        self.next_token = None

    @backoff.on_exception(
        backoff.expo,
        (
            botocore.exceptions.ClientError,
            botocore.exceptions.ConnectionClosedError,
        ),
        max_tries=16,
    )
    def _get_log_events_with_backoff(self, *args, **kwargs):
        return self.client.get_log_events(*args, **kwargs)

    def more(self) -> Generator[Tuple[datetime, str], None, None]:
        """
        Gets log events' timestamp and message from Log Stream.

        Returns:
            Generator that yields (timestamp, message) tuple until no new logs
            are found.
        """
        while True:
            args = {
                'logGroupName': '/aws/batch/job',
                'logStreamName': self.log_stream,
                'startFromHead': True,
            }
            if self.next_token:
                args['nextToken'] = self.next_token
            log_events = self._get_log_events_with_backoff(**args)

            for event in log_events['events']:
                yield (
                    datetime.utcfromtimestamp(event['timestamp'] / 1000),
                    event['message'],
                )

            next_forward_token = log_events['nextForwardToken']
            if next_forward_token == self.next_token:
                return

            self.next_token = next_forward_token
