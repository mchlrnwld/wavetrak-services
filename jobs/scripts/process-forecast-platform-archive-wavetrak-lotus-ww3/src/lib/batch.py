from datetime import datetime
from time import sleep
from typing import Any, Dict, Generator, Optional

import boto3  # type: ignore

from lib.message import Message, MessageType


class BatchJob:
    """
    BatchJob manages executing an AWS Batch Job.

    Args:
        job_name: Name of job to submit to Batch.
        job_queue: Name of queue to submit job to.
        job_definition: Name of job definition for compute environment to
                        execute.

    Attributes:
        job_id: Job ID to reference the submitted job.
        log_stream: Name of CloudWatchLogs LogStream for the submitted job.
    """

    job_id: str
    log_stream: str

    def __init__(
        self,
        job_name: str,
        job_queue: str,
        job_definition: str,
        timeout: int,
        send_message,
        **environment,
    ):
        self.client = boto3.client('batch')
        self.job_name = job_name
        self.job_queue = job_queue
        self.job_definition = job_definition
        self.timeout = timeout
        self.send_message = send_message
        self.environment = [
            {"name": key, "value": value} for key, value in environment.items()
        ]

    def _describe(self) -> Dict[str, Any]:
        return self.client.describe_jobs(jobs=[self.job_id])['jobs'][0]

    def start(self):
        """
        Submits Batch job and waits for job to start running. Sets the
        job_id and log_stream properties once job has started.
        """
        self.send_message(
            Message(
                MessageType.LOG, f'Submitting job at {datetime.utcnow()}...'
            )
        )
        self.send_message(Message(MessageType.STATUS, 'SUBMITTED'))
        self.job_id = self.client.submit_job(
            jobName=self.job_name,
            jobQueue=self.job_queue,
            jobDefinition=self.job_definition,
            containerOverrides={'environment': self.environment},
            timeout={'attemptDurationSeconds': self.timeout},
        )['jobId']

        self.send_message(Message(MessageType.JOB_ID, self.job_id))

        while True:
            sleep(15)
            description = self._describe()
            status = description['status']
            started_at = description.get('startedAt', None)

            self.send_message(Message(MessageType.STATUS, status))
            if status == 'FAILED':
                return False
            elif started_at:
                started_at_dt = datetime.fromtimestamp(started_at / 1000.0)
                self.send_message(
                    Message(
                        MessageType.LOG, f'Job started at {started_at_dt}.'
                    )
                )
                self.log_stream = description['container']['logStreamName']
                self.send_message(
                    Message(MessageType.LOG, f'Log Stream: {self.log_stream}')
                )
                return True

    def wait(self, interval: Optional[int] = 1) -> Generator[str, None, None]:
        """
        Polls job status until execution has finished.

        Returns:
            Generator that yields that job's status on each status check until
            the job is no longer running.
        """
        while True:
            if interval:
                sleep(interval)

            description = self._describe()

            yield description['status']

            if 'stoppedAt' in description:
                stopped_at = datetime.fromtimestamp(
                    description['stoppedAt'] / 1000.0
                )
                job_reason = description['statusReason']
                container_reason = description['container'].get('reason', None)
                if container_reason:
                    self.send_message(
                        Message(
                            MessageType.LOG,
                            f'Container exited for reason: {container_reason}',
                        )
                    )
                self.send_message(
                    Message(
                        MessageType.LOG,
                        f'Job stopped at {stopped_at} for '
                        f'reason: {job_reason}.',
                    )
                )
                return
