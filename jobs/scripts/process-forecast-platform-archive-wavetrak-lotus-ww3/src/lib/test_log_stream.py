from collections import deque
from datetime import datetime

import botocore  # type: ignore

from lib.log_stream import LogStream


def test_more_gets_log_events_until_token_is_unchanged(monkeypatch):
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')
    attempts = 0
    current_token = None
    tokens = deque(['first-token', 'second-token'])

    def get_log_events_with_tokens(*args, **kwargs):
        nonlocal attempts, current_token
        attempts += 1
        current_token = tokens.popleft() if len(tokens) else current_token
        return {
            'events': [
                {'timestamp': 1576540800000, 'message': f'message {attempts}'}
            ],
            'nextForwardToken': current_token,
        }

    log_stream = LogStream('test-log-stream')
    monkeypatch.setattr(
        log_stream.client, 'get_log_events', get_log_events_with_tokens
    )
    found_logs = []
    for timestamp, message in log_stream.more():
        found_logs.append((timestamp, message))

    assert found_logs == [
        (datetime(2019, 12, 17), 'message 1'),
        (datetime(2019, 12, 17), 'message 2'),
        (datetime(2019, 12, 17), 'message 3'),
    ]
    assert attempts == 3
    assert current_token == 'second-token'


def test_more_retries_client_errors_with_backoff(monkeypatch):
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')
    attempts = 0

    def get_log_events_with_client_errors(*args, **kwargs):
        nonlocal attempts
        attempts += 1
        if attempts == 1:
            raise botocore.exceptions.ClientError(
                {
                    'Error': {
                        'Code': 'ThrottlingException',
                        'Message': 'ThrottlingException',
                    }
                },
                'GetLogEvents',
            )
        return {
            'events': [
                {'timestamp': 1576540800000, 'message': 'first message'},
                {'timestamp': 1576544400000, 'message': 'second message'},
            ],
            'nextForwardToken': None,
        }

    log_stream = LogStream('test-log-stream')
    monkeypatch.setattr(
        log_stream.client, 'get_log_events', get_log_events_with_client_errors
    )
    found_logs = []
    for timestamp, message in log_stream.more():
        found_logs.append((timestamp, message))

    assert found_logs == [
        (datetime(2019, 12, 17, 0), 'first message'),
        (datetime(2019, 12, 17, 1), 'second message'),
    ]
    assert attempts == 2
