from enum import Enum


class MessageType(Enum):
    JOB_ID = 1
    STATUS = 2
    LOG = 3


class Message:
    message_type: MessageType
    value: str

    def __init__(self, message_type: MessageType, value: str):
        self.message_type = message_type
        self.value = value
