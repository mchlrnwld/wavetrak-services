from datetime import datetime
from typing import List

import requests


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.
    """

    def __init__(self, url: str, agency: str, model: str):
        self.url = url
        self.agency = agency
        self.model = model

    def get_latest_run(self) -> datetime:
        """
        Queries the GraphQL endpoint for the latest online run for the agency
        and model.

        Returns:
            integer value in yyyymmddhh form.
        """
        query = '''
        query ($agency: String!, $model: String) {
          models(agency: $agency, model: $model) {
            runs(statuses: ONLINE, types: POINT_OF_INTEREST) {
                run
            }
          }
        }'''
        headers = {'Content-Type': 'application/json'}
        response = requests.post(
            f'{self.url}/graphql',
            json=dict(
                query=query,
                variables=dict(agency=self.agency, model=self.model),
            ),
            headers=headers,
        )
        response.raise_for_status()
        runs = [d['run'] for d in response.json()['data']['models'][0]['runs']]
        return datetime.strptime(str(max(runs)) + ' +0000', '%Y%m%d%H %z')

    def get_poi_grids(self, point_of_interest_id) -> List[str]:
        """
        Queries the GraphQL endpoint for all the surf/swell grids for a POI.

        Args:
            point_of_interest_id: The id of the point of interest

        Returns:
            list of grids.
        """
        query = f'''
        query {{
          pointOfInterest(pointOfInterestId: "{point_of_interest_id}") {{
            gridPoints  {{
                grid {{ agency, model, grid }}
            }}
          }}
        }}'''
        headers = {'Content-Type': 'application/json'}
        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        return [
            grid_point_of_interest['grid']['grid']
            for grid_point_of_interest in response.json()['data'][
                'pointOfInterest'
            ]['gridPoints']
            if grid_point_of_interest['grid']['agency'] == self.agency
            and grid_point_of_interest['grid']['model'] == self.model
        ]
