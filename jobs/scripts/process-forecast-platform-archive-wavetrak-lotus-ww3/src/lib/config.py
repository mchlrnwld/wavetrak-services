import os

ENV = os.environ['ENV']

JOB_NAME = 'process-forecast-platform-archive-wavetrak-lotus-ww3'
JOB_QUEUE = f'wt-jobs-common-high-cpu-local-ssd-job-queue-{ENV}'
JOB_DEFINITION = f'wt-process-forecast-platform-archive-wt-lotus-ww3-{ENV}'
SECRETS_PREFIX = f'{ENV}/common/'
SCIENCE_DATA_SERVICE = os.getenv('SCIENCE_DATA_SERVICE', f'http://graphql-gateway.{ENV}.surfline.com')

SCIENCE_DATA_PREFIX = os.getenv('SCIENCE_DATA_PREFIX', 'lotus/archive')

POOL_SIZE = int(os.getenv('POOL_SIZE', '24'))

START = os.getenv('START', None)
END = os.getenv('END', None)
GRID = os.getenv('GRID', None)

POINT_OF_INTEREST_ID = os.getenv('POI_ID', None)
