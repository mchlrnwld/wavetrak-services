# process-forecast-platform-archive-wavetrak-lotus-ww3

This is a tool for re-importing archived Lotus analysis data to the Science
Data Service databases, updaing `swell` and `surf` tables, accounting for
changes to points of interest grid points and surf spot configurations.

## Prerequisites

### Docker image

[docker/insert-point-of-interest-data](./docker/insert-point-of-interest-data/)
is a docker image for script that inserts data for a grid and data range.

Build and deploy to ECR:

```sh
$ cd docker/insert-point-of-interest-data
$ make build
$ ENV=sandbox make tag
$ ENV=sandbox make push
```

### Infrastructure

[infra](./infra/) sets up batch job definition to run the insert point of
interest script.

## Running

```sh
$ cd src
$ cp .env.sample .env
$ conda env create -f environment.yml
$ source activate process-forecast-platform-wavetrak-lotus-ww3-check-model-ready
$ env $(xargs < .env) python main.py
```
