from itertools import chain

import check_model_ready

from lib.sds import SDS
from conftest import BUCKET


def test_main(monkeypatch, create_s3_bucket):
    monkeypatch.setenv('ENV', 'test')
    monkeypatch.setenv('GFS_BUCKET', BUCKET)
    monkeypatch.setenv('SCIENCE_DATA_SERVICE', 'http:/sds-test')
    monkeypatch.setenv('AGENCY', 'NOAA')
    monkeypatch.setenv('MODEL', 'GFS')

    monkeypatch.setattr(SDS, 'get_active_run', lambda self: 2021100600)

    expected_latest_run = None

    def mock_set_pending_model_run(agency, model, run, run_type, sds_url):
        nonlocal expected_latest_run
        expected_latest_run = run

    monkeypatch.setattr(
        check_model_ready, 'set_pending_model_run', mock_set_pending_model_run
    )

    s3_client = create_s3_bucket
    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20211006/00/atmos/gfs.t00z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)
        s3_client.put_object(Bucket=BUCKET, Key=f'{key}.idx')

    from main import main

    # Skips runs prior to and including active run.
    assert main() is None
    assert expected_latest_run is None

    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20211006/06/atmos/gfs.t06z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)
        s3_client.put_object(Bucket=BUCKET, Key=f'{key}.idx')

    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20211006/12/atmos/gfs.t12z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)

    # Returns and sets latest full run.
    assert main() == 2021100606
    assert expected_latest_run == 2021100606
