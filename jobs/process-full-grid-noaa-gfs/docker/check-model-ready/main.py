import logging
from typing import Optional

import check_model_ready

import lib.config as config
from lib.check_gfs_ready import check_gfs_ready
from lib.sds import SDS

logger = logging.getLogger('check-model-ready')

RUN_TYPE = 'FULL_GRID'


def main() -> Optional[int]:
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler())

    sds_client = SDS(config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL)
    active_run = sds_client.get_active_run()
    latest_run = check_gfs_ready(config.GFS_BUCKET, active_run)

    if latest_run:
        check_model_ready.set_pending_model_run(
            config.AGENCY,
            config.MODEL,
            latest_run,
            RUN_TYPE,
            config.SCIENCE_DATA_SERVICE,
        )
        return latest_run

    return None


if __name__ == '__main__':
    result = main()

    # Write to stdout for xcoms_push
    if result:
        print(result)
    else:
        print('')
