import boto3  # type: ignore
import pytest  # type: ignore
from moto import mock_s3  # type: ignore

BUCKET = 'test-bucket'


@pytest.fixture(scope='function')
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3')
        s3_client.create_bucket(Bucket=BUCKET)
        yield s3_client


@pytest.fixture(autouse=True, scope='function')
def aws_credentials(monkeypatch):
    """
    Mocked AWS Credentials for moto.
    This is a temporary workaround,
    otherwise moto will throw 'NoCredentialsError'.
    """
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')
