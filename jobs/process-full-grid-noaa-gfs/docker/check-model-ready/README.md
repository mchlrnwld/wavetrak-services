# Check Model Ready

Docker image to check if a NOAA GFS full grid model is ready for processing.
S3 is queried directly to find the latest model run that has been downloaded. This is accomplished with the following steps:

1. The current online model run is queried from the SDS.
2. The latest model run for each grid is queried from S3. The online model run is used as the starting point for querying S3. Only keys that are greater than the current online model run are listed.
3. If only some grids have the latest model run, then processing is skipped.
4. If each grid has the latest model run, then the following steps are taken:
   1. Each grid is checked to see if all files have been downloaded. If true, then the model is ready for processing. Otherwise processing is skipped.

## Setup

```sh
$ conda devenv
$ conda activate process-full-grid-noaa-gfs-check-model-ready
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-full-grid-noaa-gfs/check-model-ready .
$ cp .env.sample .env
$ docker run --env-file=.env --rm -v ~/.aws:/root/.aws process-full-grid-noaa-gfs/check-model-ready
```
