from itertools import chain

from conftest import BUCKET
from lib.check_gfs_ready import check_gfs_ready


def test_check_gfs_ready_returns_latest_run_ready_for_processing(
    create_s3_bucket
):
    s3_client = create_s3_bucket

    # Older run.
    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20190502/00/atmos/gfs.t00z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)
        s3_client.put_object(Bucket=BUCKET, Key=f'{key}.idx')

    # Newest run ready.
    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20190602/06/atmos/gfs.t06z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)
        s3_client.put_object(Bucket=BUCKET, Key=f'{key}.idx')

    # Latest run not ready.
    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20190602/12/atmos/gfs.t12z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)

    assert check_gfs_ready(BUCKET) == 2019060206


def test_check_gfs_ready_ignores_runs_prior_to_active(create_s3_bucket):
    s3_client = create_s3_bucket

    # Older run ready.
    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20190502/00/atmos/gfs.t00z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)
        s3_client.put_object(Bucket=BUCKET, Key=f'{key}.idx')

    # Active run ready.
    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20190602/06/atmos/gfs.t06z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)
        s3_client.put_object(Bucket=BUCKET, Key=f'{key}.idx')

    # Latest run not ready.
    for num in chain(range(120), range(120, 385, 3)):
        key = f'gfs.20190602/12/atmos/gfs.t12z.pgrb2.0p25.f{num:03d}'
        s3_client.put_object(Bucket=BUCKET, Key=key)

    assert check_gfs_ready(BUCKET, 2019060206) is None
