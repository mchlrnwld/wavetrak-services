import os

from job_secrets_helper import get_secret

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
GFS_BUCKET = get_secret(SECRETS_PREFIX, 'GFS_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
MODEL = os.environ['MODEL']
AGENCY = os.environ['AGENCY']
