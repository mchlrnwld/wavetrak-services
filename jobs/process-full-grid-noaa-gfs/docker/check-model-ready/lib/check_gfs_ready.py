import logging
import re
from itertools import chain
from typing import Optional

import boto3  # type: ignore

logger = logging.getLogger('check-model-ready')

GFS_REGEX = re.compile(
    r'(gfs\.\d{8}/\d{2})/atmos/gfs\.t\d{2}z\.pgrb2\.0p25\.f(\d{3})$'
)
IDX_REGEX = re.compile(
    r'(gfs\.\d{8}/\d{2})/atmos/gfs\.t\d{2}z\.pgrb2\.0p25\.f(\d{3})\.idx$'
)
EXPECTED_FILE_HOURS = {
    f'{str(num).zfill(3)}' for num in chain(range(120), range(120, 385, 3))
}


def check_gfs_ready(
    bucket: str, active_run: Optional[int] = None
) -> Optional[int]:
    """
    Checks if latest model run is ready based on files available in S3 bucket.

    Args:
        bucket: S3 bucket to check for file objects.
        active_run: Current active model run time (YYYYMMDDHH). Runs prior to
                    this run will be ignored.

    Returns:
        Latest run if ready for processing. Otherwise None.
    """
    if active_run:
        active_run_date = str(active_run)[:8]
        active_run_hour = str(active_run)[8:]
        active_run_key_prefix = f'gfs.{active_run_date}/{active_run_hour}/'
    else:
        active_run_key_prefix = ''
    logger.info(
        f'Checking objects in s3://{bucket} after {active_run_key_prefix}...'
    )
    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')
    object_keys = sorted(
        [
            item['Key']
            for item in chain.from_iterable(
                response['Contents']
                for response in paginator.paginate(
                    Bucket=bucket, StartAfter=active_run_key_prefix
                )
                if 'Contents' in response
            )
        ]
    )

    grib_matches = [GFS_REGEX.search(key) for key in object_keys]
    idx_matches = [IDX_REGEX.search(key) for key in object_keys]
    grib_runs_and_timestamps = [
        (int(f'{match.group(1)[4:12]}{match.group(1)[13:]}'), match.group(2))
        for match in grib_matches
        if match
    ]
    idx_runs_and_timestamps = [
        (int(f'{match.group(1)[4:12]}{match.group(1)[13:]}'), match.group(2))
        for match in idx_matches
        if match
    ]

    run = set(run for run, _ in grib_runs_and_timestamps).intersection(
        set(run for run, _ in idx_runs_and_timestamps)
    )

    for current_run in sorted(list(run), reverse=True):
        if active_run is not None and current_run <= active_run:
            continue

        grib_timestamps = [
            timestamp
            for run, timestamp in grib_runs_and_timestamps
            if run == current_run
        ]
        idx_timestamps = [
            timestamp
            for run, timestamp in idx_runs_and_timestamps
            if run == current_run
        ]

        number_of_objects = len(grib_timestamps + idx_timestamps)
        logger.info(f'Found {number_of_objects} objects for {current_run}.')

        logger.info(
            f'Found {len(grib_timestamps)} GRIB and {len(idx_timestamps)} IDX '
            f'files. Expected {len(EXPECTED_FILE_HOURS)}.'
        )

        if set(grib_timestamps) == set(idx_timestamps) == EXPECTED_FILE_HOURS:
            return current_run

    return None
