import asyncio
import logging
import os
from tempfile import TemporaryDirectory
from timeit import default_timer
from typing import Dict

import numpy as np  # type: ignore
import science_algorithms.transformations as t  # type: ignore
from grib_helpers.process import process_grib_data  # type: ignore

import lib.config as config
from lib.download import Downloader
from lib.grib import get_weather_conditions, parse_data_from_gribs
from lib.model_forecast_hour import ModelForecastHour
from lib.s3 import S3Client
from lib.sds import SDS

logger = logging.getLogger('stream-full-grid-data')


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    if not os.path.exists(config.DOWNLOAD_DIR):
        logger.info(f'Creating directory: {config.DOWNLOAD_DIR}')
        os.makedirs(config.DOWNLOAD_DIR)

    with TemporaryDirectory(dir=config.DOWNLOAD_DIR) as temp_dir:
        current_run = int(config.RUN)
        logger.info(f'Indexing S3 files for model run {current_run}...')
        sds = SDS(config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL)
        grids = sds.get_list_of_grids()
        s3_client = S3Client(config.GFS_BUCKET)

        runs_to_download = s3_client.get_runs_to_download(current_run, grids)

        model_forecast_hours = [
            ModelForecastHour(run, hour, grid)
            for grid in grids
            for run, hours in runs_to_download.items()
            for hour in hours
        ]
        if config.MODEL_HOUR_LIMIT > 0:
            model_forecast_hours = model_forecast_hours[
                : config.MODEL_HOUR_LIMIT
            ]

        model_grib_paths = [mfh.grib_path for mfh in model_forecast_hours] + [
            mfh.previous_grib_path
            for mfh in model_forecast_hours
            if mfh.previous_grib_path is not None
        ]

        logger.info(f'Downloading {len(model_grib_paths)} S3 files...')
        start = default_timer()
        grib_files_local = [
            f'{temp_dir}/{grib_path}' for grib_path in model_grib_paths
        ]
        downloader = Downloader(config.GFS_BUCKET)
        await asyncio.gather(
            *[
                downloader.download_grib_file(
                    s3_file, f'{s3_file}.idx', local_file
                )
                for s3_file, local_file in zip(
                    model_grib_paths, grib_files_local
                )
            ]
        )
        elapsed = default_timer() - start
        logger.info(f'Downloaded {len(model_grib_paths)} files in {elapsed}s')

        logger.info('Processing...')
        start = default_timer()

        # results from `parse_data_from_gribs` from each hour will be
        # stored in these variables and used for processing the next
        # hour's data in `process_grib_data`
        previous_variable_values: Dict[str, np.array] = {}
        previous_variable_details: Dict[str, str] = {}
        for mfh in model_forecast_hours:
            (
                lat,
                lon,
                variable_values,
                variable_details,
            ) = parse_data_from_gribs(
                f'{temp_dir}/{mfh.grib_path}',
                previous_file_path=(
                    f'{temp_dir}/{mfh.previous_grib_path}'
                    if mfh.previous_grib_path is not None
                    else None
                ),
            )

            temperature_c = t.kelvin_to_celsius(variable_values['temperature'])
            dewpoint_c = t.kelvin_to_celsius(variable_values['dewpoint'])
            pressure_mb = t.pascal_to_millibar(variable_values['pressure'])
            hourly_precipitation = t.get_hourly_precipitation(
                variable_values['totalPrecipitation'],
                variable_details['totalPrecipitation'],
                previous_variable_values['totalPrecipitation']
                if 'totalPrecipitation' in previous_variable_values
                and mfh.forecast_hour != 1
                else None,
                previous_variable_details['totalPrecipitation']
                if 'totalPrecipitation' in previous_variable_details
                and mfh.forecast_hour != 1
                else None,
            )

            precipitation_type = t.get_precipitation_type(
                hourly_precipitation,
                variable_values['snow'] > 0,
                variable_values['freezingRain'] > 0,
                variable_values['icePellets'] > 0,
            )
            weather_conditions = get_weather_conditions(
                variable_values, temperature_c, precipitation_type
            )
            values = [
                variable_values['windU'],
                variable_values['windV'],
                variable_values['windGust'],
                pressure_mb,
                temperature_c,
                variable_values['visibility'],
                dewpoint_c,
                variable_values['relativeHumidity'],
                precipitation_type.astype(int),
                hourly_precipitation,
                weather_conditions.astype(int),
            ]

            process_grib_data(mfh.grid, mfh.forecast_dt, lat, lon, values)
            previous_variable_values = variable_values
            previous_variable_details = variable_details

            logger.info(
                f'Processed hour {mfh.forecast_hour} ' f'({mfh.forecast_dt})'
            )
        elapsed = default_timer() - start
        logger.info(
            f'Processed {len(model_forecast_hours)} time steps in '
            f'{elapsed}s'
        )


if __name__ == '__main__':
    asyncio.run(main())
