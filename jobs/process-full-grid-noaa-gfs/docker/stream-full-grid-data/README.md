# Stream Full Grid Data

Docker image to stream full grid GFS records to kinesis.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate process-full-grid-noaa-gfs-stream-full-grid-data
$ cp .env.sample .env
```

## Run Locally

### Parse

```sh
$ env $(xargs < .env) python main.py > /dev/null
```

### Parse and Print to Stdout

```sh
$ env $(xargs < .env) python main.py
```

### Parse and Build Database

```sh
$ cp weather-writer-darwin-amd64 weather-writer
$ env $(xargs < .env) python main.py | env $(xargs < .env) ./weather-writer
```

## Docker

### Build and Run

Running the Docker image on Docker for Mac is slow because of [issues it has with IO performance](https://docs.docker.com/docker-for-mac/osxfs/#performance-issues-solutions-and-roadmap). If you want to run this image from a Mac be prepared to wait ~20 minutes per GRIB (seriously it's really slow) or try running on a remote Linux instance instead.

```sh
$ docker build -t process-full-grid-noaa-gfs/stream-full-grid-data .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data --volume ~/.aws:/root/.aws process-full-grid-noaa-gfs/stream-full-grid-data
```
