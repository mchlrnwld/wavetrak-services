from typing import List

import requests


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.
    """

    def __init__(self, url: str, agency: str, model: str):
        self.url = url
        self.agency = agency
        self.model = model

    def get_list_of_grids(self) -> List[str]:
        """
        Creates a list grids associated with the model.

        Returns:
            A list of grids associated with the model.
            Example:
            ['ak_4m', 'glo_30m']

        """
        response = requests.get(
            f'{self.url}/model_grids',
            params=dict(agency=self.agency, model=self.model),
        )
        response.raise_for_status()
        return [data['grid'] for data in response.json()]
