from typing import Dict, Optional

import numpy as np  # type: ignore
import science_algorithms.transformations as t  # type: ignore
from grib_helpers.process import parse_grib  # type: ignore
from grib_helpers.variables import GFS_GRIB_VARIABLES  # type: ignore


def parse_data_from_gribs(file_path: str, previous_file_path: Optional[str]):
    previous_variable_values: Dict[str, np.array] = {}
    previous_variable_details: Dict[str, str] = {}
    if previous_file_path:
        (
            _,
            _,
            previous_variable_values,
            previous_variable_details,
        ) = parse_grib(previous_file_path, GFS_GRIB_VARIABLES)

    lat, lon, values, details = parse_grib(file_path, GFS_GRIB_VARIABLES)

    variable_values = {
        key: (values[key] if key in values else previous_variable_values[key])
        for key in GFS_GRIB_VARIABLES.keys()
    }

    variable_details = {
        key: (
            details[key] if key in details else previous_variable_details[key]
        )
        for key in GFS_GRIB_VARIABLES.keys()
    }
    return lat, lon, variable_values, variable_details


def get_weather_conditions(variable_values, temperature_c, precipitation_type):
    # TODO: Should we replace this with dewpoint from GFS?
    dewpoint_for_weather_conditions = t.get_dewpoint_for_weather_conditions(
        temperature_c, variable_values['relativeHumidity']
    )
    storm_wind_magnitude = t.get_storm_wind_magnitude(
        variable_values['stormWindU'], variable_values['stormWindV']
    )
    thunder_type = t.get_thunder_type(
        variable_values['liftedIndex'],
        variable_values['capeSurface'],
        variable_values['capeML'],
        variable_values['verticalVelocity750mb'],
        variable_values['verticalVelocity800mb'],
        # TODO: Should we replace this with dewpoint from GFS?
        dewpoint_for_weather_conditions,
        variable_values['convectiveInhibition'],
        storm_wind_magnitude,
    )
    net_radiation = t.get_net_radiation(
        variable_values['downShortRadFlux'],
        variable_values['downLongRadFlux'],
        variable_values['upShortRadFlux'],
        variable_values['upLongRadFlux'],
    )
    # Silly hack to get around line length error.
    get_humidity = t.get_average_low_layer_relative_humidity
    average_low_layer_relative_humidity = get_humidity(
        variable_values['relativeHumidity925mb'],
        variable_values['relativeHumidity950mb'],
        variable_values['relativeHumidity975mb'],
        variable_values['relativeHumidity1000mb'],
    )
    wind_magnitude = t.get_wind_magnitude(
        variable_values['windU'], variable_values['windV']
    )
    fog_type = t.get_fog_type(
        net_radiation,
        average_low_layer_relative_humidity,
        variable_values['relativeHumidity'],
        wind_magnitude,
    )
    tod_layer_temperatures = {
        layer: variable_values[f'temperature{layer.replace(" ", "")}']
        for layer in t.total_optical_density_layers
    }
    tod_layer_cloud_mixing_ratios = {
        layer: variable_values[f'cloudMixingRatio{layer.replace(" ", "")}']
        for layer in t.total_optical_density_layers
    }
    tod_layer_heights = {
        layer: variable_values[f'height{layer.replace(" ", "")}']
        for layer in t.total_optical_density_layers
    }
    total_optical_density = t.get_total_optical_density(
        variable_values['land'],
        tod_layer_temperatures,
        tod_layer_cloud_mixing_ratios,
        tod_layer_heights,
    )
    sky_conditions = t.get_sky_conditions(
        total_optical_density, variable_values['totalCloudCover']
    )
    return t.get_weather_conditions(
        precipitation_type,
        thunder_type,
        fog_type,
        sky_conditions,
        wind_magnitude,
        variable_values['precipitationRate'],
        variable_values['convPrecipitationRate'],
        total_optical_density,
    )
