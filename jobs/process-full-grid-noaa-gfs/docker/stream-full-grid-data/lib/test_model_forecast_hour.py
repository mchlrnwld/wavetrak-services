from datetime import datetime
from random import randint

import lib.model_forecast_hour as mfh

GRID = '0p25'


def test_model_forecast_hour():
    model_analysis_hour = mfh.ModelForecastHour(2019030700, 0, GRID)
    # Forecast datetime computed correctly.
    assert [datetime(2019, 3, 7, 0)] == [model_analysis_hour.forecast_dt]

    # GRIB path computed correctly.
    assert ['gfs.20190307/00/atmos/gfs.t00z.pgrb2.0p25.f000'] == [
        model_analysis_hour.grib_path
    ]

    # Previous GRIB path computed for analysis forecast hours.
    assert ['gfs.20190306/18/atmos/gfs.t18z.pgrb2.0p25.f006'] == [
        model_analysis_hour.previous_grib_path
    ]


def test_get_model_grib_path():
    assert [
        f'gfs.20190307/00/atmos/gfs.t00z.pgrb2.0p25.f001',
        f'gfs.20190306/18/atmos/gfs.t18z.pgrb2.0p25.f001',
        f'gfs.20190306/12/atmos/gfs.t12z.pgrb2.0p25.f001',
        f'gfs.20190306/06/atmos/gfs.t06z.pgrb2.0p25.f001',
    ] == [
        mfh.get_model_grib_path(2019030700, 1, GRID),
        mfh.get_model_grib_path(2019030618, 1, GRID),
        mfh.get_model_grib_path(2019030612, 1, GRID),
        mfh.get_model_grib_path(2019030606, 1, GRID),
    ]
    # Pad forecast hour to 3 spaces
    hours = [randint(0, 9), randint(10, 99), randint(100, 999)]
    assert [
        f'gfs.20190307/00/atmos/gfs.t00z.pgrb2.0p25.f00{hours[0]}',
        f'gfs.20190307/00/atmos/gfs.t00z.pgrb2.0p25.f0{hours[1]}',
        f'gfs.20190307/00/atmos/gfs.t00z.pgrb2.0p25.f{hours[2]}',
    ] == [
        mfh.get_model_grib_path(2019030700, hours[0], GRID),
        mfh.get_model_grib_path(2019030700, hours[1], GRID),
        mfh.get_model_grib_path(2019030700, hours[2], GRID),
    ]
