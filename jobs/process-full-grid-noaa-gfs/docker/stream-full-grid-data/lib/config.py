import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
GFS_BUCKET = get_secret(SECRETS_PREFIX, 'GFS_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
DOWNLOAD_DIR = os.environ.get('DOWNLOAD_DIR', 'tmp')
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
RUN = os.environ['RUN']
MODEL_HOUR_LIMIT = int(os.getenv('MODEL_HOUR_LIMIT', 0))
