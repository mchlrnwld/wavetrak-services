from science_algorithms.enums import WeatherConditions as WC  # type: ignore

text_to_conditions = {
    'Clear': WC.CLEAR,
    'Sunny': WC.CLEAR,
    'Mostly Clear': WC.MOSTLY_CLEAR,
    'Fair': WC.MOSTLY_CLEAR,
    'Mostly Cloudy': WC.MOSTLY_CLOUDY,
    'Cloudy': WC.CLOUDY,
    'Overcast': WC.OVERCAST,
    'Mist': WC.MIST,
    'Fog': WC.FOG,
    'Drizzle': WC.DRIZZLE,
    'Brief Showers Possible': WC.BRIEF_SHOWERS_POSSIBLE,
    'Light Showers': WC.BRIEF_SHOWERS,
    'Light Showers Possible': WC.LIGHT_SHOWERS_POSSIBLE,
    'Brief Showers': WC.LIGHT_SHOWERS,
    'Showers': WC.SHOWERS,
    'Brief but Heavy Showers Possible': WC.HEAVY_SHOWERS_POSSIBLE,
    'Heavy Showers': WC.HEAVY_SHOWERS,
    'Light Rain': WC.LIGHT_RAIN,
    'Light Rain and Fog': WC.LIGHT_RAIN_AND_FOG,
    'Rain': WC.RAIN,
    'Rain and Fog': WC.RAIN_AND_FOG,
    'Heavy Rain': WC.HEAVY_RAIN,
    'Torrential Rain': WC.TORRENTIAL_RAIN,
    'Hail': WC.ICE_PELLETS,
    'Brief Hail Showers': WC.BRIEF_ICE_PELLETS_SHOWERS,
    'Hail Showers': WC.ICE_PELLETS_SHOWERS,
    'Sleet': WC.FREEZING_RAIN,
    'Brief Sleet Showers': WC.BRIEF_FREEZING_RAIN_SHOWERS,
    'Sleet Showers': WC.FREEZING_RAIN_SHOWERS,
    'Light Snow': WC.LIGHT_SNOW,
    'Snow': WC.SNOW,
    'Heavy Snow': WC.HEAVY_SNOW,
    'Blizzard': WC.BLIZZARD,
    'Severe Blizzard': WC.SEVERE_BLIZZARD,
    'Brief Snow Flurries': WC.BRIEF_SNOW_FLURRIES,
    'Brief Snow Showers': WC.BRIEF_SNOW_SHOWERS,
    'Light Snow Showers': WC.LIGHT_SNOW_SHOWERS,
    'Snow Showers': WC.SNOW_SHOWERS,
    'Brief but Heavy Snow Showers': WC.BRIEF_HEAVY_SNOW_SHOWERS,
    'Heavy Snow Showers': WC.HEAVY_SNOW_SHOWERS,
    'Snow Sqalls': WC.SNOW_SQUALLS,
    'Severe Snow Squalls': WC.SEVERE_SNOW_SQUALLS,
    'Thundery Showers': WC.THUNDER_SHOWERS,
    'Heavy Thunder Showers': WC.HEAVY_THUNDER_SHOWERS,
    'Thunderstorms': WC.THUNDER_STORMS,
    'Heavy Thunderstorms': WC.HEAVY_THUNDER_STORMS,
    'Severe Thunderstorms': WC.SEVERE_THUNDER_STORMS,
}

# TODO: Removed this unit test as it's difficult to maintain as the algorithm
# evolves
# class TestGRIB(TestCase):
#     def test_get_weather_conditions(self):
#         test_fixture = open_weather_grid_json_fixture()
#         expected_weather_conditions = np.array(
#             [
#                 text_to_conditions[point['weather']['text']]
#                 for point in test_fixture
#             ]
#         )

#         variable_values = gfs_convert_variables(test_fixture)
#         hourly_precipitation = t.get_hourly_precipitation(
#             variable_values['totalPrecipitation'],
#             variable_details['totalPrecipitation'],
#             previous_variable_values['totalPrecipitation'],
#             previous_variable_details['totalPrecipitation'],
#         )
#         actual_weather_conditions = g.get_weather_conditions(
#             variable_values,
#             t.kelvin_to_celsius(variable_values['temperature']),
#             t.get_precipitation_type(
#                 hourly_precipitation,
#                 variable_values['snow'] > 0,
#                 variable_values['freezingRain'] > 0,
#                 variable_values['icePellets'] > 0,
#             ),
#         )

#         error_indices = np.where(
#             actual_weather_conditions != expected_weather_conditions
#         )
#         error_rate = len(error_indices[0]) / len(actual_weather_conditions)
#         accuracy = round(
#             100 * (1 - error_rate),
#             1,
#         )

#         self.assertEqual(
#             accuracy, 100, f'Errors found. Accuracy: {accuracy}%.'
#         )
