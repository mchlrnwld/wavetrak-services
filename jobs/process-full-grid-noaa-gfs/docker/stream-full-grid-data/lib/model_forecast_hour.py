from datetime import datetime, timedelta

MODEL_RUN_FORMAT = '%Y%m%d%H'


class ModelForecastHour:
    """
    Encapsulates all information necessary to
    process data for a model's forecast hour.

    Args:
        run:           Model run time being processed,
                       in the format YYYYMMDDHH.
        forecast_hour: Forecasted hour from analysis hour.

    Attributes:
        run:                Model run time being processed,
                            in the format YYYYMMDDHH.
        forecast_hour:      Forecasted hour from analysis hour.
        forecast_dt:        Forecasted hour's timestamp.
        grib_path:          Path to GRIB file.
        previous_grib_path: Path to previous GRIB file to fill in
                            missing data. For analysis hour only.
    """

    def __init__(
        self, run: int, forecast_hour: int, grid: str,
    ):
        self.run = run
        self.forecast_hour = forecast_hour
        run_dt = datetime.strptime(str(self.run), MODEL_RUN_FORMAT)
        self.forecast_dt = run_dt + timedelta(hours=forecast_hour)
        self.grid = grid

        file_run = self.run
        file_hour = self.forecast_hour
        file_run_dt = run_dt

        self.grib_path = get_model_grib_path(file_run, file_hour, grid)

        # Get path to previous GRIB file to fill in missing data if file_hour
        # indicates an analysis hour.
        self.previous_grib_path = None
        if forecast_hour == 0:
            previous_run_dt = file_run_dt - timedelta(hours=6)
            previous_run = previous_run_dt.strftime(MODEL_RUN_FORMAT)
            self.previous_grib_path = get_model_grib_path(
                int(previous_run), 6, grid
            )

    def __repr__(self):
        return (
            'ModelForecastHour '
            f'(run: {self.run}, '
            f'forecast_hour: {self.forecast_hour}, '
            f'forecast_dt: {self.forecast_dt}, '
            f'grib_path: {self.grib_path}, '
            f'previous_grib_path: {self.previous_grib_path})'
        )


def get_model_grib_path(run: int, hour: int, grid: str):
    """
    Constructs path to GRIB file from a model run time and forecast hour.

    Args:
        run: Model run time.
        forecast_hour: Forecasted hour or hours from analysis hour.

    Returns:
        String path to GRIB file.
    """
    run_date = str(run)[:8]
    run_hour = str(run)[8:]
    prefix = f'gfs.{run_date}/{run_hour}/atmos'
    return f'{prefix}/gfs.t{run_hour}z.pgrb2.{grid}.f{hour:03d}'
