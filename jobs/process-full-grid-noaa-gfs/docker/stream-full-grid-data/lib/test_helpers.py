import json
import os

import numpy as np  # type: ignore


def open_weather_grid_json_fixture():
    path_to_fixture = os.path.join(
        os.getcwd(), 'fixtures', 'weather_grid_height.json'
    )

    with open(path_to_fixture) as f:
        test_fixture = json.loads(f.read())
        print(f'\nTesting against {len(test_fixture)} points...')
        return test_fixture


def gfs_variable(test_fixture, variable):
    return np.array([point['gfs'][variable] for point in test_fixture])


def gfs_convert_variable_name(variable):
    if 'RH' in variable and 'mb' in variable:
        return variable.replace('RH:', 'relativeHumidity').replace(' ', '')
    if 'TMP' in variable and 'mb' in variable:
        return variable.replace('TMP:', 'temperature').replace(' ', '')
    if 'CLWMR' in variable and 'mb' in variable:
        return variable.replace('CLWMR:', 'cloudMixingRatio').replace(' ', '')
    if 'HGT' in variable and 'mb' in variable:
        return variable.replace('HGT:', 'height').replace(' ', '')

    variable_map = {
        'HGT:surface': 'height',
        'RH:2 m above ground': 'relativeHumidity',
        'TMP:2 m above ground': 'temperature',
        'USTM:6000-0 m above ground': 'stormWindU',
        'VSTM:6000-0 m above ground': 'stormWindV',
        '4LFTX:surface': 'liftedIndex',
        'CAPE:surface': 'capeSurface',
        'CAPE:255-0 mb above ground': 'capeML',
        'VVEL:750 mb': 'verticalVelocity750mb',
        'VVEL:800 mb': 'verticalVelocity800mb',
        'CIN:surface': 'convectiveInhibition',
        'DSWRF:surface': 'downShortRadFlux',
        'DLWRF:surface': 'downLongRadFlux',
        'USWRF:surface': 'upShortRadFlux',
        'ULWRF:surface': 'upLongRadFlux',
        'UGRD:10 m above ground': 'windU',
        'VGRD:10 m above ground': 'windV',
        'LAND:surface': 'land',
        'TCDC:entire atmosphere': 'totalCloudCover',
        'PRATE:surface': 'precipitationRate',
        'CPRAT:surface': 'convPrecipitationRate',
        'CFRZR:surface': 'freezingRain',
        'CICEP:surface': 'icePellets',
    }

    if variable in variable_map:
        return variable_map[variable]

    return None


def gfs_convert_variables(test_fixture):
    return {
        gfs_convert_variable_name(v): gfs_variable(test_fixture, v)
        for v in test_fixture[0]['gfs'].keys()
    }
