import asyncio
import logging

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type:ignore
from file_transfer import S3Client
from grib_helpers.indices import GFS_INDICES
from grib_helpers.inventory import parse_byte_ranges_from_inventory

logger = logging.getLogger('stream-full-grid-data')


class Downloader:
    """
    Downloader to interface with S3 and download WW3 files asynchronously.

    Arguments:
        bucket: Name of S3 Bucket to download from.
        max_concurrency: The maximum number of concurrent download streams.

    Attributes:
        bucket: Name of S3 Bucket to download from.
        client: boto3 client for S3.
    """

    def __init__(self, bucket: str, max_concurrency: int = 5):
        self.bucket = bucket
        self.s3_client = S3Client()
        self.semaphore = asyncio.Semaphore(max_concurrency)
        self.boto_s3_client = boto3.client('s3')

    async def download_grib_file(
        self, grib_key: str, idx_key: str, grib_local_destination: str
    ):
        """
        Downloads a subset of variables for a GRIB file from the given S3
        Bucket using the IDX file.

        Args:
            grib_key: S3 key for the GRIB file to download.
            idx_key: S3 key for the IDX file to download for parsing variables
                     from inventory.
            grib_local_destination: The local file path to download the S3
                                    object to.
        """
        async with self.semaphore:
            try:
                inventory = (
                    await self.s3_client.get_object_contents(
                        self.bucket, idx_key
                    )
                ).decode('utf-8')
                byte_ranges = parse_byte_ranges_from_inventory(
                    inventory, GFS_INDICES
                )
                await self.s3_client.download_object_byte_ranges(
                    self.bucket, grib_key, grib_local_destination, byte_ranges
                )
                logger.info(
                    f'Downloaded s3://{self.bucket}/{grib_key} to '
                    f'{grib_local_destination}'
                )
            except ClientError as e:
                logger.error(
                    f'Failed to download s3://{self.bucket}/{grib_key} to '
                    f'{grib_local_destination}!'
                )
                raise e
