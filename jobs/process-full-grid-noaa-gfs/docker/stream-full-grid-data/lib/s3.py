import asyncio
import logging
import os
import re
from collections import OrderedDict
from datetime import datetime, timedelta
from functools import partial
from itertools import chain
from typing import Dict, List, Tuple

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type:ignore

# Matches:
# gfs.20210506/00/atmos/gfs.t00z.pgrb2.0p25.f000
# gfs.20210505/18/atmos/gfs.t18z.pgrb2.0p25.f006
GFS_REGEX = re.compile(
    r'gfs\.\d{8}/\d{2}/atmos/gfs\.t\d{2}z\.pgrb2\.0p25\.f(\d{3})$'
)
EXPECTED_FILL_DATA_HOURS = 24

RUN_FORMAT = '%Y%m%d%H'

logger = logging.getLogger('stream-full-grid-data')


def get_relevant_previous_runs(current_run: int) -> List[int]:
    """
    Gets a list of the previous 48 hours
    of runs from the current run.

    Args:
        current_run: The current run.

    Returns:
        A list of the previous runs.
        Example:
        ['2020021000', '2020020918']
    """
    run_dt = datetime.strptime(str(current_run), RUN_FORMAT)
    previous_runs = [
        int((run_dt - timedelta(hours=6 * i)).strftime(RUN_FORMAT))
        for i in range(1, 9)
    ]
    return previous_runs


class MissingFilesException(Exception):
    pass


class S3Client:
    """
    S3Client to interface with S3 and download
    GFS files asynchronously.

    Arguments:
        bucket: Name of S3 Bucket to download from.

    Attributes:
        bucket: Name of S3 Bucket to download from.
        client: boto3 client for S3.
    """

    def __init__(self, bucket):
        self.bucket = bucket
        self.client = boto3.client('s3')

    async def download_gfs_file(self, key: str, local_file_path: str):
        """
        Downloads a GFS file asynchronously from the given S3 Bucket.

        Args:
            key:             The key object in the S3 Bucket to download.
            local_file_path: The local file path to download the S3 object to.
        """
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None, partial(self._download_gfs_file, key, local_file_path)
        )

    def _download_gfs_file(self, key: str, local_file_path: str):
        """
        Downloads a GFS file from the given S3 Bucket.

        Args:
            key:             The key object in the S3 Bucket to download.
            local_file_path: The local file path to download the S3 object to.
        """
        try:
            os.makedirs(os.path.dirname(local_file_path), exist_ok=True)
            self.client.download_file(self.bucket, key, local_file_path)
            logger.info(
                f'Downloaded s3://{self.bucket}/{key} to {local_file_path}'
            )
        except ClientError as e:
            logger.error(
                f'Failed to download s3://{self.bucket}/{key} to '
                f'{local_file_path}!'
            )
            raise e

    def _generate_forecast_hour_ranges(
        self, current_run: int, backfill_runs: List[int]
    ) -> Dict[int, Tuple[int, int]]:
        """
        Generates forecast hour ranges for each run. The ranges correspond to
        the exact hours needed to fill data for the past 24 hours.
        Ex: Current run: 2021032212.
            Prior run: 2021032206. Hours: [0,6)
            Prior run: 2021032106. Hours: [6,12)

        Args:
            current_run: The key object in the S3 Bucket to download.
            backfill_runs: The backfill runs used to get the
                           last 24 hours of data.

        Returns:
            A dictionary of runs and the appropriate forecast hour ranges.
        """
        run_datetime = datetime.strptime(str(current_run), RUN_FORMAT)
        run_24_hour_before = datetime.strptime(
            str(current_run), RUN_FORMAT
        ) - timedelta(hours=24)
        run_54_hour_before = int(
            (run_datetime - timedelta(hours=54)).strftime(RUN_FORMAT)
        )
        all_backfill_runs = backfill_runs + [run_54_hour_before]
        forecast_hour_ranges = {
            run: (0, 6)
            if run_datetime - datetime.strptime(str(run), RUN_FORMAT)
            <= timedelta(hours=24)
            # For runs *more* than 24 hours behind the current run,
            # calculate the forecast hour ranges based on the
            # run *exactly* 24 hours from the current run.
            # Ex: Current run: 2021032212. Run 24 hour before: 2021032112
            #     Range for run 36 hour before: [12, 18)
            else (
                int(
                    (
                        run_24_hour_before
                        - datetime.strptime(str(run), RUN_FORMAT)
                    ).total_seconds()
                    / 3600
                ),
                int(
                    (
                        run_24_hour_before
                        - datetime.strptime(
                            str(all_backfill_runs[i + 1]), RUN_FORMAT
                        )
                    ).total_seconds()
                    / 3600
                ),
            )
            for i, run in enumerate(all_backfill_runs)
            if i < len(all_backfill_runs) - 1
        }
        return forecast_hour_ranges

    def _check_grid_available(
        self,
        grid: str,
        run: int,
        forecast_hour_ranges: Dict[int, Tuple[int, int]],
    ) -> bool:
        """
        Checks if a grid has all the files needed.

        Args:
            current_run: The current run to process
            grids: The local file path to download the S3 object to.
            forecast_hour_ranges: The exact forecast hours needed for each run.

        Returns:
            True if all files are available for the grid. False otherwise.
        """
        run_date = str(run)[:8]
        run_hour = str(run)[8:]
        key_prefix = f'gfs.{run_date}/{run_hour}/atmos'
        paginator = self.client.get_paginator('list_objects_v2')
        object_keys = [
            item['Key']
            for item in chain.from_iterable(
                response['Contents']
                for response in paginator.paginate(
                    Bucket=self.bucket, Prefix=key_prefix
                )
                if 'Contents' in response
            )
        ]
        if not object_keys:
            return False

        available_file_hours = sorted(
            {
                int(object_key[-3:])
                for object_key in object_keys
                if GFS_REGEX.search(object_key)
            }
        )
        forecast_hours_for_run = set(range(*forecast_hour_ranges[run]))

        if not forecast_hours_for_run.issubset(available_file_hours):
            logger.error(
                f'Found {len(available_file_hours)} file hours for {grid}.'
                f'Expected {forecast_hours_for_run}. file hours. '
                f'For run: {run}'
            )
            return False
        return True

    def _get_backfill_runs(
        self, current_run: int, grids: List[str],
    ) -> Dict[int, List[int]]:
        """
        Gets backfill runs needed to fill the last 24 hours from the current
        run. Runs from the past 48 hours may be used if there is a prior run
        missing.

        Args:
            current_run: The current run to process.
            grids: The grids associated with the run.

        Returns:
            A dict of runs and the associated hours to use for each run.
        """
        backfill_runs = get_relevant_previous_runs(current_run)
        forecast_hour_ranges = self._generate_forecast_hour_ranges(
            current_run, backfill_runs
        )
        backfill_files_to_download = OrderedDict({})
        for i, run in enumerate(backfill_runs):
            if all(
                self._check_grid_available(grid, run, forecast_hour_ranges)
                for grid in grids
            ):
                backfill_files_to_download[run] = list(
                    range(*forecast_hour_ranges[run])
                )
            else:
                if i == len(backfill_runs) - 1:
                    raise MissingFilesException(
                        f'Insufficient number of backfill files '
                        f'to complete the run.'
                    )
                current_upper_forecast_hour = forecast_hour_ranges[run][1]
                next_run = backfill_runs[i + 1]
                next_lower_forecast_hour = forecast_hour_ranges[next_run][0]
                # Increase the upper forecast hour of next run
                # (ex. [0, 6) -> [0, 12))
                # to account for the current run missing files.
                forecast_hour_ranges[next_run] = (
                    next_lower_forecast_hour,
                    current_upper_forecast_hour + 6,
                )
                logger.error(f'Files not found for run: {run}.')
                continue

            # Stop once we get 24 hours of data filled.
            if (
                len(
                    [
                        hour
                        for run_hours in backfill_files_to_download.values()
                        for hour in run_hours
                    ]
                )
                == EXPECTED_FILL_DATA_HOURS
            ):
                logger.info('Found all backfill files needed.')
                break

        if not backfill_files_to_download:
            raise MissingFilesException(
                'Insufficient number of backfill files to complete the run.'
            )

        logger.info(
            f'Found the following backfill files to download: '
            f'{backfill_files_to_download}'
        )

        return backfill_files_to_download

    def get_runs_to_download(
        self, current_run: int, grids: List[str],
    ) -> Dict[int, List[int]]:
        """
        Gets all of the runs needed to complete processing.
        Data from the previous 24 hours of data is needed.

        Args:
            current_run: The current run to process.
            grids: The grids associated with the run.

        Returns:
            An ordered dict in ascending order of runs and the associated hours
            to use for each run.
        """
        backfill_runs = self._get_backfill_runs(current_run, grids)
        backfill_runs[current_run] = list(
            chain(range(121), range(123, 385, 3))
        )
        for key in sorted(backfill_runs):
            # NOTE: Mypy does not recognize OrderedDict type in the current
            # Python version (3.7).
            backfill_runs.move_to_end(key)  # type: ignore
        return backfill_runs
