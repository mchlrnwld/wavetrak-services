data "aws_kinesis_stream" "smdb_stream" {
  name = "wt-science-models-db-${var.environment}"
}

resource "aws_iam_policy" "batch_job_kinesis_policy" {
  name = "${var.company}-${var.application}-batch-job-kinesis-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/kinesis-policy.json", {
    kinesis_stream = data.aws_kinesis_stream.smdb_stream.arn
  })
}

resource "aws_iam_role_policy_attachment" "batch_job_kinesis_policy_attachment" {
  role       = module.stream_full_grid_data_noaa_gfs.batch_job_iam_role_name
  policy_arn = aws_iam_policy.batch_job_kinesis_policy.arn
}

module "stream_full_grid_data_noaa_gfs" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company     = var.company
  application = "jobs-stream-full-grid-data-noaa-gfs"
  environment = var.environment

  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/process-full-grid-noaa-gfs/stream-full-grid-data:${var.environment}",
    "vcpus" : 2,
    "memory" : 4096,
    "volumes" : [
      {
        "host" : {
          "sourcePath" : "/mnt/ephemeral/airflow"
        },
        "name" : "tmp"
      }
    ],
    "mountPoints" : [
      {
        "sourceVolume" : "tmp",
        "containerPath" : "/mnt/ephemeral/airflow",
        "readOnly" : false
      }
    ]
  }
}
