variable "environment" {
  type = string
}

variable "company" {
  type    = string
  default = "wt"
}

variable "application" {
  type    = string
  default = "process-full-grid-noaa-gfs"
}
