provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/process-full-grid-noaa-gfs/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_full_grid_noaa_gfs" {
  source = "../../"

  environment = "sandbox"
}
