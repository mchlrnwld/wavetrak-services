provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/process-full-grid-noaa-gfs/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_full_grid_noaa_gfs" {
  source = "../../"

  environment = "prod"
}
