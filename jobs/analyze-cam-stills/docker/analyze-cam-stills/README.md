# Analyze Cam Stills

Docker image to parse cam stills from SQS messages.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate analyze-cam-stills
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t analyze-cam-stills/analyze-cam-stills .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env analyze-cam-stills/analyze-cam-stills
```
