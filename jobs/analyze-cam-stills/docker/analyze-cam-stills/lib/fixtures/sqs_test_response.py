# flake8: noqa

THREE_MESSAGE_RESPONSE = {
    'Messages': [
        {
            'MessageId': '0000',
            'ReceiptHandle': 'T35T/R3C3IPT',
            'Body': '{"Message" : "{\\"Records\\":[{\\"s3\\":{\\"bucket\\":{\\"name\\":\\"test-bucket\\"},\\"object\\":{\\"key\\":\\"hi-backdoorfixed/test_full.jpg\\"}}}]}"}',
        },
        {
            'MessageId': '0000',
            'ReceiptHandle': 'T35T/R3C3IPT',
            'Body': '{"Message" : "{\\"Records\\":[{\\"s3\\":{\\"bucket\\":{\\"name\\":\\"test-bucket\\"},\\"object\\":{\\"key\\":\\"id-padang/test_full.jpg\\"}}}]}"}',
        },
        {
            'MessageId': '0000',
            'ReceiptHandle': 'T35T/R3C3IPT',
            'Body': '{"Message" : "{\\"Records\\":[{\\"s3\\":{\\"bucket\\":{\\"name\\":\\"test-bucket\\"},\\"object\\":{\\"key\\":\\"cr-santateresaov/test_full.jpg\\"}}}]}"}',
        },
    ]
}

THREE_MESSAGE_TEST_RESPONSE = [
    '{"Message" : "{\\"Records\\":[{\\"s3\\":{\\"bucket\\":{\\"name\\":\\"test-bucket\\"},\\"object\\":{\\"key\\":\\"hi-backdoorfixed/test_full.jpg\\"}}}]}"}',
    '{"Message" : "{\\"Records\\":[{\\"s3\\":{\\"bucket\\":{\\"name\\":\\"test-bucket\\"},\\"object\\":{\\"key\\":\\"id-padang/test_full.jpg\\"}}}]}"}',
    '{"Message" : "{\\"Records\\":[{\\"s3\\":{\\"bucket\\":{\\"name\\":\\"test-bucket\\"},\\"object\\":{\\"key\\":\\"cr-santateresaov/test_full.jpg\\"}}}]}"}',
]
