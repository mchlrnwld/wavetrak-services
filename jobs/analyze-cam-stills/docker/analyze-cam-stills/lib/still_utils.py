import logging
from typing import Generator, List

from lib.sqs_client import SQSClient
from lib.still import Still

logger = logging.getLogger('analyze-cam-stills')


def build_still_batches_from_sqs(
    sqs_name: str, batch_size: int = 20, delete_messages: bool = False
) -> Generator[List[Still], None, None]:
    """
    Builds batches of stills by polling SQS for bucket_name and object_key
    parsed from SQS messages.

    Args:
        sqs_name: name for the SQS queue to poll
        batch_size: max number of stills in a batch
        delete_messages: bool to delete messages or not

    Returns:
        yields batches of Still objects
    """
    stills = []

    sqs_client = SQSClient(sqs_name)

    for bucket_name, object_key in sqs_client.poll_for_messages(
        delete_messages
    ):
        stills.append(Still(bucket_name, object_key))

        if len(stills) == batch_size:
            yield stills
            stills = []

    if len(stills) > 0:
        yield stills

    logger.info('No more messages in queue to process.')
