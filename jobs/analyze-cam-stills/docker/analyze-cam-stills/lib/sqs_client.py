import json
import logging
from typing import Generator, Tuple

import boto3  # type: ignore

logger = logging.getLogger('analyze-cam-stills')


class SQSClient:
    """
    SQSClient to interface with SQS and poll/delete messages.

    Args:
        name: name of SQS Queue to interface with

    Attributes:
        sqs_url: name of SQS Queue to interface with
        client: boto3 client for SQS
    Errors:
        exception raised if queue is not found
    """

    def __init__(self, name: str):
        self.client = boto3.client('sqs')
        self.sqs_url = self.client.get_queue_url(QueueName=name)['QueueUrl']

    def poll_for_messages(
        self, delete_messages: bool
    ) -> Generator[Tuple[str, str], None, None]:
        """
        Polls AWS SQS queue for 10 messages, and deletes each message after
        yielding the bucket name and object's key after parsing the message.

        Returns:
            generator that yields bucket_name and object_key
        """
        empty_queue = False
        while not empty_queue:
            logger.info(f'Polling messages from SQS Queue: {self.sqs_url}')
            response = self.client.receive_message(
                QueueUrl=self.sqs_url,
                AttributeNames=['SentTimestamp'],
                MaxNumberOfMessages=10,
                MessageAttributeNames=['All'],
                VisibilityTimeout=30,
            )

            if 'Messages' in response:
                for message in response['Messages']:
                    message_receipt = message['ReceiptHandle']
                    message_body = json.loads(message['Body'])['Message']
                    message_info = json.loads(message_body)

                    if 'Records' in message_info:
                        record = message_info['Records'][0]
                        bucket_name = record['s3']['bucket']['name']
                        object_key = record['s3']['object']['key']

                        if delete_messages:
                            self.client.delete_message(
                                QueueUrl=self.sqs_url,
                                ReceiptHandle=message_receipt,
                            )

                        if object_key.endswith('latest_full.jpg'):
                            continue

                        yield bucket_name, object_key
            else:
                empty_queue = True
