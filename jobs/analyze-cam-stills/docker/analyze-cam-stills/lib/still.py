import asyncio
import logging
from functools import partial
from io import BytesIO
from typing import Tuple

from PIL import Image  # type: ignore

from lib.s3_client import S3Client

logger = logging.getLogger('analyze-cam-stills')


class Still:
    """
    Still class is used to interface with stills polled from S3.

    download() function will download the still's image from S3 and resize
    the image if necessary.

    Args:
        bucket: bucket name where still is in S3
        key: object's key in S3

    Attributes:
        bucket: bucket name where still is in S3
        key: object's key in S3
    """

    def __init__(self, bucket: str, key: str):
        self.bucket = bucket
        self.key = key

    async def download(self, target_size: Tuple[int, int]):
        """
        Downloads an image using S3Client and returns image byte data.
        """
        loop = asyncio.get_event_loop()
        image = await loop.run_in_executor(
            None, partial(self._download_image, target_size)
        )

        return image

    def _download_image(self, target_size):
        """
        Private function that performs the image download process
        using S3Client. Also resizes the image, if sizes don't match what
        is passed in.
        """
        s3_client = S3Client(self.bucket)
        still_data = s3_client.download_file(self.key)

        image = Image.open(BytesIO(still_data))

        if image.size != target_size:
            image = image.resize(target_size, Image.LANCZOS)

        return image
