import logging

import boto3  # type: ignore

logger = logging.getLogger('analyze-cam-stills')


class S3Client:
    """
    S3Client to interface with S3 and download an object.

    Args:
        bucket: name of S3 bucket to download from

    Attributes:
        bucket: name of S3 bucket to download from
        client: boto3 client for S3
    """

    def __init__(self, bucket: str):
        self.bucket = bucket
        self.client = boto3.client('s3')

    def download_file(self, key: str) -> bytes:
        """
        Downloads an object from the given S3 bucket.

        Args:
            key: the object's key in the S3 bucket
        Returns:
            downloaded object's data
        """
        logger.info(f'Downloading s3://{self.bucket}/{key}')
        response = self.client.get_object(Bucket=self.bucket, Key=key)

        return response['Body'].read()
