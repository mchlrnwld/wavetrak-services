import boto3  # type: ignore
from moto import mock_sqs  # type: ignore

import lib.still_utils as still_utils
from lib.fixtures.sqs_test_response import THREE_MESSAGE_TEST_RESPONSE


@mock_sqs
def test_multiple_messages():
    """ test that we correctly build a batch of stills from an SQS response"""
    sqs = boto3.resource("sqs", region_name='us-west-1')
    queue = sqs.create_queue(QueueName='test-queue')
    for body in THREE_MESSAGE_TEST_RESPONSE:
        queue.send_message(MessageBody=body)

    still_batch_gen = still_utils.build_still_batches_from_sqs(
        'test-queue', batch_size=3, delete_messages=False
    )

    stills_batch = next(still_batch_gen)

    assert stills_batch[0].bucket == 'test-bucket'
    assert stills_batch[0].key == 'hi-backdoorfixed/test_full.jpg'
    assert stills_batch[1].bucket == 'test-bucket'
    assert stills_batch[1].key == 'id-padang/test_full.jpg'
    assert stills_batch[2].bucket == 'test-bucket'
    assert stills_batch[2].key == 'cr-santateresaov/test_full.jpg'


@mock_sqs
def test_multiple_messages_with_multiple_batches():
    """ test that we correctly build a batch of stills from an SQS response"""
    sqs = boto3.resource("sqs", region_name='us-west-1')
    queue = sqs.create_queue(QueueName='test-queue')
    for body in THREE_MESSAGE_TEST_RESPONSE:
        queue.send_message(MessageBody=body)

    still_batch_gen = still_utils.build_still_batches_from_sqs(
        'test-queue', batch_size=2, delete_messages=False
    )

    stills_batch_1 = next(still_batch_gen)
    stills_batch_2 = next(still_batch_gen)

    assert stills_batch_1[0].bucket == 'test-bucket'
    assert stills_batch_1[0].key == 'hi-backdoorfixed/test_full.jpg'

    assert stills_batch_1[1].bucket == 'test-bucket'
    assert stills_batch_1[1].key == 'id-padang/test_full.jpg'

    assert stills_batch_2[0].bucket == 'test-bucket'
    assert stills_batch_2[0].key == 'cr-santateresaov/test_full.jpg'


@mock_sqs
def test_no_messages():
    """ test that we correctly build no stills when there is no messages """
    sqs = boto3.resource("sqs", region_name='us-west-1')
    sqs.create_queue(QueueName='test-queue')

    still_batch_gen = still_utils.build_still_batches_from_sqs(
        'test-queue', batch_size=5, delete_messages=False
    )

    assert next(still_batch_gen, None) is None
