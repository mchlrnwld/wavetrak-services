import asyncio
import logging
from timeit import default_timer

import lib.config as config
import lib.still_utils as still_utils

logger = logging.getLogger('analyze-cam-stills')


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info('Starting analyze-cam-stills job')
    job_start_time = default_timer()

    total_stills = 0
    for still_batch in still_utils.build_still_batches_from_sqs(
        config.SQS_QUEUE_NAME,
        batch_size=config.STILLS_BATCH_SIZE,
        delete_messages=config.DELETE_MESSAGES,
    ):
        batch_start_time = default_timer()

        logger.info(f'Downloading {len(still_batch)} stills')

        target_frame_size = (config.STILL_WIDTH, config.STILL_HEIGHT)
        still_images = await asyncio.gather(
            *[still.download(target_frame_size) for still in still_batch]
        )

        # Print stills image data, this will probably be where we will
        # send stills to endpoint for inference
        for still in still_images:
            logger.info(still)

        elapsed = default_timer() - batch_start_time
        logger.info(
            f'Downloaded batch of {len(still_batch)} ' f'stills in {elapsed}s'
        )

        total_stills += len(still_batch)

    elapsed = default_timer() - job_start_time
    logger.info('Finished analyze-cam-stills job')
    logger.info(f'Downloaded {total_stills} stills from S3 in {elapsed}s')


if __name__ == '__main__':
    asyncio.run(main())
