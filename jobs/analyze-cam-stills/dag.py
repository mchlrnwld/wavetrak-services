from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from dag_helpers.docker import docker_image
from dag_helpers.pager_duty import create_pager_duty_failure_callback

AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')

JOB_NAME = 'analyze-cam-stills'
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
SQS_QUEUE_NAME = ('wt-{0}-s3-events-{1}').format(JOB_NAME, APP_ENVIRONMENT)

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, APP_ENVIRONMENT
)

environment_values = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'JOB_NAME': 'wt-{0}-{1}'.format(JOB_NAME, APP_ENVIRONMENT),
    'JOB_DEFINITION': 'wt-{0}-{1}'.format(JOB_NAME, APP_ENVIRONMENT),
    'JOB_QUEUE': 'wt-jobs-common-high-cpu-local-ssd-job-queue-{0}'.format(
            APP_ENVIRONMENT
    ),
    'TIMEOUT': str(1 * 60 * 60),
    'ENVIRONMENT': ';'.join(
        [
            'ENV={0}'.format(APP_ENVIRONMENT),
            'AWS_DEFAULT_REGION={0}'.format(AWS_DEFAULT_REGION),
            'SQS_QUEUE_NAME={0}'.format(SQS_QUEUE_NAME),
        ]
    ),
}

# DAG defintion
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2020, 8, 20),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'on_failure_callback': pager_duty_failure_callback,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
    'on_failure_callback': pager_duty_failure_callback
}

dag = DAG(
    '{0}-v2'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=1),
    default_args=default_args
)

analyze_cam_stills = WTDockerOperator(
    task_id='analyze-cam-stills',
    image=docker_image('execute-batch-job'),
    environment=environment_values,
    force_pull=True,
    dag=dag,
)
