provider "aws" {
  region  = "us-west-1"
  version = "~> 2.7.0"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/analyze-cam-stills/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "analyze-cam-stills" {
  source      = "../modules/analyze-cam-stills"
  environment = "prod"
  topic_names = ["sl-cam-thumbnails-prod-full-upload-event"]
}
