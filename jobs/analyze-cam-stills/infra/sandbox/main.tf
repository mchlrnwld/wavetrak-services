provider "aws" {
  region  = "us-west-1"
  version = "~> 2.7.0"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/analyze-cam-stills/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "analyze-cam-stills" {
  source      = "../modules/analyze-cam-stills"
  environment = "sandbox"
  topic_names = ["sl-cam-thumbnails-staging-full-upload-event"]
}
