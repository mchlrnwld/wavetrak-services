module "sqs_with_sns_subscription" {
  source      = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs-with-sns-subscription"
  application = var.application
  environment = var.environment
  queue_name  = var.queue_name
  topic_names = var.topic_names
  company     = var.company
}

module "analyze_cam_stills" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company     = var.company
  application = var.application
  environment = var.environment

  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/analyze-cam-stills/analyze-cam-stills:${var.environment}",
    "vcpus" : 1,
    "memory" : 1028
  }
}
