variable "environment" {
  description = "The AWS environment to apply the SQS infrastructure to."
  type        = string
}

variable "topic_names" {
  description = "List of the names for each topic that the SQS queue will subscribe to."
  type        = list(string)
}

variable "queue_name" {
  default = "s3-events"
}

variable "company" {
  default = "wt"
}

variable "application" {
  default = "analyze-cam-stills"
}
