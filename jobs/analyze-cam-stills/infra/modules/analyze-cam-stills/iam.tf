resource "aws_iam_policy" "batch_job_sqs_policy" {
  name = "${var.company}-${var.application}-batch-job-sqs-policy-${var.environment}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sqs:ReceiveMessage",
        "sqs:DeleteMessage",
        "sqs:GetQueueUrl"
      ],
      "Effect": "Allow",
      "Resource": "${module.sqs_with_sns_subscription.queue_arn}"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "batch_job_sqs_policy_attachment" {
  role       = module.analyze_cam_stills.batch_job_iam_role_name
  policy_arn = aws_iam_policy.batch_job_sqs_policy.arn
}
