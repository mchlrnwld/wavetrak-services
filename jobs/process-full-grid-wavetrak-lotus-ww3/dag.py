from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from dag_helpers.docker import docker_image
from dag_helpers.pager_duty import create_pager_duty_failure_callback

AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')
ENV = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT

JOB_NAME = 'process-full-grid-wavetrak-lotus-ww3'
AGENCY = 'Wavetrak'
MODEL = 'Lotus-WW3'
RUN = '{{ task_instance.xcom_pull(task_ids=\'check-model-ready\') }}'
RUN_TYPE = 'FULL_GRID'
SCIENCE_KEY_PREFIX = 'lotus/archive/grids'
STREAM_NAME = 'wt-science-models-db-{0}'.format(ENV)

DEFAULT_TIMEOUT = 6 * 60 * 60

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, ENV
)

common_environment_values = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'ENV': ENV,
    'AGENCY': AGENCY,
    'MODEL': MODEL,
    'MODEL_RUN_TYPE': RUN_TYPE,
    'RUN': RUN,
    'SCIENCE_KEY_PREFIX': SCIENCE_KEY_PREFIX,
    'STREAM_NAME': STREAM_NAME,
}


def task_environment(*keys, **keyvalues):
    environment = keyvalues.copy()
    for key in keys:
        environment[key] = common_environment_values[key]
    return environment


check_model_ready_environment = task_environment(
    'AWS_DEFAULT_REGION', 'AGENCY', 'ENV', 'MODEL', 'SCIENCE_KEY_PREFIX'
)

stream_full_grid_data_environment = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'JOB_NAME': 'wt-jobs-stream-full-grid-data-wavetrak-lotus-ww3-{0}'.format(
        ENV
    ),
    'JOB_DEFINITION': 'wt-jobs-stream-full-grid-data-wavetrak-lotus-ww3-{0}'.format(
        ENV
    ),
    'JOB_QUEUE': 'wt-jobs-common-high-cpu-local-ssd-job-queue-{0}'.format(ENV),
    'TIMEOUT': str(DEFAULT_TIMEOUT),
    'ENVIRONMENT': ';'.join(
        [
            'AWS_REGION={0}'.format(AWS_DEFAULT_REGION),
            'ENV={0}'.format(ENV),
            'DATE={{ ds }}',
            'DOWNLOAD_DIR={0}'.format('/mnt/ephemeral/airflow'),
            'SCIENCE_KEY_PREFIX={0}'.format(SCIENCE_KEY_PREFIX),
            'STREAM_NAME={0}'.format(STREAM_NAME),
            'AGENCY={0}'.format(AGENCY),
            'MODEL={0}'.format(MODEL),
            'RUN={0}'.format(RUN),
        ]
    ),
}

update_science_models_db_environment = task_environment(
    'ENV',
    'AWS_DEFAULT_REGION',
    'STREAM_NAME',
    'MODEL_RUN_TYPE',
    MODEL_AGENCY=AGENCY,
    MODEL_NAME=MODEL,
    MODEL_RUN=RUN,
)

backup_science_models_db_environment = task_environment(
    'ENV',
    'AWS_DEFAULT_REGION',
    MODEL_AGENCY=AGENCY,
    MODEL_NAME=MODEL,
    MODEL_RUN=RUN,
    SERVICE_NAME='wt-science-models-db-{0}'.format(ENV),
)

mark_model_run_complete_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'MODEL_RUN_TYPE',
    MODEL_AGENCY=AGENCY,
    MODEL_NAME=MODEL,
    MODEL_RUN=RUN,
    JOB_NAME=JOB_NAME,
)

prune_model_runs_environment = task_environment(
    'ENV',
    'AWS_DEFAULT_REGION',
    'STREAM_NAME',
    'MODEL_RUN_TYPE',
    MODEL_AGENCY=AGENCY,
    MODEL_NAME=MODEL,
    MODEL_RUN=RUN,
    MODEL_LIMIT='3',
)

# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2020, 2, 20),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
    'on_failure_callback': pager_duty_failure_callback,
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=5),
    default_args=default_args,
)

check_model_ready = WTDockerOperator(
    task_id='check-model-ready',
    image=docker_image('check-model-ready', JOB_NAME),
    environment=check_model_ready_environment,
    force_pull=True,
    xcom_push=True,
    depends_on_past=True,
    dag=dag,
)

check_model_ready_branch = BranchPythonOperator(
    task_id='check-model-ready-branch',
    templates_dict={'run': RUN},
    python_callable=lambda **kwargs: (
        'begin-processing-model'
        if kwargs['templates_dict']['run']
        else 'no-model-ready'
    ),
    dag=dag,
)

no_model_ready = DummyOperator(task_id='no-model-ready', dag=dag)

begin_processing_model = DummyOperator(
    task_id='begin-processing-model', dag=dag
)

stream_full_grid_data = WTDockerOperator(
    task_id='stream-full-grid-data',
    image=docker_image('execute-batch-job'),
    environment=stream_full_grid_data_environment,
    force_pull=True,
    execution_timeout=None,
    dag=dag,
)

update_science_models_db = WTDockerOperator(
    task_id='update-science-models-db',
    image=docker_image('update-science-models-db'),
    environment=update_science_models_db_environment,
    force_pull=True,
    dag=dag,
)

backup_science_models_db = WTDockerOperator(
    task_id='backup-science-models-db',
    image=docker_image('backup-science-models-db'),
    environment=backup_science_models_db_environment,
    force_pull=True,
    dag=dag,
)

mark_model_run_complete = WTDockerOperator(
    task_id='mark-model-run-complete',
    image=docker_image('mark-model-run-complete'),
    environment=mark_model_run_complete_environment,
    force_pull=True,
    dag=dag,
)

prune_model_runs = WTDockerOperator(
    task_id='prune-model-runs',
    image=docker_image('prune-model-runs'),
    environment=prune_model_runs_environment,
    force_pull=True,
    dag=dag,
)

check_model_ready_branch.set_upstream(check_model_ready)
no_model_ready.set_upstream(check_model_ready_branch)
begin_processing_model.set_upstream(check_model_ready_branch)
stream_full_grid_data.set_upstream(begin_processing_model)
update_science_models_db.set_upstream(stream_full_grid_data)
backup_science_models_db.set_upstream(update_science_models_db)
mark_model_run_complete.set_upstream(update_science_models_db)
prune_model_runs.set_upstream(mark_model_run_complete)
