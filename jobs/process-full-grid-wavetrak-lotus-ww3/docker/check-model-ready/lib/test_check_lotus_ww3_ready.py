from lib.check_lotus_ww3_ready import (
    check_lotus_ww3_ready,
    check_model_grid_ready,
)

SCIENCE_BUCKET = 'surfline-science-test'
SCIENCE_KEY_PREFIX = 'lotus/archive/grids'
AGENCY = 'Wavetrak'
MODEL = 'Lotus-WW3'
RUN = 2019050206
EXAMPLE_GRID = 'glo_30m'

GRIDS = ['CAL_3m', 'GLOB_15m', 'GLOB_30m', 'HW_3m', 'US_E_3m']


def test_check_lotus_ww3_ready(create_s3_bucket):
    s3_client = create_s3_bucket
    for grid in GRIDS:
        for num in list(range(385)):
            key = (
                f'{SCIENCE_KEY_PREFIX}/{grid}/{RUN}/'
                f'ww3_lotus_{grid}.{RUN}.{str(num).zfill(3)}.nc'
            )
            s3_client.put_object(Bucket=SCIENCE_BUCKET, Key=key)
    # Expect true when all spot files exist
    assert check_lotus_ww3_ready(
        SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, AGENCY, MODEL, RUN, GRIDS
    )


def test_check_model_grid_ready_all_files_exist(create_s3_bucket):
    s3_client = create_s3_bucket
    for num in list(range(385)):
        key = (
            f'{SCIENCE_KEY_PREFIX}/{EXAMPLE_GRID}/{RUN}/'
            f'ww3_lotus_{EXAMPLE_GRID}.{RUN}.{str(num).zfill(3)}.nc'
        )
        s3_client.put_object(Bucket=SCIENCE_BUCKET, Key=key)

    # Expect true when all files for the grid exist in S3.
    assert check_model_grid_ready(
        SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, AGENCY, MODEL, RUN, EXAMPLE_GRID
    )


def test_check_model_grid_ready_some_files_missing(create_s3_bucket):
    s3_client = create_s3_bucket
    for num in list(range(50)):
        key = (
            f'{SCIENCE_KEY_PREFIX}/{EXAMPLE_GRID}/{RUN}/'
            f'ww3_lotus_{EXAMPLE_GRID}.{RUN}.{str(num).zfill(3)}.nc'
        )
        s3_client.put_object(Bucket=SCIENCE_BUCKET, Key=key)

    # Expect false when files for some hours for grid missing in S3.
    assert not check_model_grid_ready(
        SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, AGENCY, MODEL, RUN, EXAMPLE_GRID
    )
