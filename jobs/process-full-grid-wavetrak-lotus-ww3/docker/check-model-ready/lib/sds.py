from typing import List, Optional

import requests


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        host_url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.

    Attributes:
        host_url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.
    """

    def __init__(self, host_url: str, agency: str, model: str):
        self.host_url = host_url
        self.agency = agency
        self.model = model

    def get_list_of_grids(self) -> List[str]:
        """
        Calls the Science Data Service endpoint to
        get a list of grids associated with the model.

        Returns:
            A list of grids
        """
        query = f'''
        {{
        models(agency: "{self.agency}", model: "{self.model}"){{
            grids{{
            grid
            }}
        }}
        }}'''
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
        }
        response = requests.post(
            f'{self.host_url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        grids_dict = response.json()['data']['models'][0]['grids']
        return [data['grid'] for data in grids_dict]

    def get_active_run(self) -> Optional[int]:
        """
        Calls the Science Data Service endpoint to
        get a PENDING or ONLINE model run.

        Returns:
            The model run in the Science Data Service
            that is currently PENDING or ONLINE.
            If there is no model run PENDING or ONLINE,
            then None is returned.
        """
        query = f'''
        {{
        models(agency: "{self.agency}", model: "{self.model}"){{
            runs(statuses: [PENDING, ONLINE], types: FULL_GRID){{
            run
            }}
        }}
        }}'''
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
        }
        response = requests.post(
            f'{self.host_url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        runs_dict = response.json()['data']['models'][0]['runs']
        runs = [data['run'] for data in runs_dict]
        if not runs:
            return None
        return int(sorted(runs, reverse=True)[0])
