import os

from job_secrets_helper import get_secret

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
SCIENCE_KEY_PREFIX = os.environ['SCIENCE_KEY_PREFIX']
MODEL = os.environ['MODEL']
AGENCY = os.environ['AGENCY']
