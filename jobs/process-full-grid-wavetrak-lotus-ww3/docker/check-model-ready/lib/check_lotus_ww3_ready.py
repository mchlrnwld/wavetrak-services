import logging
import re
from typing import List

import boto3  # type: ignore

logger = logging.getLogger('check-model-ready')

LOTUS_WW3_REGEX = re.compile(rf'ww3\_lotus_.+\.\d{{10}}\.\d{{3}}\.nc$')
EXPECTED_FILE_HOURS = {f'{str(num).zfill(3)}' for num in list(range(385))}


def check_model_grid_ready(
    bucket: str,
    science_key_prefix: str,
    agency: str,
    model: str,
    run: int,
    grid: str,
) -> bool:
    """
    Checks if all the files for the given grid
    and model run exist in S3.

    Args:
        bucket:             S3 bucket to check for file objects.
        science_key_prefix: Key prefix for S3 file objects.
        agency:             Model agency associated
                            with the model run (ex. Wavetrak).
        model:              Name of the model associated
                            with the model run (ex. Lotus-WW3).
        run:                Model run time (YYYYMMDDHH) to check.
        grid:               Grid to check.

    Returns:
        True if all files for model run exist for grid in S3.
    """
    logger.info(
        f'Checking objects with bucket {bucket} and prefix'
        f' {science_key_prefix}/{grid}/{run}.'
    )
    s3_client = boto3.client('s3')
    model_objects_response = s3_client.list_objects_v2(
        Bucket=bucket, Prefix=f'{science_key_prefix}/{grid}/{run}'
    )
    if 'Contents' not in model_objects_response:
        return False

    number_of_objects = len(model_objects_response['Contents'])
    logger.info(f'Found {number_of_objects} objects for {grid}.')

    available_file_hours = {
        object['Key'][-6:-3]
        for object in model_objects_response['Contents']
        if LOTUS_WW3_REGEX.search(object['Key'])
    }

    logger.info(
        f'Found {len(available_file_hours)} file hours for {grid}. Expected'
        f' {len(EXPECTED_FILE_HOURS)}. file hours.'
    )
    return available_file_hours == EXPECTED_FILE_HOURS


def check_lotus_ww3_ready(
    bucket: str,
    science_key_prefix: str,
    agency: str,
    model: str,
    run: int,
    grids: List[str],
) -> bool:
    """
    Checks if a model run is ready.
    This is accomplished by checking that all files
    for every grid are in S3.

    Args:
        bucket:             S3 bucket to check for file objects.
        science_key_prefix: Key prefix for file objects.
        agency:             Model agency associated
                            with the model run (ex. Wavetrak).
        model:              Name of the model associated
                            with the model run (ex. Lotus-WW3).
        run:                Model run time (YYYYMMDDHH) to check.
        grids:              List of grids to check.

    Returns:
        True if file objects for model run exist for all grids in S3.
    """
    return all(
        check_model_grid_ready(
            bucket, science_key_prefix, agency, model, run, grid
        )
        for grid in grids
    )
