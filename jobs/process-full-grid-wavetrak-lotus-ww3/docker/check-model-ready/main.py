import logging

import lib.config as config
from check_model_ready import get_latest_s3_run, set_pending_model_run
from lib.check_lotus_ww3_ready import check_lotus_ww3_ready
from lib.sds import SDS

logger = logging.getLogger('check-model-ready')

RUN_TYPE = 'FULL_GRID'


def main():
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler())
    sds_client = SDS(config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL)
    active_run = sds_client.get_active_run()
    grids = sds_client.get_list_of_grids()
    runs = (
        get_latest_s3_run(
            config.SCIENCE_BUCKET,
            f'{config.SCIENCE_KEY_PREFIX}/{grid}',
            active_run,
        )
        for grid in grids
    )
    latest_run = next(runs)
    if not latest_run or not all(latest_run == run for run in runs):
        print('')
        return

    result = check_lotus_ww3_ready(
        config.SCIENCE_BUCKET,
        config.SCIENCE_KEY_PREFIX,
        config.AGENCY,
        config.MODEL,
        latest_run,
        grids,
    )

    # Write to stdout for xcoms_push
    if result:
        set_pending_model_run(
            config.AGENCY,
            config.MODEL,
            latest_run,
            RUN_TYPE,
            config.SCIENCE_DATA_SERVICE,
        )
        print(latest_run)
    else:
        print('')


if __name__ == '__main__':
    main()
