import sys
from typing import Dict, List, Tuple, Union

from netCDF4 import Dataset  # type: ignore

LOTUS_WW3_VARIABLES = {
    'height': 'hs',
    'frequency': 'fp',
    'direction': 'dp',
    'swellWave1Height': 'phs0',
    'swellWave1Period': 'ptp0',
    'swellWave1Direction': 'pdir0',
    'swellWave2Height': 'phs1',
    'swellWave2Period': 'ptp1',
    'swellWave2Direction': 'pdir1',
    'swellWave3Height': 'phs2',
    'swellWave3Period': 'ptp2',
    'swellWave3Direction': 'pdir2',
    'swellWave4Height': 'phs3',
    'swellWave4Period': 'ptp3',
    'swellWave4Direction': 'pdir3',
    'swellWave5Height': 'phs4',
    'swellWave5Period': 'ptp4',
    'swellWave5Direction': 'pdir4',
    'windWaveHeight': 'phs5',
    'windWavePeriod': 'ptp5',
    'windWaveDirection': 'pdir5',
}


def parse_lotus_ww3_file(
    local_file_path: str,
) -> Tuple[List[float], List[float], Dict[str, List[List[float]]]]:
    """
    Parses a lotus_ww3 file from the local_file_path
    and creates a dictionary with the relevant variables.

    Args:
        local_file_path: Local path of lotus_ww3_file to parse.

    Returns:
        A list of latitude values and longitude values,
        along with a dictionary of the relevant variables
        from the Lotus-WW3 file.

        The index of the lats list serves as first index
        for the variables, and the index of the lons list
        serves as the second index for the variables.

        For example:

        lats: [-77.5  -77.25]
        lons: [1.0, 2.0, 3.0]
        data['height']: [[3.02, 3.03, 3.04],
                         [4.02, 4.03, 4.04]]

        For lat: -77.5, index 0. lon: 2.0, index 1.
        data['height'][0][1] == 3.03
    """
    with Dataset(local_file_path) as lotus_ww3_file:
        data = {
            key: lotus_ww3_file.variables[value][0][:].filled(0.0).tolist()
            for key, value in LOTUS_WW3_VARIABLES.items()
        }
        lats = lotus_ww3_file.variables['latitude'][:].tolist()
        lons = lotus_ww3_file.variables['longitude'][:].tolist()
        return lats, lons, data


def frequency_to_period(frequency: float) -> float:
    return 1 / frequency if frequency > 0.0 else 0.0


def _write_to_stdout(values: List[Union[str, float]]):
    record_csv = ','.join([str(value) for value in values])
    sys.stdout.write(f'{record_csv}\n')


def output_lotus_ww3_data(
    grid: str,
    forecast_hour_timestamp: str,
    lat: float,
    lat_index: int,
    lon: float,
    lon_index: int,
    lotus_ww3_variables: Dict[str, List[List[float]]],
):
    """
    Outputs the appropriate Lotus-WW3 variables to stdout
    for the lat and lon given.

    Args:
        grid:                    Grid associated with
                                 the lotus_ww3_variables.
        forecast_hour_timestamp: A formatted timestamp of
                                 type string calculated from
                                 the run and forecast_hour.
        lat:                     Latitude value to output
                                 and extract data for.
        lat_index:               Index associated with the
                                 given latitude value. Used
                                 to extract data from the
                                 lotus_ww3_variables.
        lon:                     Longitude value to output and
                                 extract data for.
        lon_index:               Index associated with the
                                 given longitude value. Used
                                 to extract data from the
                                 lotus_ww3_variables.
        lotus_ww3_variable:      Lotus-WW3 variables to extract
                                 for the given lat and lon.
    """
    _write_to_stdout(
        [
            grid,
            lat,
            lon,
            forecast_hour_timestamp,
            lotus_ww3_variables['height'][lat_index][lon_index],
            frequency_to_period(
                lotus_ww3_variables['frequency'][lat_index][lon_index]
            ),
            lotus_ww3_variables['direction'][lat_index][lon_index],
            lotus_ww3_variables['swellWave1Height'][lat_index][lon_index],
            lotus_ww3_variables['swellWave1Period'][lat_index][lon_index],
            lotus_ww3_variables['swellWave1Direction'][lat_index][lon_index],
            lotus_ww3_variables['swellWave2Height'][lat_index][lon_index],
            lotus_ww3_variables['swellWave2Period'][lat_index][lon_index],
            lotus_ww3_variables['swellWave2Direction'][lat_index][lon_index],
            lotus_ww3_variables['swellWave3Height'][lat_index][lon_index],
            lotus_ww3_variables['swellWave3Period'][lat_index][lon_index],
            lotus_ww3_variables['swellWave3Direction'][lat_index][lon_index],
            lotus_ww3_variables['swellWave4Height'][lat_index][lon_index],
            lotus_ww3_variables['swellWave4Period'][lat_index][lon_index],
            lotus_ww3_variables['swellWave4Direction'][lat_index][lon_index],
            lotus_ww3_variables['swellWave5Height'][lat_index][lon_index],
            lotus_ww3_variables['swellWave5Period'][lat_index][lon_index],
            lotus_ww3_variables['swellWave5Direction'][lat_index][lon_index],
            lotus_ww3_variables['windWaveHeight'][lat_index][lon_index],
            lotus_ww3_variables['windWavePeriod'][lat_index][lon_index],
            lotus_ww3_variables['windWaveDirection'][lat_index][lon_index],
        ]
    )
