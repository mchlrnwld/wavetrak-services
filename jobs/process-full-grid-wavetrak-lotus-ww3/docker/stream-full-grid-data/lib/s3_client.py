import asyncio
import logging
import os
import re
from datetime import datetime, timedelta
from functools import partial
from typing import Dict, List, Tuple

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type:ignore

from lib.run_helpers import get_relevant_previous_runs

# Matches:
# ww3_lotus_AUS_E_3m.2020022106.120.nc
# ww3_lotus_GLOB_30m.2020022106.002.nc
LOTUS_WW3_REGEX = re.compile(rf'ww3\_lotus_.+\.\d{{10}}\.\d{{3}}\.nc$')
EXPECTED_FILL_DATA_HOURS = 24

RUN_FORMAT = '%Y%m%d%H'

logger = logging.getLogger('stream-full-grid-data')


class MissingFilesException(Exception):
    pass


class S3Client:
    """
    S3Client to interface with S3 and download
    Lotus-WW3 files asynchronously.

    Arguments:
        bucket: Name of S3 Bucket to download from.

    Attributes:
        bucket: Name of S3 Bucket to download from.
        client: boto3 client for S3.
    """

    def __init__(self, bucket):
        self.bucket = bucket
        self.client = boto3.client('s3')

    async def download_lotus_ww3_file(self, key: str, local_file_path: str):
        """
        Downloads a Lotus-WW3 file asynchronously from the given S3 Bucket.

        Args:
            key:             The key object in the S3 Bucket to download.
            local_file_path: The local file path to download the S3 object to.
        """
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None, partial(self._download_lotus_ww3_file, key, local_file_path)
        )

    def _download_lotus_ww3_file(self, key: str, local_file_path: str):
        """
        Downloads a Lotus-WW3 file from the given S3 Bucket.

        Args:
            key:             The key object in the S3 Bucket to download.
            local_file_path: The local file path to download the S3 object to.
        """
        try:
            os.makedirs(os.path.dirname(local_file_path), exist_ok=True)
            self.client.download_file(self.bucket, key, local_file_path)
            logger.info(
                f'Downloaded s3://{self.bucket}/{key} to {local_file_path}'
            )
        except ClientError as e:
            logger.error(
                f'Failed to download s3://{self.bucket}/{key} to '
                f'{local_file_path}!'
            )
            raise e

    def _generate_forecast_hour_ranges(
        self, current_run: int, backfill_runs: List[int]
    ) -> Dict[int, Tuple[int, int]]:
        """
        Generates forecast hour ranges for each run. The ranges correspond to
        the exact hours needed to fill data for the past 24 hours.
        Ex: Current run: 2021032212.
            Prior run: 2021032206. Hours: [0,6)
            Prior run: 2021032106. Hours: [6,12)

        Args:
            current_run: The key object in the S3 Bucket to download.
            backfill_runs: The backfill runs used to get the
                           last 24 hours of data.

        Returns:
            A dictionary of runs and the appropriate forecast hour ranges.
        """
        run_datetime = datetime.strptime(str(current_run), RUN_FORMAT)
        run_24_hour_before = datetime.strptime(
            str(current_run), RUN_FORMAT
        ) - timedelta(hours=24)
        run_54_hour_before = int(
            (run_datetime - timedelta(hours=54)).strftime(RUN_FORMAT)
        )
        all_backfill_runs = backfill_runs + [run_54_hour_before]
        forecast_hour_ranges = {
            run: (0, 6)
            if run_datetime - datetime.strptime(str(run), RUN_FORMAT)
            <= timedelta(hours=24)
            # For runs *more* than 24 hours behind the current run,
            # calculate the forecast hour ranges based on the
            # run *exactly* 24 hours from the current run.
            # Ex: Current run: 2021032212. Run 24 hour before: 2021032112
            #     Range for run 36 hour before: [12, 18)
            else (
                int(
                    (
                        run_24_hour_before
                        - datetime.strptime(str(run), RUN_FORMAT)
                    ).total_seconds()
                    / 3600
                ),
                int(
                    (
                        run_24_hour_before
                        - datetime.strptime(
                            str(all_backfill_runs[i + 1]), RUN_FORMAT
                        )
                    ).total_seconds()
                    / 3600
                ),
            )
            for i, run in enumerate(all_backfill_runs)
            if i < len(all_backfill_runs) - 1
        }
        return forecast_hour_ranges

    def _check_grid_available(
        self,
        grid: str,
        run: int,
        forecast_hour_ranges: Dict[int, Tuple[int, int]],
        prefix: str,
    ) -> bool:
        """
        Checks if a grid has all the files needed.

        Args:
            current_run: The current run to process
            grids: The local file path to download the S3 object to.
            forecast_hour_ranges: The exact forecast hours needed for each run.
            prefix: he key object in the S3 Bucket to download

        Returns:
            True if all files are available for the grid. False otherwise.
        """
        response = self.client.list_objects_v2(
            Bucket=self.bucket, Prefix=f'{prefix}/{grid}/{run}',
        )
        if 'Contents' not in response:
            return False

        available_file_hours = sorted(
            {
                int(object['Key'][-6:-3])
                for object in response['Contents']
                if LOTUS_WW3_REGEX.search(object['Key'])
            }
        )
        forecast_hours_for_run = set(range(*forecast_hour_ranges[run]))

        if not forecast_hours_for_run.issubset(available_file_hours):
            logger.error(
                f'Found {len(available_file_hours)} file hours for {grid}.'
                f'Expected {forecast_hours_for_run}. file hours. '
                f'For run: {run}'
            )
            return False
        return True

    def _get_backfill_runs(
        self, current_run: int, grids: List[str], prefix: str
    ) -> Dict[int, List[int]]:
        """
        Gets backfill runs needed to fill the last 24 hours from the current
        run. Runs from the past 48 hours may be used if there is a prior run
        missing.

        Args:
            current_run: The current run to process.
            grids: The grids associated with the run.
            prefix: The key object in the S3 Bucket to download.

        Returns:
            A dict of runs and the associated hours to use for each run.
        """
        backfill_runs = get_relevant_previous_runs(current_run)
        forecast_hour_ranges = self._generate_forecast_hour_ranges(
            current_run, backfill_runs
        )
        backfill_files_to_download = {}
        for i, run in enumerate(backfill_runs):
            if all(
                self._check_grid_available(
                    grid, run, forecast_hour_ranges, prefix
                )
                for grid in grids
            ):
                backfill_files_to_download[run] = list(
                    range(*forecast_hour_ranges[run])
                )
            else:
                if i == len(backfill_runs) - 1:
                    raise MissingFilesException(
                        f'Insufficient number of backfill files '
                        f'to complete the run.'
                    )
                current_upper_forecast_hour = forecast_hour_ranges[run][1]
                next_run = backfill_runs[i + 1]
                next_lower_forecast_hour = forecast_hour_ranges[next_run][0]
                # Increase the upper forecast hour of next run
                # (ex. [0, 6) -> [0, 12))
                # to account for the current run missing files.
                forecast_hour_ranges[next_run] = (
                    next_lower_forecast_hour,
                    current_upper_forecast_hour + 6,
                )
                logger.error(f'Files not found for run: {run}.')
                continue

            # Stop once we get 24 hours of data filled.
            if (
                len(
                    [
                        hour
                        for run_hours in backfill_files_to_download.values()
                        for hour in run_hours
                    ]
                )
                == EXPECTED_FILL_DATA_HOURS
            ):
                logger.info('Found all backfill files needed.')
                break

        if not backfill_files_to_download:
            raise MissingFilesException(
                'Insufficient number of backfill files to complete the run.'
            )

        logger.info(
            f'Found the following backfill files to download: '
            f'{backfill_files_to_download}'
        )

        return backfill_files_to_download

    def get_runs_to_download(
        self, current_run: int, grids: List[str], prefix: str
    ):
        """
        Gets all of the runs needed to complete processing.
        Data from the previous 24 hours of data is needed.

        Args:
            current_run: The current run to process.
            grids: The grids associated with the run.
            prefix: The key object in the S3 Bucket to download.

        Returns:
            A dict of runs and the associated hours to use for each run.
        """
        backfill_runs = self._get_backfill_runs(current_run, grids, prefix)
        backfill_runs[current_run] = list(range(385))
        return backfill_runs
