from lib.helpers import (
    get_forecast_hour_from_file,
    get_grid_from_file,
    get_run_from_file,
)


def test_get_forecast_hour_from_file():
    file = 'ww3_lotus_AUS_E_3m.2020022106.000.nc'
    assert get_forecast_hour_from_file(file) == 0
    file = 'tmp/tmpd23a/ww3_lotus_AUS_E_3m.2020022106.045.nc'
    assert get_forecast_hour_from_file(file) == 45


def test_get_grid_from_file():
    file = 'ww3_lotus_UK_3m.2020022106.000.nc'
    assert get_grid_from_file(file) == 'UK_3m'
    file = 'tmp/tmpd23a/ww3_lotus_UK_3m.2020022106.045.nc'
    assert get_grid_from_file(file) == 'UK_3m'


def test_get_run_from_file():
    file = 'ww3_lotus_AUS_E_3m.2020022106.000.nc'
    assert get_run_from_file(file) == 2020022106
    file = 'tmp/AUS_E_3m/2020022106/ww3_lotus_AUS_E_3m.2020022106.120.nc'
    assert get_run_from_file(file) == 2020022106
