from typing import List

import boto3  # type: ignore
import pytest  # type: ignore
from moto import mock_s3  # type: ignore

from lib.s3_client import MissingFilesException, S3Client

SCIENCE_KEY_PREFIX = 'lotus/archive/grids'

GRIDS = ['CAL_3m', 'UK_3m']

CURRENT_RUN = 2021032218

SCIENCE_BUCKET = 'surfline-science-s3-dev'


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3', region_name='us-west-1')
        s3_client.create_bucket(Bucket=SCIENCE_BUCKET)
        yield s3_client


def put_items_in_bucket(
    s3_client, grids: List[str], runs: List[int], forecast_hours: int = 385,
):
    for num in list(range(forecast_hours)):
        for run in runs:
            for grid in grids:
                s3_client.put_object(
                    Bucket=SCIENCE_BUCKET,
                    Key=(
                        f'{SCIENCE_KEY_PREFIX}/{grid}/{run}/'
                        f'ww3_lotus_{grid}.{run}.{str(num).zfill(3)}.nc'
                    ),
                )


def test_get_backfill_runs_all_last_24_hours_available(create_s3_bucket):
    """
    2021032212,  # AVAILABLE. Use hours [0, 6).
    2021032206,  # AVAILABLE. Use hours [0, 6).
    2021032200,  # AVAILABLE. Use hours [0, 6).
    2021032118,  # AVAILABLE. Use hours [0, 6).
    2021032112,  # AVAILABLE.
    2021032106,  # AVAILABLE.
    2021032100,  # AVAILABLE.
    2021032018,  # AVAILABLE.
    """
    CURRENT_RUN = 2021032218
    runs = [
        2021032212,
        2021032206,
        2021032200,
        2021032118,
        2021032112,
        2021032106,
        2021032100,
        2021032118,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    files = S3Client(SCIENCE_BUCKET)._get_backfill_runs(
        CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
    )
    assert files == {
        2021032212: [0, 1, 2, 3, 4, 5],
        2021032206: [0, 1, 2, 3, 4, 5],
        2021032200: [0, 1, 2, 3, 4, 5],
        2021032118: [0, 1, 2, 3, 4, 5],
    }


def test_get_backfill_files_only_middle_runs_available(create_s3_bucket):
    """
    2021032212,  # NOT AVAILABLE.
    2021032206,  # AVAILABLE. Use hours [0, 12).
    2021032200,  # AVAILABLE. Use hours [0, 6).
    2021032118,  # NOT AVAILABLE.
    2021032112,  # AVAILABLE. Use hours [6, 12).
    2021032106,  # NOT AVAILABLE.
    2021032100,  # NOT AVAILABLE.
    2021032018,  # NOT AVAILABLE.
    """
    runs = [
        2021032206,
        2021032200,
        2021032112,
        2021032106,
        2021032100,
        2021032018,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    files = S3Client(SCIENCE_BUCKET)._get_backfill_runs(
        CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
    )
    assert files == {
        2021032206: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        2021032200: [0, 1, 2, 3, 4, 5],
        2021032112: [6, 7, 8, 9, 10, 11],
    }


def test_get_backfill_files_only_files_after_24_hours(create_s3_bucket):
    """
    2021032212,  # NOT AVAILABLE.
    2021032206,  # NOT AVAILABLE.
    2021032200,  # NOT AVAILABLE.
    2021032118,  # NOT AVAILABLE.
    2021032112,  # NOT AVAILABLE.
    2021032106,  # AVAILABLE. Use hours [12, 36).
    2021032100,  # NOT AVAILABLE.
    2021032018,  # NOT AVAILABLE.
    """
    runs = [
        2021032106,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    files = S3Client(SCIENCE_BUCKET)._get_backfill_runs(
        CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
    )
    assert files == {
        2021032106: [
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
        ]
    }


def test_get_backfill_files_only_partial_runs_available(create_s3_bucket):
    CURRENT_RUN = 2021032218
    """
    2021032212,  # NOT AVAILABLE. Only hours 0-4.
    2021032206,  # NOT AVAILABLE. Only hours 0-4.
    2021032200,  # NOT AVAILABLE. Only hours 0-4.
    2021032118,  # NOT AVAILABLE. Only hours 0-4.
    2021032112,  # NOT AVAILABLE. Only hours 0-4.
    2021032106,  # NOT AVAILABLE. Only hours 0-4.
    2021032100,  # NOT AVAILABLE. Only hours 0-4.
    2021032018,  # NOT AVAILABLE. Only hours 0-4.
    """
    runs = [
        2021032212,
        2021032206,
        2021032200,
        2021032118,
        2021032112,
        2021032106,
        2021032100,
        2021032018,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs, 4)
    with pytest.raises(MissingFilesException) as error:
        S3Client(SCIENCE_BUCKET)._get_backfill_runs(
            CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
        )
        assert (
            'Insufficient number of backfill files to complete the run.'
            in str(error.value)
        )


def test_get_backfill_files_single_run_only_has_some_files(create_s3_bucket):
    CURRENT_RUN = 2021032218
    """
    2021032212,  # AVAILABLE. Use hours [0, 6).
    2021032206,  # AVAILABLE. Use hours [0, 6).
    2021032200,  # NOT AVAILABLE. Only hours 0-2.
    2021032118,  # AVAILABLE. Use hours [0, 12).
    2021032112,  # AVAILABLE.
    2021032106,  # AVAILABLE.
    2021032100,  # AVAILABLE.
    2021032018,  # AVAILABLE.
    """
    runs = [
        2021032212,
        2021032206,
        2021032118,
        2021032112,
        2021032106,
        2021032100,
        2021032018,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    put_items_in_bucket(create_s3_bucket, GRIDS, [2021032200], 2)
    files = S3Client(SCIENCE_BUCKET)._get_backfill_runs(
        CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
    )
    assert files == {
        2021032212: [0, 1, 2, 3, 4, 5],
        2021032206: [0, 1, 2, 3, 4, 5],
        2021032118: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
    }


def test_get_backfill_files_48_hours_prior_only_file_available(
    create_s3_bucket,
):
    CURRENT_RUN = 2021032218
    """
    2021032212,  # NOT AVAILABLE.
    2021032206,  # NOT AVAILABLE.
    2021032200,  # NOT AVAILABLE.
    2021032118,  # NOT AVAILABLE.
    2021032112,  # NOT AVAILABLE.
    2021032106,  # NOT AVAILABLE.
    2021032100,  # NOT AVAILABLE.
    2021032018,  # AVAILABLE.
    """
    runs = [
        2021032018,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    files = S3Client(SCIENCE_BUCKET)._get_backfill_runs(
        CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
    )
    assert files == {
        2021032018: [
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
            36,
            37,
            38,
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            46,
            47,
        ]
    }


def test_get_backfill_files_last_24_hours_only_single_run_available(
    create_s3_bucket,
):
    CURRENT_RUN = 2021032218
    """
    2021032212,  # NOT AVAILABLE.
    2021032206,  # AVAILABLE. However the past 12 hours are needed as well.
                   This is the only run available
                   and it is insufficient to supplying the data needed.
    2021032200,  # NOT AVAILABLE.
    2021032118,  # NOT AVAILABLE.
    2021032112,  # NOT AVAILABLE.
    2021032106,  # NOT AVAILABLE.
    2021032100,  # NOT AVAILABLE.
    2021032018,  # NOT AVAILABLE.
    """
    runs = [
        2021032206,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    with pytest.raises(MissingFilesException) as error:
        S3Client(SCIENCE_BUCKET)._get_backfill_runs(
            CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
        )
        assert (
            'Insufficient number of backfill files to complete the run.'
            in str(error.value)
        )


def test_get_backfill_files_only_available_run_54_hours_before(
    create_s3_bucket,
):
    CURRENT_RUN = 2021032218
    """
    2021032212,  # NOT AVAILABLE.
    2021032206,  # NOT AVAILABLE.
    2021032200,  # NOT AVAILABLE.
    2021032118,  # NOT AVAILABLE.
    2021032112,  # NOT AVAILABLE.
    2021032106,  # NOT AVAILABLE.
    2021032100,  # NOT AVAILABLE.
    2021032018,  # NOT AVAILABLE.
    # DO NOT USE THE RUN BELOW.
    # This is 54 hours from the current run.
    # 48 hours from the current run is the cutoff.
    2021032012   # AVAILABLE
    """
    runs = [
        2021032012,
    ]
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    with pytest.raises(MissingFilesException) as error:
        S3Client(SCIENCE_BUCKET)._get_backfill_runs(
            CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
        )
        assert (
            'Insufficient number of backfill files to complete the run.'
            in str(error.value)
        )


def test_get_backfill_files_grid_missing(create_s3_bucket):
    runs = [
        2021032012,
    ]
    put_items_in_bucket(create_s3_bucket, ['CAL_3m'], runs)
    with pytest.raises(MissingFilesException) as error:
        S3Client(SCIENCE_BUCKET)._get_backfill_runs(
            CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
        )
        assert (
            'Insufficient number of backfill files to complete the run.'
            in str(error.value)
        )


def test_get_backfill_files_no_files_at_all(create_s3_bucket):
    runs = []  # type: ignore
    put_items_in_bucket(create_s3_bucket, GRIDS, runs)
    with pytest.raises(MissingFilesException) as error:
        S3Client(SCIENCE_BUCKET)._get_backfill_runs(
            CURRENT_RUN, GRIDS, SCIENCE_KEY_PREFIX
        )
        assert (
            'Insufficient number of backfill files to complete the run.'
            in str(error.value)
        )


def test_check_grid_available(create_s3_bucket):
    run = 2021032212
    put_items_in_bucket(create_s3_bucket, GRIDS, [run])
    forecast_hour_ranges = {run: (0, 6)}
    assert S3Client(SCIENCE_BUCKET)._check_grid_available(
        'CAL_3m', run, forecast_hour_ranges, SCIENCE_KEY_PREFIX
    )


def test_check_grid_not_available(create_s3_bucket):
    run = 2021032212
    forecast_hour_ranges = {run: (0, 6)}
    # No files in S3 for the run.
    assert not S3Client(SCIENCE_BUCKET)._check_grid_available(
        'CAL_3m', run, forecast_hour_ranges, SCIENCE_KEY_PREFIX
    )
    # Only 2 forecast hours available. 6 are needed.
    put_items_in_bucket(create_s3_bucket, GRIDS, [run], 2)
    assert not S3Client(SCIENCE_BUCKET)._check_grid_available(
        'CAL_3m', run, forecast_hour_ranges, SCIENCE_KEY_PREFIX
    )


def test_generate_forecast_hour_ranges(create_s3_bucket):
    CURRENT_RUN = 2021032218
    runs = [
        2021032212,
        2021032206,
        2021032200,
        2021032118,
        2021032112,
        2021032106,
        2021032100,
        2021032018,
    ]
    ranges = S3Client(SCIENCE_BUCKET)._generate_forecast_hour_ranges(
        CURRENT_RUN, runs
    )
    assert ranges == {
        2021032212: (0, 6),
        2021032206: (0, 6),
        2021032200: (0, 6),
        2021032118: (0, 6),
        2021032112: (6, 12),
        2021032106: (12, 18),
        2021032100: (18, 24),
        2021032018: (24, 30),
    }
