from typing import Dict, List

from lib.run_helpers import (
    create_filename,
    get_lotus_ww3_files,
    get_relevant_previous_runs,
)

GRIDS = ['AUS_E_3m', 'CAL_3m']


def test_create_filenames():
    run = 2020021000
    hour = 384
    grid = 'UK_3m'
    assert (
        create_filename(run, grid, hour)
        == f'ww3_lotus_{grid}.{run}.{str(hour).zfill(3)}.nc'
    )


def test_get_relevant_previous_runs():
    current_run = 2020021000
    runs_to_download = [
        2020020918,
        2020020912,
        2020020906,
        2020020900,
        2020020818,
        2020020812,
        2020020806,
        2020020800,
    ]
    assert get_relevant_previous_runs(current_run) == runs_to_download


def test_get_lotus_ww3_local_files():
    def create_files_with_prefix(
        prefix: str, current_run: int, runs_to_download: Dict[int, List[int]]
    ):
        total_run_files = list()
        for run, hours in runs_to_download.items():
            for grid in GRIDS:
                files = [
                    f'{prefix}/{grid}/{run}/'
                    f'ww3_lotus_{grid}.{run}.{str(forecast_hour).zfill(3)}.nc'
                    for forecast_hour in hours
                ]
                total_run_files.extend(files)
        return total_run_files

    current_run = 2020021000
    runs_to_download = {
        2020021000: list(range(385)),
        2020020918: list(range(6)),
        2020020912: list(range(6)),
        2020020906: list(range(6)),
        2020020900: list(range(6)),
    }
    temp_dir = 'tmp'
    files = create_files_with_prefix(temp_dir, current_run, runs_to_download)
    assert (
        get_lotus_ww3_files(temp_dir, GRIDS, runs_to_download, current_run)
        == files
    )
    science_key_prefix = 'lotus/archive/grids'
    files = create_files_with_prefix(
        science_key_prefix, current_run, runs_to_download
    )
    assert (
        get_lotus_ww3_files(
            science_key_prefix, GRIDS, runs_to_download, current_run
        )
        == files
    )
