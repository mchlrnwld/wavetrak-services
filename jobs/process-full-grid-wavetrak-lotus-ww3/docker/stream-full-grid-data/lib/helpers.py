import logging
import re
from datetime import datetime, timedelta

RUN_FORMAT = '%Y%m%d%H'

HOUR_REGEX = re.compile(rf'ww3_lotus_.+\d{{10}}\.(\d+)\.nc$')

GRID_REGEX = re.compile(rf'ww3_lotus_(.*).\d{{10}}\.\d{{3}}\.nc$')

RUN_REGEX = re.compile(r'\d{10}')

logger = logging.getLogger('insert-point-of-interest-data')


def get_forecast_hour_from_file(file: str) -> int:
    """
    Extracts the forecast hour (ex. 1, 30) from the given filename.

    Args:
        file: The Lotus-WW3 file to check.

    Returns:
        The forecast hour as an int if the filename matches,
        NoneType otherwise.
        Example: 120
                 from file:
                 ww3_lotus_AUS_E_3m.2020022106.120.nc
    """
    result = HOUR_REGEX.search(file)
    if not result:
        logger.error(f'Invalid file format: {file}')
        raise AttributeError(f'Invalid file format: {file}')
    return int(result.group(1))


def get_run_from_file(file: str) -> int:
    """
    Extracts the run (ex. 2020022106) from the given filename.

    Args:
        file: The lotus-ww3 file to check.

    Returns:
        The run as an int if the file matches,
        NoneType otherwise.
        Example: 2020022106
                 from file:
                 ww3_lotus_AUS_E_3m.2020022106.120.nc
    """
    result = RUN_REGEX.search(file)
    if not result:
        logger.error(f'Invalid file format: {file}')
        raise AttributeError(f'Invalid file format: {file}')
    return int(result.group(0))


def get_grid_from_file(file: str) -> str:
    """
    Extracts the grid (ex. AUS_E_3m) from the given filename.

    Args:
        file: The Lotus-WW3 file to check.

    Returns:
        The grid as string if the filename matches,
        NoneType otherwise.
        Example: AUS_E_3m
                 from file:
                 ww3_lotus_AUS_E_3m.2020022106.120.nc
    """
    result = GRID_REGEX.search(file)
    if not result:
        logger.error(f'Invalid file format: {file}')
        raise AttributeError(f'Invalid file format: {file}')
    return result.group(1)


def get_forecast_hour_timestamp(run: int, forecast_hour: int) -> str:
    """
    Gets a formatted timestamp from a given run and forecast_hour

    Args:
        run:           The run the forecast_hour is associated with.
        forecast_hour: The forecast_hour (ex. 6, 24)

    Returns:
        A formatted timestamp of type string calculated from
        the run and forecast_hour.
        Example: run: 2020022106, forecast_hour: 42
                 Timestamp:
                 2020-02-21T06:00:00Z
    """
    run_dt = datetime.strptime(str(run), RUN_FORMAT)
    forecast_dt = run_dt + timedelta(hours=forecast_hour)
    return f'{forecast_dt.isoformat()}Z'
