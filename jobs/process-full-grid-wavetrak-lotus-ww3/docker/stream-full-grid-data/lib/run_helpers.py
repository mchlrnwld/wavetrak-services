from datetime import datetime, timedelta
from typing import Dict, List

RUN_FORMAT = '%Y%m%d%H'


def create_filename(run: int, grid: str, hour: int) -> str:
    """
    Creates a Lotus-WW3 filename for the given run, grid, and hour.

    Args:
        run: The run that is associated with the filename.
        grid: The grid associated with the Lotus-WW3 file
              (ex. AUS_E_3m, CAL_3m).
        hour: The hour associated with the Lotus-WW3 file (ex. 1, 120).

    Returns:
        The Lotus-WW3 filename.
        Example:
        'ww3_lotus_AUS_E_3m.2020022106.120.nc'
    """
    return f'ww3_lotus_{grid}.{run}.{str(hour).zfill(3)}.nc'


def get_relevant_previous_runs(current_run: int) -> List[int]:
    """
    Gets a list of the previous 48 hours
    of runs from the current run.

    Args:
        current_run: The current run.

    Returns:
        A list of the previous runs.
        Example:
        ['2020021000', '2020020918']
    """
    run_dt = datetime.strptime(str(current_run), RUN_FORMAT)
    previous_runs = [
        int((run_dt - timedelta(hours=6 * i)).strftime(RUN_FORMAT))
        for i in range(1, 9)
    ]
    return previous_runs


def get_lotus_ww3_files(
    prefix: str,
    grids: List[str],
    runs_to_download: Dict[int, List[int]],
    current_run: int,
) -> List[str]:
    """
    Gets a list of Lotus-WW3 files to download.

    Args:
        prefix:             Prefix to store the files.
        grids:              The grids associated
                            with the Lotus-WW3 files (ex. AUS_E_3m).
        runs_to_download:   A list of runs
                            that need to be downloaded
                            Example:
                            ['2020021000', '2020020918']
        current_run:        The current run ready to be
                            processed.

    Returns:
        A list of files
        to be downloaded.
    """
    total_run_files = list()
    for run, hours in runs_to_download.items():
        for grid in grids:
            grid_files = [
                f'{prefix}/{grid}/{run}/{create_filename(run, grid, hour)}'
                for hour in hours
            ]
            total_run_files.extend(grid_files)
    return total_run_files
