import asyncio
import logging
import os
from tempfile import TemporaryDirectory
from timeit import default_timer

import lib.config as config
from lib.helpers import (
    get_forecast_hour_from_file,
    get_forecast_hour_timestamp,
    get_grid_from_file,
    get_run_from_file,
)
from lib.lotus_ww3 import output_lotus_ww3_data, parse_lotus_ww3_file
from lib.run_helpers import get_lotus_ww3_files
from lib.s3_client import S3Client
from lib.sds import SDS

logger = logging.getLogger('stream-full-grid-data')


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    if not os.path.exists(config.DOWNLOAD_DIR):
        logger.info(f'Creating directory: {config.DOWNLOAD_DIR}')
        os.makedirs(config.DOWNLOAD_DIR)

    with TemporaryDirectory(dir=config.DOWNLOAD_DIR) as temp_dir:
        run = int(config.RUN)
        logger.info(f'Indexing S3 files for model run {run}...')

        sds = SDS(config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL)
        grids = sds.get_list_of_grids()
        s3_client = S3Client(config.SCIENCE_BUCKET)

        runs_to_download = s3_client.get_runs_to_download(
            run, grids, config.SCIENCE_KEY_PREFIX
        )

        local_run_files = get_lotus_ww3_files(
            temp_dir, grids, runs_to_download, run
        )
        s3_run_files = get_lotus_ww3_files(
            config.SCIENCE_KEY_PREFIX, grids, runs_to_download, run
        )

        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.download_lotus_ww3_file(
                    s3_file_path, local_file_path
                )
                for s3_file_path, local_file_path in zip(
                    s3_run_files, local_run_files
                )
            ]
        )
        elapsed = default_timer() - start
        logger.info(f'Downloaded {len(s3_run_files)} files in {elapsed}s')

        start = default_timer()
        for lotus_ww3_file in local_run_files:
            lats, lons, lotus_ww3_data = parse_lotus_ww3_file(lotus_ww3_file)
            for lat_index, lat in enumerate(lats):
                for lon_index, lon in enumerate(lons):
                    grid = get_grid_from_file(lotus_ww3_file)
                    forecast_hour = get_forecast_hour_from_file(lotus_ww3_file)
                    file_run = get_run_from_file(lotus_ww3_file)
                    forecast_hour_timestamp = get_forecast_hour_timestamp(
                        file_run, forecast_hour
                    )
                    output_lotus_ww3_data(
                        grid,
                        forecast_hour_timestamp,
                        lat,
                        lat_index,
                        lon,
                        lon_index,
                        lotus_ww3_data,
                    )
            logger.info(f'Processed file: {lotus_ww3_file}')

        elapsed = default_timer() - start
        logger.info(f'Parsed {len(s3_run_files)} files in {elapsed}s')


if __name__ == '__main__':
    asyncio.run(main())
