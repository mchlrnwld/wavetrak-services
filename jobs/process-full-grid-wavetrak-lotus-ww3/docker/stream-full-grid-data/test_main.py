import sys
from io import StringIO
from unittest import mock

import pytest

pytestmark = pytest.mark.asyncio


def mock_get_list_of_grids():
    return ['mock grid name']


def mock_get_runs_to_download(current_run, grids, prefix):
    return {12345: [0, 1]}


def mock_get_lotus_ww3_files(prefix, grids, runs_to_download, current_run):
    return ['fixtures/ww3_lotus_HW_3m.2021111106.000.nc']


async def mock_download_lotus_ww3_file(key, local_file_path):
    pass


async def test_stream_full_grid_data():
    _stdout = sys.stdout
    sys.stdout = _stringio = StringIO()

    with mock.patch(
        'lib.sds.SDS.get_list_of_grids', side_effect=mock_get_list_of_grids,
    ), mock.patch(
        'lib.s3_client.S3Client.get_runs_to_download',
        side_effect=mock_get_runs_to_download,
    ), mock.patch(
        'lib.run_helpers.get_lotus_ww3_files',
        side_effect=mock_get_lotus_ww3_files,
    ), mock.patch(
        'lib.s3_client.S3Client.download_lotus_ww3_file',
        side_effect=mock_download_lotus_ww3_file,
    ):
        from main import main

        await main()

    sys.stdout = _stdout
    output = _stringio.getvalue().splitlines()
    assert len(output) == 14241
    assert (
        output[0] == 'HW_3m,18.0,199.0,2021-11-11T06:00:00Z,0.0,0.0,0.0,'
        '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,'
        '0.0,0.0,0.0,0.0,0.0,0.0'
    )
    assert len(output[0].split(',')) == 25
    assert (
        output[14240]
        == 'HW_3m,23.0,206.0,2021-11-11T06:00:00Z,0.0,0.0,0.0,0.0,0.0,0.0,'
        '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0'
    )
    assert len(output[14240].split(',')) == 25
