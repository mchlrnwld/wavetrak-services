FROM continuumio/miniconda3:4.9.2-alpine

SHELL ["sh", "--login", "-c"]
WORKDIR /opt/app

# NOTE: bash and coreutils are needed to run integration tests.
RUN apk add bash coreutils && conda install -c conda-forge conda-devenv

COPY pip.conf /root/.pip/pip.conf
COPY environment.devenv.yml .
RUN conda devenv && conda uninstall conda-devenv && conda clean -afy

COPY . .
RUN mv bin/swells-writer-linux-amd64 /usr/local/bin/swells-writer

RUN conda activate process-full-grid-wavetrak-lotus-ww3-stream-full-grid-data && \
    make lint && \
    make type-check && \
    make test

CMD set -xe -o pipefail && \
    conda activate process-full-grid-wavetrak-lotus-ww3-stream-full-grid-data && \
    python main.py | swells-writer
