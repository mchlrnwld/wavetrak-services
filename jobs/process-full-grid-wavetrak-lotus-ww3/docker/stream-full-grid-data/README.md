# Stream Full Grid Data

Docker image to stream full grid Lotus-WW3 records to kinesis.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate process-full-grid-wavetrak-lotus-ww3-stream-full-grid-data
$ cp .env.sample .env
```

## Run Locally

### Parse

```sh
$ env $(xargs < .env) python main.py > /dev/null
```

### Parse and Print to Stdout

```sh
$ env $(xargs < .env) python main.py
```

### Parse and Build Database

```sh
$ env $(xargs < .env) python main.py | env $(xargs < .env) ./swells-writer
```

## Docker

### Build and Run

```sh
$ docker build -t process-full-grid-wavetrak-lotus-ww3/stream-full-grid-data .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data --volume ~/.aws:/root/.aws process-full-grid-wavetrak-lotus-ww3/stream-full-grid-data
```
