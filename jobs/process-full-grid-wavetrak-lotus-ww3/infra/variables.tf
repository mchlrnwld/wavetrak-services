variable "environment" {
  type = string
}

variable "company" {
  type    = string
  default = "wt"
}

variable "application" {
  type    = string
  default = "jobs-stream-full-grid-data-wavetrak-lotus-ww3"
}

variable "container_properties" {
}
