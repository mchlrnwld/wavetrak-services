provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/process-full-grid-wavetrak-lotus-ww3/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_wavetrak_lotus_ww3" {
  source = "../../"

  environment = "prod"
  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/process-full-grid-wavetrak-lotus-ww3/stream-full-grid-data:prod",
    "memory" : 4096,
    "vcpus" : 2,
    "volumes" : [
      {
        "host" : {
          "sourcePath" : "/mnt/ephemeral/airflow"
        },
        "name" : "tmp"
      }
    ],
    "mountPoints" : [
      {
        "sourceVolume" : "tmp",
        "containerPath" : "/mnt/ephemeral/airflow",
        "readOnly" : false
      }
    ]
  }
}
