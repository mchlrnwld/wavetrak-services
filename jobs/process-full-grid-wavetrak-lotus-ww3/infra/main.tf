data "aws_kinesis_stream" "smdb_stream" {
  name = "wt-science-models-db-${var.environment}"
}

resource "aws_iam_policy" "batch_job_kinesis_policy" {
  name = "${var.company}-${var.application}-batch-job-kinesis-policy-${var.environment}"
  policy = templatefile("${path.module}/resources/kinesis-policy.json", {
    kinesis_stream = data.aws_kinesis_stream.smdb_stream.arn
  })
}

resource "aws_iam_role_policy_attachment" "batch_job_kinesis_policy_attachment" {
  role       = module.stream_full_grid_data_wavetrak_lotus_ww3.batch_job_iam_role_name
  policy_arn = aws_iam_policy.batch_job_kinesis_policy.arn
}

module "stream_full_grid_data_wavetrak_lotus_ww3" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company              = var.company
  application          = var.application
  environment          = var.environment
  container_properties = var.container_properties
}
