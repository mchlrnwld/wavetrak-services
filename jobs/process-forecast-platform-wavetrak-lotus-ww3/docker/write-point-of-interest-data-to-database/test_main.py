import os
from sys import platform
from unittest import mock

import psycopg2  # type: ignore
import pytest
from pytest_postgresql import factories  # type: ignore

from fixtures.output import surf_output, swells_output  # type: ignore

pytestmark = pytest.mark.asyncio


def load_database(**kwargs):
    with psycopg2.connect(**kwargs) as db_connection:
        with db_connection.cursor() as cur:
            cur.execute(
                "INSERT INTO points_of_interest VALUES"
                "('9dc97136-2906-4d9f-9545-33ce01258706', 'POINT1', -32.05,"
                "115.45);"
            )
            cur.execute(
                "INSERT INTO points_of_interest VALUES"
                "('aa4655a5-e83a-434c-9f35-7108c9bb6b1e', 'POINT2', -32.0,"
                "115.7);"
            )
            cur.execute(
                "INSERT INTO points_of_interest VALUES"
                "('b9a956d9-22d1-4b66-b09c-7baa6d71e551', 'POINT3', -33.55,"
                "114.95);"
            )
            cur.execute("INSERT INTO models VALUES('Wavetrak', 'Lotus-WW3');")
            cur.execute(
                "INSERT INTO model_grids VALUES('Wavetrak', 'Lotus-WW3', "
                "'AUS_SW_3m');"
            )
            cur.execute(
                "INSERT INTO model_runs_point_of_interest VALUES('Wavetrak', "
                "'Lotus-WW3', '2021-11-17 06:00:00');"
            )
            db_connection.commit()


postgresql_fixture = None

if platform == 'linux':
    postgresql_in_docker = factories.postgresql_noproc(
        host='database',
        dbname='mytestdb',
        load=[
            *[
                f'migrations/{file}'
                for file in sorted(os.listdir('migrations'))
            ],
            load_database,
        ],
    )
    postgresql_fixture = factories.postgresql(
        'postgresql_in_docker', dbname='mytestdb'
    )
elif platform == 'darwin':
    postgresql_proc = factories.postgresql_proc(
        port=8888,
        dbname='mytestdb',
        load=[
            *[
                f'migrations/{file}'
                for file in sorted(os.listdir('migrations'))
            ],
            load_database,
        ],
    )

    postgresql_fixture = factories.postgresql(
        'postgresql_proc', dbname='mytestdb'
    )


async def test_data_added_to_surf_and_swells_tables(postgresql_fixture):
    psql_parameters = postgresql_fixture.get_dsn_parameters()
    PSQL_CONNECTION_URI = (
        f"postgresql://{psql_parameters['user']}@"
        f"{psql_parameters['host']}:{psql_parameters['port']}/"
        f"{psql_parameters['dbname']}"
    )
    os.environ['SCIENCE_PLATFORM_PSQL_JOB'] = PSQL_CONNECTION_URI

    with mock.patch(
        'lib.s3_client.S3Client.download_all_files',
        return_value=[
            'fixtures/202111706/AUS_SW_3m/0/surf.csv',
            'fixtures/202111706/AUS_SW_3m/0/swells.csv',
        ],
    ):
        from main import main

        cur = postgresql_fixture.cursor()
        cur.execute("SELECT COUNT(*) FROM swells;")
        swell_data_count = cur.fetchall()
        # Expect no swell data added before main function is invoked.
        assert swell_data_count[0][0] == 0

        cur.execute("SELECT COUNT(*) FROM swells;")
        surf_data_count = cur.fetchall()
        # Expect no surf data added before main function is invoked.
        assert surf_data_count[0][0] == 0

        await main()

        cur.execute("SELECT COUNT(*) FROM swells;")
        swell_data_count = cur.fetchall()
        assert swell_data_count[0][0] == 2

        cur.execute("SELECT COUNT(*) FROM surf;")
        surf_data_count = cur.fetchall()
        assert surf_data_count[0][0] == 3

        cur.execute(
            (
                "SELECT point_of_interest_id,agency,model,grid,latitude,"
                "longitude,run,forecast_time,height,period,direction,"
                "swell_wave_1_height,swell_wave_1_period,"
                "swell_wave_1_direction,swell_wave_1_spread,"
                "swell_wave_2_height,swell_wave_2_period,"
                "swell_wave_2_direction,swell_wave_2_spread,"
                "swell_wave_3_height,swell_wave_3_period,"
                "swell_wave_3_direction,swell_wave_3_spread,"
                "swell_wave_4_height,swell_wave_4_period,"
                "swell_wave_4_direction,swell_wave_4_spread,"
                "swell_wave_5_height,swell_wave_5_period,"
                "swell_wave_5_direction,swell_wave_5_spread,wind_wave_height,"
                "wind_wave_period,wind_wave_direction,wind_wave_spread,"
                "swell_wave_1_impact,swell_wave_2_impact,swell_wave_3_impact,"
                "swell_wave_4_impact,swell_wave_5_impact,wind_wave_impact,"
                "spectra1d_energy,spectra1d_direction,"
                "spectra1d_directional_spread FROM swells;"
            )
        )
        swells_data = cur.fetchall()
        for swell_data, swell_output_data in zip(swells_data, swells_output):
            assert swell_data == swell_output_data

        cur.execute(
            (
                "SELECT point_of_interest_id,agency,model,grid,latitude,"
                "longitude,run,forecast_time, breaking_wave_height_min,"
                "breaking_wave_height_max,"
                "breaking_wave_height_algorithm FROM surf;"
            )
        )
        surf_data = cur.fetchall()
        for surf_data, surf_output_data in zip(surf_data, surf_output):
            assert surf_data == surf_output_data
