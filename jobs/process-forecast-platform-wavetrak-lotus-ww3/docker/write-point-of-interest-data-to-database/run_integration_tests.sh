#!/usr/bin/env bash

./wait-for-it.sh database:5432

source activate process-forecast-platform-wavetrak-lotus-ww3-write-point-of-interest-data-to-database && pytest -s --cov-report term-missing --cov=main test_main.py
