import asyncio
import logging
import os
from functools import partial
from typing import List

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type:ignore

logger = logging.getLogger('write-point-of-interest-data-to-database')


class S3Client:
    """
    S3Client to interface with S3 and download files asynchronously.

    Attributes:
        client: boto3 client for S3.
    """

    def __init__(self):
        self.client = boto3.client('s3')

    async def download_all_files(
        self, bucket: str, key_prefix: str, local_file_dir
    ) -> List[str]:
        """
        Asynchronously download all files in a key prefix from S3.

        Args:
            bucket: S3 bucket to download from.
            key_prefix: S3 object key_prefix to download all files from.
            local_file_path: Local path to download file to.
        """
        paginator = self.client.get_paginator('list_objects_v2')
        object_keys = [
            item['Key']
            for page in paginator.paginate(Bucket=bucket, Prefix=key_prefix)
            for item in page['Contents']
        ]
        local_file_paths = [
            key.replace(key_prefix, local_file_dir) for key in object_keys
        ]

        os.makedirs(local_file_dir, exist_ok=True)
        loop = asyncio.get_running_loop()
        await asyncio.gather(
            *[
                loop.run_in_executor(
                    None,
                    partial(self._download_file, bucket, key, local_file_path),
                )
                for key, local_file_path in zip(object_keys, local_file_paths)
            ]
        )

        return local_file_paths

    def _download_file(self, bucket: str, key: str, local_file_path: str):
        try:
            os.makedirs(os.path.dirname(local_file_path), exist_ok=True)
            self.client.download_file(bucket, key, local_file_path)
            logger.info(
                f'Downloaded s3://{bucket}/{key} to {local_file_path}.'
            )
        except ClientError as e:
            logger.error(
                f'Failed to download s3://{bucket}/{key} to {local_file_path}!'
            )
            raise e
