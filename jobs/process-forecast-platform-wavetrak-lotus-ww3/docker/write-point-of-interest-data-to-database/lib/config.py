import os
from distutils.util import strtobool

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
JOB_NAME = os.environ['JOB_NAME']
SECRETS_PREFIX = f'{ENV}/common/'
DATA_DIR = os.getenv('AIRFLOW_TMP_DIR', 'data')
RUN = int(os.environ['RUN'])
COMMIT = bool(strtobool(os.getenv('COMMIT', 'false')))

JOBS_BUCKET = get_secret(SECRETS_PREFIX, 'JOBS_BUCKET')
JOBS_KEY_PREFIX = get_secret(SECRETS_PREFIX, 'JOBS_KEY_PREFIX')
SCIENCE_PLATFORM_PSQL_JOB = get_secret(
    SECRETS_PREFIX, 'SCIENCE_PLATFORM_PSQL_JOB'
)
