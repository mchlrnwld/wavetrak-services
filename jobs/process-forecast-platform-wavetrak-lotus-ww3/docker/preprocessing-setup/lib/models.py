from typing import List, Optional


class PointOfInterestGridPoint:
    """
    Point of interest and its grid point.

    Args:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.

    Attributes:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.
        optimal_swell_direction: Optimal swell direction as a float.
        breaking_wave_height_coefficient: Breaking wave multiplier as a float.
        breaking_wave_height_intercept: Breaking wave intercept as float.
        breaking_wave_height_algorithm: Breaking wave algorithms string.
        spectral_refraction_matrix: Spectral refraction matrix
                                    as list of float.
    """

    def __init__(
        self,
        point_of_interest_id: str,
        lon: float,
        lat: float,
        optimal_swell_direction: Optional[float],
        breaking_wave_height_coefficient: Optional[float],
        breaking_wave_height_intercept: Optional[float],
        breaking_wave_height_algorithm: Optional[str],
        spectral_refraction_matrix: Optional[List[List[float]]],
    ):
        self.point_of_interest_id = point_of_interest_id
        if lon < 0:
            self.lon = lon + 360
        else:
            self.lon = lon
        self.lat = lat
        self.optimal_swell_direction = optimal_swell_direction
        self.breaking_wave_height_coefficient = (
            breaking_wave_height_coefficient
        )
        self.breaking_wave_height_intercept = breaking_wave_height_intercept
        self.breaking_wave_height_algorithm = breaking_wave_height_algorithm
        self.spectral_refraction_matrix = spectral_refraction_matrix
