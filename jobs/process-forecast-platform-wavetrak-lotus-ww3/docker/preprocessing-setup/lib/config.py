import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
JOB_NAME = os.environ['JOB_NAME']
SECRETS_PREFIX = f'{ENV}/common/'
DATA_DIR = os.getenv('AIRFLOW_TMP_DIR', 'data')
MODEL_AGENCY = os.environ['MODEL_AGENCY']
MODEL_NAME = os.environ['MODEL_NAME']
RUN = int(os.environ['RUN'])

JOBS_BUCKET = get_secret(SECRETS_PREFIX, 'JOBS_BUCKET')
JOBS_KEY_PREFIX = get_secret(SECRETS_PREFIX, 'JOBS_KEY_PREFIX')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
