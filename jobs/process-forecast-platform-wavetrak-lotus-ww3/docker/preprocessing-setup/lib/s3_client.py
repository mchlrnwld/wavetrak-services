import asyncio
import logging
from functools import partial

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type:ignore

logger = logging.getLogger('process-point-of-interest-data')


class S3Client:
    """
    S3Client to interface with S3 and download/upload files asynchronously.

    Attributes:
        client: boto3 client for S3.
    """

    def __init__(self):
        self.client = boto3.client('s3')

    async def upload_file(self, local_file_path: str, bucket: str, key: str):
        """
        Upload file from S3.

        Args:
            local_file_path: Local path to upload file from.
            bucket: S3 bucket to upload to.
            key: S3 object key to upload to.
        """
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None, partial(self._upload_file, local_file_path, bucket, key)
        )

    def _upload_file(self, local_file_path: str, bucket: str, key: str):
        try:
            self.client.upload_file(local_file_path, bucket, key)
            logger.info(f'Uploaded {local_file_path} to s3://{bucket}/{key}.')
        except ClientError as e:
            logger.error(
                f'Failed to upload {local_file_path} to s3://{bucket}/{key}!'
            )
            raise e
