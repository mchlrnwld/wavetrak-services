from typing import List

import requests

from lib.models import PointOfInterestGridPoint


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.
        grid: The model grid to query.
    """

    def __init__(self, url: str, agency: str, model: str):
        self.url = url
        self.agency = agency
        self.model = model

    def query_points_of_interest(
        self, grid: str
    ) -> List[PointOfInterestGridPoint]:
        """
        Queries the GraphQL endpoint for PointOfInterest data
        from the surf_spot_configurations table in Postgres.

        Returns:
            A list of PointOfInterest objects for
            the given agency, model, and grid.
        """
        query = f'''
        query {{
          models(agency: "{self.agency}", model: "{self.model}") {{
            grids(grid: "{grid}")  {{
              pointsOfInterest {{
                id
                lon
                lat
                surfSpotConfiguration {{
                  optimalSwellDirection
                  breakingWaveHeightCoefficient
                  breakingWaveHeightAlgorithm
                  breakingWaveHeightIntercept
                  spectralRefractionMatrix
                }}
              }}
            }}
          }}
        }}'''
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
        }
        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        return [
            PointOfInterestGridPoint(
                point_of_interest['id'],
                float(point_of_interest['lon']),
                float(point_of_interest['lat']),
                (point_of_interest.get('surfSpotConfiguration') or {}).get(
                    'optimalSwellDirection'
                ),
                (point_of_interest.get('surfSpotConfiguration') or {}).get(
                    'breakingWaveHeightCoefficient'
                ),
                (point_of_interest.get('surfSpotConfiguration') or {}).get(
                    'breakingWaveHeightIntercept'
                ),
                (point_of_interest.get('surfSpotConfiguration') or {}).get(
                    'breakingWaveHeightAlgorithm'
                ),
                (point_of_interest.get('surfSpotConfiguration') or {}).get(
                    'spectralRefractionMatrix'
                ),
            )
            for grid_points_of_interest in response.json()['data']['models'][
                0
            ]['grids']
            for point_of_interest in grid_points_of_interest[
                'pointsOfInterest'
            ]
        ]

    def query_all_grids(self) -> List[str]:
        """
        Queries the GraphQL endpoint for Grid data
        for the given agency and model.

        Returns:
            A list of grids for the given agency and model.
        """
        query = f'''
        query {{
          models(agency: "{self.agency}", model: "{self.model}") {{
            grids {{
              grid
            }}
          }}
        }}'''

        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
        }

        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()

        return [
            grid['grid']
            for grid in response.json()['data']['models'][0]['grids']
        ]
