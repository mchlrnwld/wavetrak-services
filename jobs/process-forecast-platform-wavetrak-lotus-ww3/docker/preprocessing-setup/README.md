# Preprocessing Setup Task

This DAG task uploads POI grid points (returned from SDS) to S3. This ensures any downstream task can download
the grid points from S3 instead of having to query SDS.

## Testing

Ensure Docker is running. Then you can run an integration test with the following:

```sh
make test-integration
```

## Docker

### Build and Run

```sh
docker build -t process-forecast-platform-wavetrak-lotus-ww3/preprocessing-setup .
cp .env.sample .env
docker run --env-file=.env --rm -v ~/.aws:/root/.aws process-forecast-platform-wavetrak-lotus-ww3/preprocessing-setup
```
