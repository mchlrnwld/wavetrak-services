#!/usr/bin/env bash

source activate process-forecast-platform-wavetrak-lotus-ww3-preprocessing-setup && pytest -s --cov-report term-missing --cov=main test_main.py
