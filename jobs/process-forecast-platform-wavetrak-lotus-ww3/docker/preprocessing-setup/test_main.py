import json
from unittest import mock

import boto3  # type: ignore
import moto  # type: ignore
import pytest

from lib.models import PointOfInterestGridPoint

pytestmark = pytest.mark.asyncio


def mock_query_points_of_interest():
    """
    Returns what SDS.query_points_of_interest() would return
    without calling the API. Instead uses a preloaded
    data file to give back POI data.
    """
    with open('points_of_interest_test_data.json', 'r') as poi_json:
        points_of_interest_grid_points = [
            PointOfInterestGridPoint(*dict.values())
            for dict in json.load(poi_json)
        ]

    return points_of_interest_grid_points


def mock_query_all_grids():
    """
    Returns what SDS.query_all_grids() would return
    without calling the API.
    """
    grids = [
        'AUS_E_3m',
        'AUS_SW_3m',
        'AUS_VT_3m',
        'BALI_3m',
        'CAL_3m',
        'FR_3m',
        'GLAKES_6m',
        'GLOB_15m',
        'GLOB_30m',
        'HW_3m',
        'PT_3m',
        'UK_3m',
        'US_E_3m',
    ]

    return grids


async def test_preprocesing_setup():
    """
    Integration test to test the preprocessing setup task. Uses
    moto to mock calls to S3, and unittest.mock to mock API calls.
    """
    with moto.mock_s3(), mock.patch(
        'lib.sds.SDS.query_points_of_interest',
        return_value=mock_query_points_of_interest(),
    ), mock.patch(
        'lib.sds.SDS.query_all_grids', return_value=mock_query_all_grids()
    ):
        # Must import after mocking boto3
        import lib.config as config
        from main import main

        POI_DATA_OBJECT_KEY = (
            f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/{config.RUN}'
        )

        conn = boto3.resource('s3', region_name='us-west-1')
        conn.create_bucket(
            Bucket=config.JOBS_BUCKET,
            CreateBucketConfiguration={'LocationConstraint': 'us-west-1'},
        )

        await main()

        grids = mock_query_all_grids()

        for grid in grids:
            body = (
                conn.Object(
                    config.JOBS_BUCKET,
                    f'{POI_DATA_OBJECT_KEY}/points-of-interest/{grid}.json',
                )
                .get()['Body']
                .read()
                .decode('utf-8')
            )
            points_of_interest_grid_points = json.loads(body)

            assert points_of_interest_grid_points != []
            assert all(
                x
                in [
                    'point_of_interest_id',
                    'lon',
                    'lat',
                    'optimal_swell_direction',
                    'breaking_wave_height_coefficient',
                    'breaking_wave_height_intercept',
                    'breaking_wave_height_algorithm',
                    'spectral_refraction_matrix',
                ]
                for x in points_of_interest_grid_points[0].keys()
            )
