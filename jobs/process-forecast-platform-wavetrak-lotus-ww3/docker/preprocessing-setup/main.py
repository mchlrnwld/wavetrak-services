import asyncio
import json
import logging
import os
from tempfile import TemporaryDirectory
from timeit import default_timer

import lib.config as config
from lib.s3_client import S3Client
from lib.sds import SDS

logger = logging.getLogger('preprocessing-setup')

JOB_RUN_KEY_PREFIX = f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/{config.RUN}'


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    logger.info(f'Starting POI grid point uploads for model run: {config.RUN}')

    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir:
        points_of_interest_local_path = os.path.join(
            temp_dir, 'points-of-interest'
        )
        os.makedirs(points_of_interest_local_path)

        sds_client = SDS(
            config.SCIENCE_DATA_SERVICE, config.MODEL_AGENCY, config.MODEL_NAME
        )

        grids = sds_client.query_all_grids()

        for grid in grids:
            poi_grid_points = sds_client.query_points_of_interest(grid)
            points_of_interest_local_path_with_grid = os.path.join(
                points_of_interest_local_path, f'{grid}.json'
            )

            with open(points_of_interest_local_path_with_grid, 'w') as f:
                json.dump([poi_gp.__dict__ for poi_gp in poi_grid_points], f)

        s3_client = S3Client()

        logger.info('Uploading POI grid point JSON files to S3...')
        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.upload_file(
                    os.path.join(
                        points_of_interest_local_path, f'{grid}.json'
                    ),
                    config.JOBS_BUCKET,
                    f'{JOB_RUN_KEY_PREFIX}/points-of-interest/{grid}.json',
                )
                for grid in grids
            ]
        )
        elapsed = default_timer() - start
        logger.info(
            f'Finished uploading POI grid point jsons to S3 in {elapsed}s.'
        )


if __name__ == '__main__':
    asyncio.run(main())
