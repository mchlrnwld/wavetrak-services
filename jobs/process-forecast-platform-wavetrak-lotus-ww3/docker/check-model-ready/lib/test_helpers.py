import pytest  # type: ignore

from lib.helpers import model_run_from_key

MODEL_RUN = '2019050206'
SCIENCE_KEY_PREFIX = 'lotus/archive/points'


def test_model_run_from_key():
    lotus_ww3_key = (
        f'lotus/archive/points/GLOB_30m/{MODEL_RUN}/'
        f'ww3_msw_GLOB_30m.2019081500.001.nc'
    )
    assert model_run_from_key(SCIENCE_KEY_PREFIX, lotus_ww3_key) == MODEL_RUN
    with pytest.raises(ValueError) as error:
        invalid_key = 'jobs/completed/process-forecast-platform-noaa-gfs'
        model_run_from_key(SCIENCE_KEY_PREFIX, invalid_key)
        assert 'Key: {invalid_key} is of unknown type' in error
