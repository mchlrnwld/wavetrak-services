import unittest.mock as mock

import boto3  # type: ignore
import pytest  # type: ignore
from moto import mock_s3, mock_sqs  # type: ignore

from lib.check_lotus_ww3_ready import (
    check_lotus_ww3_ready,
    check_model_grids_ready,
)
from lib.helpers import PointsFileNotAvailableError

BUCKET = 'test-bucket'
QUEUE_NAME = 'test-queue'
SCIENCE_DATA_SERVICE = 'test-service'
POINT_OF_INTEREST_PREFIX = 'lotus/archive/points'
FULL_GRID_PREFIX = 'lotus/archive/grids'
AGENCY = 'Wavetrak'
MODEL = 'Lotus-WW3'
RUN = 2019050206
EXAMPLE_GRID = 'glo_30m'

GRIDS = ['CAL_3m', 'GLOB_15m', 'GLOB_30m', 'HW_3m', 'US_E_3m']

POINT_NAMES = ['001.500_053.001', '108.500_-08.400', '350.800_054.000']


def mock_get_points_for_grid(point_names):
    def mock(bucket, prefix, grid, run):
        return {
            f'ww3_lotus_{grid}.spot_{lat_lon}.nc' for lat_lon in point_names
        }

    return mock


@pytest.fixture(autouse=True)
def aws_credentials(monkeypatch):
    """
    Mocked AWS Credentials for moto.
    This is a temporary workaround,
    otherwise moto will throw 'NoCredentialsError'.
    """
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3', region_name='us-west-1')
        s3_client.create_bucket(Bucket=BUCKET)
        yield s3_client


@pytest.fixture
def create_sqs_queue():
    with mock_sqs():
        sqs_client = boto3.client('sqs', region_name='us-west-1')
        queue_url = sqs_client.create_queue(
            QueueName=QUEUE_NAME, Attributes={'VisibilityTimeout': '0'}
        )['QueueUrl']
        yield sqs_client, queue_url


def test_check_lotus_ww3_ready(create_sqs_queue, create_s3_bucket):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_list_of_grids', return_value=GRIDS
    ), mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES),
    ):
        with mock.patch(
            'check_model_ready.check_model_run_already_exists',
            return_value=False,
        ):
            s3_client = create_s3_bucket
            for point in POINT_NAMES:
                for grid in GRIDS:
                    key = (
                        f'{POINT_OF_INTEREST_PREFIX}/{grid}/{RUN}'
                        f'/ww3_lotus_{grid}.spot_{point}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)
            for num in list(range(385)):
                for grid in GRIDS:
                    key = (
                        f'{FULL_GRID_PREFIX}/{grid}/{RUN}/'
                        f'ww3_lotus_{grid}.{RUN}.{str(num).zfill(3)}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)
            # Expect true when all spot files exist
            assert check_lotus_ww3_ready(
                BUCKET,
                POINT_OF_INTEREST_PREFIX,
                FULL_GRID_PREFIX,
                SCIENCE_DATA_SERVICE,
                AGENCY,
                MODEL,
                RUN,
            )

        # Expect false when files for all hours for all grids exist in S3 but
        # model run has already been processed.
        with mock.patch(
            'check_model_ready.check_model_run_already_exists',
            return_value=True,
        ):
            assert not check_lotus_ww3_ready(
                BUCKET,
                POINT_OF_INTEREST_PREFIX,
                FULL_GRID_PREFIX,
                SCIENCE_DATA_SERVICE,
                AGENCY,
                MODEL,
                RUN,
            )


def test_check_lotus_ww3_ready_expected_points_less_than_available_points(
    create_sqs_queue, create_s3_bucket
):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_list_of_grids', return_value=GRIDS
    ), mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES[:1]),
    ):
        with mock.patch(
            'check_model_ready.check_model_run_already_exists',
            return_value=False,
        ):
            s3_client = create_s3_bucket
            # There may be more spot files in S3 than POIs in
            # the Science Data Service.
            for point in POINT_NAMES:
                for grid in GRIDS:
                    key = (
                        f'{POINT_OF_INTEREST_PREFIX}/{grid}/{RUN}'
                        f'/ww3_lotus_{grid}.spot_{point}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)
            for num in list(range(385)):
                for grid in GRIDS:
                    key = (
                        f'{FULL_GRID_PREFIX}/{grid}/{RUN}/'
                        f'ww3_lotus_{grid}.{RUN}.{str(num).zfill(3)}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)
            # Expect true when all spot files exist
            assert check_lotus_ww3_ready(
                BUCKET,
                POINT_OF_INTEREST_PREFIX,
                FULL_GRID_PREFIX,
                SCIENCE_DATA_SERVICE,
                AGENCY,
                MODEL,
                RUN,
            )

        # Expect false when files for all hours for all grids exist in S3 but
        # model run has already been processed.
        with mock.patch(
            'check_model_ready.check_model_run_already_exists',
            return_value=True,
        ):
            assert not check_lotus_ww3_ready(
                BUCKET,
                POINT_OF_INTEREST_PREFIX,
                FULL_GRID_PREFIX,
                SCIENCE_DATA_SERVICE,
                AGENCY,
                MODEL,
                RUN,
            )


def test_check_lotus_ww3_ready_full_grid_files_missing(
    create_sqs_queue, create_s3_bucket
):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_list_of_grids', return_value=GRIDS
    ), mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES),
    ):
        with mock.patch(
            'check_model_ready.check_model_run_already_exists',
            return_value=False,
        ):
            s3_client = create_s3_bucket
            for point in POINT_NAMES:
                for grid in GRIDS:
                    key = (
                        f'{POINT_OF_INTEREST_PREFIX}/{grid}/{RUN}'
                        f'/ww3_lotus_{grid}.spot_{point}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)

            # Expect false point of interest files exist
            # but full grid files missing.
            assert not check_lotus_ww3_ready(
                BUCKET,
                POINT_OF_INTEREST_PREFIX,
                FULL_GRID_PREFIX,
                SCIENCE_DATA_SERVICE,
                AGENCY,
                MODEL,
                RUN,
            )


def test_check_lotus_ww3_ready_poi_files_missing(
    create_sqs_queue, create_s3_bucket
):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_list_of_grids', return_value=GRIDS
    ), mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES),
    ):
        with mock.patch(
            'check_model_ready.check_model_run_already_exists',
            return_value=False,
        ):
            s3_client = create_s3_bucket
            for num in list(range(385)):
                for grid in GRIDS:
                    key = (
                        f'{FULL_GRID_PREFIX}/{grid}/{RUN}/'
                        f'ww3_lotus_{grid}.{RUN}.{str(num).zfill(3)}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)

            # Expect false full grid files exist
            # but point of interest files missing.
            assert not check_lotus_ww3_ready(
                BUCKET,
                POINT_OF_INTEREST_PREFIX,
                FULL_GRID_PREFIX,
                SCIENCE_DATA_SERVICE,
                AGENCY,
                MODEL,
                RUN,
            )


def test_check_lotus_ww3_ready_points_dat_file_missing(
    create_sqs_queue, create_s3_bucket
):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_list_of_grids', return_value=GRIDS
    ), mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=PointsFileNotAvailableError,
    ):
        with mock.patch(
            'check_model_ready.check_model_run_already_exists',
            return_value=False,
        ):
            s3_client = create_s3_bucket
            for point in POINT_NAMES:
                for grid in GRIDS:
                    key = (
                        f'{POINT_OF_INTEREST_PREFIX}/{grid}/{RUN}'
                        f'/ww3_lotus_{grid}.spot_{point}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)
            for num in list(range(385)):
                for grid in GRIDS:
                    key = (
                        f'{FULL_GRID_PREFIX}/{grid}/{RUN}/'
                        f'ww3_lotus_{grid}.{RUN}.{str(num).zfill(3)}.nc'
                    )
                    s3_client.put_object(Bucket=BUCKET, Key=key)

            # Expect false full grid files exist
            # point of interest files exist.
            # But get_points_for_grid is throwing exception to indicate no
            # points catalog file.
            assert not check_lotus_ww3_ready(
                BUCKET,
                POINT_OF_INTEREST_PREFIX,
                FULL_GRID_PREFIX,
                SCIENCE_DATA_SERVICE,
                AGENCY,
                MODEL,
                RUN,
            )


def test_check_model_grids_ready_all_files_exist(create_s3_bucket):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES),
    ):
        s3_client = create_s3_bucket

        for point in POINT_NAMES:
            key = (
                f'{POINT_OF_INTEREST_PREFIX}/{EXAMPLE_GRID}/{RUN}'
                f'ww3_lotus_{EXAMPLE_GRID}.spot_{point}.nc'
            )
            s3_client.put_object(Bucket=BUCKET, Key=key)

        for num in list(range(385)):
            key = (
                f'{FULL_GRID_PREFIX}/{EXAMPLE_GRID}/{RUN}/'
                f'ww3_lotus_{EXAMPLE_GRID}.{RUN}.{str(num).zfill(3)}.nc'
            )
            s3_client.put_object(Bucket=BUCKET, Key=key)
        # Expect true when all files for POI and full grid exist in S3.
        assert check_model_grids_ready(
            BUCKET,
            POINT_OF_INTEREST_PREFIX,
            FULL_GRID_PREFIX,
            AGENCY,
            MODEL,
            RUN,
            EXAMPLE_GRID,
            SCIENCE_DATA_SERVICE,
        )


def test_check_model_grids_ready_expected_points_less_than_available_points(
    create_s3_bucket
):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES[:1]),
    ):
        s3_client = create_s3_bucket
        # There may be more spot files in S3 than POIs in
        # the Science Data Service.
        for point in POINT_NAMES:
            key = (
                f'{POINT_OF_INTEREST_PREFIX}/{EXAMPLE_GRID}/{RUN}'
                f'ww3_lotus_{EXAMPLE_GRID}.spot_{point}.nc'
            )
            s3_client.put_object(Bucket=BUCKET, Key=key)

        for num in list(range(385)):
            key = (
                f'{FULL_GRID_PREFIX}/{EXAMPLE_GRID}/{RUN}/'
                f'ww3_lotus_{EXAMPLE_GRID}.{RUN}.{str(num).zfill(3)}.nc'
            )
            s3_client.put_object(Bucket=BUCKET, Key=key)
        # Expect true when files for all points for grid exist in S3.
        assert check_model_grids_ready(
            BUCKET,
            POINT_OF_INTEREST_PREFIX,
            FULL_GRID_PREFIX,
            AGENCY,
            MODEL,
            RUN,
            EXAMPLE_GRID,
            SCIENCE_DATA_SERVICE,
        )


def test_check_model_grids_ready_some_files_missing(create_s3_bucket):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES),
    ):
        s3_client = create_s3_bucket

        for point in POINT_NAMES[0:2]:
            key = (
                f'{POINT_OF_INTEREST_PREFIX}/{EXAMPLE_GRID}/{RUN}'
                f'/ww3_lotus_{EXAMPLE_GRID}.spot_{point}.nc'
            )
            s3_client.put_object(Bucket=BUCKET, Key=key)

        for num in list(range(2)):
            key = (
                f'{FULL_GRID_PREFIX}/{EXAMPLE_GRID}/{RUN}/'
                f'ww3_lotus_{EXAMPLE_GRID}.{RUN}.{str(num).zfill(3)}.nc'
            )
            s3_client.put_object(Bucket=BUCKET, Key=key)

        # Expect false when files for some hours for grid missing in S3.
        assert not check_model_grids_ready(
            BUCKET,
            POINT_OF_INTEREST_PREFIX,
            FULL_GRID_PREFIX,
            AGENCY,
            MODEL,
            RUN,
            EXAMPLE_GRID,
            SCIENCE_DATA_SERVICE,
        )


def test_check_model_grids_ready_no_files_found(create_s3_bucket):
    with mock.patch(
        'lib.check_lotus_ww3_ready.get_points_for_grid',
        side_effect=mock_get_points_for_grid(POINT_NAMES),
    ):
        # Expect false when no files for grid found in S3.
        assert not check_model_grids_ready(
            BUCKET,
            POINT_OF_INTEREST_PREFIX,
            FULL_GRID_PREFIX,
            AGENCY,
            MODEL,
            RUN,
            EXAMPLE_GRID,
            SCIENCE_DATA_SERVICE,
        )
