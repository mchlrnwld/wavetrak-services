from typing import List, Set

import boto3  # type: ignore
import requests


def get_list_of_grids(
    agency: str, model: str, science_data_service: str
) -> List[str]:
    """
    Calls the Science Data Service endpoint to
    get a list of grids to check for.

    Args:
        agency: The model agency to to query for (ex. Wavetrak)
        model: The name of the model to query for (ex. Lotus-WW3)
        science_data_service: Endpoint to query
                              for the list of points of interest.

    Returns:
        A list of grids
    """
    response = requests.get(
        f'{science_data_service}/model_grids',
        params=dict(model=model, agency=agency),
    )
    response.raise_for_status()
    return [data['grid'] for data in response.json()]


class PointsFileNotAvailableError(Exception):
    pass


def get_points_for_grid(
    bucket: str, prefix: str, grid: str, run: int
) -> Set[str]:
    """
    For a given grid/run, gets the list of expected point output files.

    Args:
        bucket: The science data bucket
        prefix: Prefix for lotus points files
        grid: grid to check
        run: run time as yyyymmddhh, e.g. 2020013103

    Returns:
        List of expected points files
    """
    s3_client = boto3.client('s3')
    try:
        response = s3_client.get_object(
            Bucket=bucket,
            Key=f'{prefix}/{grid}/{run}/point_outputs_{grid}.dat',
        )
    except s3_client.exceptions.NoSuchKey:
        raise PointsFileNotAvailableError(f'No points file for {grid}/{run}')

    return set(response['Body'].read().decode('utf-8').splitlines())


def model_run_from_key(science_key_prefix: str, key: str) -> str:
    """
    Extracts a model run time from the given key string
    if the key contains the science_key_prefix,
    Otherwise ValueError is raised.

    Args:
        science_key_prefix: Prefix in S3 to check for
                            lotus-ww3 uploads.
        key: The key string to extract data from

    Returns:
        A string representing the model run time (ex. 2019090500)
    """
    if science_key_prefix in key:
        return key.replace(science_key_prefix, '').split('/')[2]
    else:
        raise ValueError(f'Key: {key} is of unknown type')
