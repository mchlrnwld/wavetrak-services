import logging
import re

import boto3  # type: ignore

import check_model_ready  # type: ignore
from lib.helpers import (
    PointsFileNotAvailableError,
    get_list_of_grids,
    get_points_for_grid,
)

logger = logging.getLogger('check-model-ready')

LOTUS_FULL_GRID_REGEX = re.compile(rf'ww3\_lotus_.+\.\d{{10}}\.\d{{3}}\.nc$')
LOTUS_POINT_OF_INTEREST_REGEX = re.compile(
    rf'ww3\_lotus\_.+\.spot\_\d{{3}}\.\d{{3}}\_[0-]\d{{2}}\.\d{{3}}\.nc$'
)


def check_model_grids_ready(
    bucket: str,
    point_of_interest_prefix: str,
    full_grid_prefix: str,
    agency: str,
    model: str,
    run: int,
    grid: str,
    sds_host: str,
) -> bool:
    """
    Checks if all point of interest and full grid files for a given
    grid/model run are available in S3.

    Args:
        bucket: S3 bucket to check for file objects.
        point_of_interest_prefix: Prefix to check for point of interest files.
        full_grid_prefix: Prefix to check for full grid files.
        agency: Model agency associated
                with the model run (ex. Wavetrak).
        model: Name of the model associated
               with the model run (ex. Lotus-WW3).
        run: Model run time (YYYYMMDDHH) to check.
        grid: Grid to check.
        sds_host: Endpoint to query to obtain grids and points for the model.

    Returns:
        True if file objects for model run exist for grid in S3.
    """

    logger.info(
        f'Checking objects with bucket {bucket} and'
        f'{point_of_interest_prefix}/{grid}/{run} '
        f'and {full_grid_prefix}/{grid}/{run}'
    )
    s3_client = boto3.client('s3')
    poi_objects_response = s3_client.list_objects_v2(
        Bucket=bucket, Prefix=f'{point_of_interest_prefix}/{grid}/{run}'
    )
    full_grid_objects_response = s3_client.list_objects_v2(
        Bucket=bucket, Prefix=f'{full_grid_prefix}/{grid}/{run}'
    )
    if (
        'Contents' not in poi_objects_response
        or 'Contents' not in full_grid_objects_response
    ):
        return False

    try:
        expected_points = get_points_for_grid(
            bucket, point_of_interest_prefix, grid, run
        )
    except PointsFileNotAvailableError:
        return False

    available_points = {
        match[0]
        for match in [
            LOTUS_POINT_OF_INTEREST_REGEX.search(object['Key'])
            for object in poi_objects_response['Contents']
        ]
        if match is not None
    }

    available_full_grid_files = {
        object['Key'][-6:-3]
        for object in full_grid_objects_response['Contents']
        if LOTUS_FULL_GRID_REGEX.search(object['Key'])
    }

    expected_full_grid_files = {
        f'{str(num).zfill(3)}' for num in list(range(385))
    }

    return (
        expected_points  # There may be more spot files in S3
        <= available_points  # than POIs in Science Data Service.
        and available_full_grid_files == expected_full_grid_files
    )


def check_lotus_ww3_ready(
    bucket: str,
    point_of_interest_prefix: str,
    full_grid_prefix: str,
    sds_host: str,
    agency: str,
    model: str,
    run: int,
) -> bool:
    """
    Checks if a model run is ready and not yet processed.
    This is accomplished by checking that file objects
    for all grid point outputs are in S3.

    Args:
        bucket: S3 bucket to check for file objects.
        point_of_interest_prefix: Prefix to check for point of interest files.
        full_grid_prefix: Prefix to check for full grid files.
        sds_host: Endpoint to query to obtain grids and points for the model.
        agency: Model agency associated with the model run (ex. Wavetrak).
        model: Name of the model associated with the model run (ex. Lotus-WW3).
        run: Model run time (YYYYMMDDHH) to check.

    Returns:
        True if file objects for model run exist for all grids in S3.
    """
    # Skip model run if it has already been processed
    if check_model_ready.check_model_run_already_exists(
        agency, model, run, 'POINT_OF_INTEREST', sds_host
    ):
        logger.info(f'{run} has already been processed.')
        return False

    return all(
        check_model_grids_ready(
            bucket,
            point_of_interest_prefix,
            full_grid_prefix,
            agency,
            model,
            run,
            grid,
            sds_host,
        )
        for grid in get_list_of_grids(agency, model, sds_host)
    )
