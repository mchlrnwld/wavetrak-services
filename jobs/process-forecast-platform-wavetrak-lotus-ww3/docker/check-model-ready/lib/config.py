import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
POINT_OF_INTEREST_PREFIX = os.environ['POINT_OF_INTEREST_PREFIX']
FULL_GRID_PREFIX = os.environ['FULL_GRID_PREFIX']
MODEL_NAME = os.environ['MODEL_NAME']
MODEL_AGENCY = os.environ['MODEL_AGENCY']
SQS_QUEUE_NAME = os.environ['SQS_QUEUE_NAME']

SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
