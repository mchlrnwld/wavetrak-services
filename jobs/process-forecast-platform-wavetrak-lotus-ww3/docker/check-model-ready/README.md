# Check Model Ready

Docker image to check if a Lotus WW3 model is ready for processing. This image polls messages off of SQS containing object keys from Lotus WW3 uploads to S3. For each object key, we check if all files are found for the model run to determine if the model run is ready for processing. We continue polling SQS until the queue is empty or a model run is ready for processing.

The flow of polling SQS looks like this:
1. Check SQS for up to 10 messages (this is the maximum allowed by AWS).
2. Filter messages for relevant object keys. It is possible for other keys that we don't care about to exist in the messages. Also it's possible for a message to have more than 1 object key.
3. Scan through the messages checking if the model run for each object key is ready for processing. This check is done by listing all files for all grids for the model run. If it has already been processed in S3 we short circuit this check.
4. Delete checked messages.
5. If a model run is ready, print it to stdout and mark it as processed with an S3 object to prevent future checks from attempting to process it. Otherwise go back to step 1 and continue checking for messages.
6. If no model run is ready and no messages are left then print a blank line to stdout.

## Setup

```sh
conda devenv
conda activate process-forecast-platform-wavetrak-lotus-ww3-check-model-ready
cp .env.sample .env
env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
docker build -t process-forecast-platform-wavetrak-lotus-ww3/check-model-ready .
cp .env.sample .env
docker run --env-file=.env --rm -v ~/.aws:/root/.aws process-forecast-platform-wavetrak-lotus-ww3/check-model-ready
```
