import logging
from functools import partial

import lib.config as config
from check_model_ready import check_sqs_messages, set_pending_model_run
from lib.check_lotus_ww3_ready import check_lotus_ww3_ready
from lib.helpers import model_run_from_key

logger = logging.getLogger('check-model-ready')


def main():
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler())

    logger.info(f'Polling queue: {config.SQS_QUEUE_NAME}...')

    result = None
    total_messages = 0
    while True:
        # NOTE: Added type: ignore due to the fact that mypy doesn't yet
        # recognize functools.partial as `Callable`.
        model_run, messages_found = check_sqs_messages(  # type: ignore
            config.SCIENCE_BUCKET,
            (config.POINT_OF_INTEREST_PREFIX,),
            config.SQS_QUEUE_NAME,
            partial(
                check_lotus_ww3_ready,
                config.SCIENCE_BUCKET,
                config.POINT_OF_INTEREST_PREFIX,
                config.FULL_GRID_PREFIX,
                config.SCIENCE_DATA_SERVICE,
                config.MODEL_AGENCY,
                config.MODEL_NAME,
            ),
            partial(model_run_from_key, config.POINT_OF_INTEREST_PREFIX),
        )
        result = model_run
        total_messages += messages_found
        if result is not None or messages_found == 0:
            break

    logger.info(f'Checked {total_messages} messages.')

    # Write to stdout for xcoms_push
    if result is not None:
        set_pending_model_run(
            config.MODEL_AGENCY,
            config.MODEL_NAME,
            result,
            'POINT_OF_INTEREST',
            config.SCIENCE_DATA_SERVICE,
        )
        print(result)
    else:
        print('')


if __name__ == '__main__':
    main()
