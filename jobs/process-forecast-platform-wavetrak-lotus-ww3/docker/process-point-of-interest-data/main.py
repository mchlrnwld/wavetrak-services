import asyncio
import json
import logging
import os
from datetime import datetime, timezone
from tempfile import TemporaryDirectory
from timeit import default_timer
from typing import List, Set, Tuple

from botocore.exceptions import ClientError  # type:ignore
from grib_helpers.process import get_lat_lon_index
from slack_helper import send_invalid_points_of_interest_notification

import lib.config as config
from lib.csv import append_surf_csv, append_swells_csv
from lib.file_helpers import (
    create_full_grid_file_path,
    create_spot_file_path,
    get_hour_from_file,
)
from lib.models import PointOfInterestGridPoint
from lib.parse_lotus_ww3_files import parse_lotus_full_grid_file
from lib.s3_client import S3Client
from lib.surf_and_swells import SurfAndSwellsBuilder

logger = logging.getLogger('process-point-of-interest-data')

SWELLS_CSV_FILE_NAME = 'swells.csv'
SURF_CSV_FILE_NAME = 'surf.csv'
JOB_RUN_KEY_PREFIX = f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/{config.RUN}'


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    logger.info(f'Starting job for model run: {config.RUN}')

    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir:
        s3_client = S3Client()

        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.download_file(
                    config.SCIENCE_BUCKET,
                    f'{JOB_RUN_KEY_PREFIX}/points-of-interest/'
                    f'{config.GRID}.json',
                    os.path.join(temp_dir, f'{config.GRID}.json'),
                )
            ]
        )
        elapsed = default_timer() - start
        logger.info(f'Downloaded POI grid point data in {elapsed}')

        surf_and_swells_builder = SurfAndSwellsBuilder(temp_dir)

        with open(os.path.join(temp_dir, f'{config.GRID}.json')) as poi_json:
            points_of_interest_grid_points = [
                PointOfInterestGridPoint(*dict.values())
                for dict in json.load(poi_json)
            ]

        unique_grid_points = list(
            {
                (point.lon, point.lat)
                for point in points_of_interest_grid_points
            }
        )
        local_spot_files = [
            create_spot_file_path(lon, lat, config.GRID, config.RUN, temp_dir)
            for lon, lat in unique_grid_points
        ]
        s3_spot_files = [
            create_spot_file_path(
                lon,
                lat,
                config.GRID,
                config.RUN,
                config.POINT_OF_INTEREST_PREFIX,
            )
            for lon, lat in unique_grid_points
        ]

        logger.info(
            f'Looking for {len(s3_spot_files)} spot files from S3 to '
            f'download...'
        )

        start = default_timer()

        # Turn off (most) logging while downloading the spot files.
        logger.setLevel(logging.CRITICAL)
        results = await asyncio.gather(
            *[
                s3_client.download_file(
                    config.SCIENCE_BUCKET, s3_file_path, local_file_path
                )
                for s3_file_path, local_file_path in zip(
                    s3_spot_files, local_spot_files
                )
            ],
            return_exceptions=True,
        )
        logger.setLevel(logging.INFO)

        spot_file_exceptions = [
            result for result in results if isinstance(result, ClientError)
        ]
        # Spot files may not exist for every POI grid point,
        # so catch missing file exceptions and log them.
        # All other exceptions will be raised.
        if len(spot_file_exceptions) > 0:
            missing_file_exceptions = [
                e
                for e in spot_file_exceptions
                if e.response['Error']['Message'] == 'Not Found'
            ]
            if len(spot_file_exceptions) > len(missing_file_exceptions):
                logger.error(
                    f'The following exceptions were raised while '
                    f'downloading spot files: {spot_file_exceptions}.'
                )
                raise BaseException(spot_file_exceptions)
            logger.info(
                f'Spot files do not exist for {len(missing_file_exceptions)} '
                f'POI grid points.'
            )

        spot_files_downloaded = len(s3_spot_files) - len(spot_file_exceptions)
        elapsed = default_timer() - start
        logger.info(
            f'Downloaded {spot_files_downloaded} spot files in {elapsed}s.'
        )

        local_full_grid_files = [
            create_full_grid_file_path(
                config.RUN, config.GRID, temp_dir, forecast_hour
            )
            for forecast_hour in config.FORECAST_HOURS
        ]
        s3_full_grid_files = [
            create_full_grid_file_path(
                config.RUN, config.GRID, config.FULL_GRID_PREFIX, forecast_hour
            )
            for forecast_hour in config.FORECAST_HOURS
        ]

        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.download_file(
                    config.SCIENCE_BUCKET, s3_file_path, local_file_path
                )
                for s3_file_path, local_file_path in zip(
                    s3_full_grid_files, local_full_grid_files
                )
            ]
        )
        elapsed = default_timer() - start
        logger.info(
            f'Downloaded {len(s3_full_grid_files)} full grid files in '
            f'{elapsed}s.'
        )

        swells_csv_file_path = os.path.join(temp_dir, SWELLS_CSV_FILE_NAME)
        surf_csv_file_path = os.path.join(temp_dir, SURF_CSV_FILE_NAME)

        run_dt = datetime.strptime(str(config.RUN), '%Y%m%d%H').replace(
            tzinfo=timezone.utc
        )

        all_invalid_poi_ids: Set[str] = set()

        poi_with_spot_files: List[Tuple[PointOfInterestGridPoint, str]] = []
        poi_without_spot_files: List[PointOfInterestGridPoint] = []
        for poi in points_of_interest_grid_points:
            spot_file_local_path = create_spot_file_path(
                poi.lon, poi.lat, config.GRID, config.RUN, temp_dir
            )
            if os.path.exists(spot_file_local_path):
                poi_with_spot_files.append((poi, spot_file_local_path))
            else:
                poi_without_spot_files.append(poi)

        logger.info(
            f'Parsing spot files for {len(poi_without_spot_files)} points of '
            f'interest ...'
        )
        start = default_timer()

        (
            surf_hours,
            swells_hours,
            invalid_point_of_interest_ids,
        ) = surf_and_swells_builder.build_from_spot_data(
            poi_with_spot_files, config.FORECAST_HOURS
        )

        all_invalid_poi_ids = all_invalid_poi_ids.union(
            invalid_point_of_interest_ids
        )

        logger.info(
            'Writing point of interest spot data to swells and surf CSV...'
        )

        await asyncio.gather(
            append_surf_csv(
                surf_csv_file_path,
                config.AGENCY,
                config.MODEL,
                config.GRID,
                run_dt,
                surf_hours,
            ),
            append_swells_csv(
                swells_csv_file_path,
                config.AGENCY,
                config.MODEL,
                config.GRID,
                run_dt,
                swells_hours,
            ),
        )

        elapsed = default_timer() - start
        logger.info(f'Finished parsing spot files in {elapsed}s.')

        if not poi_without_spot_files:
            logger.info(f'No points of interest for {config.GRID}.')
            return

        logger.info(f'Parsing full grid files for {config.GRID}...')
        start = default_timer()

        indexed_points_of_interest: List[
            Tuple[PointOfInterestGridPoint, Tuple[int, int]]
        ] = []
        for file in local_full_grid_files:
            forecast_hour = get_hour_from_file(file)
            logger.info(
                f'Parsing full grid file {file} for '
                f'{len(poi_without_spot_files)} points of interest...'
            )
            lats, lons, data = parse_lotus_full_grid_file(file)
            if not indexed_points_of_interest:
                for poi in poi_without_spot_files:
                    try:
                        indexed_points_of_interest.append(
                            (
                                poi,
                                get_lat_lon_index(
                                    (poi.lat, poi.lon), lats, lons
                                ),
                            )
                        )
                    except ValueError:
                        invalid_point_of_interest_ids.append(
                            poi.point_of_interest_id
                        )

            (
                surf_hours,
                swells_hours,
                invalid_point_of_interest_ids,
            ) = surf_and_swells_builder.build_from_full_grid_data(
                indexed_points_of_interest, forecast_hour, data
            )

            all_invalid_poi_ids = all_invalid_poi_ids.union(
                invalid_point_of_interest_ids
            )

            logger.info(
                f'Writing point of interest data to swells and surf CSV for '
                f'{file}...'
            )

            await asyncio.gather(
                append_surf_csv(
                    surf_csv_file_path,
                    config.AGENCY,
                    config.MODEL,
                    config.GRID,
                    run_dt,
                    surf_hours,
                ),
                append_swells_csv(
                    swells_csv_file_path,
                    config.AGENCY,
                    config.MODEL,
                    config.GRID,
                    run_dt,
                    swells_hours,
                ),
            )

        elapsed = default_timer() - start
        logger.info(
            f'Finished writing to csv for grid {config.GRID} in {elapsed}s.'
        )

        logger.info('Uploading CSVs to S3...')
        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.upload_file(
                    local_file_path,
                    config.JOBS_BUCKET,
                    (
                        f'{JOB_RUN_KEY_PREFIX}/{config.GRID}/'
                        f'{config.TASK_PARTITION}/'
                        f'{file_name}'
                    ),
                )
                for local_file_path, file_name in [
                    (swells_csv_file_path, SWELLS_CSV_FILE_NAME),
                    (surf_csv_file_path, SURF_CSV_FILE_NAME),
                ]
            ]
        )
        elapsed = default_timer() - start
        logger.info(f'Finished uploading CSVs to S3 in {elapsed}s.')

        if (
            all_invalid_poi_ids
            and config.SLACK_API_TOKEN
            and config.SLACK_CHANNEL
            # Only alert from the job that includes the 0-hour to avoid
            # generating N matching alerts (where N=number of partitions for
            # the grid in # ../../dag.py).
            and 0 in config.FORECAST_HOURS
        ):
            send_invalid_points_of_interest_notification(
                config.JOB_NAME,
                {config.GRID: all_invalid_poi_ids},
                config.SLACK_API_TOKEN,
                config.SLACK_CHANNEL,
                logger.error,
            )


if __name__ == '__main__':
    asyncio.run(main())
