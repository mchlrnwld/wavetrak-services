# Process Point of Interest Data

Docker image to:

1. Query points of interest grid points for Lotus-WW3.
2. Map the points of interest grid points to spot and full grid data files.
3. Process surf and swell data for points of interest.
4. Create CSVs for point of interest surf and swell data.

## Setup

```sh
conda devenv
conda activate process-forecast-platform-wavetrak-lotus-ww3-process-point-of-interest-data
cp .env.sample .env
env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
docker build -t process-forecast-platform-wavetrak-lotus-ww3/process-point-of-interest-data .
cp .env.sample .env
docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data process-forecast-platform-wavetrak-lotus-ww3/process-point-of-interest-data
```

## Testing

Run an integration test with:
```sh
make test-integration
```
