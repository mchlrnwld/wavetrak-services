import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
JOB_NAME = os.environ['JOB_NAME']
TASK_PARTITION = os.environ['TASK_PARTITION']
SECRETS_PREFIX = f'{ENV}/common/'
DATA_DIR = os.getenv('AIRFLOW_TMP_DIR', 'data')
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
GRID = os.environ['GRID']
RUN = int(os.environ['RUN'])
FORECAST_HOURS = [
    int(forecast_hour)
    for forecast_hour in os.environ['FORECAST_HOURS'].split(',')
]
FULL_GRID_PREFIX = os.environ['FULL_GRID_PREFIX']
POINT_OF_INTEREST_PREFIX = os.environ['POINT_OF_INTEREST_PREFIX']

JOBS_BUCKET = get_secret(SECRETS_PREFIX, 'JOBS_BUCKET')
JOBS_KEY_PREFIX = get_secret(SECRETS_PREFIX, 'JOBS_KEY_PREFIX')
SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')

SLACK_API_TOKEN = get_secret(SECRETS_PREFIX, 'SLACK_API_TOKEN', '')
SLACK_CHANNEL = os.getenv('SLACK_CHANNEL', '')
