import asyncio
import csv
import os
from datetime import datetime, timedelta
from functools import partial
from typing import Dict, List

from lib.models import SurfHour, SwellsHour


async def _async_append_csv(csvpath: str, rows: List[Dict[str, object]]):
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, partial(_append_csv, csvpath, rows))


def _append_csv(csvpath: str, rows: List[Dict[str, object]]):
    write_header = not os.path.exists(csvpath)

    with open(csvpath, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(rows[0].keys()))

        if write_header:
            writer.writeheader()

        writer.writerows(rows)


async def append_swells_csv(
    csvpath: str,
    agency: str,
    model: str,
    grid: str,
    run_dt: datetime,
    swells_hours: List[SwellsHour],
):
    """
    Creates a CSV of swells for each hour.

    Args:
        csvpath: Location of CSV file to write to.
        agency: Model agency to include in each CSV line.
        model: Model name to include in each CSV line.
        grid: Model grid to include in each CSV line.
        run_dt: Model run to include in each CSV line as a datetime
                representation.
        swells_hours: List of swell partitions by hour. Each item will be a
                      single row in the CSV.
    """
    await _async_append_csv(
        csvpath,
        [
            {
                'point_of_interest_id': (
                    swells_hour.point_of_interest.point_of_interest_id
                ),
                'agency': agency,
                'model': model,
                'grid': grid,
                'latitude': swells_hour.point_of_interest.lat,
                'longitude': swells_hour.point_of_interest.lon,
                'run': run_dt,
                'forecast_time': run_dt + timedelta(hours=swells_hour.hour),
                'height': swells_hour.height,
                'period': swells_hour.period,
                'direction': swells_hour.direction,
                'swell_wave_1_height': swells_hour.swell_wave_1_height,
                'swell_wave_1_period': swells_hour.swell_wave_1_period,
                'swell_wave_1_direction': swells_hour.swell_wave_1_direction,
                'swell_wave_1_spread': swells_hour.swell_wave_1_spread,
                'swell_wave_1_impact': swells_hour.swell_wave_1_impact,
                'swell_wave_2_height': swells_hour.swell_wave_2_height,
                'swell_wave_2_period': swells_hour.swell_wave_2_period,
                'swell_wave_2_direction': swells_hour.swell_wave_2_direction,
                'swell_wave_2_spread': swells_hour.swell_wave_2_spread,
                'swell_wave_2_impact': swells_hour.swell_wave_2_impact,
                'swell_wave_3_height': swells_hour.swell_wave_3_height,
                'swell_wave_3_period': swells_hour.swell_wave_3_period,
                'swell_wave_3_direction': swells_hour.swell_wave_3_direction,
                'swell_wave_3_spread': swells_hour.swell_wave_3_spread,
                'swell_wave_3_impact': swells_hour.swell_wave_3_impact,
                'swell_wave_4_height': swells_hour.swell_wave_4_height,
                'swell_wave_4_period': swells_hour.swell_wave_4_period,
                'swell_wave_4_direction': swells_hour.swell_wave_4_direction,
                'swell_wave_4_spread': swells_hour.swell_wave_4_spread,
                'swell_wave_4_impact': swells_hour.swell_wave_4_impact,
                'swell_wave_5_height': swells_hour.swell_wave_5_height,
                'swell_wave_5_period': swells_hour.swell_wave_5_period,
                'swell_wave_5_direction': swells_hour.swell_wave_5_direction,
                'swell_wave_5_spread': swells_hour.swell_wave_5_spread,
                'swell_wave_5_impact': swells_hour.swell_wave_5_impact,
                'wind_wave_height': swells_hour.wind_wave_height,
                'wind_wave_period': swells_hour.wind_wave_period,
                'wind_wave_direction': swells_hour.wind_wave_direction,
                'wind_wave_spread': swells_hour.wind_wave_spread,
                'wind_wave_impact': swells_hour.wind_wave_impact,
                'spectra1d_energy': str(swells_hour.spectra_energy)
                .replace('[', '{')
                .replace(']', '}')
                if swells_hour.spectra_energy is not None
                else None,
                'spectra1d_direction': str(swells_hour.spectra_direction)
                .replace('[', '{')
                .replace(']', '}')
                if swells_hour.spectra_energy is not None
                else None,
                'spectra1d_directional_spread': str(
                    swells_hour.spectra_directional_spread
                )
                .replace('[', '{')
                .replace(']', '}')
                if swells_hour.spectra_energy is not None
                else None,
            }
            for swells_hour in swells_hours
        ],
    )


async def append_surf_csv(
    csvpath: str,
    agency: str,
    model: str,
    grid: str,
    run_dt: datetime,
    surf_hours: List[SurfHour],
):
    """
    Creates a CSV of surf for each hour.
    Args:
        csvpath: Location of CSV file to write to.
        agency: Model agency to include in each CSV line.
        model: Model name to include in each CSV line.
        grid: Model grid to include in each CSV line.
        run_dt: Model run to include in each CSV line as a datetime
                representation.
        surf_hours: List of surf partitions by hour. Each item will be a
                    single row in the CSV.
    """
    await _async_append_csv(
        csvpath,
        [
            {
                'point_of_interest_id': (
                    surf_hour.point_of_interest.point_of_interest_id
                ),
                'agency': agency,
                'model': model,
                'grid': grid,
                'latitude': surf_hour.point_of_interest.lat,
                'longitude': surf_hour.point_of_interest.lon,
                'run': run_dt,
                'forecast_time': run_dt + timedelta(hours=surf_hour.hour),
                'breaking_wave_height_min': surf_hour.breaking_wave_height_min,
                'breaking_wave_height_max': surf_hour.breaking_wave_height_max,
                'breaking_wave_height_algorithm': (
                    surf_hour.breaking_wave_height_algorithm
                ),
            }
            for surf_hour in surf_hours
        ],
    )
