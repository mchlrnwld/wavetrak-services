import logging
import re

HOUR_REGEX = re.compile(rf'ww3_lotus_.+\.\d{{10}}\.(\d+)\.nc$')

logger = logging.getLogger('process-point-of-interest-data')


def get_hour_from_file(file: str) -> int:
    """
    Extracts the hour (ex. 38) from the given filename.

    Args:
        file: The Lotus-WW3 file to check.

    Returns:
        The forecast hour as an int if the filename matches.
        Example: 120
                 from file:
                 ww3_lotus_AUS_E_3m.2020022106.120.nc
    """
    result = HOUR_REGEX.search(file)
    if not result:
        logger.error(f'Invalid file format: {file}')
        raise AttributeError(f'Invalid file format: {file}')
    return int(result.group(1))


def lon_lat_to_name(lon: float, lat: float) -> str:
    """
    Converts a latitude and longitude
    to the name format used in Lotus spot files.

    Args:
        lon: A longitude.
        lat: A latitude. Must be in range [-90, +90].

    Returns:
        Lat & lon formatted for a Lotus spot file.
    """
    lon = lon % 360
    return f'{lon:07.3f}_{lat:07.3f}'


def create_spot_file_path(
    lon: float, lat: float, grid: str, run: int, prefix: str
) -> str:
    """
    Creates the file path for the spot file.

    Args:
        lon:    longitude value associated with the spot file.
        lat:    latitude value associated with the spot file.
        grid:   grid associated with the lat and lon.
        run:    The run associated with the spot_file
                (ex. 2019090500).
        prefix: Prefix of spot file location in S3 or locally.

    Returns:
        The S3 or local path for the spot file
        Ex: '/lotus/archive/points/BALI_3m/'
            'ww3_lotus_BALI_3m.spot_200.000_-035.000.nc'

    """
    return (
        f'{prefix}/{grid}/{run}/'
        f'ww3_lotus_{grid}.spot_{lon_lat_to_name(lon, lat)}.nc'
    )


def create_full_grid_file_path(
    run: int, grid: str, prefix: str, forecast_hour: int
) -> str:
    """
    Creates the file path for the full grid file.

    Args:
        run:           The run that is associated
                       with the full grid file.
        grid:          The grid associated
                       with the Lotus-WW3 file (ex. AUS_E_3m, CAL_3m).
        prefix:        Prefix of full grid file location in S3 or locally.
        forecast_hour: The forecast hour associated with the file (ex. 20).

    Returns:
        Lotus full grid file path.
        Examples:
        'ww3_lotus_AUS_E_3m.2020022106.120.nc'
        'ww3_lotus_CAL_3m.2020022100.001.nc'

    """
    return (
        f'{prefix}/{grid}/{run}/'
        f'ww3_lotus_{grid}.{run}.{str(forecast_hour).zfill(3)}.nc'
    )
