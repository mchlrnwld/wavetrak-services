import numpy as np  # type: ignore

from lib.swell_spectra import interpolate_swell_spectra

EPSILON = 0.000001


def almost_equals(a, b):
    return abs(a - b) < EPSILON


def test_interpolate_swell_spectra():
    period = [2.5, 3.5, 4.5, 5, 6, 10, 15, 20, 28]
    input_energy = [np.nan, 0, 4, 10, 12, 5, 0, 0, np.nan]
    input_direction = [
        190.0,
        350.0,
        60.0,
        130.0,
        200.0,
        270.0,
        340.0,
        50.0,
        120.0,
    ]
    frequency_hz = [1.0 / p for p in period[::-1]]

    energy, direction, spread = interpolate_swell_spectra(
        frequency_hz=frequency_hz,
        target_s=list(range(2, 29)),
        energy=input_energy[::-1],
        direction=input_direction[::-1],
        spread=[10.0, 10.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0][::-1],
    )

    assert energy == [
        0.0,
        0.0,
        2.0,
        10.0,
        12.0,
        10.25,
        8.5,
        6.75,
        5.0,
        4.0,
        3.0,
        2.0,
        1.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
    ]

    assert direction[0] == 0.0  # 2s. No data
    assert direction[1] == 0.0  # 3s. No energy data, therefore no direction
    assert almost_equals(
        direction[2], 25.0
    )  # 4s. Should have swing through north
    assert almost_equals(direction[3], 130.0)  # 5s.

    # 10s to 15s
    assert almost_equals(direction[8], 270.0)
    assert almost_equals(direction[9], 284.0)
    assert almost_equals(direction[10], 298.0)
    assert almost_equals(direction[11], 312.0)
    assert almost_equals(direction[12], 326.0)
    assert almost_equals(direction[13], 340.0)

    # 21s to 28s, no energy data, so no directions
    assert direction[-8:] == [0, 0, 0, 0, 0, 0, 0, 0]

    assert spread[0] == 0.0
    assert spread[1] == 0.0
    assert spread[2] == 15.0
    assert spread[3] == 20.0
