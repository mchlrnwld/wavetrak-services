from typing import List, Optional


class PointOfInterestGridPoint:
    """
    Point of interest and its grid point.

    Args:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.

    Attributes:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.
        optimal_swell_direction: Optimal swell direction as a float.
        breaking_wave_height_coefficient: Breaking wave multiplier as a float.
        breaking_wave_height_intercept: Breaking wave intercept as float.
        breaking_wave_height_algorithm: Breaking wave algorithms string.
        spectral_refraction_matrix: Spectral refraction matrix
                                    as list of float.
    """

    def __init__(
        self,
        point_of_interest_id: str,
        lon: float,
        lat: float,
        optimal_swell_direction: Optional[float],
        breaking_wave_height_coefficient: Optional[float],
        breaking_wave_height_intercept: Optional[float],
        breaking_wave_height_algorithm: Optional[str],
        spectral_refraction_matrix: Optional[List[List[float]]],
    ):
        self.point_of_interest_id = point_of_interest_id
        if lon < 0:
            self.lon = lon + 360
        else:
            self.lon = lon
        self.lat = lat
        self.optimal_swell_direction = optimal_swell_direction
        self.breaking_wave_height_coefficient = (
            breaking_wave_height_coefficient
        )
        self.breaking_wave_height_intercept = breaking_wave_height_intercept
        self.breaking_wave_height_algorithm = breaking_wave_height_algorithm
        self.spectral_refraction_matrix = spectral_refraction_matrix


class SwellsHour:
    """
    SwellsHour contains swell values for a single hour of a point of interest
    grid point.
    """

    def __init__(
        self,
        point_of_interest: PointOfInterestGridPoint,
        hour: int,
        height: float,
        period: float,
        direction: float,
        swell_wave_1_height: float,
        swell_wave_1_period: float,
        swell_wave_1_direction: float,
        swell_wave_1_spread: float,
        swell_wave_1_impact: float,
        swell_wave_2_height: float,
        swell_wave_2_period: float,
        swell_wave_2_direction: float,
        swell_wave_2_spread: float,
        swell_wave_2_impact: float,
        swell_wave_3_height: float,
        swell_wave_3_period: float,
        swell_wave_3_direction: float,
        swell_wave_3_spread: float,
        swell_wave_3_impact: float,
        swell_wave_4_height: float,
        swell_wave_4_period: float,
        swell_wave_4_direction: float,
        swell_wave_4_spread: float,
        swell_wave_4_impact: float,
        swell_wave_5_height: float,
        swell_wave_5_period: float,
        swell_wave_5_direction: float,
        swell_wave_5_spread: float,
        swell_wave_5_impact: float,
        wind_wave_height: float,
        wind_wave_period: float,
        wind_wave_direction: float,
        wind_wave_spread: float,
        wind_wave_impact: float,
        spectra_energy: Optional[List[float]],
        spectra_direction: Optional[List[float]],
        spectra_directional_spread: Optional[List[float]],
    ):
        self.point_of_interest = point_of_interest
        self.hour = hour
        self.height = height
        self.period = period
        self.direction = direction
        self.swell_wave_1_height = swell_wave_1_height
        self.swell_wave_1_period = swell_wave_1_period
        self.swell_wave_1_direction = swell_wave_1_direction
        self.swell_wave_1_spread = swell_wave_1_spread
        self.swell_wave_1_impact = swell_wave_1_impact
        self.swell_wave_2_height = swell_wave_2_height
        self.swell_wave_2_period = swell_wave_2_period
        self.swell_wave_2_direction = swell_wave_2_direction
        self.swell_wave_2_spread = swell_wave_2_spread
        self.swell_wave_2_impact = swell_wave_2_impact
        self.swell_wave_3_height = swell_wave_3_height
        self.swell_wave_3_period = swell_wave_3_period
        self.swell_wave_3_direction = swell_wave_3_direction
        self.swell_wave_3_spread = swell_wave_3_spread
        self.swell_wave_3_impact = swell_wave_3_impact
        self.swell_wave_4_height = swell_wave_4_height
        self.swell_wave_4_period = swell_wave_4_period
        self.swell_wave_4_direction = swell_wave_4_direction
        self.swell_wave_4_spread = swell_wave_4_spread
        self.swell_wave_4_impact = swell_wave_4_impact
        self.swell_wave_5_height = swell_wave_5_height
        self.swell_wave_5_period = swell_wave_5_period
        self.swell_wave_5_direction = swell_wave_5_direction
        self.swell_wave_5_spread = swell_wave_5_spread
        self.swell_wave_5_impact = swell_wave_5_impact
        self.wind_wave_height = wind_wave_height
        self.wind_wave_period = wind_wave_period
        self.wind_wave_direction = wind_wave_direction
        self.wind_wave_spread = wind_wave_spread
        self.wind_wave_impact = wind_wave_impact
        self.spectra_energy = spectra_energy
        self.spectra_direction = spectra_direction
        self.spectra_directional_spread = spectra_directional_spread


class SurfHour:
    """
    SurfHour contains surf values for a single hour of a point of interest
    grid point.
    """

    def __init__(
        self,
        point_of_interest: PointOfInterestGridPoint,
        hour: int,
        breaking_wave_height_min: Optional[float],
        breaking_wave_height_max: Optional[float],
        breaking_wave_height_algorithm: Optional[str],
    ):
        self.point_of_interest = point_of_interest
        self.hour = hour
        self.breaking_wave_height_min = breaking_wave_height_min
        self.breaking_wave_height_max = breaking_wave_height_max
        self.breaking_wave_height_algorithm = breaking_wave_height_algorithm
