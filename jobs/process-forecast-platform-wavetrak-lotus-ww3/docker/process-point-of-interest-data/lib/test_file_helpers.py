from lib.file_helpers import (
    create_full_grid_file_path,
    create_spot_file_path,
    get_hour_from_file,
    lon_lat_to_name,
)
from lib.models import PointOfInterestGridPoint

DOWNLOAD_DIRECTORY = 'data/tmp'
RUN = 2019090500
POINT_OF_INTEREST_GRID_POINTS = [
    PointOfInterestGridPoint(
        '12345', 151.3, -33.85, None, None, None, 'POLYNOMIAL', None
    )
]
POINT_OF_INTEREST_PREFIX = 'lotus/archive/points'
FULL_GRID_PREFIX = 'lotus/archive/grids'


def test_lon_lat_to_name():
    lon, lat = (153.65, -28.65)
    assert lon_lat_to_name(lon, lat) == '153.650_-28.650'
    lon, lat = (151.35, -33.75)
    assert lon_lat_to_name(lon, lat) == '151.350_-33.750'
    lon, lat = (151.303662, -33.790575)
    assert lon_lat_to_name(lon, lat) == '151.304_-33.791'
    lon, lat = (115.25, -8.75)
    assert lon_lat_to_name(lon, lat) == '115.250_-08.750'


def test_get_hour_and_grid_from_file():
    file = 'ww3_lotus_BALI_3m.2019090500.038.nc'
    assert get_hour_from_file(file) == 38


def test_create_spot_file_local_path():
    download_directory = 'data/tmp'
    lon = 15.30
    lat = -20.05
    grid = 'GLOB_30m'
    assert create_spot_file_path(lon, lat, grid, RUN, download_directory) == (
        'data/tmp/GLOB_30m/2019090500/'
        'ww3_lotus_GLOB_30m.spot_015.300_-20.050.nc'
    )


def test_create_spot_file_s3_path():
    grid = 'BALI_3m'
    lon = 15.30
    lat = -20.05
    assert create_spot_file_path(
        lon, lat, grid, RUN, POINT_OF_INTEREST_PREFIX
    ) == (
        'lotus/archive/points/BALI_3m/2019090500/'
        'ww3_lotus_BALI_3m.spot_015.300_-20.050.nc'
    )


def test_create_full_grid_local_path():
    download_directory = 'data/tmp'
    grid = 'BALI_3m'
    forecast_hour = 38
    assert create_full_grid_file_path(
        RUN, grid, download_directory, forecast_hour
    ) == ('data/tmp/BALI_3m/2019090500/ww3_lotus_BALI_3m.2019090500.038.nc')


def test_create_full_grid_s3_path():
    grid = 'BALI_3m'
    forecast_hour = 38
    assert create_full_grid_file_path(
        RUN, grid, FULL_GRID_PREFIX, forecast_hour
    ) == (
        'lotus/archive/grids/BALI_3m/2019090500/'
        'ww3_lotus_BALI_3m.2019090500.038.nc'
    )
