import os

import boto3  # type: ignore
import moto  # type: ignore
import pandas as pd
import pytest

pytestmark = pytest.mark.asyncio


async def test_process_point_of_interest_data():
    with moto.mock_s3():
        # Must import after mocking boto3
        import lib.config as config
        from main import main

        JOB_RUN_KEY_PREFIX = (
            f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/{config.RUN}'
        )
        POINTS_OF_INTEREST_OBJECT_KEY = (
            f'{JOB_RUN_KEY_PREFIX}/points-of-interest/{config.GRID}.json'
        )
        SPOT_FILE_OBJECT_KEY = (
            f'{config.POINT_OF_INTEREST_PREFIX}/{config.GRID}/{config.RUN}/'
            f'ww3_lotus_{config.GRID}.spot_153.200_-30.350.nc'
        )
        GRID_FILE_OBJECT_KEY = (
            f'{config.FULL_GRID_PREFIX}/{config.GRID}/{config.RUN}/'
            f'ww3_lotus_{config.GRID}.{config.RUN}.003.nc'
        )

        s3 = boto3.client('s3', region_name='us-west-1')

        s3.create_bucket(
            Bucket=config.SCIENCE_BUCKET,
            CreateBucketConfiguration={'LocationConstraint': 'us-west-1'},
        )

        for filename, destination in [
            ('points_of_interest.json', POINTS_OF_INTEREST_OBJECT_KEY),
            ('lotus_spot_file.nc', SPOT_FILE_OBJECT_KEY),
            ('lotus_grid_file.nc', GRID_FILE_OBJECT_KEY),
        ]:
            s3.upload_file(
                os.path.join('fixtures', filename),
                config.SCIENCE_BUCKET,
                destination,
            )

        await main()

        swells_csv_object_key = (
            f'{JOB_RUN_KEY_PREFIX}/'
            f'{config.GRID}/'
            f'{config.TASK_PARTITION}/'
            'swells.csv'
        )

        surf_csv_object_key = (
            f'{JOB_RUN_KEY_PREFIX}/'
            f'{config.GRID}/'
            f'{config.TASK_PARTITION}/'
            'surf.csv'
        )

        swells_df = pd.read_csv(
            s3.get_object(
                Bucket=config.SCIENCE_BUCKET, Key=swells_csv_object_key
            )['Body']
        )

        surf_df = pd.read_csv(
            s3.get_object(
                Bucket=config.SCIENCE_BUCKET, Key=surf_csv_object_key
            )['Body']
        )

        assert all(
            column
            in [
                'point_of_interest_id',
                'agency',
                'model',
                'grid',
                'latitude',
                'longitude',
                'run',
                'forecast_time',
                'height',
                'period',
                'direction',
                'swell_wave_1_height',
                'swell_wave_1_period',
                'swell_wave_1_direction',
                'swell_wave_1_spread',
                'swell_wave_1_impact',
                'swell_wave_2_height',
                'swell_wave_2_period',
                'swell_wave_2_direction',
                'swell_wave_2_spread',
                'swell_wave_2_impact',
                'swell_wave_3_height',
                'swell_wave_3_period',
                'swell_wave_3_direction',
                'swell_wave_3_spread',
                'swell_wave_3_impact',
                'swell_wave_4_height',
                'swell_wave_4_period',
                'swell_wave_4_direction',
                'swell_wave_4_spread',
                'swell_wave_4_impact',
                'swell_wave_5_height',
                'swell_wave_5_period',
                'swell_wave_5_direction',
                'swell_wave_5_spread',
                'swell_wave_5_impact',
                'wind_wave_height',
                'wind_wave_period',
                'wind_wave_direction',
                'wind_wave_spread',
                'wind_wave_impact',
                'spectra1d_energy',
                'spectra1d_direction',
                'spectra1d_directional_spread',
            ]
            for column in swells_df.columns
        )

        assert all(
            column
            in [
                'point_of_interest_id',
                'agency',
                'model',
                'grid',
                'latitude',
                'longitude',
                'run',
                'forecast_time',
                'breaking_wave_height_min',
                'breaking_wave_height_max',
                'breaking_wave_height_algorithm',
            ]
            for column in surf_df.columns
        )

        assert len(swells_df) == 414
        assert len(surf_df) == 414
