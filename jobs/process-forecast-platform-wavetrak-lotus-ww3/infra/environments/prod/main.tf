provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/process-forecast-platform-wavetrak-lotus-ww3/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_wavetrak_lotus_ww3" {
  source = "../.."

  environment = "prod"
  topic_names = ["surfline-science-s3-prod-lotus-upload-event"]
  jobs_bucket = "surfline-science-s3-prod"
}
