module "sqs_with_sns_subscription" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs-with-sns-subscription"

  application = var.application
  environment = var.environment
  queue_name  = var.queue_name
  topic_names = var.topic_names
  company     = var.company
}

locals {
  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/${var.application}/process-point-of-interest-data:${var.environment}",
    "vcpus" : 1,
    "memory" : 4096,
    "volumes" : [
      {
        "host" : {
          "sourcePath" : "/mnt/ephemeral/airflow"
        },
        "name" : "tmp"
      }
    ],
    "mountPoints" : [
      {
        "sourceVolume" : "tmp",
        "containerPath" : "/mnt/ephemeral/airflow",
        "readOnly" : false
      }
    ]
  }
}

module "process_poi_batch_job_definition" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company = var.company
  # Use shorter name because the aws_iam_role used in the module goes over
  # the 64 character limit when using the full application name.
  application          = "${var.application_short}-poi"
  environment          = var.environment
  container_properties = local.container_properties
  custom_policy        = templatefile("${path.module}/policy.tpl", { jobs_bucket = var.jobs_bucket })
}
