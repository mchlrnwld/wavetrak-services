{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject"
      ],
      "Resource": [
        "arn:aws:s3:::${jobs_bucket}/jobs/process-forecast-platform-wavetrak-lotus-ww3/*"
      ],
      "Effect": "Allow"
    }
  ]
}
