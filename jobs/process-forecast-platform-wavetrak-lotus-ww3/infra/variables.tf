variable "environment" {
  type = string
}

variable "topic_names" {
  type = list(string)
}

variable "queue_name" {
  default = "s3-events"
}

variable "company" {
  default = "wt"
}

variable "application" {
  default = "process-forecast-platform-wavetrak-lotus-ww3"
}

variable "application_short" {
  default = "process-forecast-platform-wt-lotus-ww3"
}

variable "jobs_bucket" {
  description = "S3 bucket for job state files."
}
