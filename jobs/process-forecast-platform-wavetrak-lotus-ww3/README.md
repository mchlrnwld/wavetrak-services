# Process Forecast Platform Wavetrak Lotus-WW3

Airflow DAG to process Lotus-WW3 data into Surfline's Science Models Database.

## Docker Images for Tasks

- [Check Model Ready](docker/check-model-ready)
