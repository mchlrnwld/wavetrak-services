/* istanbul ignore file */
import { getSecret } from '@surfline/services-common';

const ENV = process.env.NODE_ENV === 'prod' ? 'prod' : 'sandbox';

let config;

export const createConfig = async () => {
  if (!config) {
    config = {
      LOGSENE_KEY: await getSecret(`${ENV}/common/`, 'LOGSENE_KEY'),
      LOGSENE_LEVEL: await getSecret(`${ENV}/common/`, 'LOGSENE_LEVEL'),
      MONGO_CONNECTION_STRING_USERDB: await getSecret(
        `${ENV}/jobs/trigger-subscription-renewal-event/`,
        'MONGO_CONNECTION_STRING_USERDB',
      ),
      SUBSCRIPTION_SERVICE_URL: await getSecret(`${ENV}/common/`, 'SUBSCRIPTION_SERVICE_URL'),
    };
  }
  return config;
};

export const getConfig = () => {
  if (!config) throw new Error('Config not set, you must call `createConfig`.');

  return config;
};
