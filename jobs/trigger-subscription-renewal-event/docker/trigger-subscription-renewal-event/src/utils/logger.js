/* istanbul ignore file */
import { createLogger, setupLogsene } from '@surfline/services-common';

let logger;
/**
 * @description A helper function for setting up Surfline specific logging
 * @param {object} config
 * @returns the instance of logger created
 */
export const setupLogger = ({ LOGSENE_KEY }) => {
  setupLogsene(LOGSENE_KEY);
  logger = createLogger('trigger-subscription-renewal-event');
  return logger;
};
