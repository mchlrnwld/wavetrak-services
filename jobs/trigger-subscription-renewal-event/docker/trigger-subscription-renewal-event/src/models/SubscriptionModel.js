import mongoose, { Schema } from 'mongoose';

const SECONDS_PER_DAY = 86400;

const SubscriptionSchema = mongoose.Schema(
  {
    subscriptionId: {
      type: Schema.Types.String,
      required: [true, 'A subscription id must be defined.'],
      trim: true,
    },
    active: {
      type: Schema.Types.Boolean,
    },
    end: {
      type: Schema.Types.Number,
      required: [true, 'A subscription end timestamp is required.'],
    },
  },
  {
    collection: 'Subscriptions',
    timestamp: true,
  },
);

const SubscriptionModel = mongoose.model('Subscriptions', SubscriptionSchema);

export const getRenewingSubscriptions = () => {
  // get a timestamp for today's date at UTC 00:00:00
  const currentDate = new Date().setUTCHours(24, 0, 0, 0) / 1000;

  return SubscriptionModel.find({
    active: true,
    expiring: false,
    type: { $in: ['apple', 'google', 'stripe'] },
    $or: [
      {
        // subscriptions with an end date within a +/- 12 hour range of 27 days from today
        end: {
          $gte: currentDate + 26.5 * SECONDS_PER_DAY,
          $lte: currentDate + 27.5 * SECONDS_PER_DAY,
        },
      },
      {
        // subscriptions with an end date within a +/- 12 hour range of 60 days from today
        end: {
          $gte: currentDate + 59.5 * SECONDS_PER_DAY,
          $lte: currentDate + 60.5 * SECONDS_PER_DAY,
        },
      },
    ],
  }).lean();
};

export default SubscriptionModel;
