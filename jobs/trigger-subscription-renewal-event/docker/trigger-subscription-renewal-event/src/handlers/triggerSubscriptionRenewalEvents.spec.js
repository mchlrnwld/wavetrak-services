import { expect } from 'chai';
import sinon from 'sinon';
import * as subscription from '../external/subscriptions';
import triggerSubscriptionRenewalEvents from './triggerSubscriptionRenewalEvents';
import * as SubscriptionModel from '../models/SubscriptionModel';

describe('handlers / triggerSubscriptionRenewalEvents', () => {
  let triggerEventStub;
  let getRenewingSubscriptionsStub;
  let SubscriptionModelFindStub;
  const eventName = 'Upcoming Subscription Renewal';

  const optionsFixture = {
    log: {
      error: sinon.stub(),
      info: sinon.stub(),
      warn: () => null,
    },
  };

  const renewingSubscriptions = [
    {
      subscriptionId: 'sub_1234',
    },
    {
      subscriptionId: 'sub_2345',
    },
  ];

  beforeEach(() => {
    triggerEventStub = sinon.stub(subscription, 'triggerEvent');
    SubscriptionModelFindStub = sinon.stub(SubscriptionModel.default, 'find');
    SubscriptionModelFindStub.returns({ lean: sinon.stub().returns(renewingSubscriptions) });
  });
  afterEach(() => {
    SubscriptionModelFindStub.restore();
    triggerEventStub.restore();
    optionsFixture.log.info.reset();
    optionsFixture.log.error.reset();
  });

  it('should retrieve renewing subscriptions', async () => {
    await triggerSubscriptionRenewalEvents(optionsFixture);
    expect(SubscriptionModelFindStub).to.have.been.calledOnce();
  });

  it('should call triggerEvent per entry', async () => {
    triggerEventStub.returns({ message: 'success' });
    await triggerSubscriptionRenewalEvents(optionsFixture);
    expect(triggerEventStub).to.have.been.calledWith(
      eventName,
      renewingSubscriptions[0].subscriptionId,
    );
    expect(triggerEventStub).to.have.been.calledWith(
      eventName,
      renewingSubscriptions[1].subscriptionId,
    );
    expect(optionsFixture.log.info).to.have.been.calledWith(
      'Job completed. Subscriptions renewing within time period: 2, Events triggered: 2, Error count: 0',
    );
  });

  it('should throw a fatal error if it cannot retrieve the renewing subscriptions', async () => {
    getRenewingSubscriptionsStub = sinon.stub(SubscriptionModel, 'getRenewingSubscriptions');
    getRenewingSubscriptionsStub.throws(new Error('error'));
    let err;
    try {
      await triggerSubscriptionRenewalEvents(optionsFixture);
    } catch (error) {
      err = error;
    }
    expect(err).to.not.equal(undefined);
    expect(err.message).to.deep.equal('error');
    getRenewingSubscriptionsStub.restore();
  });

  it('should continue if an error occurs when triggering a single event', async () => {
    triggerEventStub.onFirstCall().throws(new Error('error'));
    await triggerSubscriptionRenewalEvents(optionsFixture);
    expect(triggerEventStub).to.have.been.calledWith(
      eventName,
      renewingSubscriptions[0].subscriptionId,
    );
    expect(triggerEventStub).to.have.been.calledWith(
      eventName,
      renewingSubscriptions[1].subscriptionId,
    );
    expect(optionsFixture.log.error).to.have.been.calledOnce();
  });
});
