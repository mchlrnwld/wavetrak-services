/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import { triggerEvent } from '../external/subscriptions';
import { getRenewingSubscriptions } from '../models/SubscriptionModel';

const triggerSubscriptionRenewalEvents = async (opts) => {
  const { log } = opts;
  try {
    const renewingSubscriptions = await getRenewingSubscriptions();

    let successCount = 0;
    let errorCount = 0;
    const totalCount = renewingSubscriptions.length;

    for (const subscription of renewingSubscriptions) {
      const { subscriptionId } = subscription;
      try {
        await triggerEvent('Upcoming Subscription Renewal', subscriptionId);
        successCount += 1;
      } catch (err) {
        log.error({
          message: `Error triggering event for ${subscriptionId}: ${err.message}`,
          stack: err,
        });
        errorCount += 1;
      }
    }

    log.info(
      `Job completed. Subscriptions renewing within time period: ${totalCount}, Events triggered: ${successCount}, Error count: ${errorCount}`,
    );
  } catch (err) {
    log.error({ message: `Fatal Error: ${err.message}`, stack: err });
    throw err;
  }
};

export default triggerSubscriptionRenewalEvents;
