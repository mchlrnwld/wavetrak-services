/* istanbul ignore file */
import axios from 'axios';
import { getConfig } from '../config';

/**
 * @description Makes an external call to the subscription service /integrations/subscriptions/event endpoint
 * @param {string} eventName - the name of the event to be triggered
 * @param {string} subscriptionId - the id of the subscription to trigger an event for
 */
export const triggerEvent = async (eventName, subscriptionId) => {
  const body = {
    eventName,
    subscriptionId,
  };
  const response = await axios.post(
    `http://${getConfig().SUBSCRIPTION_SERVICE_URL}/integrations/subscriptions/event`,
    body,
  );

  return response.data;
};
