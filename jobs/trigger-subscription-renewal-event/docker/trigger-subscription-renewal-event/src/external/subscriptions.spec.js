import axios from 'axios';
import { expect } from 'chai';
import sinon from 'sinon';
import { triggerEvent } from './subscriptions';
import * as config from '../config';

describe('external / subscriptions', () => {
  let axiosPostStub;
  let getConfigStub;

  const triggerEventResponse = {
    data: {
      message: 'Successfully triggered event',
    },
    status: 200,
  };

  beforeEach(() => {
    axiosPostStub = sinon.stub(axios, 'post');
    getConfigStub = sinon.stub(config, 'getConfig').returns({
      LOGSENE_KEY: '',
      LOGSENE_LEVEL: '',
      SUBSCRIPTION_SERVICE_URL: 'dummyurl.com',
      MONGO_CONNECTION_STRING_USERDB: '',
    });
  });

  afterEach(() => {
    axiosPostStub.restore();
    getConfigStub.restore();
  });

  describe('triggerEvent', () => {
    it('should call the subscription service endpoint', async () => {
      axiosPostStub.returns(triggerEventResponse);
      const res = await triggerEvent('Upcoming Subscription Renewal', 'sub_1234');
      expect(axiosPostStub).to.have.been.calledOnceWithExactly(
        `http://dummyurl.com/integrations/subscriptions/event`,
        {
          eventName: 'Upcoming Subscription Renewal',
          subscriptionId: 'sub_1234',
        },
      );
      expect(res).to.deep.equal(triggerEventResponse.data);
    });
  });
});
