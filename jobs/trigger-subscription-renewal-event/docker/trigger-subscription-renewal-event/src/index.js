/* istanbul ignore file */
/* eslint-disable no-console */
import newrelic from 'newrelic';
import mongoose from 'mongoose';
import { initMongo } from '@surfline/services-common';
import { createConfig } from './config';
import { setupLogger } from './utils/logger';
import triggerSubscriptionRenewalEvents from './handlers/triggerSubscriptionRenewalEvents';

const JOB_NAME = 'trigger-subscription-renewal-event';
let config;
let log;

const initialize = async () => {
  try {
    config = await createConfig();
    log = setupLogger(config);
    await initMongo(mongoose, config.MONGO_CONNECTION_STRING_USERDB);
  } catch (err) {
    const message = `${JOB_NAME} job initialization failed: ${err.message}`;
    if (log) log.error({ message, stack: err });
    console.log(message);
    throw err;
  }
};

newrelic.startBackgroundTransaction(JOB_NAME, async () => {
  const transaction = newrelic.getTransaction();
  try {
    await initialize();
    log.info(`Starting: ${JOB_NAME} job on ${Date.now()}`);
    await triggerSubscriptionRenewalEvents({ log });

    log.info(`Completed: ${JOB_NAME} job completed at ${Date.now()}`);
    transaction.end();

    // To guaranteed at least 1 logsene batch
    await new Promise((resolve) => setTimeout(resolve, 20000));

    process.exit(0);
  } catch (err) {
    newrelic.noticeError(err);
    log.error(err, `Failed: ${JOB_NAME} failed at ${Date.now()}`);
    transaction.end();
    process.exit(1);
  }
});
