from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTBranchDockerOperator, WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator

from dag_helpers.docker import docker_image

JOB_NAME = 'process-prerecorded-cam-clips'
WAVETRAK_PRECORDED_CLIPS_BUCKET = Variable.get(
    'WAVETRAK_PRECORDED_CLIPS_BUCKET'
)
CAMERAS_API = Variable.get('CAMERAS_API')
CAM_DETAILS_AIRTABLE_API_KEY = Variable.get('CAM_DETAILS_AIRTABLE_API_KEY')
CAM_DETAILS_AIRTABLE_API = Variable.get('CAM_DETAILS_AIRTABLE_API')
PRECORDED_CLIPS_CDN = Variable.get('PRECORDED_CLIPS_CDN')
SL_SEGMENT_WRITE_KEY = Variable.get('SL_SEGMENT_WRITE_KEY')

# update_cam_stream_url
update_cam_stream_url_environment = {
    'WAVETRAK_PRECORDED_CLIPS_BUCKET': WAVETRAK_PRECORDED_CLIPS_BUCKET,
    'CAMERAS_API': CAMERAS_API,
    'CAM_DETAILS_AIRTABLE_API_KEY': CAM_DETAILS_AIRTABLE_API_KEY,
    'CAM_DETAILS_AIRTABLE_API': CAM_DETAILS_AIRTABLE_API,
    'PRECORDED_CLIPS_CDN': PRECORDED_CLIPS_CDN,
    'SL_SEGMENT_WRITE_KEY': SL_SEGMENT_WRITE_KEY,
}

# DAG definition
default_args = {
    'owner': 'wavetrak',
    'start_date': datetime(2019, 7, 17, 20),
    'email': 'kbygsquad@surfline.com',
    'email_on_failure': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    '{0}-v2'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=5),
    max_active_runs=1,
    default_args=default_args,
)

update_cam_stream_url = WTDockerOperator(
    task_id='update-cam-stream-url',
    image=docker_image('update-cam-stream-url', JOB_NAME),
    environment=update_cam_stream_url_environment,
    force_pull=True,
    dag=dag,
)
