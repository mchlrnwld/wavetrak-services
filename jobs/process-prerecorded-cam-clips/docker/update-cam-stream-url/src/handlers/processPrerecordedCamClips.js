import fs from 'fs';
import ffmpeg from 'fluent-ffmpeg';
import {
  getPrerecordedCamsPendingUpdate,
  updateCam,
  getCameraAirtableData,
  getDahuaClip,
  getAxisClip,
} from '../external/cameras';
import { uploadToS3Bucket } from '../common/aws';
import config from '../config';

const processPrerecordedCamClips = async errors => {
  const dir = config.TMP_DIR;
  const camsPendingUpdate = await getPrerecordedCamsPendingUpdate();
  const { pendingUpdate } = camsPendingUpdate;
  if (pendingUpdate.length === 0) return; 
  console.log(`Cams pending processing: ${pendingUpdate.length}`);
  console.log('------------------------------------------------------------------------------');
  for (const cam of pendingUpdate) {
    const { id, recordingTimeRange } = cam;
    try {
      console.log('Processing cam: ', id);
      const { startDateUTC, endDateUTC } = recordingTimeRange;
      const cameraAirtableData = await getCameraAirtableData(id);
      if (cameraAirtableData.records.length === 0) return false;
      const { fields: { camPubIP, camUserPass, camBrand } } = cameraAirtableData.records[0];
      const videoFileName = `${startDateUTC}-${endDateUTC}-${id}`;
      let prerecordedClip;
      let clipFile;
      if (camBrand === 'Dahua') {
        console.log('Downloading Dahua clip');
        prerecordedClip = await getDahuaClip(
          camPubIP,
          camUserPass,
          startDateUTC,
          endDateUTC,
        );
        clipFile = `${videoFileName}.cgi`;
      } else if (camBrand === 'Axis') {
        console.log('Downloading Axis clip');
        prerecordedClip = await getAxisClip(
          camPubIP,
          camUserPass,
          startDateUTC,
          endDateUTC,
          videoFileName,
        );
        clipFile = `${videoFileName}.mkv`;
      } else {
        return;
      }

      const clipFileWithFullPath = `${dir}/${clipFile}`;
      const mp4File = `${videoFileName}.mp4`;
      const mp4FileWithFullPath = `${dir}/${mp4File}`;
      const clipVideoFile = fs.createWriteStream(clipFileWithFullPath);

      const convertVideoFile = () => new Promise((resolve, reject) => {
        prerecordedClip.body.pipe(clipVideoFile);
        prerecordedClip.body.on('error', (err) => {
          console.log('error creating video file', err);
          reject(err);
        });
        clipVideoFile.on('finish', function() {
          console.log('finished creating clip file');
          ffmpeg(clipFileWithFullPath)
            .output(mp4FileWithFullPath)
            .on('progress', (progress) => {
              console.log('ffmpeg progress...', JSON.stringify(progress));
            })
            .on('error', (err) => {
              console.log('error converting to .mp4');
              reject(err);
            })
            .on('end', () => {
              console.log('finished converting to .mp4');
              resolve();
            })
            .save(mp4FileWithFullPath);
        });
      });  

      await convertVideoFile();

      const s3Upload = await uploadToS3Bucket({
        Body: fs.createReadStream(mp4FileWithFullPath),
        Bucket: config.WAVETRAK_PRECORDED_CLIPS_BUCKET,
        Key: mp4File,
      });
      if (s3Upload) {
        const streamUrl = `${config.PRECORDED_CLIPS_CDN}/${mp4File}`;
        await updateCam(id, recordingTimeRange, streamUrl);
        console.log('updated cam CMS with new stream URL');
      }
      fs.unlink(mp4FileWithFullPath, () => {});
      fs.unlink(clipFileWithFullPath, () => {});
      console.log('Success processing: ', id);
    } catch (error) {
      console.log('Failure processing: ', id);
      errors.push({ camId: id, ...error });
    }
    console.log('------------------------------------------------------------------------------');
  };
};

export default processPrerecordedCamClips;
