import fetch from 'node-fetch';
import { format } from 'date-fns';
import digest from 'digest-header';
import xmlJs from 'xml-js';
import config from '../config';
import createParamString from '../utils/createParamString';

const getUTCDate = (date = new Date()) => { 
  return new Date(date.getTime() + date.getTimezoneOffset()*60*1000)
}

export const updateCam = async (id, recordingTimeRange, streamUrl) => {
  const { startDateUTC, endDateUTC } = recordingTimeRange;
  const url = `${config.CAMERAS_API}/cameras/${id}/UpdateCamera`;
  const body = {
    lastPrerecordedClipEndTimeUTC: endDateUTC,
    lastPrerecordedClipStartTimeUTC: startDateUTC,
    streamUrl,
  };
  const response = await fetch(url, {
    method: 'put',
    body:    JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' },
  })

  const bodyResponse = await response.json();

  if (response.status === 200) return bodyResponse;
  throw bodyResponse;
};

export const getPrerecordedCamsPendingUpdate = async () => {
  const url = `${config.CAMERAS_API}/cameras/prerecorded/pendingUpdate`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export const getCameraAirtableData = async (id) => {
  const url = `${config.CAM_DETAILS_AIRTABLE_API}/Data?api_key=${config.CAM_DETAILS_AIRTABLE_API_KEY}&filterByFormula=id="${id}"`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export const getDahuaClip = async (
  camPubIp,
  camUserPass,
  startDateUTC,
  endDateUTC,
) => {
  const startTime = format(getUTCDate(new Date(startDateUTC)), 'YYYY-MM-DD HH:mm:00');
  const endTime = format(getUTCDate(new Date(endDateUTC)), 'YYYY-MM-DD HH:mm:00');
  const domain = camPubIp;
  const path = '/cgi-bin/loadfile.cgi';
  const params = createParamString({
    action: 'startLoad',
    channel: 1,
    subtype: 0,
    startTime,
    endTime,
  });
  const fullUrl = `http://${domain}${path}?${params}`;
  const response = await fetch(fullUrl);
  const wwwAuthenticate = response.headers.get('www-authenticate');
  const auth = digest(
    'GET',
    path,
    wwwAuthenticate,
    camUserPass,
  );
  const authenticatedResponse = await fetch(fullUrl, {
    method: 'get',
    headers: { 'Authorization': auth },
  })
  if (authenticatedResponse.status === 200) return authenticatedResponse;
  throw new Error(
    `Failed to get prerecorded clip from camera: ${authenticatedResponse.statusText}`
  );
};

export const getAxisClip = async (
  camPubIp,
  camUserPass,
  startDateUTC,
  endDateUTC,
  filename,
) => {
  const baseUrl = `http://${camUserPass}@${camPubIp}/axis-cgi/record`;
  const startTime = format(getUTCDate(new Date(startDateUTC)), 'YYYY-MM-DDTHH:mm:00');
  const stopTime = format(getUTCDate(new Date(endDateUTC)), 'YYYY-MM-DDTHH:mm:00');
  const recXMLResFullURL = `${baseUrl}/list.cgi?starttime=${startTime}&stoptime=${stopTime}`;
  const recXMLRes = await fetch(recXMLResFullURL);
  const authenticatedRecXMLres = await fetch(recXMLResFullURL, {
    method: 'get',
    headers: { 'Authorization': digest(
      'GET',
      '/axis-cgi/record/list.cgi',
      recXMLRes.headers.get('www-authenticate'),
      camUserPass,
    )},
  })
  
  const recXML = await authenticatedRecXMLres.text();
  const recJSON = JSON.parse(xmlJs.xml2json(recXML, { compact: true }));
  const { root: { recordings: { recording } } } = recJSON;
  const {
    _attributes: { recordingid, diskid }
  } = recording;
  const exportParams = createParamString({
    schemaversion: 1,
    'Axis-Orig-Sw': true,
    exportformat: 'matroska',
    recordingid,
    diskid,
    starttime: startTime,
    stoptime: stopTime,
    filename,
  });

  const exportURL = `${baseUrl}/export/exportrecording.cgi?${exportParams}`;
  const response = await fetch(exportURL);
  const authenticatedResponse = await fetch(exportURL, {
    method: 'get',
    headers: { 'Authorization': digest(
      'GET',
      '/axis-cgi/record/export/exportrecording.cgi',
      response.headers.get('www-authenticate'),
      camUserPass,
    )},
  })



  if (authenticatedResponse.status === 200) return authenticatedResponse;
  throw new Error(
    `Failed to get prerecorded clip from camera: ${authenticatedResponse.statusText}`
  );
}
