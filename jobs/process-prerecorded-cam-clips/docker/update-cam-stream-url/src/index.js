import processPrerecordedCamClips from './handlers/processPrerecordedCamClips';
import fs from 'fs';
import config from './config';

console.log(`Start: update-cam-stream-url job on ${Date.now()}`);

const errors = [];

processPrerecordedCamClips(errors).then(() => {
  console.log(`Completed: update-cam-stream-url job completed at ${Date.now()}`);
  if (errors.length > 0) console.log('Errors: ', errors);
}).catch((err) => {
  console.log(`Failed: update-cam-stream-url failed at ${Date.now()}`, err);
  process.exit(1);
});
