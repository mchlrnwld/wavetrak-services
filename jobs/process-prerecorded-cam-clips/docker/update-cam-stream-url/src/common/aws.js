import AWS from 'aws-sdk';
import config from '../config';

export const uploadToS3Bucket = (params) => {
  const s3 = new AWS.S3();
  const { fileName, path } = params;
  return new Promise((resolve, reject) => {
    s3.putObject(params, (err) => {
      if (err) {
        return reject(err);
      }
      return resolve(params);
    });
  });
}
