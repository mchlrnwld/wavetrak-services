import Analytics from 'analytics-node';

const bwAnalytics = new Analytics(process.env.BW_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const fsAnalytics = new Analytics(process.env.FS_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const slAnalytics = new Analytics(process.env.SL_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });

const analytics = {
  bw: bwAnalytics,
  fs: fsAnalytics,
  sl: slAnalytics,
}

export const track = (brand, params) => {
  analytics[brand].track(params);
};

export const identify = (brand, params) => {
  analytics[brand].identify(params);
};
