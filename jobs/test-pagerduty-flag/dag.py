from datetime import datetime, timedelta
import logging

from airflow.models import DAG, Variable
from airflow.operators import BashOperator, DummyOperator, PythonOperator

from dag_helpers.pager_duty import create_pager_duty_failure_callback

ENVIRONMENT = Variable.get('ENVIRONMENT')
APP_ENV = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
JOB_NAME = 'test-pagerduty-flag'


def raise_error(ds, **kwargs):
    raise ValueError('This is a ValueError raised explicitly')


pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_platform_squad',
    JOB_NAME,
    APP_ENV
)

five_minutes_ago = datetime.combine(
    datetime.today() - timedelta(minutes=5), datetime.min.time()
)

args = {
    'owner': 'airflow',
    'start_date': five_minutes_ago,
    'on_failure_callback': pager_duty_failure_callback,
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    default_args=args,
    schedule_interval='5 * * * *',
    dagrun_timeout=timedelta(minutes=20),
)

run_this_first = BashOperator(
    task_id='run_this_first', bash_command='echo 1', dag=dag
)

run_this_second = BashOperator(
    task_id='run_this_second', bash_command='echo 2', dag=dag
)

run_this_third = PythonOperator(
    task_id='run_this_third',
    python_callable=raise_error,
    provide_context=True,
    dag=dag,
)

run_this_last = DummyOperator(task_id='run_this_last', dag=dag)

run_this_first.set_downstream(run_this_second)
run_this_second.set_downstream(run_this_third)
run_this_third.set_downstream(run_this_last)

if __name__ == "__main__":
    dag.cli()
