# Airflow Jobs

Airflow jobs including DAG definitions and Docker images to be executed as tasks.

## Table of Contents

<!-- Update TOC with `doctoc --notitle --maxlevel 3 README.md` -->

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Conventions](#conventions)
  - [Job Directory Structure](#job-directory-structure)
  - [Run Task on Custom EC2 Instance](#run-task-on-custom-ec2-instance)
- [Development](#development)
  - [Install Airflow](#install-airflow)
  - [Run Airflow](#run-airflow)
  - [Execute DAG](#execute-dag)
- [Variables](#variables)
  - [AWS Secrets Manager](#aws-secrets-manager)
  - [Airflow Variables (DEPRECATED)](#airflow-variables-deprecated)
- [Best Practices](#best-practices)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Conventions

### Job Directory Structure

[deploy-airflow-job](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-airflow-job) deploys Airflow jobs following certain conventions in the directory structure:

- Each job should be defined within a separate directory.
- A DAG definition will be defined in a Python file at the root level with the same name as the job directory's name.
- Docker images to be used in tasks should be defined in a `docker` directory within the job directory.

#### Example

```
sample-airflow-job
├-- dag.py
└-- docker
    ├-- task1
    |   └-- Dockerfile
    ├-- task2
    |   └-- Dockerfile
    └-- task3
        └-- Dockerfile
```

### Run Task on Custom EC2 Instance

[run-on-ec2](https://github.com/Surfline/docker-run-on-ec2) Docker image allows us to to launch temporary EC2 instances according to a launch template so that we have control over the virtual hardware for our tasks. To allow our Airflow Celery workers control over these instances, the `NAME` variable must be prefixed with `airflow/`.

## Development

We are using [Airflow 1.7.1.3](https://github.com/apache/incubator-airflow/tree/1.7.1.3/airflow) with Python 2.7.13. [Click here for documentation](http://sl-docs.s3-us-west-1.amazonaws.com/tools/airflow-2.7.1.3/index.html)

### Install Airflow

```sh
$ pip install airflow==1.7.1.3
$ airflow initdb
```

NOTE: Do not use `pip install apache-airflow` or `conda install airflow` as both of these only support `>1.8.1`.

### Run Airflow

#### Webserver

Airflow webserver loads DAGs into the admin dashboard for viewing logs and DAG statuses.

```sh
$ airflow webserver -p 8080
```

Global variables can be setup at http://localhost:8080/admin/variable/.

#### Scheduler

Running the scheduler service is helpful for debugging DAG execution times and task concurrency.

```sh
$ airflow scheduler
```

### Execute DAG

DAG definition files must live at `$AIRFLOW_HOME/dags`. `$AIRFLOW_HOME=~/airflow` by default.

To execute an instance of a DAG

```sh
airflow trigger_dag sample_dag_id
```

To execute a task instance of a DAG

```sh
airflow run -f -l sample_dag_id sample_task_id 2018-10-08
```

## Variables

### AWS Secrets Manager

AWS Secrets Manager is the preferred way of handling variables in Airflow jobs.

#### Manage variables in [AWS Secrets Manager](https://us-west-1.console.aws.amazon.com/secretsmanager/home?region=us-west-1#/listSecrets)

Variables should be manually managed in AWS Secrets Manager. `prod` variables should be managed in
`surfline-prod`. `dev` variables should be managed in `surfline-dev`

#### All job-specific variables must be prefixed with the environment and full job name

Use the following naming format:

```
{env}/jobs/{full-job-name}/{ENVIRONMENT_VARIABLE_NAME}
```

Examples:

```
prod/jobs/check-and-update-camera-time-settings/AIRTABLE_API_KEY
prod/jobs/check-and-update-camera-time-settings/AIRTABLE_APP_ID
prod/jobs/check-and-update-camera-time-settings/AIRTABLE_VIEW
prod/jobs/check-and-update-camera-time-settings/SLACK_CHANNEL
```

#### Use shared variables between jobs where appropriate

It doesn't make sense to repeat the same value several times for each job
that uses it. We should also keep in mind ramifications of
changing shared variables.

Use the following naming format:

```
{env}/common/{ENVIRONMENT_VARIABLE_NAME}
```

Examples:

```
prod/common/CAMERAS_API
prod/common/ECR_HOST
prod/common/SCIENCE_DATA_SERVICE
prod/common/SCIENCE_MODELS_DB
prod/common/SCIENCE_PLATFORM_PSQL_JOB
prod/common/WT_SLACK_API_TOKEN
```

#### Minimize the number of variables stored in AWS Secrets Manager

If a value is unlikely to ever change, we should rethink whether we should
parameterize it in an AWS Secrets Manager variable. It's a judgement call
as to which variables to store in AWS Secrets Manager. A good rule of thumb
is if a variable contains secure information or if it changes across
environments, we should store it in AWS Secrets Manager. Otherwise we should
store it in code.

#### Use helper libraries to access variables in AWS Secrets Manager

If you're using Python, you can use the
[job-helpers](https://github.com/Surfline/wavetrak-services/tree/master/common/job-helpers)
library to access secrets.

Example:

```python
from job_secrets_helper import get_secret

ENV = os.environ['ENVIRONMENT']
JOB_NAME = os.environ['JOB_NAME']

# Job-specific variable
AIRTABLE_API_KEY = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'AIRTABLE_API_KEY')

# Common variable
CAMERAS_API = get_secret(f'{ENV}/common/', 'CAMERAS_API')

# Unset variable handling
CAM_LIMIT = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'CAM_LIMIT', '') or None
```

You can see a live example of this in the
[check-and-update-camera-time-settings](https://github.com/Surfline/wavetrak-services/blob/master/jobs/check-and-update-camera-time-settings/docker/check-and-update-camera-time-settings/lib/config.py)
Airflow job.

The `job_secrets_helper` function uses the following logic:

1. If the secret exists in the environment, use it.
2. Otherwise get the secret value from AWS Secret Manager.
3. If the secret doesn't exist there either, raise exception.

This allows you to use environment variables when developing locally and AWS Secrets
Manager variables when deploying to `dev`/`prod`.

For Javascript jobs, we should create a secrets helper package.

#### For `dev` Airflow jobs, use `sandbox` as the secrets environment

We're in a confusing state where Airflow uses `dev`/`prod` environements
and services use `sandbox`/`staging`/`prod` environments. As we transition
Airflow to use the same environments as services, we should use `sandbox`
as the environment for variables in Airflow dev jobs.

--

### Airflow Variables (DEPRECATED)

_Note: This method of variable management is no longer used. Legacy jobs may
use this method but all new jobs should use AWS Secrets Manager to manage
variables._

Airflow job variables should be stored in Airflow under "Admin &gt; Variables",
with the following guidelines governing their use:

#### All job-specific variables must be prefixed with the full job name

For example, we use `CAM_TIME_UPDATE_` as the prefix for these variables:

```
CAM_TIME_UPDATE_AIRTABLE_API_KEY
CAM_TIME_UPDATE_AIRTABLE_APP_ID
CAM_TIME_UPDATE_AIRTABLE_VIEW
CAM_TIME_UPDATE_SLACK_API_TOKEN
CAM_TIME_UPDATE_SLACK_CHANNEL
```

#### Use shared variables between jobs where appropriate

It doesn't make sense to repeat the same value several times for each job
that uses it, although we should also keep in mind ramifications of
changing shared variables. We should prefix shared variables with `COMMON_`
and then the variable name.

#### Minimize the number of variables stored in Airflow

If a value is unlikely to ever change, we should rethink whether we should
parameterize it in an airflow variable.

## Best Practices

Airflow, especially older versions, has a lot of gotchas that can be avoided by following best practices:

- https://airflow.apache.org/docs/apache-airflow/stable/best-practices.html
- https://gtoonstra.github.io/etl-with-airflow/
- https://github.com/jghoman/awesome-apache-airflow/
