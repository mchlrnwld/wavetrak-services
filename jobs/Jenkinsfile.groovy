MULTI_JOB_BUILDS = [
    'download-*': [
        'download-nasa-mur-sst',
        'download-noaa-nam',
    ],
    'process-forecast-platform-*': [
        'process-forecast-platform-nasa-mur-sst',
        'process-forecast-platform-noaa-gefs-wave',
        'process-forecast-platform-noaa-gfs',
        'process-forecast-platform-noaa-nam',
        'process-forecast-platform-wavetrak-lotus-ww3',
    ],
    'process-full-grid-*': [
        'process-full-grid-noaa-gfs',
        'process-full-grid-wavetrak-lotus-ww3',
    ],
]

if (MULTI_JOB_BUILDS.containsKey(env.JOB)) {
    triggerBuilds(MULTI_JOB_BUILDS.get(env.JOB))
} else {
    node {
        stage('Clean and Setup Workspace') {
            deleteDir();
            checkout scm;
            configFileProvider([configFile(fileId: 'aws_assume_role_mappings', variable: 'AWS_ASSUME_ROLE_MAPPING')]) {
                dir("${HOME}/.aws") {
                    sh("cp ${AWS_ASSUME_ROLE_MAPPING} roles");
                }
            }

            GIT_COMMIT = sh(returnStdout: true, script: 'git rev-parse HEAD').trim();
            WORKING_DIRECTORY = "${pwd()}/jobs/${env.JOB}";
            REGISTRY_HOST = '833713747344.dkr.ecr.us-west-1.amazonaws.com';
            AIRFLOW_ADMIN_HOST = "${env.ENVIRONMENT}-airflow-celery-1.aws.surfline.com";
            BUILT_IMAGES = [];

            dir(WORKING_DIRECTORY) {
                sh("""
                    export PATH=\"\${PATH}:/usr/local/bin\"
                    export AWS_DEFAULT_REGION=us-west-1
                    eval \"\$(assume-role ${env.ENVIRONMENT})\"
                    echo \"Getting secrets from Secrets Manager:\"
                    aws secretsmanager get-secret-value --secret-id ${env.ENVIRONMENT}/deployment/pip/pip.conf | jq .SecretString -r > pip.conf
                    aws secretsmanager get-secret-value --secret-id ${env.ENVIRONMENT}/deployment/npm/npmrc | jq .SecretString -r > .npmrc
                   """)
            }

            currentBuild.description = "${env.JOB} - ${GIT_COMMIT} - ${env.ENVIRONMENT}"
        }

        stage('Build Docker Images') {
            dir(WORKING_DIRECTORY) {
                IMAGE_FILES = new File("${WORKING_DIRECTORY}/docker").listFiles();
            }

            if (IMAGE_FILES != null) {
                for (int i = 0; i < IMAGE_FILES.size(); i++) {
                    def imageFile = IMAGE_FILES[i];

                    dir(imageFile.getAbsolutePath()) {
                        sh("cp ${WORKING_DIRECTORY}/pip.conf .")
                        sh("cp ${WORKING_DIRECTORY}/.npmrc .")
                        BUILT_IMAGES << buildImage(env.JOB, imageFile.name);
                    }
                }
            }
        }

        stage('Run Integration Tests') {
            dir(WORKING_DIRECTORY) {
                IMAGE_FILES = new File("${WORKING_DIRECTORY}/docker").listFiles();
            }

            for (image in IMAGE_FILES) {
                TASK_FILES = image.listFiles();
                for (file in TASK_FILES){
                    if (file.getName().contains("ci.json")){
                        TEST_COMMAND=sh(script: "jq -r .test.command ${image}/ci.json", returnStdout: true)
                        if (TEST_COMMAND != null){
                            dir("${image}"){
                                sh("""
                                    export PATH=\"\${PATH}:/usr/local/bin\"
                                    ${TEST_COMMAND}
                                """)
                            }
                        }
                    }
                }
            }
        }

        stage('Deploy Docker Images') {
            sh("\$(aws --profile=prod ecr get-login --no-include-email)");

            for (int i = 0; i < BUILT_IMAGES.size(); i++) {
                deployImage(BUILT_IMAGES[i], env.ENVIRONMENT);
                if (env.ENVIRONMENT == 'dev') {
                    deployImage(BUILT_IMAGES[i], 'sandbox');
                }
            }
        }

        if (env.JOB != 'common' && fileExists("${WORKING_DIRECTORY}/dag.py")) {
            stage('Deploy Job to Airflow') {
                dir(WORKING_DIRECTORY) {
                    sh("scp -o StrictHostKeyChecking=no -i /opt/ssh/surfline-science-dev.pem ${WORKING_DIRECTORY}/dag.py airflow@${AIRFLOW_ADMIN_HOST}:/ocean/static/airflow/dags/${env.JOB}.py");
                }
            }

            stage('Restart Airflow Webserver') {
                def airflowIps = sh(returnStdout: true, script: "aws --region=us-west-1 --profile=${env.ENVIRONMENT} ec2 describe-instances --filters Name=tag:type,Values=airflow-celery Name=instance-state-name,Values=running | jq \".Reservations[].Instances[].PrivateIpAddress\"").replace('"', '').split('\n');
                for (int i = 0; i < airflowIps.size(); i++) {
                    sh("ssh -o StrictHostKeyChecking=no -i /opt/ssh/surfline-science-dev.pem airflow@${airflowIps[i]} sudo /sbin/restart airflow-webserver");
                }
            }
        }
    }
}

def buildImage(job, image) {
    def builtImage = "${REGISTRY_HOST}/jobs/${job}/${image}";
    if (fileExists("pre-build.sh")) {
        sh("sh ./pre-build.sh");
    }

    sh("docker build -t ${builtImage} .");

    return builtImage;
}

def deployImage(builtImage, tag) {
    def imageTag = "${builtImage}:${tag}"
    def imageTagWithSHA = "${imageTag}-${GIT_COMMIT}"

    sh("docker tag ${builtImage} ${imageTag}")
    sh("docker tag ${builtImage} ${imageTagWithSHA}")
    sh("docker push ${imageTag}");
    sh("docker push ${imageTagWithSHA}");
}

def triggerBuilds(jobs) {
    for (int i = 0; i < jobs.size(); i++) {
        job = jobs[i]
        build(job: env.JOB_BASE_NAME, parameters: [
            [
                $class: 'StringParameterValue',
                name: 'JOB',
                value: job,
            ],
            [
                $class: 'StringParameterValue',
                name: 'ENVIRONMENT',
                value: env.ENVIRONMENT,
            ],
            [
                $class: 'StringParameterValue',
                name: 'VERSION',
                value: env.VERSION,
            ],
        ], wait: false)
    }
}
