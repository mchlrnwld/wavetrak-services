# Download Sofar Data to S3

Downloads Sofar [Wave Spectra](https://docs.sofarocean.com/wave-spectra) and [Spotter Sensor](https://docs.sofarocean.com/spotter-sensor) data to S3 for our science team to analyze and validate the data.

## Setup

```sh
conda devenv
conda activate temporary-sofar-validation-downloads-download-sofar-data-to-s3
cp .env.sample .env
env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
docker build -t temporary-sofar-validation-downloads/download-sofar-data-to-s3 .
cp .env.sample .env
docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data temporary-sofar-validation-downloads/download-sofar-data-to-s3
```
