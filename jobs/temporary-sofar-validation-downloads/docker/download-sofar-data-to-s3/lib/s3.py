import asyncio
from functools import partial
from io import BytesIO

import boto3  # type: ignore
from boto3.s3.transfer import TransferConfig  # type: ignore


class S3Client:
    def __init__(self, bucket: str):
        self.client = boto3.client('s3')
        self.config = TransferConfig(max_concurrency=1)
        self.bucket = bucket

    async def upload_fileobj(self, data, key):
        loop = asyncio.get_running_loop()
        loop.run_in_executor(
            None,
            partial(
                self.client.upload_fileobj,
                BytesIO(data),
                self.bucket,
                key,
                Config=self.config,
            ),
        )
