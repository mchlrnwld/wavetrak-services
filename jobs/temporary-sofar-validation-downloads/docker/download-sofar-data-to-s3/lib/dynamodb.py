import asyncio
import logging
from functools import partial
from typing import Any, Dict, List

import boto3  # type: ignore

logger = logging.getLogger('download-sofar-data-to-s3')

BATCH_WRITE_CHUNK_SIZE = 25


class DynamoDB:
    def __init__(self, semaphore):
        self.client = boto3.client('dynamodb')
        self.semaphore = semaphore

    def _create_dynamo_item(
        self, spotter_id: str, record: Dict[str, Any]
    ) -> Dict[str, Dict[str, Any]]:
        return {
            'spotter_id': {
                'S': spotter_id,
            },
            'timestamp': {
                'S': record['timestamp'],
            },
            'latitude': {
                'N': str(record['latitude']),
            },
            'longitude': {
                'N': str(record['longitude']),
            },
            'significantWaveHeight': {
                'N': str(record['significantWaveHeight']),
            },
            'peakPeriod': {
                'N': str(record['peakPeriod']),
            },
            'meanPeriod': {
                'N': str(record['meanPeriod']),
            },
            'peakDirection': {
                'N': str(record['peakDirection']),
            },
            'peakDirectionalSpread': {
                'N': str(record['peakDirectionalSpread']),
            },
            'meanDirection': {
                'N': str(record['meanDirection']),
            },
            'meanDirectionalSpread': {
                'N': str(record['meanDirectionalSpread']),
            },
        }

    async def batch_write_spotter_data(
        self,
        spotter_id: str,
        data: List[Dict[str, Any]],
        table: str,
    ):
        async with self.semaphore:
            logger.info(f'Writing {len(data)} records for {spotter_id}...')
            loop = asyncio.get_running_loop()
            # HACK: Use dict to remove duplicate timestamps from list
            filtered_data = list(
                {record['timestamp']: record for record in data}.values()
            )

            for j, batch in enumerate(
                [
                    filtered_data[i : i + BATCH_WRITE_CHUNK_SIZE]
                    for i in range(
                        0, len(filtered_data), BATCH_WRITE_CHUNK_SIZE
                    )
                ]
            ):
                request_items = {
                    table: [
                        {
                            'PutRequest': {
                                'Item': self._create_dynamo_item(
                                    spotter_id,
                                    record,
                                ),
                            },
                        }
                        for record in batch
                    ]
                }
                await loop.run_in_executor(
                    None,
                    partial(
                        self.client.batch_write_item,
                        RequestItems=request_items,
                    ),
                )
