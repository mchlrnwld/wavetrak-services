import re
from datetime import datetime
from typing import Any, Dict, List, Tuple

from aiohttp import ClientSession
from netCDF4 import Dataset  # type: ignore

SPECTRAL_FILE_NAME_REGEX = re.compile(r'filename="(.+)"')


class Sofar:
    def __init__(self, api_token: str):
        self.url = 'https://api.sofarocean.com/api'
        self.session = ClientSession(headers={'token': api_token})

    async def __aenter__(self):
        await self.session.__aenter__()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.session.__aexit__(exc_type, exc_val, exc_tb)

    async def list_spotter_ids(self) -> List[str]:
        async with self.session.get(f'{self.url}/devices') as response:
            data = await response.json()
            return [device['spotterId'] for device in data['data']['devices']]

    async def get_spotter_wave_data(
        self, spotter_id: str
    ) -> List[Dict[str, Any]]:
        params = {'spotterId': spotter_id}
        async with self.session.get(
            f'{self.url}/wave-data', params=params
        ) as response:
            data = await response.json()
            return data['data']['waves']

    async def list_wave_spectra_locations(self) -> List[Tuple[float, float]]:
        async with self.session.get(f'{self.url}/op-wave-spectra') as response:
            data = await response.json()
            return [
                (point['latitude'], point['longitude'])
                for point in data['data']
            ]

    async def get_spectral_file(
        self, latitude: float, longitude: float
    ) -> Tuple[bytes, str]:
        async with self.session.get(
            f'{self.url}/op-wave-spectra/{latitude}/{longitude}'
        ) as response:
            response.raise_for_status()
            file_name_match = SPECTRAL_FILE_NAME_REGEX.search(
                response.headers['Content-Disposition']
            )
            if not file_name_match:
                raise Exception('Missing file name.')

            data = await response.read()
            nc = Dataset('inmemory.nc', memory=data)
            run = f'{datetime.utcfromtimestamp(nc["time"][0]).isoformat()}Z'

            file_name = file_name_match.group(1).replace('undefined', run)
            return data, file_name
