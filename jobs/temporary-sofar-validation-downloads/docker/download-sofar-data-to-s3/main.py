import asyncio
import logging
import os
import re
from timeit import default_timer

import awslambdaric.bootstrap as bootstrap  # type: ignore

import lib.config as config
from lib.dynamodb import DynamoDB
from lib.s3 import S3Client
from lib.sofar import Sofar

logger = logging.getLogger('download-sofar-data-to-s3')

DATE_REGEX = re.compile(r'\d{4}-\d{2}-\d{2}')


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    semaphore = asyncio.Semaphore(config.MAX_CONCURRENCY)

    async with Sofar(config.SOFAR_API_TOKEN) as sofar:
        logger.info('Requesting Spotter Sensor data...')
        start = default_timer()

        spotter_ids = await sofar.list_spotter_ids()
        if config.SPOTTER_LIMIT:
            spotter_ids = spotter_ids[: config.SPOTTER_LIMIT]

        logger.info(f'{len(spotter_ids)} spotter sensor devices found.')

        wave_data = await asyncio.gather(
            *[
                sofar.get_spotter_wave_data(spotter_id)
                for spotter_id in spotter_ids
            ]
        )

        elapsed = default_timer() - start
        logger.info(f'Requested Spotter Sensor data in {elapsed}s.')

        logger.info('Writing Spotter Sensor data to DynamoDB...')
        start = default_timer()

        dynamodb = DynamoDB(semaphore)
        await asyncio.gather(
            *[
                dynamodb.batch_write_spotter_data(
                    spotter_id,
                    data,
                    config.SPOTTER_TABLE,
                )
                for spotter_id, data in zip(spotter_ids, wave_data)
            ]
        )

        elapsed = default_timer() - start
        logger.info(f'Wrote Spotter Sensor data to DynamoDB in {elapsed}s.')

        logger.info('Downloading wave spectral files to S3...')
        start = default_timer()

        wave_spectra_locations = await sofar.list_wave_spectra_locations()
        s3_client = S3Client(config.SCIENCE_BUCKET)

        async def download_spectral_file_to_s3(
            latitude: float, longitude: float
        ):
            async with semaphore:
                logger.info(
                    f'Downloading spectral file for '
                    f'{latitude}, {longitude} to S3...'
                )
                try:
                    data, file_name = await sofar.get_spectral_file(
                        latitude, longitude
                    )
                except Exception:
                    logger.error(
                        f'Download failed for {latitude}, {longitude}.'
                    )
                    raise

                await s3_client.upload_fileobj(
                    data, f'{config.SCIENCE_KEY_PREFIX}/{file_name}'
                )

        errors = [
            result
            for result in await asyncio.gather(
                *[
                    download_spectral_file_to_s3(latitude, longitude)
                    for latitude, longitude in wave_spectra_locations
                ],
                return_exceptions=True,
            )
            if isinstance(result, Exception)
        ]
        if errors:
            logger.error(f'{len(errors)} downloads failed.')

        elapsed = default_timer() - start
        logger.info(f'Downloaded wave spectral files to S3 in {elapsed}s.')


def handler(event, context):
    asyncio.run(main())


if __name__ == '__main__':
    if config.AWS_LAMBDA_RUNTIME_API:
        bootstrap.run(
            os.getcwd(), 'main.handler', config.AWS_LAMBDA_RUNTIME_API
        )
    else:
        handler(None, None)
