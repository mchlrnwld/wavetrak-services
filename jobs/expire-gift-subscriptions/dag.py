from datetime import datetime, timedelta

from airflow.models import Variable
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators import WTDockerOperator

from dag_helpers.docker import docker_image

ENV = Variable.get('ENVIRONMENT')
APP_ENVIRONMENT = 'sandbox' if ENV == 'dev' else ENV
JOB_NAME = 'expire-gift-subscriptions'
BW_SEGMENT_WRITE_KEY = Variable.get('BW_SEGMENT_WRITE_KEY')
FS_SEGMENT_WRITE_KEY = Variable.get('FS_SEGMENT_WRITE_KEY')
SL_SEGMENT_WRITE_KEY = Variable.get('SL_SEGMENT_WRITE_KEY')

expire_giftsubs_envs = {
    'NEW_RELIC_ENABLED': False,
    'NODE_ENV': APP_ENVIRONMENT,
    'SL_SEGMENT_WRITE_KEY': SL_SEGMENT_WRITE_KEY,
    'BW_SEGMENT_WRITE_KEY': BW_SEGMENT_WRITE_KEY,
    'FS_SEGMENT_WRITE_KEY': FS_SEGMENT_WRITE_KEY,
}

# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2018, 10, 30, 22),
    'email': 'subscriptionjobs@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    '{0}_v1'.format(JOB_NAME),
    schedule_interval='@daily',
    default_args=default_args,
)

expire_giftsubs = WTDockerOperator(
    task_id='expire-gift-subscriptions',
    image=docker_image('expire-gift-subscriptions', JOB_NAME),
    environment=expire_giftsubs_envs,
    force_pull=True,
    dag=dag,
)
