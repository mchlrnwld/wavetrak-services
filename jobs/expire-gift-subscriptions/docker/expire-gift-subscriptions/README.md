# Expire Gift Subscriptions

Docker image that runs a node script for expiring Gift Subscriptions

## Docker

### Build and Run

```
$ docker build -t expire-gift-subscriptions/expire-gift-subscriptions .
$ cp .env.sample .env
$ docker run --env-file=.env expire-gift-subscriptions/expire-gift-subscriptions
```
