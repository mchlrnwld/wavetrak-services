import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

export const initMongoDB = (connectionString) => {
  const mongoDbConfig = {
    poolSize: 50,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      console.log(`Connecting to database...`);
      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        console.log(`Connected to database.`);
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        console.log('MongoDB:ConnectionError: ', error);
        reject(error);
      });
    } catch (error) {
      console.log('Database Initialization Error: ', error);
      reject(error);
    }
  });
};

export const disconnectMongoDB = () => new Promise((resolve, reject) => {
  try {
    mongoose.connection.close(() => {
      console.log('Disconnected from database.');
      resolve();
    });
  } catch (error) {
    console.log('Database Separation Error: ', error);
    reject(err);
  }
});
