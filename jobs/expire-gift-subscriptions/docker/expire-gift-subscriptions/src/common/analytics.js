import Analytics from "analytics-node";

const bwAnalytics = new Analytics(
  process.env.BW_SEGMENT_WRITE_KEY || "NoWrite",
  { flushAt: 1 }
);
const fsAnalytics = new Analytics(
  process.env.FS_SEGMENT_WRITE_KEY || "NoWrite",
  { flushAt: 1 }
);
const slAnalytics = new Analytics(
  process.env.SL_SEGMENT_WRITE_KEY || "NoWrite",
  { flushAt: 1 }
);

const analytics = {
  bw: bwAnalytics,
  fs: fsAnalytics,
  sl: slAnalytics,
};

export const track = (brand, params) => {
  analytics[brand].track(params);
};

export const identify = (brand, params) => {
  analytics[brand].identify(params);
};

export const trackSubscriptionDeleted = async (
  brand,
  subscription,
  userInfoId
) => {
  const subscriptionTraits = {
    "subscription.entitlement": subscription.entitlement,
    "subscription.plan_id": subscription.planId,
    "subscription.active": false,
    "subscription.in_trial": false,
    "subscription.in_grace_period": false,
    "subscription.trial_eligible": false,
    "subscription.interval": subscription.interval,
    "subscription.interval_count": 1,
  };

  const identifyTraits = subscriptionTraits;

  identify(brand, {
    userId: userInfoId.toString(),
    traits: identifyTraits,
  });

  track(brand, {
    event: "Expired Subscription",
    userId: userInfoId.toString(),
    properties: {
      planId: subscription.planId,
      subscriptionId: subscription.subscriptionId,
      platform: "web",
    },
  });
};
