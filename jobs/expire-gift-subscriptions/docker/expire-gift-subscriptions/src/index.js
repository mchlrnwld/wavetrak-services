import { initMongoDB, disconnectMongoDB } from './common/dbContext';
import expireSubs from './handlers/expire';
import { initConfig } from './config';

console.log(`Start: expire-gift/vip-subscriptions job on ${Date.now()}`);

const init = async () => {
  const config = await initConfig();
  await initMongoDB(config.MONGO_CONNECTION_STRING_USERDB);
};

init().then(() => {
  Promise.all([
    expireSubs('bw', 'gift', 'bw_premium'),
    expireSubs('bw', 'vip', 'bw_vip'),
    expireSubs('fs', 'gift', 'fs_premium'),
    expireSubs('fs', 'vip', 'fs_vip'),
    expireSubs('sl', 'gift', 'sl_premium'),
    expireSubs('sl', 'vip', 'sl_vip'),
    expireSubs('sl', 'vip', 'sl_vip_advertiser'),
  ]).then(() => {
    console.log(`Completed: expire-gift/vip-subscription job completed at ${Date.now()}`);
  }).catch((err) => {
    console.log(err);
    console.log(`Failed: A expire-gift/vip-subscription handler failed at ${Date.now()}`);
    process.exit(1);
  }).finally(() => {
    disconnectMongoDB();
  });
}).catch((err) => {
  console.log(err);
  console.log(`Failed: expire-gift/vip-subscription initialization failed at ${Date.now()}`);
  process.exit(1);
});
