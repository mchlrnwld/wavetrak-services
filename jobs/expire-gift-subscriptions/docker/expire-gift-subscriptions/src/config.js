import { getSecret } from '@surfline/services-common';

let config = null;

export const initConfig = async () => {
  config = {
    EXPIRE_USER_ACCOUNTS: true,
    MONGO_CONNECTION_STRING_USERDB: await getSecret(`${process.env.NODE_ENV}/jobs/expire-gift-subscriptions/`, 'MONGO_CONNECTION_STRING_USERDB'),
    SUBSCRIPTION_SERVICE_URL: await getSecret(`${process.env.NODE_ENV}/common/`, 'SUBSCRIPTION_SERVICE_URL'),
  };
  return config;
}

export const getConfig = () => config;
