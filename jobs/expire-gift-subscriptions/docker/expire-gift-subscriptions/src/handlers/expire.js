import SubscriptionsModel from '../models/SubscriptionsModel';
import UserAccountModel from '../models/UserAccountModel';
import expireGiftSubscription from '../external/subscription';
import { trackSubscriptionDeleted } from '../common/analytics';
import { getConfig } from '../config';

/**
 * TODO: Remove once UserAccounts are deprecated
 */
const expireUserAccounts = async (brand, subscriptionType, subscriptionEntitlement) => {
  try {
    const expiresDate = new Date();
    const userAccounts = await UserAccountModel.find({
      'subscriptions.type': subscriptionType,
      'subscriptions.entitlement': subscriptionEntitlement,
      'subscriptions.periodEnd': { '$lt': expiresDate }
    });
    console.log(`${userAccounts.length} user accounts to expire for v1.` +
      `${brand}, ${subscriptionType}, ${subscriptionEntitlement}`);

    for (const account of userAccounts) {
      const { subscriptions } = account;
      const subscriptionToExpire = subscriptions.find(({
        type, entitlement, periodEnd
      }) =>
        type === subscriptionType
        && entitlement === subscriptionEntitlement
        && periodEnd < expiresDate
      )
      if (subscriptionToExpire) {
        account.subscriptions.remove(subscriptionToExpire);

        // archived subscription
        account.archivedSubscriptions.push({
          ...subscriptionToExpire,
          archivedDate: new Date(),
        });

        // track and identify updated subscription and user traits
        await trackSubscriptionDeleted(brand, subscriptionToExpire, account.user);

        account.markModified('subscriptions');
        account.markModified('archivedSubscriptions');

        await account.save();
        console.log(`Expired ${subscriptionType} Subscription for: User:${account.user.toString()}`)
      }
    }
  } catch (error) {
    console.log(error);
    console.log(`Error processesing ${subscriptionType}expiration script for '${brand}'.`);
  }
};

const expireSubscriptions = async (brand, subscriptionType, subscriptionEntitlement) => {
  try {
    const expiresDate = new Date() / 1000;
    const subscriptions = await SubscriptionsModel.find({
      type: subscriptionType,
      entitlement: subscriptionEntitlement,
      end: { '$lt': expiresDate },
      active: true,
    });
    console.log(`${subscriptions.length} subscriptions to expire for v2.` +
      `${brand}-${subscriptionType}-${subscriptionEntitlement}`);
    for (const subscription of subscriptions) {
      await expireGiftSubscription(subscription.subscriptionId);
      console.log(`Expired V2-${subscriptionType} Subscription for: User:${subscription.user.toString()}`)
    }
  } catch (error) {
    console.log(error);
    console.log(`Error processesing ${subscriptionType}expiration script for '${brand}'.`);
  }
};

const expireHandler = async (brand, subscriptionType, subscriptionEntitlement) => {
  const { EXPIRE_USER_ACCOUNTS } = getConfig();
  if (EXPIRE_USER_ACCOUNTS) await expireUserAccounts(brand, subscriptionType, subscriptionEntitlement);
  await expireSubscriptions(brand, subscriptionType, subscriptionEntitlement);
};

export default expireHandler;
