import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const options = {
  collection: 'Subscriptions',
  timestamps: true,
};

const SubscriptionsSchema = new mongoose.Schema(
  {
    type: {
      type: Schema.Types.String,
      required: [true, 'A subscription must be of a specific type.'],
      enum: ['stripe', 'apple', 'google', 'gift'],
    },
    subscriptionId: {
      type: Schema.Types.String,
      required: [true, 'A subscription id must be defined.'],
      trim: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    active: {
      type: Schema.Types.Boolean,
    },
    planId: {
      type: Schema.Types.String,
      required: [true, 'A subscription must include a plan id.'],
      trim: true,
    },
    entitlement: {
      type: Schema.Types.String,
    },
    start: {
      type: Schema.Types.Number,
      required: [true, 'A subscription start timestamp is required.'],
    },
    end: {
      type: Schema.Types.Number,
      required: [true, 'A subscription end timestamp is required.'],
    },
    expired: {
      type: Schema.Types.Number,
    },
  },
  options,
);

export default mongoose.model('Subscriptions', SubscriptionsSchema);
