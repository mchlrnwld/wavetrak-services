import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const options = {
  collection: 'UserAccounts',
  timestamps: true,
};


export const UserAccountSchema = mongoose.Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'UserInfo'
  },
  subscriptions: [ Schema.Types.Mixed ],
  archivedSubscriptions: [ Schema.Types.Mixed ],
  receipts: [ Schema.Types.Mixed ],
  entitlements: [{
    type: Schema.Types.ObjectId,
    ref: 'Entitlement'
  }],
  winbacks: [ Schema.Types.Mixed ],
  stripeCustomerId: String
}, options);

export default mongoose.model('UserAccount', UserAccountSchema);
