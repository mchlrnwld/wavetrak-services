import fetch from 'isomorphic-fetch';
import { getConfig } from '../config';

const expireGiftSubscription = subscriptionId => {
  const { SUBSCRIPTION_SERVICE_URL } = getConfig();
  fetch(`http://${SUBSCRIPTION_SERVICE_URL}/admin/subscriptions/gifts`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({
      subscriptionId
    }),
  }).then((resp) => new Promise(async (resolve, reject) => {
    const body = await resp.json();
    if (resp.status > 200) {
      return reject(body);
    }
    return resolve(body);
  }));
}

export default expireGiftSubscription;
