from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTBranchDockerOperator, WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator

from dag_helpers.docker import docker_image
from dag_helpers.pager_duty import create_pager_duty_failure_callback

JOB_NAME = 'delete-users'
ENV = Variable.get('ENVIRONMENT')
APP_ENVIRONMENT = 'sandbox' if ENV == 'development' else ENV
env_variables = {'NODE_ENV': ENV}

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_subscription_squad', JOB_NAME, APP_ENVIRONMENT
)

# DAG definition
default_args = {
    'owner': 'wavetrak',
    'start_date': datetime(2021, 8, 1),
    'email': 'subscriptionjobs@surfline.com',
    'email_on_failure': True,
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'on_failure_callback': pager_duty_failure_callback
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval='@daily',
    max_active_runs=3,
    default_args=default_args,
)

WTDockerOperator(
    task_id='delete-users',
    image=docker_image('delete-users', JOB_NAME),
    environment=env_variables,
    force_pull=True,
    execution_timeout=timedelta(minutes=30),
    dag=dag,
)
