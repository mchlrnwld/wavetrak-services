/* istanbul ignore file */

import { SurflineLogger } from '@surfline/services-common';

/**
 * @description Options for the deleteUsers handler
 */
export type DeleteUserOptions = {
  logger: SurflineLogger;
  retentionPeriodDays: number;
};

export default DeleteUserOptions;
