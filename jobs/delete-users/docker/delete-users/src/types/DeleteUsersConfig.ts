/* istanbul ignore file */

/**
 * @description Configuration for the delete-users job
 */
export type DeleteUsersConfig = {
  RETENTION_PERIOD_DAYS: number;
  LOGSENE_KEY: string;
  LOGSENE_LEVEL: string;
  USER_SERVICE: string;
};
