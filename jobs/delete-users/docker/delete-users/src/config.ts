/* istanbul ignore file */
import { getSecret } from '@surfline/services-common';
import { DeleteUsersConfig } from './types/DeleteUsersConfig';

const ENV = process.env.NODE_ENV === 'prod' ? 'prod' : 'staging';

let config: DeleteUsersConfig;

export const createConfig = async (): Promise<DeleteUsersConfig> => {
  if (!config) {
    config = {
      RETENTION_PERIOD_DAYS: +(await getSecret(
        `${ENV}/jobs/delete-users/`,
        'RETENTION_PERIOD_DAYS',
      )),
      LOGSENE_KEY: await getSecret(`${ENV}/common/`, 'LOGSENE_KEY'),
      LOGSENE_LEVEL: await getSecret(`${ENV}/common/`, 'LOGSENE_LEVEL'),
      USER_SERVICE: await getSecret(`${ENV}/common/`, 'USER_SERVICE'),
    };
  }
  return config;
};

export const getConfig = (): DeleteUsersConfig => {
  if (!config) throw new Error('Config not set, you must call `createConfig`.');

  return config;
};
