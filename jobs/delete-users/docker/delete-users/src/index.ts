/* istanbul ignore file */
/* eslint-disable no-console */
import newrelic from 'newrelic';
import { SurflineLogger } from '@surfline/services-common';
import deleteUsers from './handlers/deleteUsers';
import { createConfig } from './config';
import { setupLogger } from './utils/logger';
import { DeleteUsersConfig } from './types/DeleteUsersConfig';

const JOB_NAME = 'delete-users';
let config: DeleteUsersConfig;
let log: SurflineLogger | null;

const initialize = async () => {
  try {
    config = await createConfig();
    log = setupLogger(config);
  } catch (err) {
    const message = `delete-users job initialization failed: ${err.message}`;
    if (log) log.error({ message, stack: err });
    console.log(message);
    throw err;
  }
};

newrelic.startBackgroundTransaction(JOB_NAME, async () => {
  const transaction = newrelic.getTransaction();
  try {
    await initialize();
    log.info(`Starting: ${JOB_NAME} job on ${Date.now()}`);
    await deleteUsers({ retentionPeriodDays: config.RETENTION_PERIOD_DAYS, logger: log });
    log.info(`Completed: ${JOB_NAME} job completed at ${Date.now()}`);
    transaction.end();

    // To guaranteed at least 1 logsene batch
    await new Promise((resolve) => setTimeout(resolve, 20000));

    process.exit(0);
  } catch (err) {
    newrelic.noticeError(err);
    log.error(err, `Failed: ${JOB_NAME} failed at ${Date.now()}`);
    transaction.end();
    process.exit(1);
  }
});
