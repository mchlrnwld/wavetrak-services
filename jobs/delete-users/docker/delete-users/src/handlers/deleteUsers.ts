/* eslint-disable no-await-in-loop */
import newrelic from 'newrelic';
import { deleteUser, getPendingDeletedUsers } from '../external/users';
import { DeleteUserOptions } from '../types/DeleteUsersOptions';

/**
 * @description Executes the job for deleting users.
 * @param opts DeleteUserOptions
 */
const deleteUsers = async (opts: DeleteUserOptions): Promise<void> => {
  const { logger, retentionPeriodDays } = opts;

  try {
    logger.info(
      `Job started. Deleting users that requested ${retentionPeriodDays} or more day(s) ago`,
    );

    const pendingUsers = await getPendingDeletedUsers(retentionPeriodDays);
    logger.info(`${pendingUsers?.length ?? 0} users to delete`);

    let successCount = 0;
    let failCount = 0;

    for (let i = 0; i < pendingUsers?.length; i += 1) {
      try {
        const { deleted, message } = await deleteUser(pendingUsers[i].user);

        if (deleted) successCount += 1;
        else failCount += 1;

        logger.info(
          `${deleted ? 'Success' : 'Failure'} deleting user ${pendingUsers[i].user}, ` +
            `deleted:${deleted}, message:${message}`,
        );
      } catch (err) {
        failCount += 1;
        logger.error({
          message: `Error deleting user ${pendingUsers[i].user}, error: ${err.message}`,
          stack: err,
        });
        newrelic.noticeError(err);
      }
    }
    logger.info(
      `Job complete. Success: ${successCount},` +
        ` Failure: ${failCount}, Total: ${successCount + failCount}`,
    );
  } catch (err) {
    logger.error({ message: `Fatal Error: ${err.message}`, stack: err });
    newrelic.noticeError(err);
    throw err;
  }
};

export default deleteUsers;
