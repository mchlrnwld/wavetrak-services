/* eslint-disable @typescript-eslint/no-explicit-any */
import { expect } from 'chai';
import sinon from 'sinon';
import * as user from '../external/users';
import DeleteUsersOptions from '../types/DeleteUsersOptions';
import deleteUsers from './deleteUsers';

describe('handlers / deleteUsers', () => {
  let getPendingDeletedUsersStub: sinon.SinonStub;
  let deleteUserStub: sinon.SinonStub;

  const deleteUserOptionsFixture: DeleteUsersOptions = {
    logger: {
      error: () => null,
      info: sinon.stub(),
      warn: () => null,
    } as any,
    retentionPeriodDays: 1,
  };

  const pendingDeletedUsers = [
    {
      user: 'userId1',
      deleteRequestedAt: new Date(),
    },
    {
      user: 'userId2',
      deleteRequestedAt: new Date('2021-01-01T00:00:00.000Z'),
    },
  ];

  beforeEach(() => {
    getPendingDeletedUsersStub = sinon.stub(user, 'getPendingDeletedUsers');
    deleteUserStub = sinon.stub(user, 'deleteUser');
  });
  afterEach(() => {
    getPendingDeletedUsersStub.restore();
    deleteUserStub.restore();
    (deleteUserOptionsFixture.logger.info as sinon.SinonStub).reset();
  });

  it('should call users service with the correct retention date', async () => {
    await deleteUsers(deleteUserOptionsFixture);
    expect(getPendingDeletedUsersStub).to.have.been.calledOnceWithExactly(
      deleteUserOptionsFixture.retentionPeriodDays,
    );
  });

  it('should call delete user per entry', async () => {
    getPendingDeletedUsersStub.returns(pendingDeletedUsers);
    deleteUserStub.onFirstCall().returns({ deleted: true, message: 'msg' });
    await deleteUsers(deleteUserOptionsFixture);
    expect(deleteUserStub).to.have.been.calledWith(pendingDeletedUsers[0].user);
    expect(deleteUserStub).to.have.been.calledWith(pendingDeletedUsers[1].user);

    expect(deleteUserOptionsFixture.logger.info).to.have.been.calledWith(
      `Job complete. Success: 1, Failure: 1, Total: 2`,
    );
  });

  it('should throw a fatal error if it cannot retrieve the pending users', async () => {
    getPendingDeletedUsersStub.throws(new Error('error'));
    let err;
    try {
      await deleteUsers(deleteUserOptionsFixture);
    } catch (error) {
      err = error;
    }
    expect(err).to.not.equal(undefined);
    expect(err.message).to.deep.equal('error');
  });

  it('should continue if an error occurs when deleting users', async () => {
    getPendingDeletedUsersStub.returns(pendingDeletedUsers);
    deleteUserStub.onFirstCall().throws(new Error('error'));
    await deleteUsers(deleteUserOptionsFixture);
    expect(deleteUserStub).to.have.been.calledWith(pendingDeletedUsers[0].user);
    expect(deleteUserStub).to.have.been.calledWith(pendingDeletedUsers[1].user);
  });
});
