import { expect } from 'chai';
import createParamString from './createParamString';

describe('createParamString', () => {
  it('should create a URI encoded parameter string from a key/value object', () => {
    const params = {
      param1: 'value1',
      'symbol&param': 'symbol=value',
    };
    expect(createParamString(params)).to.equal('param1=value1&symbol%26param=symbol%3Dvalue');
  });

  it('should return an empty string if params is empty', () => {
    expect(createParamString({})).to.equal('');
  });

  it('should ignore undefined params', () => {
    const params = {
      param1: 'value1',
      undefinedParam: undefined,
    };
    expect(createParamString(params)).to.equal('param1=value1');
  });
});
