/* istanbul ignore file */
import { createLogger, setupLogsene, SurflineLogger } from '@surfline/services-common';
import { DeleteUsersConfig } from '../types/DeleteUsersConfig';

let logger;
/**
 * @description A helper function for setting up Surfline specific logging
 * @param {DeleteUsersConfig} config
 * @returns the instance of logger created
 */
export const setupLogger = ({ LOGSENE_KEY }: DeleteUsersConfig): SurflineLogger => {
  setupLogsene(LOGSENE_KEY);
  logger = createLogger('delete-users');
  return logger;
};
