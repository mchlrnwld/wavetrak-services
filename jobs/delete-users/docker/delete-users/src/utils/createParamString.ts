/**
 * Creates a URL friendly query string
 * @param params object
 * @returns {string}
 */
const createParamString = (params: unknown): string =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

export default createParamString;
