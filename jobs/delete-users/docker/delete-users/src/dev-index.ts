/* eslint-disable import/first */
/* istanbul ignore file */
// eslint-disable-next-line import/no-extraneous-dependencies
import dotenv from 'dotenv';

dotenv.config();

import './index';
