import axios from 'axios';
import { getConfig } from '../config';
import createParamString from '../utils/createParamString';

/**
 * @description Calls the user service admin to get the list of users to delete
 * @param days Number of days of waiting cycle.
 * @returns
 */
export const getPendingDeletedUsers = async (
  days: number,
): Promise<
  [
    {
      user: string;
      deleteRequestedAt: Date;
    },
  ]
> => {
  const date = new Date();
  date.setDate(date.getDate() - days);
  const response = await axios.get(
    `${getConfig().USER_SERVICE}/admin/users/deleted?${createParamString({ date })}`,
  );

  if (response.status !== 200)
    throw new Error(`Error when calling getPendingDeletedUsers: ${JSON.stringify(response.data)}`);

  return response.data;
};

/**
 * @description Calls the user service admin to delete a given user
 * @param user The user id
 * @returns
 */
export const deleteUser = async (user: string): Promise<{ message: string; deleted: boolean }> => {
  const response = await axios.delete(`${getConfig().USER_SERVICE}/admin/users/${user}`);
  return response.data;
};
