import axios from 'axios';
import { expect } from 'chai';
import sinon from 'sinon';
import { getPendingDeletedUsers, deleteUser } from './users';
import * as config from '../config';

describe('external / users', () => {
  let clockStub: sinon.SinonFakeTimers;
  let axiosGetStub: sinon.SinonStub;
  let axiosDeleteStub: sinon.SinonStub;
  let getConfigStub: sinon.SinonStub;

  const getPendingDeleteUsersResponse = {
    data: [
      {
        user: '56f9eb8646f566a11a5ce971',
        deleteRequestedAt: new Date(),
      },
    ],
    status: 200,
  };

  const deleteUserResponse = {
    data: {
      deleted: true,
      message: 'test',
    },
    status: 200,
  };

  beforeEach(() => {
    clockStub = sinon.useFakeTimers();
    axiosGetStub = sinon.stub(axios, 'get');
    axiosDeleteStub = sinon.stub(axios, 'delete');
    getConfigStub = sinon.stub(config, 'getConfig').returns({
      RETENTION_PERIOD_DAYS: 1,
      LOGSENE_KEY: '',
      LOGSENE_LEVEL: '',
      USER_SERVICE: '',
    });
  });

  afterEach(() => {
    clockStub.restore();
    axiosGetStub.restore();
    axiosDeleteStub.restore();
    getConfigStub.restore();
  });

  describe('getPendingDeletedUsers', () => {
    it('should call the user service endpoint', async () => {
      axiosGetStub.returns(getPendingDeleteUsersResponse);
      const res = await getPendingDeletedUsers(1);
      const date = new Date();
      date.setDate(date.getDate() - 1);
      expect(axiosGetStub).to.have.been.calledOnceWithExactly(
        `/admin/users/deleted?date=${encodeURIComponent(date.toString())}`,
      );
      expect(res).to.deep.equal(getPendingDeleteUsersResponse.data);
    });

    it('should throw an error if the status code is not 200', async () => {
      axiosGetStub.returns({ ...getPendingDeleteUsersResponse, status: 500 });
      let err;
      try {
        expect(await getPendingDeletedUsers(1)).to.throw();
      } catch (error) {
        err = error;
      }
      expect(err).to.not.equal(undefined);
      expect(err?.message).to.deep.equal(
        `Error when calling getPendingDeletedUsers: ` +
          `${JSON.stringify(getPendingDeleteUsersResponse.data)}`,
      );
    });
  });

  describe('deleteUser', () => {
    it('should call the user service endpoint', async () => {
      axiosDeleteStub.returns(deleteUserResponse);
      const userId = '56f9eb8646f566a11a5ce971';
      const res = await deleteUser(userId);
      expect(axiosDeleteStub).to.have.been.calledOnceWithExactly(`/admin/users/${userId}`);
      expect(res).to.deep.equal(deleteUserResponse.data);
    });
  });
});
