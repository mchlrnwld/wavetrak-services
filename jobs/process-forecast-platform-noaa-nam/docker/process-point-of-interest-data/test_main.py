import json
import os
from unittest import mock

import boto3  # type: ignore
import moto  # type: ignore
import pandas as pd
import pytest
from lib.models import PointOfInterest

pytestmark = pytest.mark.asyncio


def mock_query_points_of_interest():
    with open(
        os.path.join('fixtures', 'points-of-interest-test-data.json'), 'r'
    ) as poi_json:
        points_of_interest = [
            PointOfInterest(*dict.values()) for dict in json.load(poi_json)
        ]

    return points_of_interest


async def test_process_point_of_interest_data():
    with moto.mock_s3(), mock.patch(
        'lib.sds.SDS.query_points_of_interest',
        return_value=mock_query_points_of_interest(),
    ), mock.patch(
        'slack_helper.send_invalid_points_of_interest_notification'
    ) as mocked_send_invalid_poi_notification:
        # Must import after mocking boto3
        import lib.config as config
        from main import main

        WIND_AND_WEATHER_CSV_PREFIX = (
            f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/'
            f'{config.RUN}/{config.TASK_PARTITION}'
        )
        WIND_CSV_OBJECT_KEY = f'{WIND_AND_WEATHER_CSV_PREFIX}/wind.csv'
        WEATHER_CSV_OBJECT_KEY = f'{WIND_AND_WEATHER_CSV_PREFIX}/weather.csv'

        NAM_DATA_S3_KEY_00 = (
            'noaa/nam/20220129/nam.t18z.conusnest.hiresf00.tm00.grib2'
        )
        NAM_DATA_S3_KEY_06 = (
            'noaa/nam/20220129/nam.t12z.conusnest.hiresf06.tm00.grib2'
        )

        s3 = boto3.client('s3', region_name='us-west-1')

        s3.create_bucket(
            Bucket=config.SCIENCE_BUCKET,
            CreateBucketConfiguration={'LocationConstraint': 'us-west-1'},
        )

        for key, local_path in [
            (
                NAM_DATA_S3_KEY_00,
                os.path.join(
                    'fixtures', 'nam.t18z.conusnest.hiresf00.tm00.grib2'
                ),
            ),
            (
                NAM_DATA_S3_KEY_06,
                os.path.join(
                    'fixtures', 'nam.t12z.conusnest.hiresf06.tm00.grib2'
                ),
            ),
        ]:
            s3.upload_file(local_path, config.SCIENCE_BUCKET, key)

        await main()

        wind_df = pd.read_csv(
            s3.get_object(
                Bucket=config.SCIENCE_BUCKET, Key=WIND_CSV_OBJECT_KEY
            )['Body']
        )

        weather_df = pd.read_csv(
            s3.get_object(
                Bucket=config.SCIENCE_BUCKET, Key=WEATHER_CSV_OBJECT_KEY
            )['Body']
        )

        assert all(
            column
            in [
                'point_of_interest_id',
                'agency',
                'model',
                'grid',
                'latitude',
                'longitude',
                'run',
                'forecast_time',
                'u',
                'v',
                'gust',
            ]
            for column in wind_df.columns
        )

        assert all(
            column
            in [
                'point_of_interest_id',
                'agency',
                'model',
                'grid',
                'latitude',
                'longitude',
                'run',
                'forecast_time',
                'temperature',
                'dewpoint',
                'visibility',
                'humidity',
                'pressure',
                'precipitation_volume',
                'precipitation_type',
                'weather_conditions',
            ]
            for column in weather_df.columns
        )

        assert len(wind_df) == 1153
        assert len(weather_df) == 1153

        mocked_send_invalid_poi_notification.assert_called()
