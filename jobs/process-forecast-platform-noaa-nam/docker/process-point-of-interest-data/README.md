# Process Point of Interest Data

## Setup

```sh
conda devenv
source activate process-forecast-platform-noaa-nam-process-point-of-interest-data
cp .env.sample .env
env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
docker build -t process-forecast-platform-noaa-nam/process-point-of-interest-data .
cp .env.sample .env
docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data process-forecast-platform-noaa-nam/process-point-of-interest-data
```

## Testing

Run an integration test with:
```sh
make test-integration
```
