FROM continuumio/miniconda3:4.9.2-alpine

SHELL ["sh", "--login", "-c"]
WORKDIR /opt/app

# NOTE: bash and coreutils are needed to run integration tests.
RUN apk add bash coreutils && conda install -c conda-forge conda-devenv

COPY pip.conf /root/.pip/pip.conf
COPY environment.devenv.yml .
RUN conda devenv && conda uninstall conda-devenv && conda clean -afy

COPY . .

RUN conda activate process-forecast-platform-noaa-nam-process-point-of-interest-data && \
    make lint && \
    make type-check && \
    make test-unit

CMD conda activate process-forecast-platform-noaa-nam-process-point-of-interest-data && python main.py
