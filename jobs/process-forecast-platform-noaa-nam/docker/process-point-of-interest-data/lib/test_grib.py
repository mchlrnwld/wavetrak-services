import lib.grib as grib
import numpy as np  # type: ignore
import pytest  # type: ignore


def test_get_lat_lon_index():
    lats = np.array([[0, 0, 0, 0], [1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3]])
    lons = np.array([[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]])

    assert grib.get_lat_lon_index((1, 2), lats, lons) == (1, 2)

    with pytest.raises(ValueError):
        grib.get_lat_lon_index((-1, -1), lats, lons)
