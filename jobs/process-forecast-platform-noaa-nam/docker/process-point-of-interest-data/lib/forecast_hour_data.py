from datetime import datetime, timedelta
from typing import Optional

RUN_FORMAT = '%Y%m%d%H'
ANALYSIS_HOUR = 0


class ForecastHourData:
    """
    Contains all information necessary to
    extract data for a forecast hour.

    Args:
        run:                The current model run being processed,
                            in the format YYYYMMDDHH.
        forecast_hour:      Forecasted hour from analysis hour.
        grid:               The grid associated with the run.
        temp_dir:           Location on disk of where to download the file.
        science_key_prefix: Location in S3 from where to download the file.

    Attributes:
        hour:                      Hour associated with the file.
                                   Example:
                                   local_file:
                                   tmp/20200307/nam.t00z.hawaiinest.hiresf07.tm00.grib2
                                   file_hour: 7
        timestamp:                 Formatted forecast hour timestamp.
        grid:                      Grid associated with then run
                                   and forecast hour.
        local_file:                Path to NOAA NAM file locally.
        local_fill_data_file:      Path to NOAA NAM file to fill in
                                   totalPrecipitation data.
                                   For analysis hour (0) only.
                                   Example:
                                   local_file:
                                   tmp/20200307/nam.t00z.hawaiinest.hiresf00.tm00.grib2
                                   local_fill_data_file:
                                   tmp/20200306/nam.t18z.hawaiinest.hiresf06.tm00.grib2
        s3_file:                   Path to NOAA NAM file in S3.
        s3_fill_data_file:         Path to NOAA NAM file in s3 to fill in
                                   totalPrecipitation data.
                                   For analysis hour (0) only.
                                   Example:
                                   s3_file:
                                   noaa/nam/20200307/nam.t00z.hawaiinest.hiresf00.tm00.grib2
                                   s3_fill_data_file:
                                   noaa/nam/20200306/nam.t18z.hawaiinest.hiresf06.tm00.grib2
    """

    def __init__(
        self,
        run: int,
        forecast_hour: int,
        grid: str,
        temp_dir: str,
        science_key_prefix: str,
        start_forecast_hour: int,
    ):
        run_datetime = datetime.strptime(str(run), RUN_FORMAT)
        forecast_datetime = run_datetime + timedelta(hours=forecast_hour)

        self.timestamp = f'{forecast_datetime.isoformat()}Z'
        self.grid = grid
        self.hour = forecast_hour
        self.local_file = self._get_file(run, self.hour, grid, temp_dir)
        self.s3_file = self._get_file(run, self.hour, grid, science_key_prefix)
        # Get the fill data file for the forecast_hour
        # corresponding to the ANALYSIS Hour.
        # This is needed to calculate precipitation due to
        # the fact that the ANALYSIS hour file is missing data.
        self.local_fill_data_file = (
            self._get_fill_data_file(run, grid, temp_dir)
            if self.hour == ANALYSIS_HOUR
            else None
        )
        self.s3_fill_data_file = (
            self._get_fill_data_file(run, grid, science_key_prefix)
            if self.hour == ANALYSIS_HOUR
            else None
        )
        # Get the previous grib file for the
        # forecast_hour that corresponds to the start_forecast_hour.
        # The previous file is needed to calculate marginal
        # precipitation correctly. This is only needed for
        # forecast hours greater than 1 due to the fact that
        # the analysis hour 0 does not contain precipitation data.
        self.local_previous_file = (
            self._get_file(run, self.hour - 1, grid, temp_dir)
            if self.hour == start_forecast_hour and forecast_hour > 1
            else None
        )
        self.s3_previous_file = (
            self._get_file(run, self.hour - 1, grid, science_key_prefix)
            if self.hour == start_forecast_hour and forecast_hour > 1
            else None
        )

    def _get_fill_data_file(
        self, run: int, grid: str, prefix: str
    ) -> Optional[str]:
        """
        Gets a fill data NOAA NAM file from a run,
        forecast hour, and the given prefix.

        Args:
            run:    Model run time.
            grid:   Grid associated with the NOAA NAM file.
            prefix: Prefix for the file.

        Returns:
            Path to the NOAA NAM fill data file.
        """
        run_datetime = datetime.strptime(str(run), RUN_FORMAT)
        fill_data_run_datetime = run_datetime - timedelta(hours=6)
        fill_data_run = fill_data_run_datetime.strftime(RUN_FORMAT)
        return self._get_file(int(fill_data_run), 6, grid, prefix)

    def _get_file(
        self, run: int, file_hour: int, grid: str, prefix: str
    ) -> str:
        """
        Gets a NOAA NAM file from a run,
        forecast hour, and the given prefix.

        Args:
            run:           Model run time.
            forecast_hour: Forecasted hour or hours from analysis hour.
            grid:          Grid associated with the NOAA NAM file.
            prefix:        Prefix for the file.

        Returns:
            Path to the NOAA NAM file.
        """
        return (
            f'{prefix}/{str(run)[:8]}/nam.t{str(run)[-2:]}z.'
            f'{grid}nest.hiresf{str(file_hour).zfill(2)}.tm00.grib2'
        )
