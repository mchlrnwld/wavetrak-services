import os

from job_secrets_helper import get_secret

ENV = os.environ['ENV']
JOB_NAME = os.environ['JOB_NAME']
TASK_PARTITION = os.environ['TASK_PARTITION']
SECRETS_PREFIX = f'{ENV}/common/'
TMP_DIR = os.getenv('AIRFLOW_TMP_DIR', 'tmp')
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
GRID = os.environ['GRID']
RUN = int(os.environ['RUN'])
SCIENCE_KEY_PREFIX = os.environ['SCIENCE_KEY_PREFIX']
FORECAST_HOURS = os.environ['FORECAST_HOURS']
JOBS_BUCKET = get_secret(SECRETS_PREFIX, 'JOBS_BUCKET')
JOBS_KEY_PREFIX = get_secret(SECRETS_PREFIX, 'JOBS_KEY_PREFIX')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
COMMIT_CHANGES = os.getenv('COMMIT_CHANGES', 'false') == 'true'

SLACK_API_TOKEN = get_secret(SECRETS_PREFIX, 'SLACK_API_TOKEN', '')
SLACK_CHANNEL = os.getenv('SLACK_CHANNEL', '')
