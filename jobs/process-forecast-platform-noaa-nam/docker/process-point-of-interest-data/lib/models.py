from science_algorithms.enums import PrecipitationType, WeatherConditions


class PointOfInterest:
    """
    Point of interest and its grid point.
    """

    def __init__(self, point_of_interest_id: str, lon: float, lat: float):
        self.point_of_interest_id = point_of_interest_id
        # Adjust to (-180, 180]
        self.lon = lon % 360 if lon % 360 < 180 else lon % 360 - 360
        self.lat = lat


class WeatherHour:
    """
    Contains weather values for a single
    forecast hour for a point of interest.
    """

    def __init__(
        self,
        agency: str,
        model: str,
        run: int,
        grid: str,
        forecast_time: str,
        poi: PointOfInterest,
        temperature: float,
        dewpoint: float,
        visibility: float,
        humidity: float,
        pressure: float,
        precipitation_volume: float,
        precipitation_type: int,
        weather_conditions: int,
    ):
        self.point_of_interest_id = poi.point_of_interest_id
        self.agency = agency
        self.model = model
        self.grid = grid
        self.latitude = poi.lat
        self.longitude = poi.lon
        self.run = run
        self.forecast_time = forecast_time
        self.temperature = temperature
        self.dewpoint = dewpoint
        self.visibility = visibility
        self.humidity = humidity
        self.pressure = pressure
        self.precipitation_type = (
            None
            if not PrecipitationType(precipitation_type)
            else PrecipitationType(precipitation_type).name
        )
        self.precipitation_volume = precipitation_volume
        self.weather_conditions = WeatherConditions(weather_conditions).name


class WindHour:
    """
    Contains wind values for a single
    forecast hour for a point of interest.
    """

    def __init__(
        self,
        poi: PointOfInterest,
        agency: str,
        model: str,
        grid: str,
        forecast_time: str,
        run: int,
        u: float,
        v: float,
        gust: float,
    ):
        self.point_of_interest_id = poi.point_of_interest_id
        self.agency = agency
        self.model = model
        self.grid = grid
        self.latitude = poi.lat
        self.longitude = poi.lon
        self.run = run
        self.forecast_time = forecast_time
        self.u = u
        self.v = v
        self.gust = gust
