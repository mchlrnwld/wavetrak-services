from typing import List

import requests
from lib.models import PointOfInterest


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.
    """

    def __init__(self, url: str, agency: str, model: str, grid: str):
        self.url = url
        self.agency = agency
        self.model = model
        self.grid = grid

    def query_points_of_interest(self) -> List[PointOfInterest]:
        """
        Queries the GraphQL endpoint for PointOfInterest data
        from the surf_spot_configurations table in Postgres.

        Args:
            grid: Grid to query for PointOfInterest data.

        Returns:
            A list of PointOfInterest objects for
            the given agency, model, and grid.

        """
        query = f'''
        query {{
          models(agency: "{self.agency}", model: "{self.model}"){{
            grids(grid: "{self.grid}"){{
              pointsOfInterest {{
                id
                lat
                lon
              }}
            }}
          }}
        }}'''
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
        }
        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        model_data = response.json()['data']['models']
        if not model_data:
            return list()

        grid_data = model_data[0]['grids']
        if not grid_data:
            return list()
        point_of_interest_data = grid_data[0]['pointsOfInterest']
        return [
            PointOfInterest(
                point_of_interest['id'],
                float(point_of_interest['lon']),
                float(point_of_interest['lat']),
            )
            for point_of_interest in point_of_interest_data
        ]
