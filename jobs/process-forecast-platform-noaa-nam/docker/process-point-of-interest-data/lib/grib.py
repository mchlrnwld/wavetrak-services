import logging
from typing import Any, Dict, Optional, Tuple

import numpy as np  # type: ignore
from grib_helpers.process import parse_grib
from grib_helpers.variables import NAM_GRIB_VARIABLES

logger = logging.getLogger('process-point-of-interest-data')


def get_lat_lon_index(
    target_lat_lon: Tuple[float, float], lats: Any, lons: Any
) -> Tuple[int, int]:
    """
    Gets the index for the given latitude and longitude.

    Args:
        target_lat_lon: Pair of lat and lon values to find the index for.
        lats: numpy array of floats representing latitude values.
        lons: numpy array of floats representing longitude values.

    Returns:
        Tuple containing the latitude index and longitude index.
    """
    found_indices = np.where(
        np.logical_and(
            np.isclose(lats, target_lat_lon[0]),
            np.isclose(lons, target_lat_lon[1]),
        )
    )
    if found_indices[0].size == 0 or found_indices[1].size == 0:
        raise ValueError(
            f'Latitude: {target_lat_lon[0]} and longitude: '
            f'{target_lat_lon[1]} combination does not exist in the grid'
        )

    return found_indices[0][0], found_indices[1][0]


def parse_poi_data_from_gribs(
    local_file: str,
    local_fill_data_file: Optional[str],
    lat_indexes: Any,
    lon_indexes: Any,
) -> Tuple[str, Dict[str, Any]]:
    """
    This function gets data from NOAA NAM grib files
    for the points of interest and variables desired.
    If there is a fill data file,
    then the totalPrecipitation from the fill data file is used.

    Args:
        local_file: Path to current GRIB file for the forecast hour to be
                    parsed.
        local_fill_data_file: Path to fill data GRIB file for the forecast hour
                              to be parsed. This is used to fill data the
                              current GRIB may be missing.
        lat_indexes: numpy array of latitude indexes used to extract data for
                     the points of interest.
        lon_indexes: numpy array of longitude indexes used to extract data for
                     the points of interest.

    Returns:
        Total precipitation details and data for the desired variables.
    """
    lats, lons, variable_values, variable_details = parse_grib(
        local_file, NAM_GRIB_VARIABLES
    )
    if local_fill_data_file:
        (
            _,
            _,
            fill_data_variable_values,
            fill_data_variable_details,
        ) = parse_grib(local_fill_data_file, NAM_GRIB_VARIABLES)
        variable_values['totalPrecipitation'] = fill_data_variable_values[
            'totalPrecipitation'
        ]
        variable_details['totalPrecipitation'] = fill_data_variable_details[
            'totalPrecipitation'
        ]

    poi_variable_values = {
        key: values[lat_indexes, lon_indexes]
        for key, values in variable_values.items()
    }

    return variable_details['totalPrecipitation'], poi_variable_values
