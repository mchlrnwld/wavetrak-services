from typing import Any, Dict

import science_algorithms.transformations as t


def get_weather_conditions(
    variable_values: Dict[str, Any],
    temperature_c: Any,
    precipitation_type: Any,
) -> Any:
    """
    This function collects values needed then
    computes weather conditions given temperature,
    precipitation type and variable values.

    Args:
        variable_values: Dictionary of numpy arrays of values for NOAA NAM
                         variables.
        temperature_c: Temperature numpy array.
        precipitation_type: Precipitation type as numpy array.

    Returns:
        Computed weather conditions as a numpy array.
    """
    dewpoint_for_weather_conditions = t.get_dewpoint_for_weather_conditions(
        temperature_c, variable_values['relativeHumidity']
    )
    storm_wind_magnitude = t.get_storm_wind_magnitude(
        variable_values['stormWindU'], variable_values['stormWindV']
    )
    thunder_type = t.get_thunder_type(
        variable_values['liftedIndex'],
        variable_values['capeSurface'],
        variable_values['capeML'],
        variable_values['verticalVelocity750mb'],
        variable_values['verticalVelocity800mb'],
        dewpoint_for_weather_conditions,
        variable_values['convectiveInhibition'],
        storm_wind_magnitude,
    )
    net_radiation = t.get_net_radiation(
        variable_values['downShortRadFlux'],
        variable_values['downLongRadFlux'],
        variable_values['upShortRadFlux'],
        variable_values['upLongRadFlux'],
    )
    avg_low_layer_humidity = t.get_average_low_layer_relative_humidity(
        variable_values['relativeHumidity925mb'],
        variable_values['relativeHumidity950mb'],
        variable_values['relativeHumidity975mb'],
        variable_values['relativeHumidity1000mb'],
    )
    wind_magnitude = t.get_wind_magnitude(
        variable_values['windU'], variable_values['windV']
    )
    fog_type = t.get_fog_type(
        net_radiation,
        avg_low_layer_humidity,
        variable_values['relativeHumidity'],
        wind_magnitude,
    )
    tod_layer_temperatures = {
        layer: variable_values[f'temperature{layer.replace(" ", "")}']
        for layer in t.total_optical_density_layers
    }
    tod_layer_cloud_mixing_ratios = {
        layer: variable_values[f'cloudMixingRatio{layer.replace(" ", "")}']
        for layer in t.total_optical_density_layers
    }
    tod_layer_heights = {
        layer: variable_values[f'height{layer.replace(" ", "")}']
        for layer in t.total_optical_density_layers
    }
    total_optical_density = t.get_total_optical_density(
        variable_values['land'],
        tod_layer_temperatures,
        tod_layer_cloud_mixing_ratios,
        tod_layer_heights,
    )
    sky_conditions = t.get_sky_conditions(
        total_optical_density, variable_values['totalCloudCover']
    )
    return t.get_weather_conditions(
        precipitation_type,
        thunder_type,
        fog_type,
        sky_conditions,
        wind_magnitude,
        variable_values['precipitationRate'],
        # Convective Precipitation Rate does not exist in NOAA NAM files,
        # so instead pass in 0.
        0,
        total_optical_density,
    )
