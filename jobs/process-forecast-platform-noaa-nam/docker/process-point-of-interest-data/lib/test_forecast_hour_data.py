from lib.forecast_hour_data import ForecastHourData

GRID = 'hawaii'
SCIENCE_KEY_PREFIX = 'noaa/nam'
TEMP_DIR = 'data/tmp'


def test_forecast_hour():
    analysis_hour = ForecastHourData(
        2020030700, 0, GRID, TEMP_DIR, SCIENCE_KEY_PREFIX, 0
    )

    forecast_hour = ForecastHourData(
        2020030700, 54, GRID, TEMP_DIR, SCIENCE_KEY_PREFIX, 0
    )
    # Check for forecast timestamp.
    assert ['2020-03-07T00:00:00Z', '2020-03-09T06:00:00Z'] == [
        analysis_hour.timestamp,
        forecast_hour.timestamp,
    ]
    # Check for local file path.
    assert [
        'data/tmp/20200307/nam.t00z.hawaiinest.hiresf00.tm00.grib2',
        'data/tmp/20200307/nam.t00z.hawaiinest.hiresf54.tm00.grib2',
    ] == [analysis_hour.local_file, forecast_hour.local_file]
    # Check for fill data local file path.
    assert [
        'data/tmp/20200306/nam.t18z.hawaiinest.hiresf06.tm00.grib2',
        None,
    ] == [
        analysis_hour.local_fill_data_file,
        forecast_hour.local_fill_data_file,
    ]
    # Check for forecast hour.
    assert [0, 54] == [analysis_hour.hour, forecast_hour.hour]


def test_get_file_path():
    forecast_hour = ForecastHourData(
        2020030700, 54, GRID, TEMP_DIR, SCIENCE_KEY_PREFIX, 0
    )
    assert [
        'noaa/nam/20200307/nam.t00z.hawaiinest.hiresf01.tm00.grib2',
        'noaa/nam/20200306/nam.t18z.hawaiinest.hiresf01.tm00.grib2',
        'noaa/nam/20200306/nam.t12z.hawaiinest.hiresf01.tm00.grib2',
        'noaa/nam/20200306/nam.t06z.hawaiinest.hiresf01.tm00.grib2',
    ] == [
        forecast_hour._get_file(2020030700, 1, GRID, SCIENCE_KEY_PREFIX),
        forecast_hour._get_file(2020030618, 1, GRID, SCIENCE_KEY_PREFIX),
        forecast_hour._get_file(2020030612, 1, GRID, SCIENCE_KEY_PREFIX),
        forecast_hour._get_file(2020030606, 1, GRID, SCIENCE_KEY_PREFIX),
    ]
    hours = [9, 53]
    assert [
        (
            f'data/tmp/20200307/nam.t00z.hawaiinest.'
            f'hiresf{str(hours[0]).zfill(2)}.tm00.grib2'
        ),
        (
            f'data/tmp/20200307/nam.t00z.hawaiinest.'
            f'hiresf{str(hours[1]).zfill(2)}.tm00.grib2'
        ),
    ] == [
        forecast_hour._get_file(2020030700, hours[0], GRID, TEMP_DIR),
        forecast_hour._get_file(2020030700, hours[1], GRID, TEMP_DIR),
    ]


def test_get_fill_data_file():
    forecast_hour = ForecastHourData(
        2020030700, 54, GRID, TEMP_DIR, SCIENCE_KEY_PREFIX, 0
    )
    assert [
        'noaa/nam/20200306/nam.t18z.hawaiinest.hiresf06.tm00.grib2',
        'noaa/nam/20200306/nam.t12z.hawaiinest.hiresf06.tm00.grib2',
        'noaa/nam/20200306/nam.t06z.hawaiinest.hiresf06.tm00.grib2',
        'noaa/nam/20200306/nam.t00z.hawaiinest.hiresf06.tm00.grib2',
    ] == [
        forecast_hour._get_fill_data_file(
            2020030700, GRID, SCIENCE_KEY_PREFIX
        ),
        forecast_hour._get_fill_data_file(
            2020030618, GRID, SCIENCE_KEY_PREFIX
        ),
        forecast_hour._get_fill_data_file(
            2020030612, GRID, SCIENCE_KEY_PREFIX
        ),
        forecast_hour._get_fill_data_file(
            2020030606, GRID, SCIENCE_KEY_PREFIX
        ),
    ]
