import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
SCIENCE_KEY_PREFIX = os.environ['SCIENCE_KEY_PREFIX']
SECRETS_PREFIX = f'{ENV}/common/'
SQS_QUEUE_NAME = os.environ['SQS_QUEUE_NAME']

SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')
