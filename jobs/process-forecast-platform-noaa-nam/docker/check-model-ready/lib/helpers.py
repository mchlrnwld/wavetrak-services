import re
from typing import List

import requests

RUN_HOUR_REGEX = re.compile(r'nam\.t(\d{2})z\.')


def get_list_of_grids(
    science_data_service: str, agency: str, model: str
) -> List[str]:
    """
    Creates a list grids associated with the current_model_run.
    Args:
        science_data_service: Internal API to call
                              to obtain the
                              necessary grids.
        agency: The model agency to
                to query for (ex. NOAA).
        model:  The name of the model
                to query for (ex. NAM).
    Returns:
        A list of grids associated with the model_run
        Example:
        ['conus', 'hawaii', ...]
    """
    query = '''
        query($agency: String!, $model: String!) {
            models(agency: $agency, model: $model) {
                agency
                model
                grids {
                    grid
                }
            }
        }
    '''
    headers = {'Content-Type': 'application/json', 'Cache-Control': 'no-cache'}
    response = requests.post(
        f'{science_data_service}/graphql',
        json=dict(query=query, variables=dict(agency=agency, model=model)),
        headers=headers,
    )
    response.raise_for_status()
    response_json = response.json()
    if not response_json['data']['models']:
        raise ValueError(f'No grids for {agency}/{model}')
    return [
        data['grid'] for data in response_json['data']['models'][0]['grids']
    ]


# TODO: Extract these helper functions into modules.
def model_run_from_key(prefix: str, key: str) -> int:
    """
    Extracts a model run from the given key string

    Args:
        prefix: The common prefix for the model.
        key: The key string to extract data from.

    Returns:
        A integer representing the model run.
    """
    suffix = key.replace(prefix, '')
    yyyymmdd = suffix.split('/')[1]
    match = RUN_HOUR_REGEX.search(suffix)
    if not match:
        raise ValueError(f'{key} does not match expected filename format')
    hh = match[1]
    return int(f'{yyyymmdd}{hh}')
