import re

import boto3  # type: ignore
import check_model_ready

from lib.helpers import get_list_of_grids

FORECAST_HOUR_REGEX = re.compile(r'nest.hiresf(\d{2}).tm00.grib2$')


def check_nam_ready(
    bucket: str,
    science_key_prefix: str,
    sds_host: str,
    agency: str,
    model: str,
    run: int,
) -> bool:
    """
    Checks if the given model_run is ready for processing.
    This is accomplished by checking that the model_run.
    has all of the files needed for every grid
    in the given S3 bucket.

    Args:
        bucket: S3 bucket to check for file objects.
        science_key_prefix: Key prefix for file objects.
        sds_host: Science Data Service host URI.
        agency: The model agency to query for (ex. NOAA).
        model: The name of the model to query for (ex. WW3-Ensemble).
        run: Model run (YYYYMMDDHH) to check.

    Returns:
        True if all files exist in the s3 bucket for the model_run.
        False if the model_run was already processed,
        if the some files exist in s3, but not all,
        or lastly if there are no files in s3.
    """
    if check_model_ready.check_model_run_already_exists(
        agency, model, run, 'POINT_OF_INTEREST', sds_host
    ):
        return False

    return all(
        check_nam_grid(bucket, science_key_prefix, run, grid)
        for grid in get_list_of_grids(sds_host, agency, model)
    )


def check_nam_grid(
    bucket: str, science_key_prefix: str, run: int, grid: str
) -> bool:
    """
    Checks that the given grid and model_run
    has all of the files needed in the S3 bucket.

    Args:
        bucket: S3 bucket to check for file objects.
        science_key_prefix: Key prefix for file objects.
        run: Model run (YYYYMMDDHH) to check.
        grid: The grid to check for (ex. glo_30m)

    Returns:
        True if all files exist in the s3 bucket
        for the grid and model_run.
        False if the some files exist in s3, but not all,
        or if there are no files in s3 related to the grid
        and model_run.
    """
    yyyymmdd = str(run)[0:8]
    hh = str(run)[8:10]
    s3_client = boto3.client('s3')
    model_objects_response = s3_client.list_objects_v2(
        Bucket=bucket,
        Prefix=f'{science_key_prefix}/{yyyymmdd}/nam.t{hh}z.{grid}nest.hiresf',
    )
    if 'Contents' not in model_objects_response:
        return False
    matches = [
        FORECAST_HOUR_REGEX.search(object['Key'])
        for object in model_objects_response['Contents']
    ]
    model_timestamps = [int(match[1]) for match in matches if match]
    return sorted(model_timestamps) == list(range(61))
