from unittest.mock import patch

import boto3  # type: ignore
import pytest  # type: ignore
from boto3_type_annotations.s3 import Client as s3_client  # type: ignore
from moto import mock_s3  # type: ignore

from lib.check_nam import check_nam_grid, check_nam_ready

GRID = 'hawaii'
AGENCY = 'NOAA'
MODEL = 'NAM'
RUN_YYYYMMDD = '20190502'
RUN_HH = '00'
RUN = int(f'{RUN_YYYYMMDD}{RUN_HH}')
SCIENCE_BUCKET = 'surfline-science-s3-dev'
SCIENCE_KEY_PREFIX = 'noaa/nam'
SCIENCE_DATA_SERVICE = 'surfline-data-service'


@pytest.fixture(autouse=True)
def aws_credentials(monkeypatch):
    """
    Mocked AWS Credentials for moto.
    This is a temporary workaround,
    otherwise moto will throw 'NoCredentialsError'.
    """
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3', region_name='us-west-1')
        s3_client.create_bucket(Bucket=SCIENCE_BUCKET)
        yield s3_client


def put_items_in_bucket(
    s3_client: s3_client,
    times: list,
    grid: str,
    model_key_prefix: str = (f'{SCIENCE_KEY_PREFIX}/' f'{RUN_YYYYMMDD}'),
):
    for time in times:
        s3_client.put_object(
            Bucket=SCIENCE_BUCKET,
            Key=(
                f'{model_key_prefix}/nam.t{RUN_HH}z.{grid}nest.'
                f'hiresf{str(time).zfill(2)}.tm00.grib2'
            ),
        )


def test_check_nam_ready(create_s3_bucket):
    grids = ['hawaii', 'prico']
    with patch('lib.check_nam.get_list_of_grids', return_value=grids), patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        for grid in grids:
            expected_times = list(range(61))
            put_items_in_bucket(create_s3_bucket, expected_times, grid)

        assert check_nam_ready(
            SCIENCE_BUCKET,
            SCIENCE_KEY_PREFIX,
            SCIENCE_DATA_SERVICE,
            AGENCY,
            MODEL,
            RUN,
        )


def test_check_nam_already_exists(create_s3_bucket):
    grids = ['hawaii', 'prico']
    with patch('lib.check_nam.get_list_of_grids', return_value=grids), patch(
        'check_model_ready.check_model_run_already_exists', return_value=True
    ):
        for grid in grids:
            expected_times = list(range(61))
            put_items_in_bucket(create_s3_bucket, expected_times, grid)

        assert not check_nam_ready(
            SCIENCE_BUCKET,
            SCIENCE_KEY_PREFIX,
            SCIENCE_DATA_SERVICE,
            AGENCY,
            MODEL,
            RUN,
        )


def test_check_nam_not_ready(create_s3_bucket):
    grids = ['hawaii', 'prico']
    with patch('lib.check_nam.get_list_of_grids', return_value=grids), patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        put_items_in_bucket(create_s3_bucket, list(range(61)), grids[0])
        put_items_in_bucket(create_s3_bucket, list(range(58)), grids[1])

        assert not check_nam_ready(
            SCIENCE_BUCKET,
            SCIENCE_KEY_PREFIX,
            SCIENCE_DATA_SERVICE,
            AGENCY,
            MODEL,
            RUN,
        )


def test_check_nam_grid_ready(create_s3_bucket):
    expected_times = list(range(61))
    put_items_in_bucket(create_s3_bucket, expected_times, GRID)
    assert check_nam_grid(SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, RUN, GRID)


def test_check_nam_grid_not_ready(create_s3_bucket):
    expected_times = list(range(51))
    put_items_in_bucket(create_s3_bucket, expected_times, GRID)
    assert not check_nam_grid(SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, RUN, GRID)


def test_check_nam_grid_s3_empty(create_s3_bucket):
    assert not check_nam_grid(SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, RUN, GRID)
