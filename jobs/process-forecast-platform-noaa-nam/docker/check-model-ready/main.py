import logging
from functools import partial

from check_model_ready import (  # type: ignore
    check_sqs_messages,
    set_pending_model_run,
)

import lib.config as config
from lib.check_nam import check_nam_ready
from lib.helpers import model_run_from_key

logger = logging.getLogger('check-model-ready')


def main():
    # TODO: Uncomment below to stream info level logging to stderr once
    #       WTDockerOperator can distinguish between stdout and stderr streams.
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler())

    logger.info(f'Polling queue: {config.SQS_QUEUE_NAME}...')

    result = None
    total_messages = 0
    while True:
        model_run, messages_found = check_sqs_messages(
            config.SCIENCE_BUCKET,
            (config.SCIENCE_KEY_PREFIX,),
            config.SQS_QUEUE_NAME,
            partial(
                check_nam_ready,
                config.SCIENCE_BUCKET,
                config.SCIENCE_KEY_PREFIX,
                config.SCIENCE_DATA_SERVICE,
                config.AGENCY,
                config.MODEL,
            ),
            partial(model_run_from_key, config.SCIENCE_KEY_PREFIX),
        )
        result = model_run
        total_messages += messages_found
        if result is not None or messages_found == 0:
            break

    logger.info(f'Checked {total_messages} messages.')

    # Write to stdout for xcoms_push
    if result is not None:
        set_pending_model_run(
            config.AGENCY,
            config.MODEL,
            result,
            'POINT_OF_INTEREST',
            config.SCIENCE_DATA_SERVICE,
        )
        print(result)
    else:
        print('')


if __name__ == '__main__':
    main()
