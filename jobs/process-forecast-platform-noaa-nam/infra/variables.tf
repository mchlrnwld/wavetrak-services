variable "application" {
  type    = string
  default = "process-forecast-platform-noaa-nam"
}

variable "company" {
  type    = string
  default = "wt"
}

variable "environment" {
  type = string
}
variable "queue_name" {
  type    = string
  default = "s3-events"
}

variable "topic_arns" {
  type = list(string)
}

variable "jobs_bucket" {
  description = "S3 bucket for job state files."
}
