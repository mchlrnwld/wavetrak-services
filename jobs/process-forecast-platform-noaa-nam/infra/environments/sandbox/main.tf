provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/process-forecast-platform-noaa-nam/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_noaa_nam" {
  source = "../../"

  providers = {
    aws = aws
  }

  environment = "sandbox"
  topic_arns  = ["arn:aws:sns:us-west-1:665294954271:surfline-science-s3-dev-noaa-nam-upload-event"]
  jobs_bucket = "surfline-science-s3-dev"
}
