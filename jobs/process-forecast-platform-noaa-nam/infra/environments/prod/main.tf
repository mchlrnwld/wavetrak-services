provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/process-forecast-platform-noaa-nam/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_noaa_nam" {
  source = "../../"

  providers = {
    aws = aws
  }

  environment = "prod"
  topic_arns  = ["arn:aws:sns:us-west-1:833713747344:surfline-science-s3-prod-noaa-nam-upload-event"]
  jobs_bucket = "surfline-science-s3-prod"
}
