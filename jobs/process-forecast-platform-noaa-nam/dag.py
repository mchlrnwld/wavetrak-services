import re
from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from dag_helpers.docker import docker_image
from dag_helpers.pager_duty import create_pager_duty_failure_callback
from dag_helpers.utilities import partition_for_nodes

AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')

JOB_NAME = 'process-forecast-platform-noaa-nam'
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
SQS_QUEUE_NAME = 'wt-{0}-s3-events-{1}'.format(JOB_NAME, APP_ENVIRONMENT)
AGENCY = 'NOAA'
MODEL = 'NAM'
RUN = '{{ task_instance.xcom_pull(task_ids=\'check-model-ready\') }}'
SCIENCE_KEY_PREFIX = 'noaa/nam'

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, APP_ENVIRONMENT
)

common_environment_values = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'ENV': APP_ENVIRONMENT,
    'AGENCY': AGENCY,
    'JOB_NAME': JOB_NAME,
    'MODEL': MODEL,
    'RUN': RUN,
    'MODEL_RUN_TYPE': 'POINT_OF_INTEREST',
    'SCIENCE_KEY_PREFIX': SCIENCE_KEY_PREFIX,
}


def task_environment(*keys, **keyvalues):
    environment = keyvalues.copy()
    for key in keys:
        environment[key] = common_environment_values[key]
    return environment


check_model_ready_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'AGENCY',
    'MODEL',
    'SCIENCE_KEY_PREFIX',
    SQS_QUEUE_NAME=SQS_QUEUE_NAME,
)


def create_process_point_of_interest_data_environment(
    grid, forecast_hours, partition
):
    return {
        'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
        'JOB_NAME': 'wt-{0}-{1}-{2}-{3}'.format(
            JOB_NAME, grid, partition, APP_ENVIRONMENT
        ),
        'JOB_DEFINITION': 'wt-{0}-poi-{1}'.format(JOB_NAME, APP_ENVIRONMENT),
        'JOB_QUEUE': 'wt-jobs-common-high-cpu-local-ssd-job-queue-{0}'.format(
            APP_ENVIRONMENT
        ),
        'TIMEOUT': str(30 * 60),
        'ENVIRONMENT': ';'.join(
            [
                'DATA_DIR=/mnt/ephemeral/airflow',
                'ENV={0}'.format(APP_ENVIRONMENT),
                'JOB_NAME={0}'.format(JOB_NAME),
                'TASK_PARTITION={0}'.format(partition),
                'AWS_DEFAULT_REGION={0}'.format(AWS_DEFAULT_REGION),
                'AGENCY={0}'.format(AGENCY),
                'MODEL={0}'.format(MODEL),
                'GRID={0}'.format(grid),
                'RUN={0}'.format(RUN),
                'COMMIT_CHANGES=true',
                'FORECAST_HOURS={0}'.format(
                    ','.join(
                        str(forecast_hour) for forecast_hour in forecast_hours
                    )
                ),
                'SCIENCE_KEY_PREFIX={0}'.format(SCIENCE_KEY_PREFIX),
                'SLACK_CHANNEL=surfspots',
            ]
        ),
    }


write_point_of_interest_data_to_database_environment = task_environment(
    'AWS_DEFAULT_REGION', 'ENV', 'JOB_NAME', RUN=RUN, COMMIT='true',
)

mark_model_run_complete_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'MODEL_RUN_TYPE',
    MODEL_AGENCY=AGENCY,
    MODEL_NAME=MODEL,
    MODEL_RUN=RUN,
    JOB_NAME=JOB_NAME,
)

prune_model_runs_environment = task_environment(
    'AWS_DEFAULT_REGION',
    'ENV',
    'MODEL_RUN_TYPE',
    MODEL_AGENCY=AGENCY,
    MODEL_NAME=MODEL,
    MODEL_RUN=RUN,
    MODEL_LIMIT='3',
)

# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2020, 3, 6),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
    'on_failure_callback': pager_duty_failure_callback,
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=5),
    default_args=default_args,
)

check_model_ready = WTDockerOperator(
    task_id='check-model-ready',
    image=docker_image('check-model-ready', JOB_NAME),
    environment=check_model_ready_environment,
    force_pull=True,
    xcom_push=True,
    depends_on_past=True,
    dag=dag,
)

check_model_ready_branch = BranchPythonOperator(
    task_id='check-model-ready-branch',
    templates_dict={'model_run': RUN},
    python_callable=lambda **kwargs: (
        'begin-processing-model'
        if kwargs['templates_dict']['model_run']
        # HACK: Check model run to prevent 'None' from triggering processing.
        and re.match(r'\d{10}', kwargs['templates_dict']['model_run'])
        else 'no-model-ready'
    ),
    dag=dag,
)

no_model_ready = DummyOperator(task_id='no-model-ready', dag=dag)

begin_processing_model = DummyOperator(
    task_id='begin-processing-model', dag=dag
)

all_forecast_hours = list(range(61))
process_point_of_interest_data_tasks = [
    WTDockerOperator(
        task_id='process-point-of-interest-data-{0}-{1}'.format(
            grid, partition
        ),
        image=docker_image('execute-batch-job'),
        environment=create_process_point_of_interest_data_environment(
            grid, forecast_hours, partition
        ),
        force_pull=True,
        execution_timeout=None,
        dag=dag,
    )
    for grid, partitions in [
        ('hawaii', partition_for_nodes(all_forecast_hours, 1)),
        ('prico', partition_for_nodes(all_forecast_hours, 1)),
        ('conus', partition_for_nodes(all_forecast_hours, 5)),
    ]
    for partition, forecast_hours in enumerate(partitions)
]

write_point_of_interest_data_to_database = WTDockerOperator(
    task_id='write-point-of-interest-data-to-database',
    image=docker_image('write-point-of-interest-data-to-database', JOB_NAME),
    environment=write_point_of_interest_data_to_database_environment,
    force_pull=True,
    dag=dag,
)

mark_model_run_complete = WTDockerOperator(
    task_id='mark-model-run-complete',
    image=docker_image('mark-model-run-complete'),
    environment=mark_model_run_complete_environment,
    force_pull=True,
    dag=dag,
)

prune_model_runs = WTDockerOperator(
    task_id='prune-model-runs',
    image=docker_image('prune-model-runs'),
    environment=prune_model_runs_environment,
    force_pull=True,
    dag=dag,
)

check_model_ready_branch.set_upstream(check_model_ready)
no_model_ready.set_upstream(check_model_ready_branch)
begin_processing_model.set_upstream(check_model_ready_branch)
begin_processing_model.set_downstream(process_point_of_interest_data_tasks)
write_point_of_interest_data_to_database.set_upstream(
    process_point_of_interest_data_tasks
)
mark_model_run_complete.set_upstream(write_point_of_interest_data_to_database)
prune_model_runs.set_upstream(mark_model_run_complete)
