from datetime import datetime, timedelta

from airflow.models import Variable
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators import WTDockerOperator
from dag_helpers.pager_duty import create_pager_duty_failure_callback

from dag_helpers.docker import docker_image

JOB_NAME = 'verify-in-app-purchases'

# A dev environment for this job does not exist
APP_ENVIRONMENT = 'prod'

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_subscription_squad', JOB_NAME, APP_ENVIRONMENT
)

apple_envs = {
    'NEW_RELIC_ENABLED': False,
    'NODE_ENV': APP_ENVIRONMENT,
    'TYPE': 'apple'
}

google_envs = {
    'NEW_RELIC_ENABLED': False,
    'NODE_ENV': APP_ENVIRONMENT,
    'TYPE': 'google'
}

# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2022, 2, 1, 22),
    'email': 'subscriptionjobs@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'on_failure_callback': pager_duty_failure_callback,
    'execution_timeout': timedelta(hours=6)
}

dag = DAG(
    '{0}_v1'.format(JOB_NAME),
    schedule_interval='@daily',
    default_args=default_args,
    concurrency=1,
    max_active_runs=1
)

verify_in_app_purchases_google = WTDockerOperator(
    task_id='verify-in-app-purchases-google',
    image=docker_image('verify-in-app-purchases', JOB_NAME),
    force_pull=True,
    dag=dag,
    environment=google_envs
)

verify_in_app_purchases_apple = WTDockerOperator(
    task_id='verify-in-app-purchases-apple',
    image=docker_image('verify-in-app-purchases', JOB_NAME),
    force_pull=True,
    dag=dag,
    environment=apple_envs
)
