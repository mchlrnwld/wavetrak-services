import fetch from 'isomorphic-fetch';

/**
 * @description - Responsible for parsing the response from an API call
 * @param {object} resp - Response object
 * @returns {Promise<object>} A promise that contains the json response
 */
 const parseRequest = resp =>
 new Promise(async (resolve, reject) => {
   if(resp.ok) {
     const body = await resp.json();
     return resolve(body);
   }
   const error = new Error(resp.statusText);
   const { statusText, status, url } = resp;
   error.response = {
     statusText,
     status,
     url,
   };
   return reject(error);
 });

export const verifyAppleReceipt = (userId, receipt, config) =>
  fetch(`http://${config.SUBSCRIPTION_API}/subscriptions/itunes`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'x-auth-userid': userId.toString(),
    },
    body: JSON.stringify({
      receipt,
    }),
  }).then(parseRequest);

export const verifyGooglePurchase = (userId, purchase, config) =>
  fetch(`http://${config.SUBSCRIPTION_API}/subscriptions/google`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'x-auth-userid': userId.toString(),
    },
    body: JSON.stringify({
      purchase,
    }),
  }).then(parseRequest);
