import { createLogger, setupLogsene } from '@surfline/services-common/dist/';

/**
 * @description A helper function for setting up Surfline specific logging
 * @param {object} config
 * @returns the instance of logger created
 */

const setupLogger = (config) => {
    setupLogsene(config.LOGSENE_KEY);
    return createLogger('verify-in-app-purchases')
};

export default setupLogger;
