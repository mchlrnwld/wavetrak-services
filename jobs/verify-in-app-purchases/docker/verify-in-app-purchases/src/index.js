import { initMongoDB, disconnectMongoDB } from './common/dbContext';
import verifyGooglePurchases from './handlers/verifyGooglePurchases';
import verifyAppleReceipts from './handlers/verifyAppleReceipts';
import getConfigs from './config';
import setupLogger from './common/logger';

const initialize = async () => {
  let log;
  const config = await getConfigs();
  log = setupLogger(config);
  await initMongoDB(config);
  return { config, log };
};

initialize()
  .then(async ({ config, log }) => {
    try {
      if (config.TYPE === 'google')
        await verifyGooglePurchases(config, log);
      else
        await verifyAppleReceipts(config, log);

      log.info(`Completed: verify-in-app-purchases Job @ ${Date.now()}`);

      process.exit(0);
    }
    catch (err) {
      log.error({
        message: `verify-in-app-purchases Job execution failed: ${err.message}`,
        stack: err,
      });
      process.exit(1);
    }
    finally {
      disconnectMongoDB();
    }
  })
  .catch((err) => {
    console.log(`Failed: in-app-purchase verification @ ${err.message}`);
    process.exit(1);
  });
