import { getSecret } from '@surfline/services-common/dist/utils/secretsHelper';

export default async () => ({
  MONGO_CONNECTION_STRING_USERDB: await getSecret('prod/common/', 'MONGO_CONNECTION_STRING_USERDB'),
  SUBSCRIPTION_API: await getSecret('prod/common/', 'SUBSCRIPTION_API'),
  LOGSENE_KEY: await getSecret('prod/common/', 'LOGSENE_KEY'),
  LOGSENE_LEVEL: await getSecret('prod/common/', 'LOGSENE_LEVEL'),
  TYPE: process.env.TYPE,
});
