import mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types;

const options = {
  collection: 'SubscriptionPurchases',
};

const AppleReceiptSchema = new mongoose.Schema(
  {
    user: {
      type: ObjectId,
      ref: 'UserInfo',
    },
    type: {
      type: String,
    },
    latestReceipt: {
      type: String,
    },
  },
  options,
);

const GooglePurchaseSchema = new mongoose.Schema(
  {
    user: {
      type: ObjectId,
      ref: 'UserInfo',
    },
    type: {
      type: String,
    },
    purchaseToken: {
      type: String,
    },
    packageName: {
      type: String,
    },
    productId: {
      type: String,
    },
    expired: {
      type: Boolean,
    }
  },
  options,
);

export const AppleReceiptModel = mongoose.model('AppleReceipt', AppleReceiptSchema);
export const GooglePurchaseModel = mongoose.model('GooglePurchase', GooglePurchaseSchema);
