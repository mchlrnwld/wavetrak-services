import { GooglePurchaseModel } from '../models/SubscriptionPurchaseModel';
import { verifyGooglePurchase } from '../common/fetch';

export const verifyGooglePurchases = async (config, log) => {
  const googlePurchases = await GooglePurchaseModel.find(
    { type: 'google-play', expired: { $ne: true } },
    {
      user: 1,
      purchaseToken: 1,
      packageName: 1,
      productId: 1,
    },
  );
  let successCount = 0;
  for (const purchase of googlePurchases) {
    const { user, purchaseToken, packageName, productId } = purchase;
    const data = {
      purchaseToken,
      packageName,
      productId,
    };
    try {
      await verifyGooglePurchase(user, { data }, config);
      successCount++;
    } catch (error) {
      log.error({
        userId: user,
        message: `verifyGooglePurchase failed: ${error.message}`,
        stack: error,
      });
    }
  }
  log.info({
    message: `verifyGooglePurchases stats: Total Receipts: ${googlePurchases.length}. Success Count: ${successCount}`
  });
}

export default verifyGooglePurchases;
