import { AppleReceiptModel } from '../models/SubscriptionPurchaseModel';
import { verifyAppleReceipt } from '../common/fetch';

export const verifyAppleReceipts = async (config, log) => {
  const appleReceipts = await AppleReceiptModel.find(
    { type: 'itunes', expired: { $ne: true } },
    { user: 1, latestReceipt: 1 },
  );
  let successCount = 0;
  for (const receipt of appleReceipts) {
    const { user, latestReceipt } = receipt;
    try {
      await verifyAppleReceipt(user, latestReceipt, config);
      successCount++;
    } catch (error) {
      log.error({
        userId: user,
        message: `verifyAppleReceipt failed: ${error.message}`,
        stack: error,
      });
    }
  }
  log.info({
    message: `verifyAppleReceipt stats: Total Receipts: ${appleReceipts.length}. Success Count: ${successCount}`
    });
};

export default verifyAppleReceipts;
