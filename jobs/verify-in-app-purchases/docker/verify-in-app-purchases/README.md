# Verify In-App-Purchases

Docker image that runs a node script for expiring Gift Subscriptions

## Docker

### Build and Run

```
$ docker build -t verify-in-app-purchases/verify-in-app-purchases .
$ cp .env.sample .env
$ docker run --env-file=.env verify-in-app-purchases/verify-in-app-purchases
```
