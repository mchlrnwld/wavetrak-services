from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTBranchDockerOperator, WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator

from dag_helpers.docker import docker_image

JOB_NAME = 'retry-failed-sessions'
SESSIONS_API = Variable.get('SESSIONS_API')

retry_failed_sessions_environment = {'SESSIONS_API': SESSIONS_API}

# DAG definition
default_args = {
    'owner': 'wavetrak',
    'start_date': datetime(2020, 5, 27, 8),
    'email': 'kbygsquad@surfline.com',
    'email_on_failure': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    '{0}-v2'.format(JOB_NAME),
    schedule_interval=timedelta(days=1),
    max_active_runs=1,
    default_args=default_args,
)

WTDockerOperator(
    task_id='retry-failed-sessions',
    image=docker_image('retry-failed-sessions', JOB_NAME),
    environment=retry_failed_sessions_environment,
    force_pull=True,
    dag=dag,
)
