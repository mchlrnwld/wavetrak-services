import retryFailedSessions from './handlers/retryFailedSessions';

console.log(`Start: retry-failed-sessions job on ${Date.now()}`);

retryFailedSessions().then(() => {
  console.log(`Completed: retry-failed-sessions job completed at ${Date.now()}`);
}).catch((err) => {
  console.log(`Failed: retry-failed-sessions failed at ${Date.now()}`, err);
  process.exit(1);
});
