import fetch from 'node-fetch';
import config from '../config';

export const getfailedSessions = async () => {
  const url = `${config.SESSIONS_API}/sessions/failed`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export const reEnrichFailedSession = async ({ id, user, client}) => {
  const url = `${config.SESSIONS_API}/sessions/${id}/details`;
  const body = { reEnrich: 'all' };
  const response = await fetch(url, {
    method: 'patch',
    body:    JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'x-auth-userid': user,
      'x-api-key': client,
    },
  })

  const bodyResponse = await response.json();

  if (response.status === 200) return bodyResponse;
  throw bodyResponse;
}
