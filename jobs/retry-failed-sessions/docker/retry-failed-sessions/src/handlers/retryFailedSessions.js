import { getfailedSessions, reEnrichFailedSession } from "../external/sessions";
import wait from "../utils/wait";

const retryFailedSessions = async () => {
  const failedSessions = await getfailedSessions();
  for (const count in failedSessions) {
    const session = failedSessions[count];
    console.log(`Retrying session: ${session.id} -  ${Date.now()}`);
    try {
      await reEnrichFailedSession(session);
    } catch (error) {
      console.log("Session enrichment failed for:" + session._id);
    }
    await wait(5000);
  }
};

export default retryFailedSessions;
