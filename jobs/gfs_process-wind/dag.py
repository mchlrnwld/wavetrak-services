import subprocess
from datetime import datetime, timedelta
import json
import logging

from airflow import DAG
from airflow.models import Variable
from airflow.operators import (
    BashOperator,
    BranchPythonOperator,
    DummyOperator,
    PythonOperator,
)
from airflow.exceptions import AirflowException
from dag_helpers.pager_duty import create_pager_duty_failure_callback

JOB_NAME = 'gfs_process-wind'
ENVIRONMENT = Variable.get('ENVIRONMENT')
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
RETURN_CODE = {
                "20": "Data download hung",
                "21": "Script is running",
                "22": "No available data to run",
                "23": "Has already been completed for the latest data",
                "24": "Full set of files not found",
                "25": "makesurf.sh.FIXED is empty",
                "50": "High resolution (small) dataset only",
                "51": "No available glz model to run"
              }

DEFAULT_DATA = "s3://default_bucket/"

pager_duty_forecast_squad_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, APP_ENVIRONMENT
)

pager_duty_science_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_science', JOB_NAME, APP_ENVIRONMENT
)

args = {
    'owner': 'surfline',
    'depends_on_past': False,
    'start_date': datetime(2017, 10, 3, 18, 15),
    'email': ['datateam@surfline.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=2),
    'provide_context': True,
    'on_failure_callback': pager_duty_science_failure_callback,
}

dag = DAG(JOB_NAME, schedule_interval=timedelta(minutes=15), max_active_runs=1, default_args=args)

def gfs_check_wind_cb(**context):
    cmd = ['/ocean/code/science-scripts/scripts/download/gfs0p25/gfs_check.sh']
    ti = context['ti']
    input_ = ''


    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command
        logging.info(' '.join(cmd))
        res = subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd='/tmp')
        # Process the result and save to xcom if any
        logging.info(res.decode('utf-8', errors='replace').strip())
        outputs_line = [r for r in res.split('\n') if ("workflow_message" in r and "echo" not in r)]
        if outputs_line:
            d = json.loads(outputs_line[0])
            for k, v in d['workflow_message'].iteritems():
                context['ti'].xcom_push(key=k, value=v)
        return 'gfs_0p25_wind_download'
    except subprocess.CalledProcessError, e:
        if e.returncode in range(21,30):
            logging.info('Return code: %d %s' % (e.returncode, RETURN_CODE[str(e.returncode)]))
            return 'stop'

        else:
            logging.error('Return code: %d %s' % (e.returncode, e.output))
            raise AirflowException(
                'Command failed from gfs_check_wind ' + ' '.join(cmd))


def gfs_0p25_wind_download_cb(**context):
    cmd = [
        'python',
        '/ocean/code/science-scripts/scripts/process/common/ec2/create_dynamic_ec2_and_run_parallel_cmds.py',
        '-e',
        ENVIRONMENT,
        '-c',
        '/ocean/static/ec2/ec2_instance.cfg',
        '-p',
        '/ocean/code/science-sh-config/run_gfs0p25_wind_download_parallel.txt',
        '-n',
        '1',
        '-t',
        't2.small',
    ]
    ti = context['ti']
    input_ = ''


    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command
        logging.info(' '.join(cmd))
        res = subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd='/tmp')
        # Process the result and save to xcom if any
        logging.info(res.decode('utf-8', errors='replace').strip())
        outputs_line = [r for r in res.split('\n') if ("workflow_message" in r and "echo" not in r)]
        if outputs_line:
            d = json.loads(outputs_line[0])
            for k, v in d['workflow_message'].iteritems():
                context['ti'].xcom_push(key=k, value=v)
        return 'wwiii_wind_prep'
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs_0p25_wind_download ' + ' '.join(cmd))


def wwiii_wind_prep_cb(**context):
    cmd = ['/ocean/code/science-scripts/scripts/process/gfs0p25/process_gfs0p25_lola.sh']
    ti = context['ti']
    input_ = ''


    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command
        logging.info(' '.join(cmd))
        res = subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd='/tmp')
        # Process the result and save to xcom if any
        logging.info(res.decode('utf-8', errors='replace').strip())
        outputs_line = [r for r in res.split('\n') if ("workflow_message" in r and "echo" not in r)]
        if outputs_line:
            d = json.loads(outputs_line[0])
            for k, v in d['workflow_message'].iteritems():
                context['ti'].xcom_push(key=k, value=v)
        return 'gfs0p25_rasterize_shortrange'
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from wwiii_wind_prep ' + ' '.join(cmd))



def gfs0p25_rasterize_shortrange_cb(**context):
    cmd = ['/ocean/code/science-scripts/scripts/process/gfs0p25/gfs0p25_rasterize_shortrange.sh']
    ti = context['ti']
    input_ = ''

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command
        logging.info(' '.join(cmd))
        res = subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd='/tmp')
        # Process the result and save to xcom if any
        logging.info(res.decode('utf-8', errors='replace').strip())
        outputs_line = [r for r in res.split('\n') if ("workflow_message" in r and "echo" not in r)]
        if outputs_line:
            d = json.loads(outputs_line[0])
            for k, v in d['workflow_message'].iteritems():
                context['ti'].xcom_push(key=k, value=v)
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_rasterize_shortrange ' + ' '.join(cmd))


def gfs0p25_rasterize_longrange_cb(**context):
    cmd = ['/ocean/code/science-scripts/scripts/process/gfs0p25/gfs0p25_rasterize_longrange.sh','{{MODELTAU}}']
    ti = context['ti']
    input_ = '{"MODELTAU": "gfs0p25_rasterize_shortrange:MODELTAU"}'

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command
        logging.info(' '.join(cmd))
        res = subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd='/tmp')
        # Process the result and save to xcom if any
        logging.info(res.decode('utf-8', errors='replace').strip())
        outputs_line = [r for r in res.split('\n') if ("workflow_message" in r and "echo" not in r)]
        if outputs_line:
            d = json.loads(outputs_line[0])
            for k, v in d['workflow_message'].iteritems():
                context['ti'].xcom_push(key=k, value=v)
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_rasterize_longrange ' + ' '.join(cmd))


def gfs0p25_raster_2_ascii_for_bestwind_cb(**context):
    cmd = [
        'python',
        '/ocean/code/science-scripts/scripts/process/common/ec2/create_dynamic_ec2_and_run_parallel_cmds.py',
        '-e',
        ENVIRONMENT,
        '-c',
        '/ocean/static/ec2/ec2_instance.cfg',
        '-p',
        '/ocean/code/science-sh-config/run_gfs0p25_raster_2_ascii_pmdb_wind_parallel.txt',
        '-a',
        '{{MODELTAU}}',
        '-n',
        '1',
        '-t',
        't2.small',
    ]
    ti = context['ti']
    input_ = '{"MODELTAU": "gfs0p25_rasterize_shortrange:MODELTAU"}'

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command
        logging.info(' '.join(cmd))
        res = subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd='/tmp')
        # Process the result and save to xcom if any
        logging.info(res.decode('utf-8', errors='replace').strip())
        outputs_line = [r for r in res.split('\n') if ("workflow_message" in r and "echo" not in r)]
        if outputs_line:
            d = json.loads(outputs_line[0])
            for k, v in d['workflow_message'].iteritems():
                context['ti'].xcom_push(key=k, value=v)
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_raster_2_ascii_for_bestwind ' + ' '.join(cmd))


def gfs0p25_spacex_consulting_cb(**context):
    cmd = ['/ocean/code/science-scripts/scripts/process/gfs0p25/gfs0p25_compare_pmdb_analysis_and_forecast_with_buoy.sh','{{MODELTAU}}']
    ti = context['ti']
    input_ = '{"MODELTAU": "gfs0p25_rasterize_shortrange:MODELTAU"}'

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command
        logging.info(' '.join(cmd))
        res = subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd='/tmp')
        # Process the result and save to xcom if any
        logging.info(res.decode('utf-8', errors='replace').strip())
        outputs_line = [r for r in res.split('\n') if ("workflow_message" in r and "echo" not in r)]
        if outputs_line:
            d = json.loads(outputs_line[0])
            for k, v in d['workflow_message'].iteritems():
                context['ti'].xcom_push(key=k, value=v)
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_spacex_consulting ' + ' '.join(cmd))



def gfs_0p25_wind_download_failure_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/cleanup_gfs_download_runlog.sh']
    ti = context['ti']
    input_ = ''

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs_0p25_wind_download ' + ' '.join(cmd))
    finally:
        pager_duty_forecast_squad_failure_callback(context, **kwarg)


def wwiii_wind_prep_failure_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/cleanup_gfs_download_runlog.sh','&&','/ocean/code/science-scripts/scripts/libs/kill_process_by_name.sh','/ocean/code/science-scripts/scripts/process/gfs0p25/process_gfs0p25_lola.sh']
    ti = context['ti']
    input_ = ''

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from wwiii_wind_prep ' + ' '.join(cmd))
    finally:
        pager_duty_science_failure_callback(context, **kwarg)


def gfs0p25_rasterize_shortrange_failure_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/cleanup_gfs_wind_process_runlog.sh','&&','/ocean/code/science-scripts/scripts/libs/kill_process_by_name.sh','/ocean/code/science-scripts/scripts/process/gfs0p25/gfs0p25_rasterize_shortrange.sh']
    ti = context['ti']
    input_ = ''

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_rasterize_shortrange ' + ' '.join(cmd))
    finally:
        pager_duty_science_failure_callback(context, **kwarg)


def gfs0p25_rasterize_longrange_failure_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/cleanup_gfs_wind_process_runlog.sh','&&','/ocean/code/science-scripts/scripts/libs/kill_process_by_name.sh','/ocean/code/science-scripts/scripts/process/gfs0p25/gfs0p25_rasterize_longrange.sh']
    ti = context['ti']
    input_ = '{"MODELTAU": "gfs0p25_rasterize_shortrange:MODELTAU"}'

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_rasterize_longrange ' + ' '.join(cmd))
    finally:
        pager_duty_science_failure_callback(context, **kwarg)


def gfs0p25_raster_2_ascii_for_bestwind_failure_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/cleanup_gfs_wind_process_runlog.sh']
    ti = context['ti']
    input_ = '{"MODELTAU": "gfs0p25_rasterize_shortrange:MODELTAU"}'

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_raster_2_ascii_for_bestwind ' + ' '.join(cmd))
    finally:
        pager_duty_science_failure_callback(context, **kwarg)


def wwiii_wind_prep_retry_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/kill_process_by_name.sh','/ocean/code/science-scripts/scripts/process/gfs0p25/process_gfs0p25_lola.sh']
    ti = context['ti']
    input_ = ''

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from wwiii_wind_prep ' + ' '.join(cmd))


def gfs0p25_rasterize_shortrange_retry_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/kill_process_by_name.sh','/ocean/code/science-scripts/scripts/process/gfs0p25/gfs0p25_rasterize_shortrange.sh']
    ti = context['ti']
    input_ = ''

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_rasterize_shortrange ' + ' '.join(cmd))


def gfs0p25_rasterize_longrange_retry_cb(context, **kwarg):
    cmd = ['/ocean/code/science-scripts/scripts/libs/kill_process_by_name.sh','/ocean/code/science-scripts/scripts/process/gfs0p25/gfs0p25_rasterize_longrange.sh']
    ti = context['ti']
    input_ = '{"MODELTAU": "gfs0p25_rasterize_shortrange:MODELTAU"}'

    try:
        # Prepare command with appropriate inputs retrieved from upstream
        if input_:
            para = json.loads(input_)
            for k, v in para.iteritems():
                xcom_key, up_task = (v.split(":")[1], v.split(":")[0])
                pre_out = ti.xcom_pull(key=xcom_key, task_ids=up_task)
                cmd = [x.replace('{{' + k + '}}', pre_out) if ('{{' + k + '}}' in x) else x for x in cmd]
        cmd = [DEFAULT_DATA if x is None else x for x in cmd]
        # Run the command.
        # This supports multiple commands (so, it uses shell=True)
        logging.info(' '.join(cmd))
        res = subprocess.check_output(' '.join(cmd), stderr=subprocess.STDOUT, shell=True)
        # Process the result and not save to xcom
        # This is an event for on success/fail which does not need pass out xcom
        logging.info(res.decode('utf-8', errors='replace').strip())
    except subprocess.CalledProcessError, e:
        logging.error('Return code: %d %s' % (e.returncode, e.output))
        raise AirflowException(
            'Command failed from gfs0p25_rasterize_longrange ' + ' '.join(cmd))


gfs_check_wind = BranchPythonOperator(
    task_id='gfs_check_wind',
    python_callable=gfs_check_wind_cb,
    execution_timeout=timedelta(minutes=5),
    on_failure_callback=pager_duty_forecast_squad_failure_callback,
    on_retry_callback=None,
    dag=dag)

gfs_0p25_wind_download = PythonOperator(
    task_id='gfs_0p25_wind_download',
    python_callable=gfs_0p25_wind_download_cb,
    execution_timeout=timedelta(minutes=60),
    on_failure_callback=gfs_0p25_wind_download_failure_cb,
    on_retry_callback=None,
    dag=dag)

wwiii_wind_prep = PythonOperator(
    task_id='wwiii_wind_prep',
    python_callable=wwiii_wind_prep_cb,
    execution_timeout=timedelta(minutes=20),
    on_failure_callback=wwiii_wind_prep_failure_cb,
    on_retry_callback=wwiii_wind_prep_retry_cb,
    dag=dag)

gfs0p25_rasterize_shortrange = PythonOperator(
    task_id='gfs0p25_rasterize_shortrange',
    python_callable=gfs0p25_rasterize_shortrange_cb,
    execution_timeout=timedelta(minutes=20),
    on_failure_callback=gfs0p25_rasterize_shortrange_failure_cb,
    on_retry_callback=gfs0p25_rasterize_shortrange_retry_cb,
    dag=dag)

gfs0p25_rasterize_longrange = PythonOperator(
    task_id='gfs0p25_rasterize_longrange',
    python_callable=gfs0p25_rasterize_longrange_cb,
    execution_timeout=timedelta(minutes=20),
    on_failure_callback=gfs0p25_rasterize_longrange_failure_cb,
    on_retry_callback=gfs0p25_rasterize_longrange_retry_cb,
    dag=dag)

gfs0p25_raster_2_ascii_for_bestwind = PythonOperator(
    task_id='gfs0p25_raster_2_ascii_for_bestwind',
    python_callable=gfs0p25_raster_2_ascii_for_bestwind_cb,
    execution_timeout=timedelta(minutes=90),
    on_failure_callback=gfs0p25_raster_2_ascii_for_bestwind_failure_cb,
    on_retry_callback=None,
    dag=dag)

gfs0p25_spacex_consulting = PythonOperator(
    task_id='gfs0p25_spacex_consulting',
    python_callable=gfs0p25_spacex_consulting_cb,
    execution_timeout=timedelta(minutes=45),
    on_failure_callback=None,
    on_retry_callback=None,
    dag=dag) if ENVIRONMENT == 'prod' else None

stop = DummyOperator(
    task_id='stop',
    dag=dag)


# Setup operator dependencies
stop.set_upstream([gfs_check_wind])
gfs_0p25_wind_download.set_upstream([gfs_check_wind])
wwiii_wind_prep.set_upstream([gfs_0p25_wind_download])
gfs0p25_rasterize_shortrange.set_upstream([wwiii_wind_prep])
gfs0p25_rasterize_longrange.set_upstream([gfs0p25_rasterize_shortrange])
gfs0p25_raster_2_ascii_for_bestwind.set_upstream([gfs0p25_rasterize_longrange])

if gfs0p25_spacex_consulting:
    gfs0p25_spacex_consulting.set_upstream([gfs0p25_raster_2_ascii_for_bestwind])
