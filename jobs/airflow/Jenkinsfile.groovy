node {
    stage('Clean and Setup Workspace') {
        deleteDir();
        checkout scm;
        configFileProvider([configFile(fileId: 'aws_assume_role_mappings', variable: 'AWS_ASSUME_ROLE_MAPPING')]) {
            dir("${HOME}/.aws") {
                sh("cp ${AWS_ASSUME_ROLE_MAPPING} roles");
            }
        }

        GIT_COMMIT = sh(returnStdout: true, script: 'git rev-parse HEAD').trim();
        WORKING_DIRECTORY = "${pwd()}/jobs/airflow";
        AIRFLOW_ADMIN_HOST = "${env.ENVIRONMENT}-airflow-celery-1.aws.surfline.com";
    }

    stage('Deploy Plugins to Airflow') {
        dir("${WORKING_DIRECTORY}/plugins") {
            sh("rsync -e 'ssh -o StrictHostKeyChecking=no -i /opt/ssh/surfline-science-dev.pem' -azvhP --delete . airflow@${AIRFLOW_ADMIN_HOST}:/ocean/static/airflow/plugins");
        }
    }

    stage('Deploy DAG Helpers to Airflow') {
        dir("${WORKING_DIRECTORY}/dag_helpers") {
            sh("rsync -e 'ssh -o StrictHostKeyChecking=no -i /opt/ssh/surfline-science-dev.pem' -azvhP --delete . airflow@${AIRFLOW_ADMIN_HOST}:/ocean/static/airflow/dags/dag_helpers");
        }
    }

    stage('Restart Airflow Webserver') {
        def airflowIps = sh(returnStdout: true, script: "aws --region=us-west-1 --profile=${env.ENVIRONMENT} ec2 describe-instances --filters=\"Name=tag:type,Values=airflow-celery\" | jq \".Reservations[].Instances[].PrivateIpAddress\"").replace('"', '').split('\n');
        for (int i = 0; i < airflowIps.size(); i++) {
            sh("ssh -o StrictHostKeyChecking=no -i /opt/ssh/surfline-science-dev.pem airflow@${airflowIps[i]} sudo /sbin/restart airflow-webserver");
        }
    }
}
