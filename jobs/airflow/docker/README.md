# Airflow Docker

Docker setup to easily run Airflow for development.

## Setup

Copy DAGs into `test_dags` directory. For example,

```sh
$ cp ../../process-forecast-platform-noaa-gfs/dag.py test_dags/process-forecast-platform-noaa-gfs.py
```

## Standalone

This setup uses the `LocalExecutor` and is the simplest way to run Airflow and test DAGs.

```sh
$ docker-compose -f standalone/docker-compose.yml up
```
