from datetime import datetime, timedelta
import logging

from airflow.models import DAG, Variable
from airflow.operators import BashOperator, DummyOperator, WTPagerDutyOperator


five_minutes_ago = datetime.combine(datetime.today() - timedelta(minutes=5),
                                    datetime.min.time())
args = {
    'owner': 'airflow',
    'start_date': five_minutes_ago
}

dag = DAG(
    dag_id='test_pagerduty_operator',
    default_args=args,
    schedule_interval='5 * * * *',
    dagrun_timeout=timedelta(minutes=20)
)

run_this_first = BashOperator(
    task_id='run_this_first',
    bash_command='echo 1',
    dag=dag
)

run_this_second = BashOperator(
    task_id='run_this_second',
    bash_command='echo 2',
    dag=dag
)

run_this_last = WTPagerDutyOperator(
    task_id='run_this_last',
    pd_conn_id='pagerduty_platform_squad',
    summary='Airflow: test_pagerduty_operator.run_this_last',
    severity='error',
    component='run_this_last',
    group='test_pagerduty_operator',
    event_class='Airflow',
    dag=dag
)

run_this_first.set_downstream(run_this_second)
run_this_second.set_downstream(run_this_last)

if __name__ == "__main__":
    dag.cli()
