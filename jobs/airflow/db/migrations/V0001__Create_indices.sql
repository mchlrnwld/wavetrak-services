CREATE INDEX dag_id_task_id_execution_date_idx
ON xcom (dag_id, task_id, execution_date);

CREATE INDEX dag_id_external_trigger_state_idx
ON dag_run (dag_id, external_trigger, state);

CREATE INDEX dag_id_state_execution_date_idx
ON dag_run (dag_id, state, execution_date);

CREATE INDEX dag_id_external_trigger_run_id_execution_date_idx
ON dag_run (dag_id, external_trigger, run_id, execution_date DESC);
