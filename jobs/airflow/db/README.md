# Airflow Database Management

## Writing and Running Migrations

Migrations are stored in `./migrations`. They are applied to the Airflow database manually.

```bash
mysql -h <hostname> -u <user> <database> < ./migrations/V0001__Create_indices.sql
```

Note: We use [Flyway](https://flywaydb.org/) to manage database migrations for other databases. We are opting not to use it for Airflow due to cost. Flyway Community Edition doesn't support MySQL 5.6 and Flyway Enterprise Edition is very expensive.
