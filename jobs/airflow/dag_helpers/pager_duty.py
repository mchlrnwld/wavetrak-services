from distutils.util import strtobool
import logging

from airflow.hooks import WTPagerDutyHook
import boto3


def create_pager_duty_failure_callback(
    pd_conn_id, job_name=None, environment=None, aws_region='us-west-1'
):
    """
    Creates an on_failure_callback for a task, which either triggers a
    PagerDuty alert or logs the failure, depending on whether a
    `pager_duty_off` flag is set in AWS Parameter Store.

    Args:
        pd_conn_id (str): PagerDuty Airflow connection ID.
        job_name (str): Airflow job name
        environment (str): Execution environment

    Returns:
        A callback function that either triggers a PagerDuty alert or logs the
        failure, depending on whether a `pager_duty_off` flag is set in AWS
        Parameter Store.
    """
    if job_name and environment:
        ssm = boto3.client('ssm', region_name=aws_region)
        param_name = '/{environment}/jobs/{job_name}/pager_duty_off'.format(
            environment=environment, job_name=job_name
        )
        try:
            ssm_response = ssm.get_parameter(Name=param_name)

            # True `strtobool` values are y, yes, t, true, on and 1
            # False `strtobool` values are n, no, f, false, off and 0
            if ssm_response and strtobool(ssm_response['Parameter']['Value']):
                return lambda context: logging.info(
                    (
                        'Skipping PagerDuty notification due to '
                        '{param_name} flag set. PagerDuty event: '
                        '{pd_event}'
                    ).format(
                        param_name=param_name,
                        pd_event=WTPagerDutyHook(
                            context=context
                        ).get_pd_event(),
                    )
                )
        except (ssm.exceptions.ParameterNotFound, KeyError, ValueError):
            pass

    return lambda context: WTPagerDutyHook(
        context=context, pd_conn_id=pd_conn_id
    ).execute()
