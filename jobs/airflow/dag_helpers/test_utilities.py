import utilities


def test_partition_for_nodes():
    all_forecast_hours = list(range(0, 240, 3))
    expected_partitioned_nodes = [
        [0, 3, 6],
        [9, 12, 15],
        [18, 21, 24],
        [27, 30, 33],
        [36, 39, 42],
        [45, 48, 51],
        [54, 57, 60],
        [63, 66, 69],
        [72, 75, 78],
        [81, 84, 87],
        [90, 93, 96],
        [99, 102, 105],
        [108, 111, 114],
        [117, 120, 123],
        [126, 129, 132],
        [135, 138, 141],
        [144, 147, 150],
        [153, 156, 159],
        [162, 165, 168],
        [171, 174, 177],
        [180, 183, 186],
        [189, 192, 195],
        [198, 201, 204],
        [207, 210, 213],
        [216, 219, 222],
        [225, 228, 231],
        [234, 237],
    ]
    # Should distribute inputs (forecast hours) among number of nodes
    # requested.
    assert (
        utilities.partition_for_nodes(all_forecast_hours, 27)
        == expected_partitioned_nodes
    )
    # Should limit number of nodes to most efficient packing of inputs. For
    # example, when requesting 32 nodes to distribute 80 forecast hours we
    # will end up with 26 nodes processing 3 forecast hours, 1 node processing
    # 2 forecast hours, and 5 nodes processing 0 forecast hours. The last 5
    # nodes should be removed from the partitioning.
    assert (
        utilities.partition_for_nodes(all_forecast_hours, 32)
        == expected_partitioned_nodes
    )
