from airflow.models import Variable

ECR_HOST = Variable.get('ECR_HOST')
ENVIRONMENT = Variable.get('ENVIRONMENT')


def docker_image(image, prefix='common'):
    """
    Returns the full path for the Docker image.

    Args:
        image (str): Name of Docker image.
        prefix (str): Prefix for the Docker image. Usually the name of the
                      Airflow job. Defaults to `common`.

    Returns:
        Docker image repository path.
    """
    return '{ecr_host}/jobs/{prefix}/{image}:{environment}'.format(
        ecr_host=ECR_HOST, prefix=prefix, image=image, environment=ENVIRONMENT
    )
