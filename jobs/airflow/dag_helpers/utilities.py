import math


def partition_for_nodes(inputs, number_of_nodes):
    inputs_counts_per_node = int(math.ceil(len(inputs) / float(number_of_nodes)))
    inputs_by_node = [
        inputs[
            (i * inputs_counts_per_node) : ((i + 1) * inputs_counts_per_node)
        ]
        for i in range(number_of_nodes)
    ]
    return [
        node_inputs
        for node_inputs in inputs_by_node
        if node_inputs
    ]
