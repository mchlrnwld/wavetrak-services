"""
WTPagerDuty Airflow Plugin

Vendored from https://github.com/airflow-plugins/airflow-pagerduty-plugin/

Refactored to add Airflow hook and to use PagerDuty EventV2 API
"""
import logging
from datetime import datetime

from airflow.exceptions import AirflowException
from airflow.hooks.base_hook import BaseHook
from airflow.models import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

import pypd


class WTPagerDutyOperator(BaseOperator):
    """
    Operator to create Airflow PagerDuty events

    Args:
        pd_conn_id (str): PagerDuty Airflow connection ID
        routing_key (str): PagerDuty service API key
        context (dict): Metadata about a task instance's execution
        summary (str): A brief text summary of the event
        source (str): The unique location of the affected system, preferably a
            hostname or FQDN
        severity (str): The perceived severity of the status the event. This
            can be `critical`, `error`, `warning`, or `info`.
        component (str): Airflow task or component that is responsible for the
            event
        group (str): Airflow DAG or logical grouping of components
        event_class (str): Common description of the event
    """
    @apply_defaults
    def __init__(self,
                 pd_conn_id=None,
                 routing_key=None,
                 summary=None,
                 source=None,
                 severity='error',
                 component=None,
                 group=None,
                 event_class=None,
                 *args,
                 **kwargs):
        super(WTPagerDutyOperator, self).__init__(*args, **kwargs)

        self.pd_conn_id = pd_conn_id
        self.routing_key = routing_key
        self.summary = summary
        self.source = source
        self.severity = severity
        self.component = component
        self.group = group
        self.event_class = event_class

    def execute(self, context):
        """
        Executes Airflow operator
        """
        return WTPagerDutyHook(
            pd_conn_id=self.pd_conn_id,
            routing_key=self.routing_key,
            context=context,
            summary=self.summary,
            source=self.source,
            severity=self.severity,
            component=self.component,
            group=self.group,
            event_class=self.event_class
        ).execute()


class WTPagerDutyHook(BaseHook):
    """
    Hook to create Airflow PagerDuty events

    Args:
        pd_conn_id (str): PagerDuty Airflow connection ID
        routing_key (str): PagerDuty service API key
        context (dict): Metadata about a task instance's execution
        summary (str): A brief text summary of the event
        source (str): The unique location of the affected system, preferably a
            hostname or FQDN
        severity (str): The perceived severity of the status the event. This
            can be `critical`, `error`, `warning`, or `info`.
        component (str): Airflow task or component that is responsible for the
            event
        group (str): Airflow DAG or logical grouping of components
        event_class (str): Common description of the event
    """
    def __init__(self,
                 pd_conn_id=None,
                 routing_key=None,
                 context=None,
                 summary=None,
                 source=None,
                 severity='error',
                 component=None,
                 group=None,
                 event_class=None,
                 *args,
                 **kwargs):

        self.routing_key = self._get_key(routing_key, pd_conn_id)

        # Accept provided event values or build values from task instance
        ti = context.get('task_instance', None) if context else None

        self.source = (source if source
                       else ti.hostname if ti
                       else 'no_hostname_available')

        self.component = (component if component
                          else ti.task_id if ti
                          else 'no_task_id_available')

        self.group = (group if group
                      else ti.dag_id if ti
                      else 'no_dag_id_available')

        self.severity = severity if severity else 'error'
        self.event_class = event_class if event_class else 'Airflow'

        exec_date = ti.execution_date if ti else 'no_execution_date_available'
        state = ti.state if ti else 'no_state_available'

        self.log_url = self._get_log_url(
            ti.hostname,
            ti.dag_id,
            ti.execution_date,
            ti.task_id,
        ) if ti else None

        self.summary = (summary
                        if summary
                        else ('{event_class}: {dag_id}.{task_id} '
                              '{exec_date} [{state}]').format(
                                  event_class=self.event_class,
                                  dag_id=self.group,
                                  task_id=self.component,
                                  exec_date=exec_date,
                                  state=state))

    def _get_log_url(self, hostname, dag_id, execution_date, task_id):
        """
        Returns an Airflow log URL for the failed task

        Args:
            hostname (str): Hostname for Airflow worker instance where the
            task failed.
            dag_id (str): DAG ID for failed task.
            execution_date (str): String representation of execution date for
                                  failed task.
            task_id (str): Task ID for failed task.

        Returns:
            (str) Airflow log URL.
        """
        if not hostname or not dag_id or not execution_date or not task_id:
            return None

        formatted_execution_date = execution_date.strftime('%Y-%m-%dT%H:%M:%S')

        return (
            'http://{0}.aws.surfline.com:8080/admin/airflow/log'
            '?dag_id={1}&execution_date={2}&task_id={3}'
        ).format(
            hostname,
            dag_id,
            formatted_execution_date,
            task_id,
        )

    def _get_key(self, routing_key, pd_conn_id):
        """
        Returns a PagerDuty API routing key

        Args:
            routing_key (str): The explicitly-provided routing_key
            pd_conn_id (str): The Airflow connection ID

            To define which PagerDuty service receives alerts, we require that
            either `pd_conn_id` or `routing_key` be set.

            `pd_conn_id` references a connection defined in
            [Airflow > Admin > Connections] whose `extra` field contains JSON
            containing a `routing_key` field.

            Example:

                {"routing_key": "fa29acd9dc6742d981aab934b3145fef"}

            In either case, the routing key should refer to a PagerDuty service
            integration key of type _Events API v2_.

        Returns:
            (str) A PagerDuty API routing key

        Raises:
            AirflowException: No valid routing key
        """
        if routing_key:
            return routing_key

        key = None
        if pd_conn_id:
            conn = self.get_connection(pd_conn_id)
            extra = conn.extra_dejson
            key = extra.get('routing_key', None)

        if key is None:
            logging.warning("Can't get routing_key: No valid PagerDuty "
                            "routing_key or pd_conn_id supplied")

        return key

    def get_pd_event(self):
        """
        Returns a dictionary representing a PagerDuty event

        Returns:
            (dict) A PagerDuty event
        """
        return {
            'routing_key': self.routing_key,
            'event_action': 'trigger',
            'dedup_key': self.group,
            'payload': {
                'summary': self.summary,
                'source': self.source,
                'severity': self.severity,
                'component': self.component,
                'group': self.group,
                'class': self.event_class,
                'custom_details': {
                    'airflow_log_url': self.log_url,
                },
            }
        }

    def execute(self):
        """
        Executes Airflow hook

        Raises:
            AirflowException: PagerDuty API call failed
        """
        pd_event = self.get_pd_event()
        logging.info("Creating PagerDuty event: {}".format(pd_event))
        try:
            r = pypd.EventV2.create(data=pd_event)
            logging.info("PagerDuty event created: {}".format(r))
        except Exception as ex:
            msg = "PagerDuty API call failed ({})".format(ex)
            logging.error(msg)
            raise AirflowException(msg)


class WTPagerDutyPlugin(AirflowPlugin):
    """
    Wavetrak PagerDuty Airflow Plugin
    """
    name = "wt_pagerduty"
    hooks = [WTPagerDutyHook]
    operators = [WTPagerDutyOperator]
