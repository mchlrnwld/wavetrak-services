from __future__ import print_function
from datetime import datetime
import json
import logging
import os
from airflow import settings
from airflow.models import BaseOperator, TaskInstance, Variable
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.utils.file import TemporaryDirectory
from airflow.utils.state import State
import docker


class WTDockerOperator(BaseOperator):
    template_fields = ('command', 'environment', 'tmp_dir', 'volumes')
    ui_color = '#2496ED'
    ui_fgcolor = '#FFFFFF'

    @apply_defaults
    def __init__(
        self,
        image,
        command=None,
        environment=None,
        force_pull=False,
        tmp_dir='/tmp/airflow',
        volumes=None,
        xcom_push=False,
        *args,
        **kwargs
    ):
        super(WTDockerOperator, self).__init__(*args, **kwargs)

        self.image = image
        self.command = command
        self.environment = (
            {k: v for k, v in environment.items() if v} if environment else {}
        )
        self.force_pull = force_pull
        self.tmp_dir = tmp_dir
        self.volumes = volumes or []
        self.xcom_push = xcom_push
        self.client = None
        self.container_id = None

    def remove_container(self):
        if self.client and self.container_id:
            self.client.remove_container(self.container_id, v=True, force=True)
            self.container_id = None
            logging.info('Removed Docker container')

    def on_kill(self, *args, **kwargs):
        self.remove_container()
        super(WTDockerOperator, self).on_kill(*args, **kwargs)

    def post_execute(self, *args, **kwargs):
        self.remove_container()
        super(WTDockerOperator, self).post_execute(*args, **kwargs)

    def handle_failure(self, *args, **kwargs):
        self.remove_container()
        super(WTDockerOperator, self).handle_failure(*args, **kwargs)

    def run_docker_image(self, context):
        tmp_dir_prefix = 'airflow-{0}-'.format(self.task_id)
        with TemporaryDirectory(
            prefix=tmp_dir_prefix, dir='/tmp'
        ) as host_tmp_dir:
            self.environment['AIRFLOW_TMP_DIR'] = self.tmp_dir
            self.volumes.append('{0}:{1}'.format(host_tmp_dir, self.tmp_dir))

            logging.info('Running Docker image {0}'.format(self.image))

            if self.environment:
                logging.info(
                    'Environment: {0}'.format(
                        json.dumps(self.environment, indent=4)
                    )
                )

            if self.command:
                logging.info('Command: {0}'.format(self.command))

            if self.volumes:
                logging.info('Volumes: {0}'.format(self.volumes))

            self.client = docker.APIClient()

            if self.force_pull or not self.client.images(name=self.image):
                pull_log_stream = self.client.pull(self.image, stream=True)
                for output in pull_log_stream:
                    json_lines = [
                        json.loads(line)
                        for line in output.split('\r\n')
                        if line
                    ]
                    for json_line in json_lines:
                        if 'error' in json_line:
                            raise Exception(json_line['error'])
                        else:
                            logging.info(json_line['status'])

            try:
                volumes = [volume.split(':')[1] for volume in self.volumes]
            except IndexError:
                error = (
                    'Volume bindings must be list colon-separated strings'
                    ':{0}'.format(self.volumes)
                )
                raise ValueError(error)

            container = self.client.create_container(
                self.image,
                command=self.command,
                environment=self.environment,
                volumes=volumes,
                host_config=self.client.create_host_config(binds=self.volumes),
                # User needs permission to write to temp directory
                user='{0}:{1}'.format(os.getuid(), os.getgid()),
            )
            self.container_id = container['Id']

            self.client.start(self.container_id)
            # TODO: Replace this with
            #       attach(self.container_id, demux=True, stream=True)
            #       to split response into a stderr and stdout log streams
            container_log_stream = self.client.logs(
                self.container_id, stream=True
            )
            log_outputs = []
            for output in container_log_stream:
                log_outputs.append(output)
                logging.info(output.strip('\n'))
            command_result = self.client.wait(self.container_id)
            status_code = command_result['StatusCode']
            command_result_message = (
                'Command exited with return code ' '{0}'.format(status_code)
            )

            if status_code != 0:
                raise Exception(command_result_message)

            log_lines = ''.join(log_outputs).strip().split('\n')
            logging.info(command_result_message)
            return log_lines[-1]

    def execute(self, context):
        result = self.run_docker_image(context)

        if self.xcom_push:
            return result


class WTBranchDockerOperator(WTDockerOperator):
    def execute(self, context):
        branch = super(WTBranchDockerOperator, self).run_docker_image(context)
        logging.info('Following branch {0}'.format(branch))
        logging.info('Marking other directly downstream tasks as skipped')
        session = settings.Session()
        for task in context['task'].downstream_list:
            if task.task_id != branch:
                ti = TaskInstance(
                    task,
                    execution_date=context['task_instance'].execution_date,
                )
                ti.state = State.SKIPPED
                ti.start_date = datetime.now()
                ti.end_date = datetime.now()
                session.merge(ti)
        session.commit()
        session.close()
        logging.info("Done.")


class WTDockerPlugin(AirflowPlugin):
    name = 'wt_docker'
    operators = [WTDockerOperator, WTBranchDockerOperator]
