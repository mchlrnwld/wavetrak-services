# Airflow Plugins

## WTDockerOperator

Operator for running a Docker image.

### Parameters

Parameters and defaults inherited from [BaseOperator](https://airflow.apache.org/code.html#airflow.models.BaseOperator).

| Parameter     | Type   | Description                                                                                                                                                                                                                                                                                                                           | Required | Default        | [Templated](https://airflow.apache.org/macros.html#macros) |
| ------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | -------------- | ---------------------------------------------------------- |
| `image`       | `str`  | Docker image to run from repository defined by `ECR_HOST` Airflow variable. For example, `ECR_HOST=833713747344.dkr.ecr.us-west-1.amazonaws.com` and `image=jobs/process-forecast-platform-noaa-gfs/stage-to-s3` will run `ECR_HOST=833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/process-forecast-platform-noaa-gfs/stage-to-s3` | Yes      |                | No                                                         |
| `environment` | `dict` | Environment variables to pass into Docker container.                                                                                                                                                                                                                                                                                  | No       |                | Yes                                                        |
| `force_pull`  | `bool` | Pull Docker image before running. Makes sure the Docker image is up-to-date before each run.                                                                                                                                                                                                                                          | No       | `False`        |                                                            |
| `tmp_dir`     | `str`  | Temporary directory path within Docker container to mount to a temporary directory created on this host.                                                                                                                                                                                                                              | No       | `/tmp/airflow` | Yes                                                        |
| `volumes`     | `list` | Volume bindings to mount to Docker container. Example: `['/path/on/host:/path/in/container']`                                                                                                                                                                                                                                         | No       |                | Yes                                                        |
| `xcom_push`   | `bool` | Boolean to `xcom_push` last line of `stdout` from the Docker container run.                                                                                                                                                                                                                                                           | No       | `False`        |                                                            |

## WTBranchDockerOperator

Operator for running a Docker image to determine which downstream task to execute in a branch. The last line of `stdout` from the Docker run should be the downstream `task_id`.

### Parameters

Parameters and defaults inherited from [WTDockerOperator](#wtdockeroperator)

## WTPagerDutyHook

Hook for triggering PagerDuty events.

### Requirements

Requires that `pypd==1.1.0` be installed on the Airflow server.

### Parameters

| Parameter     | Type   | Description                                                                                            | Required | Default   | [Templated](https://airflow.apache.org/macros.html#macros) |
| ------------- | ------ | ------------------------------------------------------------------------------------------------------ | -------- | --------- | ---------------------------------------------------------- |
| `pd_conn_id`  | `str`  | PagerDuty Airflow connection ID                                                                        | Yes\*    |           | No                                                         |
| `routing_key` | `str`  | PagerDuty service API key                                                                              | Yes\*    |           | No                                                         |
| `context`     | `dict` | Metadata about a task instance's execution                                                             | Yes\*\*  |           | No                                                         |
| `summary`     | `str`  | A brief text summary of the event                                                                      | No       |           | No                                                         |
| `source`      | `str`  | The unique location of the affected system, preferably a hostname or FQDN.                             | No       |           | No                                                         |
| `severity`    | `str`  | The perceived severity of the status the event. This can be `critical`, `error`, `warning`, or `info`. | No       | `error`   | No                                                         |
| `component`   | `str`  | Airflow task or component that is responsible for the event.                                           | No       |           | No                                                         |
| `group`       | `str`  | Airflow DAG or logical grouping of components                                                          | No       |           | No                                                         |
| `event_class` | `str`  | Common description of the event                                                                        | No       | `Airflow` | No                                                         |

#### Notes on Parameters

##### PagerDuty Service Routing Keys

To define which PagerDuty service receives alerts, `WTPagerDutyOperator` requires that either `pd_conn_id` or `routing_key` be set.

`routing_key` explicitly sets a PagerDuty service integration key.

`pd_conn_id` references a connection defined in [Airflow &gt; Admin &gt; Connections](https://airflow.apache.org/howto/connection/index.html) whose `extra` field contains JSON containing a `routing_key` field.

Ex.

```json
{ "routing_key": "fa29acd9dc6742d981aab934b3145fef" }
```

In either case, the routing key should refer to a PagerDuty service integration key of type _Events API v2_.

##### `context` vs. Explicitly-Defined Parameters

`WTPagerDutyHook` and `WTPagerDutyOperator` offer the option of automatically building a PagerDuty event using a task instance's context dictionary. You must pass either a `context` object or `summary`, `source`, `severity`, `component`, `group`, and `event_class`.

### Usage

#### Failure Callback Hook

```python
from airflow.hooks import WTPagerDutyHook
from airflow.models import DAG, Variable
from airflow.operators.bash_operator import BashOperator

def pager_duty_failure_callback(context):
    return WTPagerDutyHook(
        context=context,
        pd_conn_id='pagerduty_platform_squad'
    ).execute()

my_dag = DAG('tutorial')

op = BashOperator(
    dag=my_dag,
    task_id='my_task',
    python_callable=my_python_job,
    provide_context=True,
    on_failure_callback=pager_duty_failure_callback
)
```

## WTPagerDutyOperator

Operator for triggering PagerDuty events.

### Requirements

Requires that `pypd==1.1.0` be installed on the Airflow server.

### Parameters

Parameters and defaults inherited from [WTPagerDutyHook](#wtpagerdutyhook), with the exception of `context`. For `WTPagerDutyOperator`, `context` is provided at execution time.

### Usage

#### Custom Operator

```python
from datetime import datetime, timedelta
import logging

from airflow.models import DAG, Variable
from airflow.operators import BashOperator, DummyOperator, WTPagerDutyOperator


five_minutes_ago = datetime.combine(datetime.today() - timedelta(minutes=5),
                                    datetime.min.time())
args = {
    'owner': 'airflow',
    'start_date': five_minutes_ago
}

dag = DAG(
    dag_id='test_pagerduty_operator',
    default_args=args,
    schedule_interval='5 * * * *',
    dagrun_timeout=timedelta(minutes=20)
)

run_this_first = BashOperator(
    task_id='run_this_first',
    bash_command='echo 1',
    dag=dag
)

run_this_second = BashOperator(
    task_id='run_this_second',
    bash_command='echo 2',
    dag=dag
)

run_this_last = WTPagerDutyOperator(
    task_id='run_this_last',
    pd_conn_id='pagerduty_platform_squad',
    summary='Airflow: test_pagerduty_operator.run_this_last',
    severity='error',
    component='run_this_last',
    group='test_pagerduty_operator',
    event_class='Airflow',
    dag=dag
)

run_this_first.set_downstream(run_this_second)
run_this_second.set_downstream(run_this_last)

if __name__ == "__main__":
    dag.cli()
```
