provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-legacy-tf-state-prod"
    key    = "jobs/query-wowza-error-logs/legacy/terraform.tfstate"
    region = "us-west-1"
  }
}

module "query-wowza-error-logs" {
  source = "../../"

  environment                          = "legacy"
  default_bucket_logs_region           = "us-west-1"
  default_bucket_logs_region_principal = "logs.us-west-1.amazonaws.com"
  athena_bucket_logs_region            = "us-west-2"
}
