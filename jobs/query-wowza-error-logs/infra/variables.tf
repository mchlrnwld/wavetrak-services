variable "environment" {
}

variable "company" {
  default = "wt"
}

variable "application" {
  default = "query-wowza-error-logs"
}

variable "default_bucket_logs_region" {
}

variable "athena_bucket_logs_region" {
}

variable "default_bucket_logs_region_principal" {
}
