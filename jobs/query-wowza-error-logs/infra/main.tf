# Bucket where Cloudwatch log exports are shipped

provider "aws" {
  alias  = "us-west-2"
  region = "us-west-2"
}

resource "aws_s3_bucket" "wavetrak_wowza_exported_logs_default_region" {
  bucket = "${var.company}-${var.application}-${var.environment}-${var.default_bucket_logs_region}"
  acl    = "private"

  tags = {
    Name        = "${var.company}-${var.application}-wowza-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Terraform   = true
  }

  lifecycle_rule {
    id      = "${var.company}-${var.application}-${var.environment}-${var.default_bucket_logs_region}"
    enabled = true

    tags = {
      "rule"      = "${var.company}-${var.application}-wowza-${var.environment}-rule"
      "autoclean" = "true"
    }

    expiration {
      days = 7
    }
  }
}

resource "aws_s3_bucket_policy" "wavetrak_wowza_exported_logs" {
  bucket = aws_s3_bucket.wavetrak_wowza_exported_logs_default_region.id
  policy = templatefile("${path.module}/resources/s3-bucket-policy.json", {
    s3_bucket = aws_s3_bucket.wavetrak_wowza_exported_logs_default_region.arn
    region    = var.default_bucket_logs_region_principal
  })
}

# Bucket where Cloudwatch logs are copied over into Athena-supported region
resource "aws_s3_bucket" "wavetrak_wowza_exported_logs_athena_region" {
  bucket   = "${var.company}-${var.application}-${var.environment}-${var.athena_bucket_logs_region}"
  region   = var.athena_bucket_logs_region
  provider = aws.us-west-2

  lifecycle_rule {
    id      = "${var.company}-${var.application}-${var.environment}-${var.default_bucket_logs_region}"
    enabled = true

    tags = {
      "rule"      = "${var.company}-${var.application}-wowza-${var.environment}-rule"
      "autoclean" = "true"
    }

    expiration {
      days = 7
    }
  }
}

# Bucket where Athena query results are shipped
resource "aws_s3_bucket" "athena_results_bucket" {
  bucket   = "${var.company}-${var.application}-${var.environment}-results"
  region   = var.athena_bucket_logs_region
  provider = aws.us-west-2

  lifecycle_rule {
    id      = "${var.company}-${var.application}-${var.environment}"
    enabled = true

    tags = {
      "rule"      = "${var.company}-${var.application}-wowza-${var.environment}-results-rule"
      "autoclean" = "true"
    }

    expiration {
      days = 7
    }
  }
}
