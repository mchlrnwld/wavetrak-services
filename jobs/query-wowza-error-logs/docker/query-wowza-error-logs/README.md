# Query Wowza Error Logs

## Overview

This application exports the Wowza error logs from Cloudwatch to S3, and queries them for dropped packet messages.

## Quickstart

```sh
make conda-create
conda activate query-wowza-error-logs
cp .env.sample .env
make
```

The `make` command on its own executes the `lint`, `type-check`, and `run` targets described below.

### Environment

Before running any of the commands listed below, be sure to set up your local environment file:

```sh
cp .env.sample .env
```

### Lint, Type Check, & Run

```sh
make
```

### Lint

```sh
make lint
```

### Type Check

This application uses [mypy](http://mypy-lang.org/) to do static type-checking.

```sh
make type-check
```

### Run

```sh
make run
```

## Testing

```sh
make test
```

## Docker

### Build & Run

```sh
make docker
```

### Build

```sh
make docker-build
```

### Run

```sh
make docker-run
```

## Deploying

Deploy via the Jenkins job [shared/deploy-airflow-job](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-airflow-job)

## Future Improvements

1. Parameterize the number of hours to query.
