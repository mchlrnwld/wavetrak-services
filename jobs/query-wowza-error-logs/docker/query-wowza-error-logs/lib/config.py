import os

from job_secrets_helper import get_secret  # type: ignore

# TODO: import lib.config only into main.py and pass variables in
# as arguments to other modules that require them.
# TODO: Group the functions into higher-order modules
# (i.e., /lib/athena.py would contain athena-specific logic, etc)

ENV = os.environ['ENVIRONMENT']
JOB_NAME = os.environ['JOB_NAME']

# Athena constants
ATHENA_QUERY_DESTINATION_PREFIX = os.environ['ATHENA_QUERY_DESTINATION_PREFIX']
ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1 = os.environ[
    'ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1'
]
ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2 = os.environ[
    'ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2'
]
ATHENA_QUERY_EXPORT_TASK_NAME = os.environ['ATHENA_QUERY_EXPORT_TASK_NAME']
ATHENA_QUERY_LOG_GROUP_NAME = os.environ['ATHENA_QUERY_LOG_GROUP_NAME']
ATHENA_QUERY_LOG_STREAM_AU = os.environ['ATHENA_QUERY_LOG_STREAM_AU']
ATHENA_QUERY_LOG_STREAM_EC = os.environ['ATHENA_QUERY_LOG_STREAM_EC']
ATHENA_QUERY_LOG_STREAM_INT = os.environ['ATHENA_QUERY_LOG_STREAM_INT']
ATHENA_QUERY_LOG_STREAM_WC = os.environ['ATHENA_QUERY_LOG_STREAM_WC']
ATHENA_ROLE_ARN = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'ATHENA_ROLE_ARN')

# Athena query details
ATHENA_QUERY_DB = os.environ['ATHENA_QUERY_DB']
ATHENA_QUERY_REGION = os.environ['ATHENA_QUERY_REGION']
ATHENA_QUERY_RESULTS_BUCKET_NAME = os.environ[
    'ATHENA_QUERY_RESULTS_BUCKET_NAME'
]
ATHENA_QUERY_TABLE = os.environ['ATHENA_QUERY_TABLE']

# General constants
AIRFLOW_TMP_DIR = os.environ['AIRFLOW_TMP_DIR']
AWS_DEFAULT_REGION = os.environ['AWS_DEFAULT_REGION']
SLACK_API_TOKEN = get_secret(f'{ENV}/common/', 'WT_SLACK_API_TOKEN')
SLACK_CHANNEL = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'SLACK_CHANNEL')
