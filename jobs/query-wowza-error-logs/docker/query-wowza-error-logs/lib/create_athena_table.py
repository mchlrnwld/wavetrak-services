import logging

import boto3

from lib.assume_iam_role import assume_iam_role
from lib.config import (
    ATHENA_QUERY_DB,
    ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2,
    ATHENA_QUERY_REGION,
)
from templates.create_athena_table_text import return_query_text


logger = logging.getLogger('create-athena-table')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.info('Logging create_athena_table.py')

SESSION_NAME = 'create-athena-table-session'


def create_athena_table():
    """
    Create Athena table if one doesn't exist.
    """

    boto3_session = assume_iam_role(
        SESSION_NAME,
        ATHENA_QUERY_REGION
    )

    athena_client = boto3_session.client(
        'athena',
        region_name=ATHENA_QUERY_REGION
    )

    query_string = return_query_text()
    logger.debug(f'Running the following Athena query: {query_string}')
    athena_response = athena_client.start_query_execution(
        QueryString=query_string,
        QueryExecutionContext={
            'Database': ATHENA_QUERY_DB
        },
        ResultConfiguration={
            'OutputLocation': f's3://{ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2}/',
            'EncryptionConfiguration': {
                'EncryptionOption': 'SSE_S3',
            }
        }
    )

    logger.debug(athena_response)
