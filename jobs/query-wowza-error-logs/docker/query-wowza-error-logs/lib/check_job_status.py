import logging
import time

import boto3

from lib.assume_iam_role import assume_iam_role
from lib.config import AWS_DEFAULT_REGION

logger = logging.getLogger('check-job-status')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.info('Logging check_job_status.py')

SESSION_NAME = 'airflow-check-job-status'


def check_job_status(task_id):
    """
    The export task is asynchronous, so we'll have to poll the aws service
    until we get a success response on the task. Max of 5 min wait.
    """

    boto3_session = assume_iam_role(
        SESSION_NAME,
        AWS_DEFAULT_REGION
    )

    cloudwatch_client = boto3_session.client('logs')

    num_attempts = 0
    while True:
        num_attempts = num_attempts + 1
        try:
            # Wait 7 seconds between polling attempts
            time.sleep(7)
            logger.debug(f'Beginning job-check attempt number {num_attempts}')
            job_status = cloudwatch_client.describe_export_tasks(
                taskId=task_id,
                statusCode='COMPLETED'
            )
            logger.debug('Log export job status: ')
            logger.debug(job_status['exportTasks'][0])
            logger.debug('Log export job sucessfully completed.')
            break
        except IndexError as error:
            logger.debug('Job not yet successfully completed. Retrying.')
            logger.error(error)
