from collections import OrderedDict
import logging
import time

import slack

from lib.config import (
    SLACK_API_TOKEN,
    SLACK_CHANNEL
)

logger = logging.getLogger('slack')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.info('Logging slack.py')


def send_dropped_packet_notification(wowza_error_messages, execution_time):
    """
    Send a summary of updates to Slack

    Arguments:
        wowza_errors: Dictionary of wowza cam aliases and
          their respective number of dropped cam errors
        execution_time: Airflow execution time of this job
     """
    slack_client = slack.WebClient(token=SLACK_API_TOKEN)

    header_block = {
        'type': 'section',
        'text': {
            'type': 'mrkdwn',
            'text': (
                f':package: *Cams with High Dropped Packet Rates '
                'Over the Last Hour* \n'
            )
        }
    }

    execution_time_block = {
        'type': 'context',
        'elements': [
            {
                'type': 'mrkdwn',
                'text': f'{execution_time}'
            }
        ]
    }

    # Generate slack output
    if wowza_error_messages:
        message_list = [
            f'{k} : {v} packets dropped'
            for (k, v)
            in wowza_error_messages.items()
            if int(v) > 1000
        ]

        wowza_error_messages_text = '\n'.join(message_list)

        logger.debug(f'wowza_error_messages_text: {wowza_error_messages_text}')
    else:
        wowza_error_messages_text = 'No cams with > 1000 dropped packet '\
                                    'errors in the last hour.'

    alias_and_error_count_block = {
        'type': 'section',
        'text': {
            'type': 'mrkdwn',
            'text': (
                f'*Cam Alias | Dropped Packets Errors*\n'
                f'{wowza_error_messages_text}'
            )
        }
    }

    msg_blocks = [
        header_block,
        alias_and_error_count_block
    ]

    # Slack will error if we post message blocks > 3000 chars.
    # For now, log this failure and move on.
    # TODO: Break message into multipart chunks of 3000 chars
    # to prevent Slack character limit
    try:
        slack_client.chat_postMessage(
            channel=SLACK_CHANNEL,
            blocks=msg_blocks
        )

    except slack.errors.SlackApiError:
        logger.error(f'Slack error posting the following: {msg_blocks}')

        exception_block = {
            'type': 'section',
            'text': {
                'type': 'mrkdwn',
                'text': (
                    ':warning: *Exception posting Slack message* :warning:\n'
                    'There were too many updated or down cams to post in a '
                    'Slack message. See Airflow logs for a full listing.'
                )
            }
        }

        exception_msg_blocks = [
            header_block,
            exception_block,
            execution_time_block
        ]

        # TODO: if the inner try/catch below is an API error,
        # allow the Airflow job to fail so that it surfaces
        try:
            slack_client.chat_postMessage(
                channel=SLACK_CHANNEL,
                blocks=exception_msg_blocks
            )
        except slack.errors.SlackApiError:
            logger.warning(f'Slack error posting the following: '
                           f'{exception_msg_blocks}')
