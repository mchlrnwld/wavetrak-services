import logging

import boto3

from lib.assume_iam_role import assume_iam_role
from lib.config import (ATHENA_QUERY_REGION)

logger = logging.getLogger('query-clear-s3-bucket')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.info('Logging assume_iam_role.py')

SESSION_NAME = 'delete-s3-objects'


def delete_s3_objects(bucket, object_file_list):
    """
    Delete items from S3 buckets.
    """

    # Set up boto3 session with approriate role/region
    boto3_session = assume_iam_role(
        SESSION_NAME,
        ATHENA_QUERY_REGION
    )

    s3_client = boto3_session.client('s3')

    try:
        deletion_queue = [{'Key': f} for f in object_file_list]

        logger.debug(f"Files to delete {deletion_queue}")

        del_response = s3_client.delete_objects(
            Bucket=bucket,
            Delete={
                'Objects': deletion_queue,
            },
        )
        logger.debug(f'Deletion response is {del_response}')

    except IndexError as error:
        logger.error(
            f'There were problems deleting files from {bucket}'
        )
        logger.error(error)
