import logging
import time

import backoff
import boto3

from lib.assume_iam_role import assume_iam_role
from lib.config import (
    AIRFLOW_TMP_DIR,
    ATHENA_QUERY_REGION,
    ATHENA_QUERY_RESULTS_BUCKET_NAME,
)


logger = logging.getLogger('download-athena-query-results')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.info('Logging download_ahtena_query_results.py')

SESSION_NAME = 'SESSION_NAME'


@backoff.on_predicate(backoff.expo, lambda s3_files: len(s3_files) == 0)
def download_athena_query_results():
    """
    Download results of Athena query from S3
    """

    boto3_session = assume_iam_role(
        SESSION_NAME,
        ATHENA_QUERY_REGION
    )

    s3_client = boto3_session.client(
        's3',
        region_name=ATHENA_QUERY_REGION
    )
    s3_resource = boto3_session.resource('s3')

    all_s3_objects = s3_client.list_objects_v2(
        Bucket=ATHENA_QUERY_RESULTS_BUCKET_NAME
    )
    logger.debug(f'List of S3 objects list: {all_s3_objects}')
    csv_files = all_s3_objects['Contents']

    # Gather the names of the csv files from the paginated S3 object list
    s3_files = []
    paginator = s3_client.get_paginator('list_objects')
    page_iterator = paginator.paginate(
        Bucket=ATHENA_QUERY_RESULTS_BUCKET_NAME,
        PaginationConfig={'PageSize': 20}
    )
    for page in page_iterator:
        for file in page['Contents']:
            if 'metadata' not in file['Key'] and 'csv' in file['Key']:
                s3_files.append(file['Key'])

    # Download files
    for file in s3_files:
        logger.debug(f'Currently downloading file {file}')
        s3_resource.Bucket(ATHENA_QUERY_RESULTS_BUCKET_NAME).\
            download_file(file, f'{AIRFLOW_TMP_DIR}/{file}')
    logger.debug('Query completed and results written to S3.')

    return s3_files
