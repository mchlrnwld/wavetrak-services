import time

import boto3

from lib.config import (
    ATHENA_ROLE_ARN
)


def assume_iam_role(role_session_name, region):
    """
    Configure boto3 session to
    use the correct IAM role.
    """

    sts_client = boto3.client('sts')
    sts_assume_role_response = sts_client.assume_role(
        RoleArn=ATHENA_ROLE_ARN,
        RoleSessionName=f'airflow-session-{role_session_name}'
    )
    sts_acccess_key_id = sts_assume_role_response['Credentials']['AccessKeyId']
    sts_secret_access_key = sts_assume_role_response[
        'Credentials'
    ][
        'SecretAccessKey'
    ]
    sts_session_token = sts_assume_role_response['Credentials']['SessionToken']

    boto3_session = boto3.Session(
          aws_access_key_id=sts_acccess_key_id,
          aws_secret_access_key=sts_secret_access_key,
          aws_session_token=sts_session_token,
          region_name=region
    )

    return boto3_session
