from lib.config import (
    ATHENA_QUERY_DB,
    ATHENA_QUERY_TABLE
)

ATHENA_QUERY = r"""
SELECT DISTINCT
    Regexp_extract_all("x-comment", '\w*\-\w*(?=\.stream:trackID\=)') AS cam_alias,
    Count(Regexp_extract_all("x-comment", '\w*\-\w*(?=\.stream:trackID\=)'))
    OVER (PARTITION BY Regexp_extract_all("x-comment", '\w*\-\w*(?=\.stream:trackID\=)')) AS dropped_packet_messages
FROM
    "{}"."{}"
WHERE
    Regexp_like("x-comment", '\w*\-\w*(?=\.stream:trackID\=*)')
ORDER BY
    dropped_packet_messages DESC
""".format(ATHENA_QUERY_DB, ATHENA_QUERY_TABLE)  # noqa
