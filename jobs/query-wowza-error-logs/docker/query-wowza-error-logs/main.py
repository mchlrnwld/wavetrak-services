import collections
import logging
import os
import re
import time

import boto3  # type: ignore

from datetime import datetime
from datetime import timedelta
from lib.athena_query import ATHENA_QUERY
from lib.assume_iam_role import assume_iam_role
from lib.check_job_status import check_job_status
from lib.config import (
    AIRFLOW_TMP_DIR,
    ATHENA_QUERY_DB,
    ATHENA_QUERY_DESTINATION_PREFIX,
    ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1,
    ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2,
    ATHENA_QUERY_EXPORT_TASK_NAME,
    ATHENA_QUERY_LOG_GROUP_NAME,
    ATHENA_QUERY_LOG_STREAM_AU,
    ATHENA_QUERY_LOG_STREAM_EC,
    ATHENA_QUERY_LOG_STREAM_INT,
    ATHENA_QUERY_LOG_STREAM_WC,
    ATHENA_QUERY_REGION,
    ATHENA_QUERY_RESULTS_BUCKET_NAME,
    ATHENA_QUERY_TABLE,
    AWS_DEFAULT_REGION,
)
from lib.create_athena_table import create_athena_table
from lib.delete_s3_objects import delete_s3_objects
from lib.download_athena_query_results import download_athena_query_results
from lib.slack import send_dropped_packet_notification

# TODO: main.py has many responsibilities, and should be
# refactored into a series of simple abstracted function calls

logger = logging.getLogger('query-wowza-error-logs')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.info('Logging main.py')


def main():
    """
    Export logs from Cloudwatch to S3.
    Perform Athena query.
    Parse results.
    Send output text to Slack.
    """

    boto3_session = assume_iam_role('main-session', ATHENA_QUERY_REGION)

    # Exports wowza's error logs from cloudwatch to
    # S3 to prepare them for Athena processing.
    # Also runs an Athena query that counts dropped packet
    # messages per cam alias.

    cloudwatch_logs_client = boto3_session.client(
        'logs', region_name=AWS_DEFAULT_REGION
    )
    # Prefixes for the log streams to be exported
    LOG_STREAM_NAME_PREFIX = [
        ATHENA_QUERY_LOG_STREAM_AU, ATHENA_QUERY_LOG_STREAM_EC,
        ATHENA_QUERY_LOG_STREAM_INT,
        ATHENA_QUERY_LOG_STREAM_WC
    ]

    # Determine the current time

    current_UTC_time = datetime.utcnow()
    current_epoch_time = int(datetime.timestamp(datetime.utcnow())) * 1000

    # Determine the time 1 hour ago
    past_UTC_time = current_UTC_time - timedelta(hours=1)
    past_epoch_time = int(
        datetime.timestamp(current_UTC_time - timedelta(hours=1))
    ) * 1000

    for stream in LOG_STREAM_NAME_PREFIX:
        response = cloudwatch_logs_client.create_export_task(
            taskName=f'{ATHENA_QUERY_EXPORT_TASK_NAME}_{stream}',
            logGroupName=ATHENA_QUERY_LOG_GROUP_NAME,
            logStreamNamePrefix=stream,
            fromTime=past_epoch_time,
            to=current_epoch_time,
            destination=ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1,
            destinationPrefix=ATHENA_QUERY_DESTINATION_PREFIX
        )

        # Get taskId which will be used to check job status
        taskId = response['taskId']

        logger.debug(f'Cloudwatch logs export task id: {taskId}')

        # The export task is asynchronous, so
        # we'll have to poll the aws service
        # until we get a success response on
        # the task. Max of a five min wait.

        check_job_status(taskId)

    logger.debug(f'Exporting logs from the following time range:')
    logger.debug(f'Start time: {past_UTC_time}')
    logger.debug(f'End time: {current_UTC_time}')
    logger.debug(f'Response:{response}')
    logger.debug(f'TaskId:{taskId}')

    # The export task is asynchronous, so
    # we'll have to poll the aws service
    # until we get a success response on
    # the task. Max of 10 attempts.

    # Query S3 for the newly exported objects in bucket
    s3_client = boto3_session.client('s3')
    all_s3_objects = s3_client.list_objects(
        Bucket=ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1
    )
    zip_file_contents = all_s3_objects['Contents']
    zip_file_names = all_s3_objects['Contents'][0]['Key']
    logger.debug(f'Exported zip file names: {zip_file_names}')

    # Gather all the names of the gzip files into a list
    s3_gzip_files = [file['Key'] for file in zip_file_contents
                     if 'gz' in file['Key']]

    # Copy the data to a second bucket in an Athena-supported region
    s3_resource = boto3_session.resource('s3')
    for file in s3_gzip_files:
        copy_source = {
            'Bucket': ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1,
            'Key': file
        }

        destination_bucket = s3_resource.Bucket(
            ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2
        )
        destination_bucket.copy(copy_source, file)

    # Create Athena table if one doesn't exist
    create_athena_table()

    # Perform Athena query
    # Note: the following query must be performed
    # in the Oregon (US_WEST_2) region

    athena_client = boto3_session.client(
        'athena',
        region_name=ATHENA_QUERY_REGION
    )
    logger.debug(f'ATHENA_QUERY to be performed: {ATHENA_QUERY}')
    athena_query_response = athena_client.start_query_execution(
        QueryString=ATHENA_QUERY,
        QueryExecutionContext={
            'Database': ATHENA_QUERY_DB
        },
        ResultConfiguration={
            'OutputLocation': f's3://{ATHENA_QUERY_RESULTS_BUCKET_NAME}/',
            'EncryptionConfiguration': {
                'EncryptionOption': 'SSE_S3',
            }
        }
    )
    logger.debug(f'Response of athena query: {athena_query_response}')

    # Download the results of the athena query
    downloaded_result_files = download_athena_query_results()

    # Parse results of the query
    cam_error_dict = collections.OrderedDict()
    file_parse_list = [
        file for file in os.listdir(AIRFLOW_TMP_DIR)
        if '.csv' in file
    ]
    logger.info(f'Parsing the following file(s): {file_parse_list}')
    for f in file_parse_list:
        with open(f'{AIRFLOW_TMP_DIR}/{f}', 'r') as file_handle:
            content = file_handle.read()
            content = content.replace('\"', '')
            content = content.split('\n')

            cam_alias = re.compile(r'\w*-\w*')
            error_count = re.compile(r'(?<!(r|-|\d))([\d.]+)')

            for item in content[1:-1]:
                cam_alias_match = cam_alias.search(item).group()
                error_count_match = error_count.search(item).group()
                cam_error_dict[cam_alias_match] = error_count_match

    logger.debug(f'Cam dropped packet errors: {cam_error_dict}')

    # Send Slack notification
    report_time = time.strftime('%a, %d %b %Y %H:%M')
    send_dropped_packet_notification(cam_error_dict, report_time)

    # Delete downloaded files from Athena query results bucket
    delete_s3_objects(
        ATHENA_QUERY_RESULTS_BUCKET_NAME,
        downloaded_result_files
    )
    # Delete downloaded files from log export buckets
    delete_s3_objects(ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1, s3_gzip_files)
    delete_s3_objects(ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2, s3_gzip_files)


if __name__ == '__main__':
    main()
