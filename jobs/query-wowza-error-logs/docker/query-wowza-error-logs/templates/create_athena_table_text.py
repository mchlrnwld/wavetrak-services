from lib.config import (
    ATHENA_QUERY_TABLE,
    ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2
)


def return_query_text():
    CREATE_TABLE_TEXT =\
        f'''
        CREATE EXTERNAL TABLE IF NOT EXISTS `{ATHENA_QUERY_TABLE}`(
        `x-severity` string,
        `x-category` string,
        `x-event` string,
        `date` string,
        `time` string,
        `c-client-id` string,
        `c-ip` string,
        `c-port` string,
        `cs-bytes` string,
        `sc-bytes` string,
        `x-duration` string,
        `x-sname` string,
        `x-stream-id` string,
        `x-spos` string,
        `sc-stream-bytes` string,
        `cs-stream-bytes` string,
        `x-file-size` string,
        `x-file-length` string,
        `x-ctx` string,
        `x-comment` string)
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t'
        STORED AS INPUTFORMAT
        'org.apache.hadoop.mapred.TextInputFormat'
        OUTPUTFORMAT
        'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
        LOCATION
        's3://{ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2}/'
        TBLPROPERTIES (
        'has_encrypted_data'='false',
        'transient_lastDdlTime'='1566510044')
        '''
    return CREATE_TABLE_TEXT
