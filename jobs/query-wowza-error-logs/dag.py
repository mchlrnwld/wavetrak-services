import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from airflow.operators.bash_operator import BashOperator
from dag_helpers.docker import docker_image

# TODO: implement `wavetrak-job-helpers` secrets management abstraction
# TODO: change ATHENA_QUERY environment prefix to better name
# TODO: update bucket names to `QUERY_WOWZA_ERROR_LOGS_TEMP_BUCKET`
# and `QUERY_WOWZA_ERROR_LOGS_ATHENA_BUCKET`

ATHENA_QUERY_DB = 'wowza_log_analysis'
ATHENA_QUERY_REGION = 'us-west-2'
ATHENA_QUERY_RESULTS_BUCKET_NAME = 'wt-query-wowza-error-logs-legacy-results'
ATHENA_QUERY_TABLE = 'wowza_dropped_packets_table'
ATHENA_QUERY_DESTINATION_PREFIX = 'export-task-output'
ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1 = (
    'wt-query-wowza-error-logs-legacy-us-west-1'
)
ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2 = (
    'wt-query-wowza-error-logs-legacy-us-west-2'
)
ATHENA_QUERY_EXPORT_TASK_NAME = 'cloudwatch_export_task'
ATHENA_QUERY_LOG_GROUP_NAME = 'wt-logs-wowza-recorder'
ATHENA_QUERY_LOG_STREAM_AU = 'surfline-wowza-recorder-au-prod-error'
ATHENA_QUERY_LOG_STREAM_EC = 'surfline-wowza-recorder-ec-prod-error'
ATHENA_QUERY_LOG_STREAM_INT = 'surfline-wowza-recorder-int-prod-error'
ATHENA_QUERY_LOG_STREAM_WC = 'surfline-wowza-recorder-wc-prod-error'
AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
JOB_NAME = 'query-wowza-error-logs'

query_wowza_error_logs_environment = {
    'ATHENA_QUERY_DB': ATHENA_QUERY_DB,
    'ATHENA_QUERY_REGION': ATHENA_QUERY_REGION,
    'ATHENA_QUERY_RESULTS_BUCKET_NAME': ATHENA_QUERY_RESULTS_BUCKET_NAME,
    'ATHENA_QUERY_TABLE': ATHENA_QUERY_TABLE,
    'ATHENA_QUERY_DESTINATION_PREFIX': ATHENA_QUERY_DESTINATION_PREFIX,
    'ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1': ATHENA_QUERY_EXPORT_BUCKET_US_WEST_1,
    'ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2': ATHENA_QUERY_EXPORT_BUCKET_US_WEST_2,
    'ATHENA_QUERY_EXPORT_TASK_NAME': ATHENA_QUERY_EXPORT_TASK_NAME,
    'ATHENA_QUERY_LOG_GROUP_NAME': ATHENA_QUERY_LOG_GROUP_NAME,
    'ATHENA_QUERY_LOG_STREAM_AU': ATHENA_QUERY_LOG_STREAM_AU,
    'ATHENA_QUERY_LOG_STREAM_EC': ATHENA_QUERY_LOG_STREAM_EC,
    'ATHENA_QUERY_LOG_STREAM_INT': ATHENA_QUERY_LOG_STREAM_INT,
    'ATHENA_QUERY_LOG_STREAM_WC': ATHENA_QUERY_LOG_STREAM_WC,
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'ENVIRONMENT': APP_ENVIRONMENT,
    'JOB_NAME': JOB_NAME,
}

default_args = {
    'owner': 'surfline',
    'start_date': datetime(2019, 9, 20),
    # 'email': 'platformsquad@surfline.com',
    # 'email_on_failure': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=0),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=60),
    max_active_runs=1,
    default_args=default_args,
)

WTDockerOperator(
    task_id='query-wowza-error-logs',
    image=docker_image('query-wowza-error-logs', JOB_NAME),
    environment=query_wowza_error_logs_environment,
    force_pull=True,
    email_on_retry=True,
    dag=dag,
)
