.PHONY: all
all: lint type-check run

.PHONY: format
format:
	@black .

.PHONY: lint
lint:
	@black --check .

.PHONY: type-check
type-check:
	@mypy .

.PHONY: run
run:
	env $(shell xargs < .env) python main.py

.PHONY: test
test:
	env $(shell xargs < .env.test) pytest .

.PHONY: conda-create
conda-create:
	conda env create -f environment.yml

.PHONY: conda-activate
conda-activate:
	@echo "It's not possible to activate a conda environment from make. Use the following command to activate conda environment:"
	@echo
	@echo "    conda activate check-and-update-camera-time-settings"
	@echo

.PHONY: docker
docker: docker-build docker-run

.PHONY: docker-build
docker-build:
	docker build -t check-and-update-camera-time-settings/check-and-update-camera-time-settings .

.PHONY: docker-run
docker-run:
	docker run --env-file=.env --rm check-and-update-camera-time-settings/check-and-update-camera-time-settings
