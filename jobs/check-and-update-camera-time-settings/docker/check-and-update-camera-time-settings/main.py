import logging
from typing import List

from lib.cam import Cam, CamError
from lib.cam_utils import get_cams, get_down_cam_ids
from lib.config import CAM_LIMIT, CAM_TIME_THRESHOLD, EXECUTION_TIMESTAMP
from lib.slack import send_cam_notification

logger = logging.getLogger('check-and-update-camera-time-settings')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    cam_updates = {}
    cam_updates['updated'] = []
    cam_updates['unchanged'] = []
    cam_updates['down_known'] = []
    cam_updates['down_unknown'] = []

    # Get a list of cam ids known to be "down"
    down_cam_ids = get_down_cam_ids()

    # Iterate over all cams
    for cam in get_cams(CAM_LIMIT):
        try:
            # Get camera clock time
            (cam_dt, clock_skew) = cam.get_datetime()

            logger.info(cam)

            # If clock skew exceeds threshold, update time and NTP settings
            if clock_skew > CAM_TIME_THRESHOLD:
                logger.warn(f'{cam.name}\'s time is off')
                cam.set_datetime()
                cam.set_ntp_options()
                cam_updates['updated'].append(cam)
            else:
                cam_updates['unchanged'].append(cam)
        except CamError as e:
            logger.info(cam)
            logger.warn(f'Camera Error: {e}')

            # If cam is unreachable, check to see if this is known
            if cam.cam_id in down_cam_ids:
                cam_updates['down_known'].append(cam)
            else:
                cam_updates['down_unknown'].append(cam)

        # Include separator to differentiate entries
        logger.info('-' * 20)

    # Send Slack notification
    send_cam_notification(cam_updates, EXECUTION_TIMESTAMP)


if __name__ == '__main__':
    main()
