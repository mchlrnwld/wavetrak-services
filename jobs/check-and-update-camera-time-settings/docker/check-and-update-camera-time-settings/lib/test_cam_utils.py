from airtable import Airtable  # type: ignore
import requests

from lib.axis_cam import AxisCam
from lib.cam_utils import get_cams, get_down_cam_ids, parse_credentials
from lib.config import CAMERAS_API
from lib.dahua_cam import DahuaCam


def test_parse_credentials():
    assert parse_credentials('username:password') == ['username', 'password']


def test_parse_credentials_containing_colon():
    assert parse_credentials('username:pass:word') == ['username', 'pass:word']


def test_parse_credentials_with_no_colon():
    assert parse_credentials('username') == ['username', None]


def test_parse_credentials_with_empty_string():
    assert parse_credentials('') == [None, None]


def test_parse_credentials_with_only_colon():
    assert parse_credentials(':') == [None, None]


def test_get_down_cam_ids_single_id(requests_mock):
    requests_mock.get(
        f'{CAMERAS_API}/cameras/down',
        json=[{'_id': '5cd2f613e64ac813641c836a'}],
    )
    assert get_down_cam_ids() == ['5cd2f613e64ac813641c836a']
    assert requests_mock.called


def test_get_down_cam_ids_multiple_ids(requests_mock):
    requests_mock.get(
        f'{CAMERAS_API}/cameras/down',
        json=[
            {'_id': '5cd2f613e64ac813641c836a'},
            {'_id': '5bfd97ff7dccf87939966b0f'},
            {'_id': '5b0d1840ca4ef9000fffe184'},
        ],
    )
    assert get_down_cam_ids() == [
        '5cd2f613e64ac813641c836a',
        '5bfd97ff7dccf87939966b0f',
        '5b0d1840ca4ef9000fffe184',
    ]
    assert requests_mock.called


def test_get_down_cam_ids_no_ids(requests_mock):
    requests_mock.get(f'{CAMERAS_API}/cameras/down', json=[])
    assert get_down_cam_ids() == []
    assert requests_mock.called


def test_get_cams(mocker):
    mocker.patch.object(
        Airtable,
        'get_all',
        autospec=True,
        return_value=[
            {
                'id': 'recdWuuccXBEdy1gc',
                'fields': {
                    'id': '5834970f3421b20545c41234',
                    'name': 'LA - Fake Dahua Cam Name',
                    'alias': 'fake-dahua-cam-name',
                    'camBrand': 'Dahua',
                    'camUserPass': 'username:password',
                    'camPubIP': 'fake-dahua-cam-name.ddns.net:82',
                },
                'createdTime': '2017-10-26T10:37:57.000Z',
            },
            {
                'id': 'recKSracArb8sZuVH',
                'fields': {
                    'id': '12340c3fca4ef9000f048873',
                    'name': 'LA - Fake Axis Cam Name',
                    'alias': 'fake-axis-cam-name',
                    'camBrand': 'Axis',
                    'camUserPass': 'username:password',
                    'camPubIP': 'fake-axis-cam-name.ddns.net:82',
                },
                'createdTime': '2017-10-26T10:37:57.000Z',
            },
        ],
    )
    cams = [
        DahuaCam(
            '5834970f3421b20545c41234',
            'LA - Fake Dahua Cam Name',
            'fake-dahua-cam-name',
            'fake-dahua-cam-name.ddns.net:82',
            'username',
            'password',
        ),
        AxisCam(
            '12340c3fca4ef9000f048873',
            'LA - Fake Axis Cam Name',
            'fake-axis-cam-name',
            'fake-axis-cam-name.ddns.net:82',
            'username',
            'password',
        ),
    ]
    assert get_cams() == cams
