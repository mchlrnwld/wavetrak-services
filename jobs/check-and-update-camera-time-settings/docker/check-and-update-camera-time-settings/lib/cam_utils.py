import logging
from typing import List, Sequence, Optional, Union

from airtable import Airtable  # type: ignore
import requests

from lib.axis_cam import AxisCam
from lib.cam import Cam
from lib.config import (
    AIRTABLE_API_KEY,
    AIRTABLE_APP_ID,
    AIRTABLE_VIEW,
    CAMERAS_API,
)
from lib.dahua_cam import DahuaCam
from lib.sony_cam import SonyCam

logger = logging.getLogger('check-and-update-camera-time-settings')


def parse_credentials(credentials: str) -> Sequence[Optional[str]]:
    '''
    Parse credentials from a string

    Arguments:
        credentials: A string containing username and password separated by ':'
    '''
    if not credentials or credentials == ':':
        return [None, None]

    cred_list: Sequence[Optional[str]] = credentials.split(':', 1)
    if len(cred_list) == 1:
        cred_list.append(None)  # type: ignore

    return cred_list


def get_cams(count: int = None) -> Sequence[Cam]:
    '''
    Get a list of Cam objects from Airtable.

    Arguments:
        count: Number of cams to retrieve, or None for all

    Returns:
        A sequence of Cam objects, subclassed by camera brand
    '''
    airtable = Airtable(AIRTABLE_APP_ID, 'Data', api_key=AIRTABLE_API_KEY)
    airtable_cams = airtable.get_all(
        view=AIRTABLE_VIEW,
        fields=['id', 'alias', 'name', 'camBrand', 'camPubIP', 'camUserPass'],
        max_records=count,
    )

    cams = []
    for cam_info in airtable_cams:
        cam_id = cam_info['fields'].get('id', None)
        name = cam_info['fields'].get('name', None)
        alias = cam_info['fields'].get('alias', None)
        host_and_port = cam_info['fields'].get('camPubIP', None)
        credentials = cam_info['fields'].get('camUserPass', None)
        brand = cam_info['fields'].get('camBrand', None)

        (username, password) = (
            parse_credentials(credentials) if credentials else [None, None]
        )  # type: ignore

        cam: Optional[Cam] = None
        if brand == 'Axis':
            cam = AxisCam(
                cam_id, name, alias, host_and_port, username, password
            )
        elif brand == 'Dahua':
            cam = DahuaCam(
                cam_id, name, alias, host_and_port, username, password
            )
        elif brand == 'Sony':
            cam = SonyCam(
                cam_id, name, alias, host_and_port, username, password
            )
        else:
            logger.error(f'InvalidCamBrand: {brand}')

        if cam:
            cams.append(cam)

    return cams


def get_down_cam_ids() -> List[str]:
    '''
    Get a list of down camera ids from the cameras service

    Returns:
        A list of down camera ids
    '''
    down_cams = requests.get(f'{CAMERAS_API}/cameras/down').json()
    down_cam_ids = [cam['_id'] for cam in down_cams]
    return down_cam_ids
