from datetime import datetime
import logging
import json
import time
from typing import Dict, List

import slack  # type: ignore

from lib.config import SLACK_API_TOKEN, SLACK_CHANNEL, SLACK_CHAR_LIMIT

logger = logging.getLogger('check-and-update-camera-time-settings')


def split_and_send(cam_data: str):
    '''
    Breaks Slack message blocks into smaller chunks and sends to Slack

    Arguments:
        cam_data: String of cams + status data
    '''
    slack_client = slack.WebClient(token=SLACK_API_TOKEN)
    chunked_text = cam_data.splitlines()
    running_string = ''

    for chunk in chunked_text:
        # Subtract 50 to account for non-text payload characters
        if len(running_string) + len(chunk) > SLACK_CHAR_LIMIT - 50:

            payload = {
                'type': 'section',
                'text': {'type': 'mrkdwn', 'text': running_string},
            }
            slack_client.chat_postMessage(
                channel=SLACK_CHANNEL, blocks=[payload]
            )
            time.sleep(2)
            running_string = ''

        running_string += chunk + '\n'

    payload = {
        'type': 'section',
        'text': {'type': 'mrkdwn', 'text': running_string},
    }

    slack_client.chat_postMessage(channel=SLACK_CHANNEL, blocks=[payload])
    time.sleep(2)


def send_cam_notification(cam_updates: Dict[str, List], execution_time: str):
    '''
    Send a summary of updates to Slack

    Arguments:
        cam_updates: Dictionary of cam statuses and the cams that fall
            under each status
        execution_time: Airflow execution time of this job
    '''
    unchanged_cams = cam_updates['unchanged']
    updated_cams = cam_updates['updated']
    down_known_cams = cam_updates['down_known']
    down_unknown_cams = cam_updates['down_unknown']
    total_cam_count = (
        len(unchanged_cams)
        + len(updated_cams)
        + len(down_known_cams)
        + len(down_unknown_cams)
    )

    slack_client = slack.WebClient(token=SLACK_API_TOKEN)

    header_block = {
        'type': 'section',
        'text': {
            'type': 'mrkdwn',
            'text': (f':video_camera: *Camera Time Update* :clock1:\n'),
        },
    }

    execution_time_block = {
        'type': 'context',
        'elements': [{'type': 'mrkdwn', 'text': f'{execution_time}'}],
    }

    summary_text = (
        f'{total_cam_count} Total | '
        f'{len(unchanged_cams)} Unchanged | '
        f'{len(updated_cams)} Updated | '
        f'{len(down_known_cams)} Down (Known) | '
        f'{len(down_unknown_cams)} Down (Unknown)'
    )

    logger.info(f'Summary:\n{summary_text}')
    logger.info('-' * 20)

    summary_block = {
        'type': 'section',
        'text': {'type': 'mrkdwn', 'text': summary_text},
    }

    if updated_cams:
        updated_cams_text = '\n'.join(
            [
                f'{cam.name} - `{cam.alias}` - ±'
                f'{cam.last_clock_skew.total_seconds():.3f}s'
                for cam in updated_cams
            ]
        )
    else:
        updated_cams_text = '_None_'

    logger.info(f'Updated:\n{updated_cams_text}')
    logger.info('-' * 20)

    updated_cams_block = {
        'type': 'section',
        'text': {
            'type': 'mrkdwn',
            'text': (f'*Updated*\n' f'{updated_cams_text}'),
        },
    }

    if down_known_cams:
        down_known_cams_text = '\n'.join(
            [
                f'{cam.name} - `{cam.alias}`' f' - {cam.last_error}'
                for cam in down_known_cams
            ]
        )
    else:
        down_known_cams_text = '_None_'

    # Don't include known down cams in Slack report but log them here
    logger.info(f'Down (known):\n{down_known_cams_text}')
    logger.info('-' * 20)

    if down_unknown_cams:
        down_unknown_cams_text = '\n'.join(
            [
                f'{cam.name} - `{cam.alias}`' f' - {cam.last_error}'
                for cam in down_unknown_cams
            ]
        )
    else:
        down_unknown_cams_text = '_None_'

    logger.info(f'Down (unknown):\n{down_unknown_cams_text}')
    logger.info('-' * 20)

    down_unknown_cams_block = {
        'type': 'section',
        'text': {
            'type': 'mrkdwn',
            'text': (
                f':warning: *Down (Unknown)* :warning:\n'
                f'{down_unknown_cams_text}'
            ),
        },
    }

    msg_blocks = [
        header_block,
        summary_block,
        updated_cams_block,
        down_unknown_cams_block,
        execution_time_block,
    ]

    try:
        if len(json.dumps(msg_blocks)) < SLACK_CHAR_LIMIT:
            slack_client.chat_postMessage(
                channel=SLACK_CHANNEL, blocks=msg_blocks
            )
            time.sleep(2)

        else:
            slack_client.chat_postMessage(
                channel=SLACK_CHANNEL, blocks=[header_block]
            )
            time.sleep(2)
            slack_client.chat_postMessage(
                channel=SLACK_CHANNEL, blocks=[summary_block]
            )
            time.sleep(2)

            for block in [updated_cams_block, down_unknown_cams_block]:
                if len(json.dumps(block)) < SLACK_CHAR_LIMIT:
                    slack_client.chat_postMessage(
                        channel=SLACK_CHANNEL, blocks=[block]
                    )
                    time.sleep(2)

                else:
                    # Type is ignored below beause of a bug similar to:
                    # https://github.com/python/mypy/issues/8277
                    split_and_send(block['text']['text'])  # type: ignore

    except slack.errors.SlackApiError:
        logger.warning(f'Slack error posting the following: {msg_blocks}')

        exception_block = {
            'type': 'section',
            'text': {
                'type': 'mrkdwn',
                'text': (
                    ':warning: *Exception posting Slack message* :warning:\n'
                    'There were too many updated or down cams to post in a '
                    'Slack message. See Airflow logs for a full listing.'
                ),
            },
        }

        exception_msg_blocks = [
            header_block,
            summary_block,
            exception_block,
            execution_time_block,
        ]

        try:
            slack_client.chat_postMessage(
                channel=SLACK_CHANNEL, blocks=exception_msg_blocks
            )
            time.sleep(2)
        except slack.errors.SlackApiError:
            logger.warning(
                f'Slack error posting the following: '
                f'{exception_msg_blocks}'
            )
