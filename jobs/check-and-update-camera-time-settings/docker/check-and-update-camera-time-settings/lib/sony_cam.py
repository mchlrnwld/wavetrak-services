from datetime import datetime, timedelta
import logging
from typing import Optional, Tuple
from urllib.parse import parse_qs

from lib.cam import Cam, CamError

logger = logging.getLogger('check-and-update-camera-time-settings')


class SonyCam(Cam):
    '''
    Sony camera class

    Manages cam API operations, specifically for Sony cams.

    Sony cam API doc:
    https://pro.sony/s3/cms-static-content/uploadfile/04/1237493603504.pdf

    Attributes:
        cam_id: Camera ID, in BSON ObjectId string format
        name: Camera name
        alias: Camera alias
        host_and_port: Hostname and port, in `{hostname}:{port}` format
        username: Camera API username
        password: Camera API password
        brand: Camera brand, `Sony`
    '''

    def __init__(
        self,
        cam_id: str,
        name: str,
        alias: str,
        host_and_port: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
    ):
        '''
        Initializes Cam
        '''
        super().__init__(
            cam_id, name, alias, host_and_port, username, password
        )
        self.brand = 'Sony'

    def get_ntp_options(self):
        '''
        Get NTP options from Sony cam

        Implements the following API contract with the physical camera:

            HTTP request:
                ???

            HTTP response:
                ???
        '''
        # Not implemented - Will add when needed
        pass

    def set_ntp_options(self):
        '''
        Set NTP options for Sony cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /command/system.cgi
                    NtpService=on
                    NtpSrvAdd=conf
                    NtpServer=pool.ntp.org
                    TimeZone=UTC

            HTTP response:
                None (204 status code)

            Note: All updates are done via HTTP GET.

        Raises:
            CamError: An error occurred communicating with a camera
        '''
        logger.info(f'Setting NTP Options for {self.alias}')

        params = {
            'NtpService': 'on',
            'NtpSrvAdd': 'conf',
            'NtpServer': 'pool.ntp.org',
            'TimeZone': 'Etc/UTC',
            'DstMode': 'off',
        }
        r = super().http_get(
            f'http://{self.host_and_port}' f'/command/system.cgi',
            params=params,
            username=self.username,
            password=self.password,
        )

        if r.text == 'ERROR':
            self.last_error = f'Failed to update NTP options'
            raise CamError(f'Failed to update NTP options for {self.alias}')

    def get_datetime(self) -> Tuple[datetime, timedelta]:
        '''
        Get Datetime from Sony Cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /command/inquiry.cgi?inq=system

            HTTP response:
                This response contains many key/value pairs in querystring fmt

                ModelName=SNC-WR602&Serial=3200436&SoftVersion=2.7.2&[...]

        Returns:
            This returns a tuple containing the following data:

                Camera datetime: The camera's clocktime
                Clock skew: A timedelta representing the difference between
                    the camera clock time and actual time

        Raises:
            CamError: An error occurred communicating with a camera
        '''
        logger.info(f'Getting datetime for {self.alias}')

        request_dt = datetime.utcnow()

        r = super().http_get(
            (f'http://{self.host_and_port}' f'/command/inquiry.cgi'),
            params={'inq': 'system'},
            username=self.username,
            password=self.password,
        )

        elapsed_time = datetime.utcnow() - request_dt

        try:
            cam_info = parse_qs(r.text)

            # GmTime returned in the format 'yymmddHHMMSSw'
            # `w` at the end is day-of-week as a decimal number,
            # where Sunday is 1 and Saturday is 7. We can strip it.
            clock_dt_str = cam_info.get('GmTime', [])[0][:-1]
            clock_dt = datetime.strptime(clock_dt_str, '%y%m%d%H%M%S')
            self.last_clock_dt = clock_dt

            clock_skew = abs(clock_dt - (request_dt + elapsed_time))
            self.last_clock_skew = clock_skew

            return (clock_dt, clock_skew)
        except (IndexError, ValueError) as e:
            self.last_error = f'Invalid Datetime Format'
            raise CamError('Invalid datetime format', e)

    def set_datetime(self):
        '''
        Set Datetime for Sony Cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /command/etc.cgi
                    GmTime=1907182152055

            HTTP response:
                None (204 status code)

            Note: All updates are done via HTTP GET.

            Timestamp format 'yymmddHHMMSSw'. `w` at the end is day-of-week as
            a decimal number, where Sunday is 1 and Saturday is 7.

        Raises:
            CamError: An error occurred communicating with a camera
        '''
        logger.info(f'Setting datetime for {self.alias}')

        dt = datetime.utcnow()
        dt_str = datetime.strftime(dt, '%y%m%d%H%M%S')

        # Sony seems to use 1..7 for day-of-week index, Python uses 0..6
        day_of_week = int(datetime.strftime(dt, '%w')) + 1

        params = {'GmTime': f'{dt_str}{day_of_week}'}

        r = super().http_get(
            f'http://{self.host_and_port}/command/etc.cgi',
            params=params,
            username=self.username,
            password=self.password,
        )

        if r.text == 'Error':
            self.last_error = f'Failed to set datetime'
            raise CamError(f'Failed to set datetime for {self.alias}')
