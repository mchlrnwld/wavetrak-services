from abc import ABC, abstractmethod
from datetime import datetime, timedelta
import logging
from typing import Dict, Optional, Tuple

import requests

logger = logging.getLogger('check-and-update-camera-time-settings')

REQUEST_TIMEOUT = 10


class Cam(ABC):
    '''
    Base camera class

    Manages cam API operations. Subclassed to represent cam brands and models
    with unique APIs. Defines common interface via `@abstractmethod`
    annotations.

    Attributes:
        cam_id: Camera ID, in BSON ObjectId string format
        name: Camera name
        alias: Camera alias
        host_and_port: Hostname and port, in `{hostname}:{port}` format
        username: Camera API username
        password: Camera API password
        brand: Camera brand, likely `Dahua` or `Axis`
    '''

    def __init__(
        self,
        cam_id: str,
        name: str,
        alias: str,
        host_and_port: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
    ):
        '''
        Initializes Cam
        '''
        self.cam_id = cam_id
        self.name = name
        self.alias = alias
        self.host_and_port = host_and_port
        self.username = username
        self.password = password
        self.brand: Optional[str] = None
        self.last_clock_dt: Optional[datetime] = None
        self.last_clock_skew: Optional[timedelta] = None
        self.last_error: Optional[str] = None

    def __repr__(self) -> str:
        '''
        Returns string representation of Cam
        '''
        return str(
            {
                'cam_id': self.cam_id,
                'name': self.name,
                'alias': self.alias,
                'host_and_port': self.host_and_port,
                'username': self.username,
                'password': self.password,
                'brand': self.brand,
                'last_clock_dt': self.last_clock_dt,
                'last_clock_skew': self.last_clock_skew,
                'last_error': self.last_error,
            }
        )

    def __eq__(self, other):
        if not isinstance(other, Cam):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return (
            self.cam_id == other.cam_id
            and self.name == other.name
            and self.alias == other.alias
            and self.host_and_port == other.host_and_port
            and self.username == other.username
            and self.password == other.password
            and self.brand == other.brand
        )

    @abstractmethod
    def get_ntp_options(self):
        '''
        Get NTP options from generic cam
        '''
        pass

    @abstractmethod
    def set_ntp_options(self):
        '''
        Set NTP options for generic cam
        '''
        pass

    @abstractmethod
    def get_datetime(self) -> Tuple[datetime, timedelta]:
        '''
        Get Datetime from generic cam
        '''
        pass

    @abstractmethod
    def set_datetime(self):
        '''
        Set Datetime for generic cam
        '''
        pass

    def http_get(
        self,
        url: str,
        params: Dict[str, str] = {},
        username: str = None,
        password: str = None,
        auth_type: str = 'digest',
    ) -> requests.Response:
        '''
        Do HTTP GET for a given URL

        Arguments:
            url: The url to make the GET request against
            params: A dictionary of query string params
            username: Auth username
            password: Auth password

        Returns:
            A requests.Response object representing the HTTP response

        Raises:
            CamError: An error occurred communicating with a camera
        '''
        auth = {}  # type: Dict[str, Optional[requests.auth.AuthBase]]
        auth['digest'] = (
            requests.auth.HTTPDigestAuth(username, password)
            if username and password
            else None
        )
        auth['basic'] = (
            requests.auth.HTTPBasicAuth(username, password)
            if username and password
            else None
        )

        try:
            r = requests.get(
                url,
                auth=auth.get(auth_type, None),
                params=params,
                timeout=REQUEST_TIMEOUT,
            )
        except requests.exceptions.ConnectionError as e:
            self.last_error = f'Connection Error'
            raise CamError(f'Unable to connect to {url}', e)
        except requests.exceptions.ReadTimeout as e:
            self.last_error = f'Read Timeout'
            raise CamError(f'Timeout reading from {url}', e)
        except requests.exceptions.InvalidURL as e:
            self.last_error = f'Failed to parse URL location'
            raise CamError(f'Failed to parse {url}', e)
        except UnicodeError as e:
            self.last_error = f'Unicode Error'
            raise CamError(
                f'''
                Failed to get datetime for the following url: {url}
                This is due to a known Python socket class bug:
                https://bugs.python.org/issue32958
            ''',
                e,
            )

        # If we get a 401 status code, check www-authenticate header & retry.
        # `requests` will do this for us but only if we try basic auth first.
        # Since most cams use digest auth and basic auth is the edge case, we
        # shouldn't use this.
        auth_header = r.headers.get('www-authenticate', '').lower()
        if r.status_code == 401 and auth_header:
            retry_auth_type = 'basic' if 'basic' in auth_header else 'digest'

            logger.info(f'Retrying request with {retry_auth_type} auth')

            try:
                r = requests.get(
                    url,
                    auth=auth.get(retry_auth_type, None),
                    params=params,
                    timeout=REQUEST_TIMEOUT,
                )
            except requests.exceptions.ConnectionError as e:
                self.last_error = f'Connection Error'
                raise CamError(f'Unable to connect to {url}', e)
            except requests.exceptions.ReadTimeout as e:
                self.last_error = f'Read Timeout'
                raise CamError(f'Timeout reading from {url}', e)
            except requests.exceptions.InvalidURL as e:
                self.last_error = f'Failed to parse URL location'
                raise CamError(f'Failed to parse {url}', e)

        try:
            r.raise_for_status()
        except requests.exceptions.HTTPError as e:
            self.last_error = f'HTTP Error: {r.status_code} {r.reason}'
            raise CamError(f'HTTP Error', e)

        return r


class CamError(Exception):
    '''
    Camera Error

    Wraps Cam exceptions
    '''

    def __init__(self, msg: str, orig_exception: Optional[Exception] = None):
        '''
        Initialize Camera Error

        Arguments:
            msg: The error message
            orig_exception: The exception that this error wraps
        '''
        full_msg = f'{msg}: {orig_exception}' if orig_exception else msg
        super().__init__(full_msg)
