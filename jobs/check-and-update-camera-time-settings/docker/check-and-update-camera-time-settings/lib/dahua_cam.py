from datetime import datetime, timedelta
import logging
from typing import Optional, Tuple

from lib.cam import Cam, CamError

logger = logging.getLogger('check-and-update-camera-time-settings')


class DahuaCam(Cam):
    '''
    Dahua camera class

    Manages cam API operations, specifically for Dahua cams.

    Dahua cam API doc:
    https://www.planetseguridad.com/descargas/DAHUA_HTTP_API_FOR_IPC&SD-V1.36.pdf

    Attributes:
        cam_id: Camera ID, in BSON ObjectId string format
        name: Camera name
        alias: Camera alias
        host_and_port: Hostname and port, in `{hostname}:{port}` format
        username: Camera API username
        password: Camera API password
        brand: Camera brand, `Dahua`
    '''

    def __init__(
        self,
        cam_id: str,
        name: str,
        alias: str,
        host_and_port: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
    ):
        '''
        Initializes Cam
        '''
        super().__init__(
            cam_id, name, alias, host_and_port, username, password
        )
        self.brand = 'Dahua'

    def get_ntp_options(self):
        '''
        Get NTP options from Dahua cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /cgi-bin/configManager.cgi?action=getConfig&name=NTP

            HTTP response:
                table.NTP.Address=clock.isc.org
                table.NTP.Enable=true
                table.NTP.Port=123
                table.NTP.TimeZone=0
                table.NTP.UpdatePeriod=10
        '''
        # Not implemented - Will add when needed
        pass

    def set_ntp_options(self):
        '''
        Set NTP options for Dahua cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /cgi-bin/configManager.cgi
                    action=setConfig
                    NTP.Address=pool.ntp.org
                    NTP.Enable=true
                    NTP.TimeZone=0
                    NTP.UpdatePeriod=5
                    Locales.DSTEnable=false

            HTTP response:
                OK or ERROR

            Note: All updates are done via HTTP GET.

        Raises:
            CamError: An error occurred communicating with a camera
        '''
        logger.info(f'Setting NTP Options for {self.alias}')

        params = {
            'action': 'setConfig',
            'NTP.Address': 'pool.ntp.org',
            'NTP.Enable': 'true',
            'NTP.TimeZone': 0,
            'NTP.UpdatePeriod': 5,
            'Locales.DSTEnable': 'false',
        }
        r = super().http_get(
            f'http://{self.host_and_port}' f'/cgi-bin/configManager.cgi',
            params=params,
            username=self.username,
            password=self.password,
        )

        if r.text == 'ERROR':
            self.last_error = f'Failed to update NTP options'
            raise CamError(f'Failed to update NTP options for {self.alias}')

    def get_datetime(self) -> Tuple[datetime, timedelta]:
        '''
        Get Datetime from Dahua Cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /cgi-bin/global.cgi?action=getCurrentTime

            HTTP response:
                result=2019-06-13 04:21:19

        Returns:
            This returns a tuple containing the following data:

                Camera datetime: The camera's clocktime
                Clock skew: A timedelta representing the difference between
                    the camera clock time and actual time
        '''
        logger.info(f'Getting datetime for {self.alias}')

        request_dt = datetime.utcnow()

        r = super().http_get(
            f'http://{self.host_and_port}/cgi-bin/global.cgi',
            params={'action': 'getCurrentTime'},
            username=self.username,
            password=self.password,
        )

        elapsed_time = datetime.utcnow() - request_dt

        try:
            # datetime format: "result=2019-06-29 20:39:40\n"
            clock_dt = datetime.strptime(
                r.text.strip(), 'result=%Y-%m-%d %H:%M:%S'
            )
            self.last_clock_dt = clock_dt

            clock_skew = abs(clock_dt - (request_dt + elapsed_time))
            self.last_clock_skew = clock_skew

            return (clock_dt, clock_skew)
        except ValueError as e:
            self.last_error = f'Invalid Datetime Format'
            raise CamError('Invalid datetime format', e)

    def set_datetime(self):
        '''
        Set Datetime for Dahua Cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /cgi-bin/global.cgi?action=setCurrentTime&time=2011-7-3%2021:02:32

            HTTP response:
                ???

            Note: All updates are done via HTTP GET.

        Raises:
            CamError: An error occurred communicating with a camera
        '''
        logger.info(f'Setting datetime for {self.alias}')

        dt = datetime.utcnow()

        params = {
            'action': 'setCurrentTime',
            # time format: '2011-7-3 21:02:32'
            # %-m is month with no leading `0`, %-d is day with no leading `0`
            'time': dt.strftime('%Y-%-m-%-d %H:%M:%S'),
        }

        # Encode params into string explicitly - when requests does this,
        # it percent-encodes special chars (like `:`) in a way that's
        # incompatible with the Dahua cam api.
        params_str = '&'.join(f'{k}={v}' for k, v in params.items())

        r = super().http_get(
            f'http://{self.host_and_port}/cgi-bin/global.cgi',
            params=params_str,
            username=self.username,
            password=self.password,
        )

        if r.text == 'Error':
            self.last_error = f'Failed to set datetime'
            raise CamError(f'Failed to set datetime for {self.alias}')
