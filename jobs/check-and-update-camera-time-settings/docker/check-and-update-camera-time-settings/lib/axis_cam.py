from datetime import datetime, timedelta
import logging
from typing import Optional, Tuple

from lib.cam import Cam, CamError

logger = logging.getLogger('check-and-update-camera-time-settings')


class AxisCam(Cam):
    '''
    Axis camera class

    Manages cam API operations, specifically for Axis cams.

    Axis cam API doc:
    https://www.axis.com/files/manuals/vapix_general_system_settings_52921_en_1307.pdf

    Attributes:
        cam_id: Camera ID, in BSON ObjectId string format
        name: Camera name
        alias: Camera alias
        host_and_port: Hostname and port, in `{hostname}:{port}` format
        username: Camera API username
        password: Camera API password
        brand: Camera brand, `Axis`
    '''

    def __init__(
        self,
        cam_id: str,
        name: str,
        alias: str,
        host_and_port: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
    ):
        '''
        Initializes Cam
        '''
        super().__init__(
            cam_id, name, alias, host_and_port, username, password
        )
        self.brand = 'Axis'

    def get_ntp_options(self):
        '''
        Get NTP options from Axis cam

        Implements the following API contract with the physical camera:

            HTTP request:
                ???

            HTTP response:
                ???
        '''
        # Not implemented - Will add when needed
        pass

    def set_ntp_options(self):
        '''
        Set NTP options for Axis cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /axis-cgi/param.cgi?
                    action=update
                    root.Time.DST.Enabled=false
                    root.Time.POSIXTimeZone=UTC
                    root.Time.NTP.Server=pool.ntp.org
                    root.Time.SyncSource=NTP
                    root.Time.ObtainFromDHCP=no

            HTTP response:
                OK

            Note: All updates are done via HTTP GET.

        Raises:
            CamError: An error occurred communicating with a camera
        '''

        logger.info(f'Setting NTP Options for {self.alias}')

        params = {
            'action': 'update',
            'root.Time.DST.Enabled': 'no',
            'root.Time.POSIXTimeZone': 'UTC',
            'root.Time.NTP.Server': 'pool.ntp.org',
            'root.Time.SyncSource': 'NTP',
            'root.Time.ObtainFromDHCP': 'no',
        }
        r = super().http_get(
            f'http://{self.host_and_port}/axis-cgi/param.cgi',
            params=params,
            username=self.username,
            password=self.password,
        )

    def get_datetime(self) -> Tuple[datetime, timedelta]:
        '''
        Get Datetime from Axis Cam

        Implements the following API contract with the physical camera:

            HTTP request:
                /axis-cgi/date.cgi?action=get

            HTTP response:
                Jun 29, 2019 20:37:07

        Returns:
            This returns a tuple containing the following data:

                Camera datetime: The camera's clocktime
                Clock skew: A timedelta representing the difference between
                    the camera clock time and actual time
        '''
        logger.info(f'Getting datetime for {self.alias}')

        request_dt = datetime.utcnow()

        r = super().http_get(
            f'http://{self.host_and_port}/axis-cgi/date.cgi',
            params={'action': 'get'},
            username=self.username,
            password=self.password,
        )

        elapsed_time = datetime.utcnow() - request_dt

        try:
            # datetime format: "Jun 29, 2019 20:37:07"
            clock_dt = datetime.strptime(r.text.strip(), '%b %d, %Y %H:%M:%S')
            self.last_clock_dt = clock_dt

            clock_skew = abs(clock_dt - (request_dt + elapsed_time))
            self.last_clock_skew = clock_skew

            return (clock_dt, clock_skew)
        except ValueError as e:
            self.last_error = f'Invalid Datetime Format'
            raise CamError('Invalid datetime format', e)

    def set_datetime(self):
        '''
        Set Datetime for Axis Cam

        Implements the following API contract:

            HTTP request:
                /axis-cgi/date.cgi
                    action=set
                    year=${date.getUTCFullYear()}
                    month=${date.getUTCMonth()+1}
                    day=${date.getUTCDate()}
                    hour=${date.getUTCHours()}minute=${date.getUTCMinutes()}
                    second=${date.getUTCSeconds()}

            HTTP response:
                OK

            Note: All updates are done via HTTP GET.

        Raises:
            CamError: An error occurred communicating with a camera
        '''
        logger.info(f'Setting datetime for {self.alias}')

        dt = datetime.utcnow()

        params = {
            'action': 'set',
            'year': dt.year,
            'month': dt.month,
            'day': dt.day,
            'hour': dt.hour,
            'minute': dt.minute,
            'second': dt.second,
        }
        r = super().http_get(
            f'http://{self.host_and_port}/axis-cgi/date.cgi',
            params=params,
            username=self.username,
            password=self.password,
        )

        # Some axis models prevent setting date when NTP is enabled
        if 'Blocked set date. Not allowed when NTP sync is enabled.' in r.text:
            logger.warning(
                f'Set date blocked on {self.alias}' f' - NTP sync enabled'
            )
        elif r.text != 'OK':
            self.last_error = f'Failed to set datetime'
            raise CamError(f'Failed to set datetime for {self.alias}')
