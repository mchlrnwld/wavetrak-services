import os
from datetime import timedelta

from job_secrets_helper import get_secret  # type: ignore

CAM_TIME_THRESHOLD = timedelta(seconds=5)
ENV = os.environ['ENVIRONMENT']
EXECUTION_TIMESTAMP = os.environ['EXECUTION_TIMESTAMP']
JOB_NAME = os.environ['JOB_NAME']
SLACK_CHAR_LIMIT = 2500

# Job-specific variables
AIRTABLE_API_KEY = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'AIRTABLE_API_KEY')
AIRTABLE_APP_ID = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'AIRTABLE_APP_ID')
AIRTABLE_VIEW = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'AIRTABLE_VIEW')
SLACK_CHANNEL = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'SLACK_CHANNEL')

CAM_LIMIT = get_secret(f'{ENV}/jobs/{JOB_NAME}/', 'CAM_LIMIT', '') or None

# Common variables
CAMERAS_API = get_secret(f'{ENV}/common/', 'CAMERAS_API')
SLACK_API_TOKEN = get_secret(f'{ENV}/common/', 'WT_SLACK_API_TOKEN')
