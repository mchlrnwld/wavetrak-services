# Check and Update Camera Time

## Overview

This application gets a list of cameras, checks their time settings, updates them if needed, and reports results to Slack.

## Quickstart

```sh
make conda-create
conda activate check-and-update-camera-time-settings
cp .env.sample .env
make
```

The `make` command on its own executes the `lint`, `type-check`, and `run` targets described below.

## Developing

### Environment

Before running any of the commands listed below, be sure to set up your local environment file:

```sh
cp .env.sample .env
```

### Lint, Type Check, & Run

```sh
make
```

### Lint

```sh
make lint
```

### Type Check

This application uses [mypy](http://mypy-lang.org/) to do static type-checking.

```sh
make type-check
```

### Run

```sh
make run
```

## Testing

```sh
make test
```

## Docker

### Build & Run

```sh
make docker
```

### Build

```sh
make docker-build
```

### Run

```sh
make docker-run
```

## Deploying

Deploy via the Jenkins job [shared/deploy-airflow-job](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-airflow-job)

## Future Improvements

1. Add Sony cam class
1. Write results to MongoDB, create a status webpage to display current state of the system.
1. Refactor `lib/Cam.py` and camera brand subclasses into separate Python package to be used in all cam-related Python applications.
1. Add `argparse` and allow for dry-runs, limiting the number of cams processed, filtering by cam region, filtering by cam brand, etc.