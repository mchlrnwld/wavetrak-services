# Check and Update Camera Time Settings

Airflow DAG to check and update camera time settings.

## Docker Images for Tasks

- [Check and Update Camera Time Settings](docker/check-and-update-camera-time-settings)

## Deploying

1. Create ECR repository for each Docker task: [misc/docker/create-ecr-repository](https://surfline-jenkins-master-prod.surflineadmin.com/job/misc/job/docker/job/create-ecr-repository/)

2. Deploy DAG: [shared/deploy-airflow-job](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-airflow-job)
