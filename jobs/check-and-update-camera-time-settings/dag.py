from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator

from dag_helpers.docker import docker_image

AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT

JOB_NAME = 'check-and-update-camera-time-settings'
EXECUTION_TIMESTAMP = '{{ ts }}'


check_and_update_time_settings_environment = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'ENVIRONMENT': APP_ENVIRONMENT,
    'EXECUTION_TIMESTAMP': EXECUTION_TIMESTAMP,
    'JOB_NAME': JOB_NAME,
}

default_args = {
    'owner': 'surfline',
    'start_date': datetime(2019, 7, 16),
    # 'email': 'platformsquad@surfline.com',
    # 'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(hours=1),
    max_active_runs=1,
    default_args=default_args,
)

check_and_update_camera_time_settings = WTDockerOperator(
    task_id=JOB_NAME,
    image=docker_image(JOB_NAME, JOB_NAME),
    environment=check_and_update_time_settings_environment,
    force_pull=True,
    email_on_retry=False,
    dag=dag,
)
