provider "aws" {
  alias = "noaa"
}

module "sqs_with_sns_subscription" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs-with-sns-subscription"

  providers = {
    aws.topic_provider = aws.noaa
  }

  application = var.application
  environment = var.environment
  queue_name  = var.queue_name
  topic_arns  = ["arn:aws:sns:us-east-1:123901341784:NewGFSObject"]
  company     = var.company
}

module "process_point_of_interest_data_noaa_gfs" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company     = var.company
  application = "${var.application}-poi"
  environment = var.environment

  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/process-forecast-platform-noaa-gfs/process-point-of-interest-data:${var.environment}",
    "vcpus" : 1,
    "memory" : 2048,
    "volumes" : [
      {
        "host" : {
          "sourcePath" : "/mnt/ephemeral/airflow"
        },
        "name" : "tmp"
      }
    ],
    "mountPoints" : [
      {
        "sourceVolume" : "tmp",
        "containerPath" : "/mnt/ephemeral/airflow",
        "readOnly" : false
      }
    ],
    "ulimits" : [
      {
        "name" : "nofile",
        "softLimit" : 10024,
        "hardLimit" : 10024,
      }
    ]
  }
  custom_policy = templatefile("${path.module}/policy.tpl", { jobs_bucket = var.jobs_bucket })
}
