# Process Forecast Platform NOAA GFS

Airflow DAG to Process GFS data from NOAA servers into Wavetrak's Science Models Database.

## Docker Images for Tasks

- [Check Model Ready](docker/check-model-ready)
- [Stream Full Grid Data](docker/stream-full-grid-data)
- [Insert Point of Interest Data](docker/insert-point-of-interest-data)
