from contextlib import ExitStack

import psycopg2  # type: ignore


class PSQL:
    """
    PSQL allows copying data from multiple CSVs into Postgres in a single
    transaction.

    Args:
        connection_uri: URI to connect to the database.
    """

    def __init__(self, connection_uri: str):
        self.connection_uri = connection_uri

    def __enter__(self):
        self.stack = ExitStack()
        self.stack.__enter__()
        self.conn = self.stack.enter_context(
            psycopg2.connect(self.connection_uri)
        )
        self.cur = self.stack.enter_context(self.conn.cursor())
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stack.__exit__(exc_type, exc_value, traceback)

    def copy_csv(self, csv_path, table):
        """
        Copy CSV file data to a table in Postgres.

        Args:
            csv_path: Path to CSV file.
            table: Table to copy data to.
        """
        with open(csv_path) as csv_file:
            columns = csv_file.readline()
            csv_file.seek(0)
            self.cur.copy_expert(
                f'COPY {table} ({columns}) FROM STDIN WITH CSV HEADER',
                csv_file,
            )

    def commit(self):
        """
        Commit transaction to Postgres.
        """
        self.conn.commit()
