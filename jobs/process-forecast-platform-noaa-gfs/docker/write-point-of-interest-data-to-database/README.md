# Write Point of Interest Data to Database

Docker image to:

1. Load CSVs for point of interest surf and swell data.
2. Write data to Postgres database.

## Setup

```sh
conda devenv
conda activate process-forecast-platform-noaa-gfs-write-point-of-interest-data-to-database
cp .env.sample .env
env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
docker build -t process-forecast-platform-noaa-gfs/write-point-of-interest-data-to-database .
cp .env.sample .env
docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data process-forecast-platform-noaa-gfs/write-point-of-interest-data-to-database
```
