from itertools import chain
import logging
import re
from typing import Optional

import boto3  # type: ignore

import check_model_ready  # type: ignore

GRIB_REGEX = re.compile(r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f(\d{3})$')
IDX_REGEX = re.compile(r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f(\d{3})\.idx$')

logger = logging.getLogger('check-model-ready')


def check_gfs_ready(
    bucket: str, sds_host: str, agency: str, model: str, run: int
) -> bool:
    """
    Checks if the given run is ready for processing. This is accomplished by
    checking that the run has all of the files needed in the given s3 bucket.

    Args:
        bucket: S3 bucket to check for file objects.
        sds_host: Science Data Service host URI.
        agency: Model agency to check.
        model: Name of model to check.
        run: Model run time (YYYYMMDDHH) to check.

    Returns:
        True if all files exist in the s3 bucket for the model_run.
        False if the model_run was already processed, if the some files exist
        in s3, but not all, or if there are no files in s3.
    """
    logger.debug(f'Checking {run} model run ready for processing...')
    if check_model_ready.check_model_run_already_exists(
        agency, model, run, 'POINT_OF_INTEREST', sds_host
    ):
        logger.debug('Model run already exists.')
        return False

    expected_timestamps = list(chain(range(121), range(123, 385, 3)))
    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')
    run_date = str(run)[:8]
    run_hour = str(run)[8:]
    object_keys = [
        item['Key']
        for item in chain.from_iterable(
            response['Contents']
            for response in paginator.paginate(
                Bucket=bucket, Prefix=f'gfs.{run_date}/{run_hour}/atmos'
            )
            if 'Contents' in response
        )
    ]
    grib_matches = [GRIB_REGEX.search(key) for key in object_keys]
    grib_timestamps = [int(match.group(2)) for match in grib_matches if match]
    idx_matches = [IDX_REGEX.search(key) for key in object_keys]
    idx_timestamps = [int(match.group(2)) for match in idx_matches if match]
    logger.debug(f'Expected timestamps: {expected_timestamps}')
    logger.debug(f'Found GRIB timestamps: {grib_timestamps}')
    logger.debug(f'Found IDX timestamps: {idx_timestamps}')
    return grib_timestamps == idx_timestamps == expected_timestamps
