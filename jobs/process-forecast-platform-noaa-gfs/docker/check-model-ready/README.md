# Check Model Ready

Docker image to check if latest GFS model is ready for processing. This image checks S3 for a model run. If the model run is fully downloaded then we execute `begin_processing_model` for the model run. Otherwise we execute `model_not_ready`.

## Setup

```sh
conda devenv
source activate process-forecast-platform-noaa-gfs-check-model-ready
cp .env.sample .env
env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
docker build -t process-forecast-platform-noaa-gfs/check-model-ready .
cp .env.sample .env
docker run --env-file=.env --rm process-forecast-platform-noaa-gfs/check-model-ready
```
