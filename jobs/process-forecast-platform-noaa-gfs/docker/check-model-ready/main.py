from functools import partial
import logging

from check_model_ready import check_sqs_messages, set_pending_model_run
from lib.check_gfs_ready import check_gfs_ready
import lib.config as config
from lib.helpers import model_run_from_key

logger = logging.getLogger('check-model-ready')


def main():
    # TODO: Uncomment below to stream info level logging to stderr once
    #       WTDockerOperator can distinguish between stdout and stderr streams.
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler())

    logger.info(f'Polling queue: {config.SQS_QUEUE_NAME}...')

    result = None
    total_messages = 0
    while True:
        model_run, messages_found = check_sqs_messages(  # type: ignore
            config.GFS_BUCKET,
            'gfs.',
            config.SQS_QUEUE_NAME,
            partial(
                check_gfs_ready,
                config.GFS_BUCKET,
                config.SCIENCE_DATA_SERVICE,
                config.MODEL_AGENCY,
                config.MODEL_NAME,
            ),
            model_run_from_key,
        )
        result = model_run
        total_messages += messages_found
        if result is not None or messages_found == 0:
            break

    logger.info(f'Checked {total_messages} messages.')
    logger.debug(f'Resulting model run: {result}')

    # Write to stdout for xcoms_push
    if result is not None:
        set_pending_model_run(
            config.MODEL_AGENCY,
            config.MODEL_NAME,
            result,
            'POINT_OF_INTEREST',
            config.SCIENCE_DATA_SERVICE,
        )
        print(result)
    else:
        print('')


if __name__ == '__main__':
    main()
