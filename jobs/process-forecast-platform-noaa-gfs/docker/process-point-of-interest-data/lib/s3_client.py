import asyncio
import logging
from functools import partial

import boto3  # type: ignore
import file_transfer
from botocore.exceptions import ClientError  # type:ignore
from grib_helpers.indices import GFS_INDICES
from grib_helpers.inventory import parse_byte_ranges_from_inventory

logger = logging.getLogger('process-point-of-interest-data')


# TODO: Replace with S3Client
class S3Client:
    """
    S3Client to interface with S3, download files asynchronously, and
    upload files asynchronously.

    Args:
        max_concurrency: The maximum number of concurrent download/upload
                         streams.

    Attributes:
        client: boto3 client for S3.
    """

    def __init__(self, max_concurrency: int = 5):
        self.s3_client = file_transfer.S3Client()
        self.semaphore = asyncio.Semaphore(max_concurrency)
        self.boto_s3_client = boto3.client('s3')

    async def download_grib_file(
        self,
        bucket: str,
        grib_key: str,
        idx_key: str,
        grib_local_destination: str,
    ):
        """
        Downloads a subset of variables for a GRIB file from the given S3
        Bucket using the IDX file.

        Args:
            bucket: S3 bucket to download from.
            grib_key: S3 key for the GRIB file to download.
            idx_key: S3 key for the IDX file to download for parsing variables
                     from inventory.
            grib_local_destination: The local file path to download the S3
                                    object to.
        """
        async with self.semaphore:
            try:
                inventory = (
                    await self.s3_client.get_object_contents(bucket, idx_key)
                ).decode('utf-8')
                byte_ranges = parse_byte_ranges_from_inventory(
                    inventory, GFS_INDICES
                )
                await self.s3_client.download_object_byte_ranges(
                    bucket, grib_key, grib_local_destination, byte_ranges
                )
                logger.info(
                    f'Downloaded s3://{bucket}/{grib_key} to '
                    f'{grib_local_destination}'
                )
            except ClientError as e:
                logger.error(
                    f'Failed to download s3://{bucket}/{grib_key} to '
                    f'{grib_local_destination}!'
                )
                raise e

    async def upload_file(self, local_file_path: str, bucket: str, key: str):
        """
        Upload file to S3.

        Args:
            local_file_path: Local path to upload file from.
            bucket: S3 bucket to upload to.
            key: S3 object key to upload to.
        """
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None, partial(self._upload_file, local_file_path, bucket, key)
        )

    def _upload_file(self, local_file_path: str, bucket: str, key: str):
        try:
            self.boto_s3_client.upload_file(local_file_path, bucket, key)
            logger.info(f'Uploaded {local_file_path} to s3://{bucket}/{key}.')
        except ClientError as e:
            logger.error(
                f'Failed to upload {local_file_path} to s3://{bucket}/{key}!'
            )
            raise e
