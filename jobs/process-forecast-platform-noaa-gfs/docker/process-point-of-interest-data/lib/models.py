from datetime import datetime

from science_algorithms.enums import PrecipitationType, WeatherConditions


class PointOfInterestGridPoint:
    """
    Point of interest and its grid point.

    Args:
        id: Point of interest ID.
        grid: Name of model grid for the grid point.
        lat: Grid point latitude.
        lon: Grid point longitude.
    """

    def __init__(self, id: str, grid: str, lat: float, lon: float):
        self.id = id
        self.grid = grid
        self.lat = lat
        if lon < 0:
            self.lon = lon + 360
        else:
            self.lon = lon


class WeatherHour:
    """
    Contains weather values for a single forecast hour for a point of interest.
    """

    def __init__(
        self,
        poi: PointOfInterestGridPoint,
        agency: str,
        model: str,
        run: int,
        grid: str,
        forecast_time: datetime,
        temperature: float,
        dewpoint: float,
        visibility: float,
        humidity: float,
        pressure: float,
        precipitation_volume: float,
        precipitation_type: int,
        weather_conditions: int,
    ):
        self.point_of_interest_id = poi.id
        self.agency = agency
        self.model = model
        self.grid = grid
        self.latitude = poi.lat
        self.longitude = poi.lon
        self.run = run
        self.forecast_time = forecast_time
        self.temperature = temperature
        self.dewpoint = dewpoint
        self.visibility = visibility
        self.humidity = humidity
        self.pressure = pressure
        self.precipitation_type = (
            None
            if not PrecipitationType(precipitation_type)
            else PrecipitationType(precipitation_type).name
        )
        self.precipitation_volume = precipitation_volume
        self.weather_conditions = WeatherConditions(weather_conditions).name


class WindHour:
    """
    Contains wind values for a single forecast hour for a point of interest.
    """

    def __init__(
        self,
        poi: PointOfInterestGridPoint,
        agency: str,
        model: str,
        run: int,
        grid: str,
        forecast_time: datetime,
        u: float,
        v: float,
        gust: float,
    ):
        self.point_of_interest_id = poi.id
        self.agency = agency
        self.model = model
        self.grid = grid
        self.latitude = poi.lat
        self.longitude = poi.lon
        self.run = run
        self.forecast_time = forecast_time
        self.u = u
        self.v = v
        self.gust = gust
