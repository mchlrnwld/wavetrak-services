import logging
from typing import Dict, List, Optional, Tuple

import numpy as np  # type: ignore
from grib_helpers.process import parse_grib
from grib_helpers.variables import GFS_GRIB_VARIABLES

logger = logging.getLogger('process-point-of-interest-data')


def parse_data_from_gribs(
    local_file: str, local_fill_data_file: Optional[str]
) -> Tuple[List[float], List[float], Dict[str, np.array], Dict[str, str]]:
    """
    This function gets data from NOAA GFS grib files for the variables
    desired. If there is a fill data file, then the totalPrecipitation from
    the fill data file is used.

    Args:
        local_file: Path to current GRIB file for the forecast hour to be
                    parsed.
        local_fill_data_file: Path to fill data GRIB file for the forecast hour
                              to be parsed. This is used to fill data the
                              current GRIB may be missing.

    Returns:
        Latitude, longitude, data values, and metadata.
    """
    lats, lons, variable_values, variable_details = parse_grib(
        local_file, GFS_GRIB_VARIABLES
    )
    if local_fill_data_file:
        (
            _,
            _,
            fill_data_variable_values,
            fill_data_variable_details,
        ) = parse_grib(local_fill_data_file, GFS_GRIB_VARIABLES)

    combined_values = {
        key: (
            variable_values[key]
            if key in variable_values
            else fill_data_variable_values[key]
        )
        for key in GFS_GRIB_VARIABLES.keys()
    }

    combined_details = {
        key: (
            variable_details[key]
            if key in variable_details
            else fill_data_variable_details[key]
        )
        for key in GFS_GRIB_VARIABLES.keys()
    }

    return (
        [lat[0] for lat in lats.tolist()],
        lons[0].tolist(),
        combined_values,
        combined_details,
    )
