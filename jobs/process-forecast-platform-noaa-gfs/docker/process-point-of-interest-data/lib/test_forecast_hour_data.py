from lib.forecast_hour_data import ForecastHourData

RUN = 2021020300
GRID = '0p25'
TEMP_DIR = 'temp_dir'


def test_forecast_hour_data_analysis_hour():
    forecast_hour_data = ForecastHourData(RUN, 0, GRID, 0, TEMP_DIR)
    assert (
        forecast_hour_data.local_file
        == 'temp_dir/gfs.20210203/00/atmos/gfs.t00z.pgrb2.0p25.f000'
    )
    assert (
        forecast_hour_data.s3_file
        == 'gfs.20210203/00/atmos/gfs.t00z.pgrb2.0p25.f000'
    )
    assert (
        forecast_hour_data.local_fill_data_file
        == 'temp_dir/gfs.20210202/18/atmos/gfs.t18z.pgrb2.0p25.f006'
    )
    assert (
        forecast_hour_data.s3_fill_data_file
        == 'gfs.20210202/18/atmos/gfs.t18z.pgrb2.0p25.f006'
    )
    assert forecast_hour_data.local_previous_file is None
    assert forecast_hour_data.s3_previous_file is None


def test_forecast_hour_data_missing_previous():
    forecast_hour_data = ForecastHourData(RUN, 2, GRID, 2, TEMP_DIR)
    assert (
        forecast_hour_data.local_file
        == 'temp_dir/gfs.20210203/00/atmos/gfs.t00z.pgrb2.0p25.f002'
    )
    assert (
        forecast_hour_data.s3_file
        == 'gfs.20210203/00/atmos/gfs.t00z.pgrb2.0p25.f002'
    )
    assert forecast_hour_data.local_fill_data_file is None
    assert forecast_hour_data.s3_fill_data_file is None
    assert (
        forecast_hour_data.local_previous_file
        == 'temp_dir/gfs.20210203/00/atmos/gfs.t00z.pgrb2.0p25.f001'
    )
    assert (
        forecast_hour_data.s3_previous_file
        == 'gfs.20210203/00/atmos/gfs.t00z.pgrb2.0p25.f001'
    )
