from datetime import datetime, timedelta
from typing import Optional

RUN_FORMAT = '%Y%m%d%H'
ANALYSIS_HOUR = 0


class ForecastHourData:
    """
    Contains all information necessary to
    extract data for a forecast hour.

    Args:
        run: The current model run being processed,
             in the format YYYYMMDDHH.
        forecast_time: Forecasted hour from analysis hour.
        grid: The grid associated with the run.
        start_forecast_hour: The starting forecast hour for the
                             forecast hour range given. This is needed
                             to grab the previous file so that the
                             precipitation can be calculated correctly.
                             For example:
                             forecast_hours: 3,4,5,6. start_forecast_hour: 3.
        temp_dir: Location on disk of where to download the file.

    Attributes:
        hour: Hour associated with the file.
        timestamp: Formatted forecast hour timestamp.
        grid: Grid associated with then run
              and forecast hour.
        local_file: Path to NOAA GFS file locally.
        local_fill_data_file: Path to NOAA GFS file to fill in
                              totalPrecipitation data.
                              For analysis hour (0) only.
        s3_file: Path to NOAA GFS file in S3.
        s3_fill_data_file: Path to NOAA GFS file in s3 to fill in
                           totalPrecipitation data. For analysis hour (0) only.
        local_previous_file: Path to the previous file locally
                             for the file hour that corresponds
                             to the start_forecast_hour.
                             The previous file is needed to
                             accurately calculate the marginal precipiation.
        s3_previous_file: Path to the previous file in s3
                          for the file hour that corresponds
                          to the start_forecast_hour.
                          The previous file is needed to accurately calculate
                          the marginal precipitation.
    """

    def __init__(
        self,
        run: int,
        forecast_hour: int,
        grid: str,
        start_forecast_hour: int,
        temp_dir: str,
    ):
        run_datetime = datetime.strptime(str(run), RUN_FORMAT)
        forecast_datetime = run_datetime + timedelta(hours=forecast_hour)

        self.forecast_time = forecast_datetime
        self.grid = grid
        self.hour = forecast_hour
        self.temp_dir = temp_dir

        self.s3_file = self._get_file(run, self.hour, grid)

        # Get the fill data file for the forecast_hour
        # corresponding to the ANALYSIS Hour.
        # This is needed to calculate precipitation due to
        # the fact that the ANALYSIS hour file is missing data.
        self.s3_fill_data_file = (
            self._get_fill_data_file(run, grid)
            if self.hour == ANALYSIS_HOUR
            else None
        )

        # Get the previous grib file for the
        # forecast_hour that corresponds to the start_forecast_hour.
        # The previous file is needed to calculate marginal
        # precipitation correctly. This is only needed for
        # forecast hours greater than 1 due to the fact that
        # the analysis hour 0 does not contain precipitation data.
        self.s3_previous_file = (
            self._get_previous_file(run, self.hour, grid, start_forecast_hour)
            if self.hour == start_forecast_hour and forecast_hour > 1
            else None
        )

    @property
    def local_file(self) -> str:
        return f'{self.temp_dir}/{self.s3_file}'

    @property
    def local_fill_data_file(self) -> Optional[str]:
        if self.s3_fill_data_file:
            return f'{self.temp_dir}/{self.s3_fill_data_file}'

        return None

    @property
    def local_previous_file(self) -> Optional[str]:
        if self.s3_previous_file:
            return f'{self.temp_dir}/{self.s3_previous_file}'

        return None

    def _get_previous_file(
        self, run: int, hour: int, grid: str, start_forecast_hour: int
    ) -> str:
        """
        Gets the previous NOAA GFS file path.

        Args:
            run: Model run time.
            hour: Forecasted hour or hours from analysis hour.
            grid: Grid associated with the NOAA GFS file.

        Returns:
            Path to the previous NOAA GFS file.
        """
        previous_hour = hour - 1 if start_forecast_hour <= 120 else hour - 3
        return self._get_file(run, previous_hour, grid)

    def _get_fill_data_file(self, run: int, grid: str) -> str:
        """
        Gets a fill data NOAA GFS file path.

        Args:
            run: Model run time.
            grid: Grid associated with the NOAA GFS file.

        Returns:
            Path to the NOAA GFS fill data file.
        """
        run_datetime = datetime.strptime(str(run), RUN_FORMAT)
        fill_data_run_datetime = run_datetime - timedelta(hours=6)
        fill_data_run = fill_data_run_datetime.strftime(RUN_FORMAT)
        return self._get_file(int(fill_data_run), 6, grid)

    def _get_file(self, run: int, hour: int, grid: str) -> str:
        """
        Creates the file path for a NOAA GFS file.

        Args:
            run: Model run time.
            hour: Forecasted hour or hours from analysis hour.
            grid: Grid associated with the NOAA GFS file.

        Returns:
            Path to the NOAA GFS file.
        """
        run_date = str(run)[:8]
        run_hour = str(run)[8:]

        return (
            f'gfs.{run_date}/'
            f'{run_hour}/'
            f'atmos/'
            f'gfs.t{run_hour}z.pgrb2.{grid}.f{hour:03d}'
        )
