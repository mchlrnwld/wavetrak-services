import asyncio
import csv
import os
from datetime import datetime
from functools import partial
from typing import Dict, List

from lib.models import WeatherHour, WindHour

RUN_FORMAT = '%Y%m%d%H'


async def _async_write_csv(csvpath: str, rows: List[Dict[str, object]]):
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, partial(_write_csv, csvpath, rows))


def _write_csv(csvpath: str, rows: List[Dict[str, object]]):
    write_header = True
    if os.path.exists(csvpath):
        write_header = False

    with open(csvpath, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(rows[0].keys()))

        if write_header:
            writer.writeheader()

        writer.writerows(rows)


async def write_wind_csv(
    csvpath: str,
    agency: str,
    model: str,
    grid: str,
    run: int,
    wind_hours: List[WindHour],
):
    """
    Creates a CSV of wind for each forecast hour.

    Args:
        csvpath: Location of CSV file to write to.
        agency: Model agency to include in each CSV line.
        model: Model name to include in each CSV line.
        grid: Model grid to include in each CSV line.
        run: Model run to include in each CSV line.
        wind_hours: List of wind values by hour. Each item will be a single
                    row in the CSV.
    """
    await _async_write_csv(
        csvpath,
        [
            {
                'point_of_interest_id': wind_hour.point_of_interest_id,
                'agency': agency,
                'model': model,
                'grid': grid,
                'latitude': wind_hour.latitude,
                'longitude': wind_hour.longitude,
                'run': datetime.strptime(str(run), RUN_FORMAT),
                'forecast_time': wind_hour.forecast_time,
                'u': wind_hour.u,
                'v': wind_hour.v,
                'gust': wind_hour.gust,
            }
            for wind_hour in wind_hours
        ],
    )


async def write_weather_csv(
    csvpath: str,
    agency: str,
    model: str,
    grid: str,
    run: int,
    weather_hours: List[WeatherHour],
):
    """
    Creates a CSV of weather for each forecast hour.

    Args:
        csvpath: Location of CSV file to write to.
        agency: Model agency to include in each CSV line.
        model: Model name to include in each CSV line.
        grid: Model grid to include in each CSV line.
        run: Model run to include in each CSV line.
        weather_hours: List of wind values by hour. Each item will be a single
                       row in the CSV.
    """
    await _async_write_csv(
        csvpath,
        [
            {
                'point_of_interest_id': weather_hour.point_of_interest_id,
                'agency': agency,
                'model': model,
                'grid': grid,
                'latitude': weather_hour.latitude,
                'longitude': weather_hour.longitude,
                'run': datetime.strptime(str(run), RUN_FORMAT),
                'forecast_time': weather_hour.forecast_time,
                'temperature': weather_hour.temperature,
                'dewpoint': weather_hour.dewpoint,
                'visibility': weather_hour.visibility,
                'humidity': weather_hour.humidity,
                'pressure': weather_hour.pressure,
                'precipitation_volume': weather_hour.precipitation_volume,
                'precipitation_type': weather_hour.precipitation_type,
                'weather_conditions': weather_hour.weather_conditions,
            }
            for weather_hour in weather_hours
        ],
    )
