import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
JOB_NAME = os.environ['JOB_NAME']
TASK_PARTITION = os.environ['TASK_PARTITION']
SECRETS_PREFIX = f'{ENV}/common/'
JOBS_BUCKET = get_secret(SECRETS_PREFIX, 'JOBS_BUCKET')
JOBS_KEY_PREFIX = get_secret(SECRETS_PREFIX, 'JOBS_KEY_PREFIX')
GFS_BUCKET = get_secret(SECRETS_PREFIX, 'GFS_BUCKET')
DOWNLOAD_DIR = os.environ.get('DOWNLOAD_DIR', 'tmp')
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
RUN = int(os.environ['RUN'])
FORECAST_HOURS = os.environ['FORECAST_HOURS']

SCIENCE_DATA_SERVICE = get_secret(SECRETS_PREFIX, 'SCIENCE_DATA_SERVICE')

SLACK_API_TOKEN = get_secret(SECRETS_PREFIX, 'SLACK_API_TOKEN', '')
SLACK_CHANNEL = os.getenv('SLACK_CHANNEL', '')
