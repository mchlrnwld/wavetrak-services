#!/usr/bin/env bash

source activate process-forecast-platform-noaa-gfs-process-point-of-interest-data && pytest -s --cov-report term-missing --cov=main test_main.py
