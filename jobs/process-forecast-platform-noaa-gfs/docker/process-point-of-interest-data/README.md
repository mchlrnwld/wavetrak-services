# Process Point of Interest Data

Docker image to query GFS forecast data for points of interest and insert the data into the Science Platform Postgres database. Choose a subset of forecast hours to download and process by specifying a range between 0 and 384. For example:
```
FORECAST_HOURS = 0,1,2,3,4
```
## Setup

```sh
conda devenv
source activate process-forecast-platform-noaa-gfs-process-point-of-interest-data
cp .env.sample .env
env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
docker build -t process-forecast-platform-noaa-gfs/process-point-of-interest-data .
cp .env.sample .env
docker run --rm -it --env-file=.env --volume $(pwd)/data:/opt/app/data process-forecast-platform-noaa-gfs/process-point-of-interest-data
```

## Testing

Run an integration test with:
```sh
make test-integration
```
