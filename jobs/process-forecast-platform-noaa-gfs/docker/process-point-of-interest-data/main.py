import asyncio
import logging
import os
from tempfile import TemporaryDirectory
from timeit import default_timer

import numpy as np  # type: ignore
import science_algorithms.transformations as t  # type: ignore
from grib_helpers import get_lat_lon_index  # type: ignore
from slack_helper import send_invalid_points_of_interest_notification

import lib.config as config
from lib.csv import write_weather_csv, write_wind_csv
from lib.forecast_hour_data import ForecastHourData
from lib.grib import parse_data_from_gribs
from lib.models import WeatherHour, WindHour
from lib.s3_client import S3Client
from lib.sds import SDS
from lib.weather import get_weather_conditions

logger = logging.getLogger('process-point-of-interest-data')

WIND_CSV_FILE_NAME = 'wind.csv'
WEATHER_CSV_FILE_NAME = 'weather.csv'


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info(f'Starting job for model run: {config.RUN}')

    if not os.path.exists(config.DOWNLOAD_DIR):
        logger.info(f'Creating directory: {config.DOWNLOAD_DIR}')
        os.makedirs(config.DOWNLOAD_DIR)

    with TemporaryDirectory(dir=config.DOWNLOAD_DIR) as temp_dir:
        logger.info(f'Indexing S3 files for model run {config.RUN}...')

        s3_client = S3Client()
        sds_client = SDS(
            config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL
        )

        forecast_hours = [
            int(forecast_hour)
            for forecast_hour in config.FORECAST_HOURS.split(',')
        ]
        start_forecast_hour = forecast_hours[0]

        points_of_interest = sds_client.query_points_of_interest()
        all_forecast_hour_data = [
            ForecastHourData(
                int(config.RUN),
                forecast_hour,
                '0p25',
                start_forecast_hour,
                temp_dir,
            )
            for forecast_hour in forecast_hours
        ]

        local_files = (
            [
                forecast_hour_data.local_file
                for forecast_hour_data in all_forecast_hour_data
            ]
            + [
                forecast_hour_data.local_fill_data_file
                for forecast_hour_data in all_forecast_hour_data
                if forecast_hour_data.local_fill_data_file is not None
            ]
            + [
                forecast_hour_data.local_previous_file
                for forecast_hour_data in all_forecast_hour_data
                if forecast_hour_data.local_previous_file is not None
            ]
        )

        s3_files = (
            [
                forecast_hour_data.s3_file
                for forecast_hour_data in all_forecast_hour_data
            ]
            + [
                forecast_hour_data.s3_fill_data_file
                for forecast_hour_data in all_forecast_hour_data
                if forecast_hour_data.s3_fill_data_file is not None
            ]
            + [
                forecast_hour_data.s3_previous_file
                for forecast_hour_data in all_forecast_hour_data
                if forecast_hour_data.s3_previous_file is not None
            ]
        )

        logger.info(f'Downloading {len(s3_files)} S3 files...')
        start = default_timer()
        results = await asyncio.gather(
            *[
                (
                    s3_client.download_grib_file(
                        config.GFS_BUCKET,
                        s3_file,
                        f'{s3_file}.idx',
                        local_file,
                    )
                )
                for s3_file, local_file in zip(s3_files, local_files)
            ],
            return_exceptions=True,
        )
        elapsed = default_timer() - start
        logger.info(f'Downloaded {len(s3_files)} files in {elapsed}s')

        exceptions = [
            result for result in results if isinstance(result, BaseException)
        ]
        if exceptions:
            raise BaseException(exceptions)

        wind_csv_file_path = os.path.join(temp_dir, WIND_CSV_FILE_NAME)
        weather_csv_file_path = os.path.join(temp_dir, WEATHER_CSV_FILE_NAME)

        previous_total_precipitation_values = None
        previous_total_precipitation_details = None

        logger.info(f'Parsing {len(local_files)} files')
        start = default_timer()
        invalid_points_of_interest = []
        lat_indexes, lon_indexes = (None, None)
        for forecast_hour_data in all_forecast_hour_data:
            logger.info(f'Parsing file: {forecast_hour_data.local_file}')

            (
                lats,
                lons,
                variable_values,
                variable_details,
            ) = parse_data_from_gribs(
                forecast_hour_data.local_file,
                forecast_hour_data.local_fill_data_file
                if forecast_hour_data.local_fill_data_file
                else None,
            )

            if forecast_hour_data.hour == start_forecast_hour:
                lat_lon_indexes = []
                for point_of_interest in points_of_interest:
                    try:
                        lat_lon_indexes.append(
                            get_lat_lon_index(
                                (point_of_interest.lat, point_of_interest.lon),
                                lats,
                                lons,
                            )
                        )
                    except ValueError:
                        invalid_points_of_interest.append(point_of_interest)

                valid_points_of_interest = [
                    poi
                    for poi in points_of_interest
                    if poi not in invalid_points_of_interest
                ]

                lat_indexes = np.array([i for i, _ in lat_lon_indexes])
                lon_indexes = np.array([j for _, j in lat_lon_indexes])

            poi_variable_values = {
                key: values[lat_indexes, lon_indexes]
                for key, values in variable_values.items()
            }

            # Get the previous file's totalPrecipitation value.
            # This is used to calculate marginal precipitation
            # where the start_forecast_hour is greater than 1.
            if forecast_hour_data.local_previous_file is not None:
                (
                    _,
                    _,
                    previous_variable_values,
                    previous_variable_details,
                ) = parse_data_from_gribs(
                    forecast_hour_data.local_previous_file, None
                )
                (
                    previous_total_precipitation_details
                ) = previous_variable_details['totalPrecipitation']

                previous_total_precipitation_values = previous_variable_values[
                    'totalPrecipitation'
                ][lat_indexes, lon_indexes]

            temperature_c = t.kelvin_to_celsius(
                poi_variable_values['temperature']
            )
            dewpoint_c = t.kelvin_to_celsius(poi_variable_values['dewpoint'])
            pressure_mb = t.pascal_to_millibar(poi_variable_values['pressure'])
            # Calculated the hourly precipitation given
            # the current totalPrecipitation and the previous
            # file's totalPrecipitation.
            # If the previous file is None, then calculate the
            # hourly precipitation only from the current totalPrecipitation.
            # If the forecast hour is 1, then pass in None instead of the
            # previous totalPrecipitation. This is due the fact that the
            # previous totalPrecipitation from forecast hour 0 should be
            # excluded from the calculation.
            # Otherwise calculate the marginal hourly precipitation from the
            # current totalPrecipitation and previous totalPrecipitation.
            hourly_precipitation = t.get_hourly_precipitation(
                poi_variable_values['totalPrecipitation'],
                variable_details['totalPrecipitation'],
                previous_total_precipitation_values
                if previous_total_precipitation_values is not None
                and forecast_hour_data.hour != 1
                else None,
                previous_total_precipitation_details
                if previous_total_precipitation_details is not None
                and forecast_hour_data.hour != 1
                else None,
            )

            precipitation_type = t.get_precipitation_type(
                hourly_precipitation,
                poi_variable_values['snow'] > 0,
                poi_variable_values['freezingRain'] > 0,
                poi_variable_values['icePellets'] > 0,
            )
            weather_conditions = get_weather_conditions(
                poi_variable_values, temperature_c, precipitation_type
            )

            weather_hours = [
                WeatherHour(
                    point_of_interest,
                    config.AGENCY,
                    config.MODEL,
                    config.RUN,
                    forecast_hour_data.grid,
                    forecast_hour_data.forecast_time,
                    float(temperature_c[i]),
                    float(dewpoint_c[i]),
                    float(poi_variable_values['visibility'][i]),
                    float(poi_variable_values['relativeHumidity'][i]),
                    float(pressure_mb[i]),
                    float(hourly_precipitation[i]),
                    int(precipitation_type.astype(int)[i]),
                    int(weather_conditions.astype(int)[i]),
                )
                for i, point_of_interest in enumerate(valid_points_of_interest)
            ]
            wind_hours = [
                WindHour(
                    point_of_interest,
                    config.AGENCY,
                    config.MODEL,
                    config.RUN,
                    forecast_hour_data.grid,
                    forecast_hour_data.forecast_time,
                    float(poi_variable_values['windU'][i]),
                    float(poi_variable_values['windV'][i]),
                    float(poi_variable_values['windGust'][i]),
                )
                for i, point_of_interest in enumerate(valid_points_of_interest)
            ]

            await asyncio.gather(
                write_wind_csv(
                    wind_csv_file_path,
                    config.AGENCY,
                    config.MODEL,
                    forecast_hour_data.grid,
                    config.RUN,
                    wind_hours,
                ),
                write_weather_csv(
                    weather_csv_file_path,
                    config.AGENCY,
                    config.MODEL,
                    forecast_hour_data.grid,
                    config.RUN,
                    weather_hours,
                ),
            )

            previous_total_precipitation_values = poi_variable_values[
                'totalPrecipitation'
            ]
            previous_total_precipitation_details = variable_details[
                'totalPrecipitation'
            ]
        elapsed = default_timer() - start
        logger.info(f'Parsed {len(local_files)} files in {elapsed}s')

        logger.info('Uploading CSVs to S3...')
        start = default_timer()
        await asyncio.gather(
            *[
                s3_client.upload_file(
                    local_file_path,
                    config.JOBS_BUCKET,
                    (
                        f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/'
                        f'{config.RUN}/{config.TASK_PARTITION}/{file_name}'
                    ),
                )
                for local_file_path, file_name in [
                    (wind_csv_file_path, WIND_CSV_FILE_NAME),
                    (weather_csv_file_path, WEATHER_CSV_FILE_NAME),
                ]
            ]
        )
        elapsed = default_timer() - start
        logger.info(f'Finished uploading CSVs to S3 in {elapsed}s.')

        if (
            invalid_points_of_interest
            and config.SLACK_API_TOKEN
            and config.SLACK_CHANNEL
        ):
            send_invalid_points_of_interest_notification(
                config.JOB_NAME,
                {
                    '0p25': [
                        point_of_interest.id
                        for point_of_interest in invalid_points_of_interest
                    ]
                },
                config.SLACK_API_TOKEN,
                config.SLACK_CHANNEL,
                logging.error,
            )


if __name__ == '__main__':
    asyncio.run(main())
