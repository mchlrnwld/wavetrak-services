import json
import os
import re
from unittest import mock

import boto3  # type: ignore
import moto  # type: ignore
import pandas as pd
import pytest

from lib.models import PointOfInterestGridPoint

pytestmark = pytest.mark.asyncio


def mock_query_points_of_interest():
    with open(
        os.path.join('fixtures', 'points-of-interest-test-data.json'), 'r'
    ) as poi_json:
        points_of_interest_grid_points = [
            PointOfInterestGridPoint(*dict.values())
            for dict in json.load(poi_json)
        ]

    return points_of_interest_grid_points


async def test_process_point_of_interest_data():
    with moto.mock_s3(), mock.patch(
        'lib.sds.SDS.query_points_of_interest',
        return_value=mock_query_points_of_interest(),
    ), mock.patch(
        'slack_helper.send_invalid_points_of_interest_notification'
    ) as mocked_send_invalid_poi_notification:
        # Must import after mocking boto3
        import lib.config as config
        from main import main

        WIND_AND_WEATHER_CSV_PREFIX = (
            f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/'
            f'{config.RUN}/{config.TASK_PARTITION}'
        )
        WIND_CSV_OBJECT_KEY = f'{WIND_AND_WEATHER_CSV_PREFIX}/wind.csv'
        WEATHER_CSV_OBJECT_KEY = f'{WIND_AND_WEATHER_CSV_PREFIX}/weather.csv'

        YEAR_MONTH_DAY_MATCH = re.search(r'^\d{8}', str(config.RUN))
        HOUR_MATCH = re.search(r'\d{2}$', str(config.RUN))

        # mypy needs assert before group()
        assert YEAR_MONTH_DAY_MATCH is not None
        assert HOUR_MATCH is not None

        YEAR_MONTH_DAY = YEAR_MONTH_DAY_MATCH.group()
        HOUR = HOUR_MATCH.group()

        GFS_DATA_S3_KEY_00 = (
            f'gfs.{YEAR_MONTH_DAY}/{HOUR}/'
            f'atmos/gfs.t{HOUR}z.pgrb2.0p25.f000'
        )
        GFS_DATA_S3_KEY_06 = (
            f'gfs.{YEAR_MONTH_DAY}/00/atmos/gfs.t00z.pgrb2.0p25.f0{HOUR}'
        )

        s3 = boto3.client('s3', region_name='us-west-1')

        for bucket in [config.GFS_BUCKET, config.JOBS_BUCKET]:
            s3.create_bucket(
                Bucket=bucket,
                CreateBucketConfiguration={'LocationConstraint': 'us-west-1'},
            )

        for key, local_path in [
            (
                GFS_DATA_S3_KEY_00,
                os.path.join('fixtures', 'gfs.t06z.pgrb2.0p25.f000'),
            ),
            (
                GFS_DATA_S3_KEY_06,
                os.path.join('fixtures', 'gfs.t00z.pgrb2.0p25.f006'),
            ),
            (
                f'{GFS_DATA_S3_KEY_00}.idx',
                os.path.join('fixtures', 'gfs.t06z.pgrb2.0p25.f000.idx'),
            ),
            (
                f'{GFS_DATA_S3_KEY_06}.idx',
                os.path.join('fixtures', 'gfs.t00z.pgrb2.0p25.f006.idx'),
            ),
        ]:
            s3.upload_file(local_path, config.GFS_BUCKET, key)

        await main()

        wind_df = pd.read_csv(
            s3.get_object(Bucket=config.JOBS_BUCKET, Key=WIND_CSV_OBJECT_KEY)[
                'Body'
            ]
        )

        weather_df = pd.read_csv(
            s3.get_object(
                Bucket=config.JOBS_BUCKET, Key=WEATHER_CSV_OBJECT_KEY
            )['Body']
        )

        assert all(
            column
            in [
                'point_of_interest_id',
                'agency',
                'model',
                'grid',
                'latitude',
                'longitude',
                'run',
                'forecast_time',
                'u',
                'v',
                'gust',
            ]
            for column in wind_df.columns
        )

        assert all(
            column
            in [
                'point_of_interest_id',
                'agency',
                'model',
                'grid',
                'latitude',
                'longitude',
                'run',
                'forecast_time',
                'temperature',
                'dewpoint',
                'visibility',
                'humidity',
                'pressure',
                'precipitation_volume',
                'precipitation_type',
                'weather_conditions',
            ]
            for column in weather_df.columns
        )

        assert len(wind_df) == 11572
        assert len(weather_df) == 11572

        mocked_send_invalid_poi_notification.assert_called()
