# Get environment variables
import os
from typing import Dict

# Get environment variables (from EC2 or .env)
ENVIRONMENT = os.environ['ENVIRONMENT']
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
JOB_NAME = os.environ['JOB_NAME']
BRAND = os.environ['BRAND']

# S3 envs
S3_OBJECT = f'{BRAND}_state.json'
STATE_FILE_PATH = (
    os.path.join(os.environ['AIRFLOW_TMP_DIR'], 'state.json')
    if 'AIRFLOW_TMP_DIR' in os.environ
    else './state.json'
)

# tap-google-play-console constants
TAP_PATH = os.getenv('AIRFLOW_TMP_DIR', 'tap_google_play_console')
TAP_CONFIG_FILENAME = 'config_tap_google_play_console.json'
KEYFILE_NAME = 'tap-google-play-service-account-keyfile.json'
START_DATE = '2016-11-01T00:00:00'

# target-stitch constants
TARGET_PATH = os.getenv('AIRFLOW_TMP_DIR', 'target_stitch')
TARGET_CONFIG_FILENAME = 'config_target_stitch.json'
SMALL_BATCH_URL = 'https://api.stitchdata.com/v2/import/batch'
BIG_BATCH_URL = 'https://api.stitchdata.com/v2/import/batch'
BATCH_SIZE_PREFERENCES: Dict = {}
