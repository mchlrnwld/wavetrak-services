# Google Play Console ETL Pipeline
---

## Overview

This application gets Google Play Console reports data from Google
Cloud Storage and sends the data to Stitch (i.e. `target-stitch`) according to the 
[Singer Spec](https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md). 
Stitch then sends the data on to the Wavetrak Redshift database.

## Quickstart 

```bash
$ make conda-create
$ conda activate tap-google-play-console
$ cp .env.sample .env
$ make
```
The `make` command on its own executes the `lint`, `type-check`, and `run`
targets described below.

## Developing <a name="development_anchor"></a>
### Environment
Before running any of the commands listed below, be sure to set up your local
environment file:
```bash
$ cp .env.sample .env
```

The `make` commands below can be run in shell and are useful during development. Details for each command are found in the [Makefile](Makefile).

### Lint
```bash
$ make lint
```

### Format
- Removes unused imports and variables using [autoflake](https://github.com/myint/autoflake)
- Formats package imports using [isort](https://github.com/timothycrosley/isort)
- Formats code according to [black](https://github.com/psf/black)

```bash
$ make format
```

### Type Check
This application uses [mypy](http://mypy-lang.org/) to do static type-checking.

```bash
$ make
```

### Generate configuration files
```bash
$ make generate-config-files
```

### Run `tap-google-play-console`
```bash
$ make run-tap
```

### Run `tap-google-play-console` and send to `target-stitch`
```bash
$ make run-etl
```

## Testing
---
*This package currently has no traditional* `pytest` *tests.* 

[Singer](https://github.com/singer-io/singer-python) handles all of the checks
we need to ensure the data we stream to Stitch is of the correct format and 
structure. Additionally, the tools provided in [singer-tools](https://github.com/singer-io/singer-tools) (e.g., `singer-check-tap`) can be used during development to 
ensure your tap conforms to the [Singer Spec](https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md).


## Docker
Run the commands below in your shell to build and run the Docker container.
### Build Container
```bash
$ make docker-build
```

### Run Container
```bash
$ make docker-run
```

### Build & Run Container
```bash
$ make docker
```

## Developing & Debugging Inside the Docker Container
At times, it may be hard to debug errors related to Docker without actually 
being inside the container itself and accessing it's shell. Once there, you can
run the Python code or commands in the [Dockerfile](Dockerfile) one at a time and see
the output.

**NOTE:**<br> 
You cannot run `make docker-build`, `make docker-run`, `docker-run-bash`, or `make docker` from inside the Docker container itself.

#### Accessing the Docker Container's Shell:
1. Build the docker container (if not done already)

```bash
$ make docker-build
```

2. Run the container in bash mode using `make docker-run-bash`. This will return
the commands being run by `make` (first 5 lines returned) and the `CONTAINER ID`
(last line returned).

```bash
$ make docker-run-bash
docker run --rm -dit \
--env-file=.env \
--volume ~/Repos/wavetrak-services/jobs/google-play-console-etl-pipeline/docker/google-play-console-etl-pipeline/state.json:/app/google-play-console-etl-pipeline/state.json \
--volume ~/.aws:/root/.aws \
google-play-console-etl-pipeline bash
a50bd916088c6878032c8989be19444954600366077eb58dada5c88244b52d15
```

You will need to make sure the local path to `state.json` in the first 
`--volume` set by `make docker-run-bash` above corresponds to the path for 
`state.json` on your local machine by modifying the path in the [Makefile](Makefile)
of this repo. Edit this part of `Makefile` as needed:

```jinja
# NOTE: Set the first --volume below to the correct path for your machine.
.PHONY: docker-run-bash
docker-run-bash:
	docker run --rm -dit \
	--env-file=.env \
	--volume ~/<modify_this_part_of_path_in_Makefile>/wavetrak-services/jobs/google-play-console-etl-pipeline/docker/google-play-console-etl-pipeline/state.json:/app/google-play-console-etl-pipeline/state.json \
	--volume ~/.aws:/root/.aws \
	google-play-console-etl-pipeline bash
```

3. `Docker exec` into the container using the `CONTAINER ID` returned from `make docker-run-bash` to access the container's shell. You will see a new shell 
prompt for the container, showing the `conda` environment running in the 
container and an abbreviated `CONTAINER ID`. You can now run commands inside 
the container's shell. For example:

```jinja
$ docker exec -it <CONTAINER_ID_from_make_docker_run_bash> bash
(base) root@a50bd916088c:/app# echo "hello world"
hello world
```

4. Activate the conda environment for the job inside the
container in order to access the `make` commands listed in the 
[Developing](#developing_anchor) section above. Find the conda environment by 
running `conda env list` in the container's shell, then activate the 
environment.

```jinja
(base) root@5a2db7afb6be:/app# conda env list
# conda environments:
#
base                  *  /opt/conda
tap-google-play-console     /opt/conda/envs/tap-google-play-console
target-stitch            /opt/conda/envs/target-stitch
(base) root@a45d0aebaeb6:/app# conda activate tap-google-play-console
(tap-google-play-console) root@a45d0aebaeb6:/app#
```

**Note:**<br>
This tap actually has 2 conda environments to run the job, one for the
Singer tap (`tap-google-play-console`) and the other for the Singer target
(`target-stitch`). You want to activate `tap-google-play-console` to access the
`make` commands.

```jinja
(base) root@5a2db7afb6be:/app# conda activate tap-google-play-console
(tap-google-play-console) root@5a2db7afb6be:/app# make lint
All done! ✨ 🍰 ✨
14 files would be left unchanged.
```

## Deploying
See instructions [here](../README.MD)
