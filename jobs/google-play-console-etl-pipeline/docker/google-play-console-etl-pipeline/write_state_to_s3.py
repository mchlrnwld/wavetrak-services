import logging

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type: ignore

from config import S3_OBJECT, STATE_FILE_PATH
from get_secrets import S3_BUCKET_NAME


def upload_s3_file(file_name, bucket, object_name=None):
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')  # Authentication via ~/.aws/credentials
    try:
        s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        raise


if __name__ == '__main__':
    # Write state.json file to AWS S3
    upload_s3_file(STATE_FILE_PATH, S3_BUCKET_NAME, S3_OBJECT)
