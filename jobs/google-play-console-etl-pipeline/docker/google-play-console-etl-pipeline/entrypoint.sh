#!/usr/bin/env bash
#
# Run the gooogle-play-console-etl-pipeline.
set -xe

# Generate the configuration files needed to run the Singer tap and target
/opt/conda/envs/tap-google-play-console/bin/python generate_config_files.py

# Get the state.json from S3
/opt/conda/envs/tap-google-play-console/bin/python get_state_from_s3.py

# Setup the paths to config and state files based where job is run (Airflow or
# local Docker)
if [[ -v AIRFLOW_TMP_DIR ]]; then
  TAP_CONFIG_FILE="$AIRFLOW_TMP_DIR/config_tap_google_play_console.json"
  TARGET_CONFIG_FILE="$AIRFLOW_TMP_DIR/config_target_stitch.json"
  STATE_FILE="$AIRFLOW_TMP_DIR/state.json"
  TMP_STATE_FILE="$AIRFLOW_TMP_DIR/state.json.tmp"
else
  TAP_CONFIG_FILE=tap_google_play_console/config_tap_google_play_console.json
  TARGET_CONFIG_FILE=target_stitch/config_target_stitch.json
  STATE_FILE=state.json
  TMP_STATE_FILE=state.json.tmp
fi

# Run tap-google-play-console and pipe output to target-stitch, write the final
# state emmitted by the target to state.json
/opt/conda/envs/tap-google-play-console/bin/tap-google-play-console \
  --config $TAP_CONFIG_FILE \
  --state $STATE_FILE \
  --catalog tap_google_play_console/catalog_tap_google_play_console.json \
    | /opt/conda/envs/target-stitch/bin/target-stitch \
      --config $TARGET_CONFIG_FILE \
        >> $STATE_FILE

# Get the last STATE message written to state.json and write to a temp file
tail -1 $STATE_FILE > $TMP_STATE_FILE

# Copy state.json.tmp and use it overwrite state.json so it can be used the
# next time the ETL pipeline is run
cp $TMP_STATE_FILE $STATE_FILE

# Write the state.json back to S3
/opt/conda/envs/tap-google-play-console/bin/python write_state_to_s3.py
