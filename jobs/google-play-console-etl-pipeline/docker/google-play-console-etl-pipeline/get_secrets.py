import json

from job_secrets_helper import get_secret

from config import APP_ENVIRONMENT, BRAND, JOB_NAME

# The JSON string assigned to GOOGLE_PLAY_CONSOLE_SERVICE_ACCOUNT_KEYFILE
# can be written directly to a keyfile.json (no parsing of JSON required).
# keyfile.json is used to authenticate when connecting to Google Cloud Platform
GOOGLE_PLAY_CONSOLE_SERVICE_ACCOUNT_KEYFILE = json.loads(
    get_secret(
        f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
        f'{BRAND.upper()}_GOOGLE_PLAY_CONSOLE_SERVICE_ACCOUNT_KEYFILE',
    )
)

GOOGLE_CLOUD_PLATFORM_BUCKET_NAME = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
    f'{BRAND.upper()}_GOOGLE_CLOUD_PLATFORM_BUCKET_NAME',
)

# Same Stitch Client ID for all brands
TARGET_STITCH_CLIENT_ID = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/', 'TARGET_STITCH_CLIENT_ID'
)

TARGET_STITCH_IMPORT_TOKEN = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
    f'{BRAND.upper()}_TARGET_STITCH_IMPORT_TOKEN',
)

# We use the SAME bucket to store the state JSON files for the wavetrak brands
# (surfline, buoyweather, fishtrack) and magicseaweed.
S3_BUCKET_NAME = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/', 'S3_BUCKET_NAME'
)
