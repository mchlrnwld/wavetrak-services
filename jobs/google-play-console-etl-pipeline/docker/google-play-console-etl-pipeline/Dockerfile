FROM continuumio/miniconda:4.7.12

SHELL ["/bin/bash", "-c"]
WORKDIR /app

# In production: pip.conf is pulled from the production environment.
# For local development: make a copy of your local pip.conf in the same 
# directory as the Dockerfile and add it to .gitignore so it is not committed.
COPY pip.conf /root/.pip/pip.conf

# Install gcc compiler needed for psutil dependency in target-stitch 
RUN apt-get update && \
    apt-get -y install gcc

# Setup the conda environments
COPY tap-google-play-console-env.yml target-stitch-env.yml ./
RUN conda env create --file tap-google-play-console-env.yml && \
    conda env create --file target-stitch-env.yml

# Copy all th files needed to run the job to Docker container
COPY . .

# There are no tests so we do not check for them, (0 tests found throws error)
RUN source activate tap-google-play-console && \
    make lint && \
    make type-check

# Run script to start tap and send data to target, save state for next job run
ENTRYPOINT ["./entrypoint.sh"]
