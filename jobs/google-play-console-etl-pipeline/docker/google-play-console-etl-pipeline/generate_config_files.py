import json
from typing import Dict

from config import (
    BATCH_SIZE_PREFERENCES,
    BIG_BATCH_URL,
    KEYFILE_NAME,
    SMALL_BATCH_URL,
    START_DATE,
    TAP_CONFIG_FILENAME,
    TAP_PATH,
    TARGET_CONFIG_FILENAME,
    TARGET_PATH,
)

# fmt: off
from get_secrets import (
    GOOGLE_CLOUD_PLATFORM_BUCKET_NAME,
    GOOGLE_PLAY_CONSOLE_SERVICE_ACCOUNT_KEYFILE,
    TARGET_STITCH_CLIENT_ID,
    TARGET_STITCH_IMPORT_TOKEN
)

# fmt: on


def create_tap_json(
    developer_bucket_id: str,
    tap_path: str = TAP_PATH,
    keyfile_name: str = KEYFILE_NAME,
    start_date: str = START_DATE,
) -> str:
    json_payload = json.dumps(
        {
            'key_file': f'{tap_path}/{keyfile_name}',
            'start_date': f'{start_date}Z',
            'developer_bucket_id': developer_bucket_id,
        }
    )
    return json_payload


def create_target_json(
    token: str,
    client_id: str = TARGET_STITCH_CLIENT_ID,
    small_batch_url: str = SMALL_BATCH_URL,
    big_batch_url: str = BIG_BATCH_URL,
    batch_size_preferences: Dict = BATCH_SIZE_PREFERENCES,
) -> str:
    json_payload = json.dumps(
        {
            'client_id': client_id,
            'token': token,
            'small_batch_url': small_batch_url,
            'big_batch_url': big_batch_url,
            'batch_size_preferences': batch_size_preferences,
        }
    )
    return json_payload


def write_config_file(path: str, filename: str, data: str):
    with open(f'{path}/{filename}', 'w') as write_file:
        write_file.write(data)


def write_keyfile(path: str, filename: str, data: Dict):
    with open(f'{path}/{filename}', 'w') as write_file:
        json.dump(data, write_file)


# Write config file for tap
tap_json_data = create_tap_json(
    developer_bucket_id=GOOGLE_CLOUD_PLATFORM_BUCKET_NAME
)
write_config_file(
    path=TAP_PATH, filename=TAP_CONFIG_FILENAME, data=tap_json_data
)

# Write config file for target
target_json_data = create_target_json(token=TARGET_STITCH_IMPORT_TOKEN)
write_config_file(
    path=TARGET_PATH, filename=TARGET_CONFIG_FILENAME, data=target_json_data
)

write_keyfile(
    path=TAP_PATH,
    filename=KEYFILE_NAME,
    data=GOOGLE_PLAY_CONSOLE_SERVICE_ACCOUNT_KEYFILE,
)
