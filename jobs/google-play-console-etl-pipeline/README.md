# Google Play Console ETL Pipeline
---
Airflow DAG to get Wavetrak (Surfline, Buoyweather, Fishtrack) and Magicseaweed Google Play Console reports data from Google Cloud Platform and send the data to Stitch (i.e., `target-stitch`) according to the [Singer Spec](https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md). Stitch then sends the data on to our Redshift data warehouse.
