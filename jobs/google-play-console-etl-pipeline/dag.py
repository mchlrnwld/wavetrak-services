from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator

from dag_helpers.docker import docker_image


JOB_NAME = 'google-play-console-etl-pipeline'
AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT

default_args = {
    'owner': 'surfline',
    'start_date': datetime(2020, 8, 11, 0, 0, 0),
    'email': 'analytics@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    '{0}-v3'.format(JOB_NAME),
    schedule_interval='0 */6 * * *',
    max_active_runs=1,
    default_args=default_args,
)

google_play_console_etl_pipelines = [
    WTDockerOperator(
        task_id='{0}_google_play_console_etl_pipeline'.format(brand),
        image=docker_image(JOB_NAME, JOB_NAME),
        environment={
            'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
            'ENVIRONMENT': APP_ENVIRONMENT,
            'JOB_NAME': JOB_NAME,
            'BRAND': brand,
        },
        force_pull=True,
        email_on_retry=False,
        dag=dag,
    )
    for brand in ['wavetrak', 'msw']
]
