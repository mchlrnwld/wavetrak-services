# Apple App Store Connect ETL Pipeline
---
Airflow DAG to get Wavetrak (Surfline, Buoyweather, Fishtrack) and Magicseaweed [Apple App Store Connect](https://developer.apple.com/documentation/appstoreconnectapi/sales_and_finance_reports) reports data from the Apple App Store Connect API and send the data to Stitch (i.e., `target-stitch`) according to the [Singer Spec](https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md). Stitch then sends the data on to our Redshift data warehouse.
