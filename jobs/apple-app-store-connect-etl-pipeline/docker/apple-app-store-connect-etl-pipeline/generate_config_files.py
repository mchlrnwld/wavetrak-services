import json
import os
from secrets import (
    APP_STORE_CONNECT_FINANCE_ROLE_KEYFILE,
    APP_STORE_CONNECT_REPORTS_ROLE_KEYFILE,
    ISSUER_ID,
    KEY_ID_FINANCE_ROLE,
    KEY_ID_REPORTS_ROLE,
    TARGET_STITCH_CLIENT_ID,
    TARGET_STITCH_IMPORT_TOKEN,
    VENDOR_NUMBER,
)
from typing import Dict

from config import (
    BATCH_SIZE_PREFERENCES,
    BIG_BATCH_URL,
    FINANACE_ROLE_KEYFILE_NAME,
    REPORT_FREQUENCY,
    REPORTS_ROLE_KEYFILE_NAME,
    SMALL_BATCH_URL,
    START_DATE,
    TAP_CONFIG_FILENAME,
    TAP_PATH,
    TARGET_CONFIG_FILENAME,
    TARGET_PATH,
)


def create_tap_config_json(
    vendor_number: int,
    issuer_id: str,
    tap_path: str,
    key_id_reports_role: str,
    reports_role_keyfile_name: str,
    key_id_finance_role: str,
    finance_role_keyfile_name: str,
    report_frequency: str,
    start_date_pst: str,
    download_single_day: bool,
    download_single_day_date_pst: str,
) -> str:
    json_payload = json.dumps(
        {
            'vendor_number': vendor_number,
            'issuer_id': issuer_id,
            'key_id_reports': key_id_reports_role,
            'path_to_key_file_reports': os.path.join(
                tap_path, reports_role_keyfile_name
            ),
            'key_id_finance': key_id_finance_role,
            'path_to_key_file_finance': os.path.join(
                tap_path, finance_role_keyfile_name
            ),
            'report_frequency': report_frequency,
            'start_date_pst': start_date_pst,
            'download_single_day': download_single_day,
            'download_single_day_date_pst': download_single_day_date_pst,
        }
    )
    return json_payload


def create_target_config_json(
    token: str,
    client_id: str = TARGET_STITCH_CLIENT_ID,
    small_batch_url: str = SMALL_BATCH_URL,
    big_batch_url: str = BIG_BATCH_URL,
    batch_size_preferences: Dict = BATCH_SIZE_PREFERENCES,
) -> str:
    json_payload = json.dumps(
        {
            'client_id': client_id,
            'token': token,
            'small_batch_url': small_batch_url,
            'big_batch_url': big_batch_url,
            'batch_size_preferences': batch_size_preferences,
        }
    )
    return json_payload


def write_file(path: str, filename: str, data: str):
    with open(os.path.join(path, filename), 'w') as write_file:
        write_file.write(data)


if __name__ == "__main__":
    # Write config file for tap
    tap_json_data = create_tap_config_json(
        vendor_number=int(VENDOR_NUMBER),
        issuer_id=ISSUER_ID,
        tap_path=TAP_PATH,
        key_id_reports_role=KEY_ID_REPORTS_ROLE,
        reports_role_keyfile_name=REPORTS_ROLE_KEYFILE_NAME,
        key_id_finance_role=KEY_ID_FINANCE_ROLE,
        finance_role_keyfile_name=FINANACE_ROLE_KEYFILE_NAME,
        report_frequency=REPORT_FREQUENCY,
        start_date_pst=START_DATE,
        download_single_day=False,
        download_single_day_date_pst='',
    )

    write_file(path=TAP_PATH, filename=TAP_CONFIG_FILENAME, data=tap_json_data)

    # Write config file for target
    if not os.path.isdir(TARGET_PATH):
        os.mkdir(TARGET_PATH)

    target_json_data = create_target_config_json(
        token=TARGET_STITCH_IMPORT_TOKEN
    )
    write_file(
        path=TARGET_PATH,
        filename=TARGET_CONFIG_FILENAME,
        data=target_json_data,
    )

    # Write the Reports role key file
    write_file(
        path=TAP_PATH,
        filename=REPORTS_ROLE_KEYFILE_NAME,
        data=APP_STORE_CONNECT_REPORTS_ROLE_KEYFILE,
    )

    # Write the Finance role key file
    write_file(
        path=TAP_PATH,
        filename=FINANACE_ROLE_KEYFILE_NAME,
        data=APP_STORE_CONNECT_FINANCE_ROLE_KEYFILE,
    )
