from job_secrets_helper import get_secret

from config import APP_ENVIRONMENT, BRAND, JOB_NAME

VENDOR_NUMBER = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/', f'{BRAND.upper()}_VENDOR_NUMBER'
)

ISSUER_ID = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/', f'{BRAND.upper()}_ISSUER_ID'
)

KEY_ID_REPORTS_ROLE = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
    f'{BRAND.upper()}_KEY_ID_REPORTS_ROLE',
)

APP_STORE_CONNECT_REPORTS_ROLE_KEYFILE = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
    f'{BRAND.upper()}_APP_STORE_CONNECT_REPORTS_ROLE_KEYFILE',
)

KEY_ID_FINANCE_ROLE = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
    f'{BRAND.upper()}_KEY_ID_FINANCE_ROLE',
)

APP_STORE_CONNECT_FINANCE_ROLE_KEYFILE = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
    f'{BRAND.upper()}_APP_STORE_CONNECT_FINANCE_ROLE_KEYFILE',
)

# Same Stitch Client ID for all brands
TARGET_STITCH_CLIENT_ID = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/', 'TARGET_STITCH_CLIENT_ID'
)

TARGET_STITCH_IMPORT_TOKEN = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/',
    f'{BRAND.upper()}_TARGET_STITCH_IMPORT_TOKEN',
)

# We use the SAME S3 bucket to store state.json files for the "Wavetrak" brands
# (surfline, buoyweather, fishtrack) and Magicseaweed.
S3_BUCKET_NAME = get_secret(
    f'{APP_ENVIRONMENT}/jobs/{JOB_NAME}/', 'S3_BUCKET_NAME'
)
