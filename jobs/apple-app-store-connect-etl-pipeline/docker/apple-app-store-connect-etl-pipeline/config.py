import os
from typing import Dict

import pendulum  # type: ignore

# Statick Constants
BOOKMARK_DATE_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"
TIMEZONE = "America/Los_Angeles"
REPORT_FREQUENCY = 'DAILY'

# Get environment variables (from EC2 or .env)
ENVIRONMENT = os.environ['ENVIRONMENT']
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
JOB_NAME = os.environ['JOB_NAME']
BRAND = os.environ['BRAND']

# S3 envs
S3_OBJECT = f'{BRAND}_state.json'
STATE_FILE_PATH = (
    os.path.join(os.environ['AIRFLOW_TMP_DIR'], 'state.json')
    if 'AIRFLOW_TMP_DIR' in os.environ
    else './state.json'
)

# tap-apple-app-store-connect constants
TAP_PATH = os.getenv('AIRFLOW_TMP_DIR', 'tap_apple_app_store_connect')
TAP_CONFIG_FILENAME = 'config_tap_apple_app_store_connect.json'
FINANACE_ROLE_KEYFILE_NAME = (
    f'{BRAND}_app_store_connect_key_file_finance_role.p8'
)
REPORTS_ROLE_KEYFILE_NAME = (
    f'{BRAND}_app_store_connect_key_file_reports_role.p8'
)
START_DATE = (
    pendulum.today(tz=TIMEZONE)
    .subtract(years=1)
    .strftime(BOOKMARK_DATE_FORMAT)
)

# target-stitch constants
TARGET_PATH = os.getenv('AIRFLOW_TMP_DIR', 'target_stitch')
TARGET_CONFIG_FILENAME = 'config_target_stitch.json'
SMALL_BATCH_URL = 'https://api.stitchdata.com/v2/import/batch'
BIG_BATCH_URL = 'https://api.stitchdata.com/v2/import/batch'
BATCH_SIZE_PREFERENCES: Dict = {}
