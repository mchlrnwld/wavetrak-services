from secrets import S3_BUCKET_NAME

import boto3  # type: ignore

from config import S3_OBJECT, STATE_FILE_PATH


def download_s3_file(file_name, bucket, object_name=None):
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')  # Authentication via ~/.aws/credentials
    s3_client.download_file(bucket, object_name, file_name)


if __name__ == '__main__':
    # Write state.json file to AWS S3
    download_s3_file(STATE_FILE_PATH, S3_BUCKET_NAME, S3_OBJECT)
