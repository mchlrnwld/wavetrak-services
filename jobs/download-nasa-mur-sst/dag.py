import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from airflow_helpers import (
    create_pager_duty_failure_callback,
    create_slack_failure_callback,
    docker_image,
)

ENVIRONMENT = Variable.get('ENVIRONMENT')
APP_ENV = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
JOB_NAME = 'download-nasa-mur-sst'
DEFAULT_TIMEOUT = 30 * 60

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, APP_ENV
)

slack_failure_callback = create_slack_failure_callback(
    APP_ENV, 'forecast-engineering', JOB_NAME
)

download_to_s3_environment = {
    'AWS_DEFAULT_REGION': 'us-west-1',
    'JOB_NAME': 'wt-{0}-{1}'.format(JOB_NAME, APP_ENV),
    'JOB_DEFINITION': 'wt-jobs-{0}-{1}'.format(JOB_NAME, APP_ENV),
    'JOB_QUEUE': 'wt-jobs-common-download-files-job-queue-{0}'.format(APP_ENV),
    'TIMEOUT': str(DEFAULT_TIMEOUT),
    'ENVIRONMENT': ';'.join(
        [
            'ENV={0}'.format(APP_ENV),
            'DATE={{ ds }}',
            'SCIENCE_KEY_PREFIX=nasa/mur-sst',
            'NASA_SST_BASE_CATALOG_URL=https://podaac-opendap.jpl.nasa.gov/opendap/allData/ghrsst/data/GDS2/L4/GLOB/JPL/MUR/v4.1/catalog.xml',
        ]
    ),
}
# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2019, 11, 27),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'retries': 15,
    'retry_delay': timedelta(minutes=1),
    'execution_timeout': timedelta(seconds=DEFAULT_TIMEOUT),
    'sla': timedelta(minutes=15),
    'on_failure_callback': slack_failure_callback,
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=60),
    default_args=default_args,
    concurrency=1,
    max_active_runs=1,
    sla_miss_callback=pager_duty_failure_callback,
)

download_to_s3 = WTDockerOperator(
    task_id='download-to-s3',
    image=docker_image('execute-batch-job'),
    environment=download_to_s3_environment,
    force_pull=True,
    email_on_retry=False,
    dag=dag,
)
