# Download to S3

Docker image to download NASA SST analysis data to S3. This image downloads any data files we're missing from NASA into in S3 to be ready for processing.

## Setup

```sh
$ conda env create -f environment.yml
$ source activate download-nasa-mur-sst-download-to-s3
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t download-nasa-mur-sst/download-to-s3 .
$ cp .env.sample .env
$ docker run --env-file=.env --volume $(pwd)/data:/opt/app/data --rm -it download-nasa-mur-sst/download-to-s3
```
