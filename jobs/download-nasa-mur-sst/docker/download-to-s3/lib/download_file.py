import os

import requests


def download_file(url: str, path: str, filename: str):
    """
    Downloads the file.

    Args:
        url: File to download (str).
        path: File system path to put the download (str).
        filename: what to call the file (str).
    """
    local_path = os.path.join(path, filename)
    response = requests.get(url, stream=True, verify=False)
    response.raise_for_status()
    with open(local_path, 'wb') as file:
        for chunk in response.iter_content(chunk_size=128):
            file.write(chunk)
