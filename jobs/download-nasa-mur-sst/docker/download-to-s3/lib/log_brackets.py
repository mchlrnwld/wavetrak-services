from timeit import default_timer


class LogBrackets:
    """
    Context manager to write a log message before and after a code block, with
    the time taken to execute.
    """

    def __init__(self, logger, task: str):
        """
        Args:
            logger: Instance of a logger object.
            task: String description of the task being done. Will be written
                (with prefixes) to the log.
        """
        self.logger = logger
        self.task = task

    def __enter__(self):
        self.logger.info(f'Starting: {self.task}')
        self.start = default_timer()

    def __exit__(self, type, value, tb):
        elapsed = round(default_timer() - self.start, 3)
        if type is not None:
            # There was an error
            self.logger.error(f'Failed: {self.task}. Took {elapsed}s')
            # Cause the error to re-throw
            return False
        self.logger.info(f'Complete: {self.task}. Took {elapsed}s')
