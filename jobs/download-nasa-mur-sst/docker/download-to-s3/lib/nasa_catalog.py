from datetime import datetime
from posixpath import basename
import re
from typing import List, Tuple
from urllib.parse import urljoin, urlparse

from bs4 import BeautifulSoup  # type: ignore
import requests

import lib.config as config


def list_catalog(url: str) -> List[Tuple[int, str]]:
    """
    Parsers and OPeNDAP catalog XML file for links to additional catalogs.

    Name comes back as int as in SST configs the name are either year or day
    number.

    Args:
        url: url for xml catalog.

    Returns:
        A list of (name, url) pairs, for futher linked catalogs. For SST
        catalogs `name` is either a year of day-of-year value, and is returned
        as an integer.
    """
    response = requests.get(url, verify=False)
    response.raise_for_status()
    soup = BeautifulSoup(response.text, 'lxml-xml')
    return [
        (int(node.get('name')), urljoin(url, node.get('xlink:href')))
        for node in soup.find_all('thredds:catalogRef')
    ]


def list_datasets(url: str) -> List[str]:
    """
    Parsers and OPeNDAP catalog XML file for urls of dataset files.

    Args:
        url: url for xml catalog.

    Returns:
        A list of urls of linked dataset files.
    """
    response = requests.get(url, verify=False)
    response.raise_for_status()
    soup = BeautifulSoup(response.text, 'lxml-xml')
    service = soup.find('thredds:service', attrs={'name': 'file'})
    return [
        urljoin(url, service.get('base') + node.get('urlPath'))
        for node in soup.find_all('thredds:access', serviceName='dap')
    ]


def date_from_day_of_year(year: int, day_of_year: int) -> str:
    day = str(day_of_year).zfill(3)
    return datetime.strptime(f'{year}-{day}', '%Y-%j').strftime('%Y%m%d')


def list_current_sst_files(
    catalog_url: str, from_date: datetime
) -> List[Tuple[datetime, str]]:
    """
    Walks the NASA OPeNDAP catalog XML files for urls of netcdf files of SST
    for days greater than or equal to the given date.

    Args:
        catalog_url: URL of the NASA OPeNDAP catalog XML file (str).
        from_date: Look for files since this date

    Returns:
        A list of (datetime, url) pairs for NASA SST netcdf files.
    """
    from_day_of_year = int(from_date.strftime('%j'))
    from_year = int(from_date.strftime('%Y'))

    sst_files_by_day = [
        (
            year,
            day,
            [url for url in list_datasets(day_catalog) if url.endswith('.nc')],
        )
        for year, year_catalog in list_catalog(catalog_url)
        if year >= from_year
        for day, day_catalog in list_catalog(year_catalog)
        if year > from_year or day >= from_day_of_year
    ]
    analysis_datetime_regex = re.compile(r'^[0-9]{10}')
    sst_files_by_datetime: List[Tuple[datetime, str]] = []
    for year, day, files_to_download in sst_files_by_day:
        for url in files_to_download:
            filename = basename(urlparse(url).path)
            analysis_datetime_match = analysis_datetime_regex.match(filename)
            if analysis_datetime_match is None:
                raise Exception(
                    f'Filename {filename} does not match expected format'
                )
            analysis_datetime = datetime.strptime(
                analysis_datetime_match[0], '%Y%m%d%H'
            )
            if (
                int(analysis_datetime.strftime('%Y')) != year
                or int(analysis_datetime.strftime('%j')) != day
            ):
                raise Exception(
                    f'Filename\'s ({filename}) date time string does not '
                    f'match point in the OpenDAP folder structure'
                )
            sst_files_by_datetime.append((analysis_datetime, url))
    return sst_files_by_datetime
