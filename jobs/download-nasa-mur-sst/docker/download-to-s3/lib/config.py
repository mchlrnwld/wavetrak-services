from datetime import datetime
import os

from job_secrets_helper import get_secret  # type: ignore

DATE = datetime.strptime(os.environ['DATE'], '%Y-%m-%d')
ENV = os.environ['ENV']
LOCAL_FILE_PATH = os.getenv('AIRFLOW_TMP_DIR', 'data')
LOG_NAME = "download-to-s3"
NASA_SST_BASE_CATALOG_URL = os.environ['NASA_SST_BASE_CATALOG_URL']
SECRETS_PREFIX = f'{ENV}/common/'
SCIENCE_BUCKET = get_secret(SECRETS_PREFIX, 'SCIENCE_BUCKET')
SCIENCE_KEY_PREFIX = os.environ['SCIENCE_KEY_PREFIX']
