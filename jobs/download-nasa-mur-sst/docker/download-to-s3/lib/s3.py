import os
import logging
from posixpath import basename
from tempfile import TemporaryDirectory

import boto3  # type: ignore

import lib.config as config
from lib.download_file import download_file
from lib.log_brackets import LogBrackets

client = boto3.client('s3')
logger = logging.getLogger(config.LOG_NAME)


def list_s3_files(bucket: str, prefix: str) -> list:
    """
    List objects in an S3 bucket with a prefix.

    Args:
        bucket: The S3 bucket (str).
        prefix: List objects only with this prefix (str).

    Returns:
        A list of s3 object dictionarys.
    """
    response = client.list_objects_v2(Bucket=bucket, Prefix=prefix)
    if 'Contents' not in response:
        return []
    return response['Contents']


def download_file_to_s3(url: str, bucket: str, key: str):
    """
    Downloads a file and places it in S3.

    Args:
        url: The URL of the asset to download (str).
        bucket: The S3 bucket to place it in (str).
        key: The S3 key to give it (str).
    """
    if not os.path.exists(config.LOCAL_FILE_PATH):
        os.makedirs(config.LOCAL_FILE_PATH)
    filename = basename(key)
    with TemporaryDirectory(dir=config.LOCAL_FILE_PATH) as temp_dir:
        with LogBrackets(logger, f'Download {url} to {temp_dir}/{filename}'):
            download_file(url, temp_dir, filename)

        with LogBrackets(logger, f'Upload {filename} to s3://{bucket}/{key}'):
            client.upload_file(os.path.join(temp_dir, filename), bucket, key)
