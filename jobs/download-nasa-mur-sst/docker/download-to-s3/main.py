from datetime import datetime, timedelta
import logging
from posixpath import basename
import re
from typing import Dict, List, Tuple
from urllib.parse import urlparse

import boto3  # type: ignore
import requests

import lib.config as config
from lib.log_brackets import LogBrackets
from lib.nasa_catalog import list_current_sst_files
from lib.s3 import download_file_to_s3, list_s3_files

client = boto3.client('s3')

logger = logging.getLogger(config.LOG_NAME)


def s3_sst_prefix(analysis_date: datetime) -> str:
    analysis_date_str = analysis_date.strftime('%Y%m%d%H')
    return f'{config.SCIENCE_KEY_PREFIX}/{analysis_date_str}/'


def get_sst_files_to_download() -> List[Tuple[datetime, str]]:
    """
    Finds current NASA SST netcdf files that are not already in S3.

    Returns:
        A list of (datetime, str) pairs containg the SST analysis date and the
        netdcf file url to download.
    """
    current_sst = list_current_sst_files(
        config.NASA_SST_BASE_CATALOG_URL, config.DATE + timedelta(days=-3)
    )
    to_download = []
    for analysis_date, url in current_sst:
        s3_files = [
            basename(obj['Key'])
            for obj in list_s3_files(
                config.SCIENCE_BUCKET, s3_sst_prefix(analysis_date)
            )
        ]
        if basename(urlparse(url).path) not in s3_files:
            to_download.append((analysis_date, url))
    return to_download


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info(f'Date: {config.DATE}')
    with LogBrackets(logger, 'Indexing new NASA-SST data'):
        sst_files_by_date = get_sst_files_to_download()
    if not sst_files_by_date:
        logger.info('No sst files to download')
        return
    number_of_files = len(sst_files_by_date)
    with LogBrackets(logger, f'Download {number_of_files} SST files to S3'):
        for analysis_date, url in sst_files_by_date:
            filename = basename(urlparse(url).path)
            prefix = s3_sst_prefix(analysis_date)
            download_file_to_s3(
                url,
                config.SCIENCE_BUCKET,
                f"{prefix}{filename.replace('.nc4', '')}",
            )


if __name__ == '__main__':
    main()
