#!/bin/bash

# This script should take care of the custom renaming, or even tweaking, on any
# of the resources that should be named differently after the module migration.
# You should safely remove this file if all migrations on all environments were
# already applied.

# Add module name to resources without/outside a module.
subject="batch_job batch_job_policy batch_job_policy_attachment"
replace="batch"

# This script will output the content of the .tfstate file.
if [ "$tfstate" != "" ]; then
  for i in $subject; do
    cat $tfstate | \
    jq \
      --arg subj $i \
      --arg repl $replace \
      '.resources=[.resources[] | if (.name == $subj) then (.name = $repl) else (.) end]' \
    > $tfstate.tmp
    mv $tfstate.tmp $tfstate
  done
fi

# This script will update the state file renaming the necessary resources.
if [ "$tfplan" != "" ]; then
  for i in $subject; do
    cat $tfplan | \
    jq -r \
      --arg subj $i \
      '.resources[] | select(.name == $subj) | select(.mode != "data") | (.module + "." + .type)' | \
    awk \
      -v subj=$i \
      -v repl=$replace \
      '{ print $1"."subj,$1"."repl }' | \
    xargs -r -n 2 \
      terraform state mv
  done
fi
