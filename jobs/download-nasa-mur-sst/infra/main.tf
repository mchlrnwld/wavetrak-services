module "download-nasa-mur-sst" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company     = "wt"
  application = "jobs-download-nasa-mur-sst"
  environment = var.environment
  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/download-nasa-mur-sst/download-to-s3:${var.environment}",
    "vcpus" : 1,
    "memory" : 1024,
  }
  custom_policy = templatefile("${path.module}/policy.json", {
    s3_bucket = var.s3_bucket
  })
}
