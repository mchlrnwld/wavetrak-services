provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/download-nasa-mur-sst/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "download_nasa_mur_sst" {
  source = "../../"

  environment = "sandbox"
  s3_bucket   = "surfline-science-s3-dev"
}
