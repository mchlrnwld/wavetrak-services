provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/process-wave-buoys-and-wind-stations/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_wave_buoys_and_wind_stations" {
  source = "../../"

  environment = "prod"
}
