locals {
  container_properties = {
    "image" : "833713747344.dkr.ecr.us-west-1.amazonaws.com/jobs/process-wave-buoys-and-wind-stations/insert-buoy-data-from-ndbc:${var.environment}",
    "vcpus" : 1,
    "memory" : 1028,
    "volumes" : [
      {
        "host" : {
          "sourcePath" : "/mnt/ephemeral/airflow"
        },
        "name" : "tmp"
      }
    ],
    "mountPoints" : [
      {
        "sourceVolume" : "tmp",
        "containerPath" : "/mnt/ephemeral/airflow",
        "readOnly" : false
      }
    ],
    "ulimits" : [
      {
        "name" : "nofile",
        "softLimit" : 10024,
        "hardLimit" : 10024
      }
    ]
  }
}

module "insert_buoy_data_ndbc_batch_job_definition" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-job-definition"

  company              = "wt"
  application          = "${var.application}-ndbc"
  environment          = var.environment
  container_properties = local.container_properties
}
