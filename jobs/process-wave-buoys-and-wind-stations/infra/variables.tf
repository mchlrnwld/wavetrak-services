variable "environment" {
  type = string
}

variable "company" {
  type    = string
  default = "wt"
}

variable "application" {
  type    = string
  default = "process-wave-buoys-and-wind-stations"
}
