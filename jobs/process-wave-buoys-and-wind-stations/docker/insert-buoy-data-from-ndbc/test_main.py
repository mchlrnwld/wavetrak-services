import os
from datetime import datetime
from sys import platform
from unittest import mock

import psycopg2  # type: ignore
import pytest
from freezegun import freeze_time  # type: ignore
from pytest_postgresql import factories  # type: ignore

pytestmark = pytest.mark.asyncio


def load_database(**kwargs):
    with psycopg2.connect(**kwargs) as db_connection:
        with db_connection.cursor() as cur:
            cur.execute("INSERT INTO station_sources VALUES('NDBC');")
            db_connection.commit()


postgresql_fixture = None

if platform == 'linux':
    postgresql_in_docker = factories.postgresql_noproc(
        host='database',
        dbname='mytestdb',
        load=[
            *[
                f'migrations/{file}'
                for file in sorted(os.listdir('migrations'))
            ],
            load_database,
        ],
    )
    postgresql_fixture = factories.postgresql(
        'postgresql_in_docker', dbname='mytestdb'
    )
elif platform == 'darwin':
    postgresql_proc = factories.postgresql_proc(
        port=8888,
        dbname='mytestdb',
        load=[
            *[
                f'migrations/{file}'
                for file in sorted(os.listdir('migrations'))
            ],
            load_database,
        ],
    )

    postgresql_fixture = factories.postgresql(
        'postgresql_proc', dbname='mytestdb'
    )

NDBC_MAIN_URL = os.environ['NDBC_MAIN_URL']
NDBC_TEXT_URL = os.environ['NDBC_TEXT_URL']
PSQL_CONNECTION_URI = None


def mock_get_secret(secrets_prefix, secret):
    if f'{secrets_prefix}{secret}' == f'{secrets_prefix}NDBC_MAIN_URL':
        return NDBC_MAIN_URL
    elif f'{secrets_prefix}{secret}' == f'{secrets_prefix}NDBC_TEXT_URL':
        return NDBC_TEXT_URL
    elif (
        f'{secrets_prefix}{secret}'
        == f'{secrets_prefix}SCIENCE_PLATFORM_PSQL_JOB'
    ):
        return PSQL_CONNECTION_URI


class MockAiohttp:
    def __init__(self, url: str, raise_for_status=None):
        self.url = url
        self.page_text = None
        self.content = self.Content()

    class Content:
        async def read(self):
            return self.page_text

        def add_text(self, page_text):
            self.page_text = page_text

    async def __aenter__(self):
        if self.url == NDBC_MAIN_URL:
            with open('./fixtures/ndbc_page.txt') as f:
                self.page_text = f.read()
                return self
        elif self.url == NDBC_TEXT_URL:
            with open('./fixtures/ndbc_station_table.txt') as f:
                self.page_text = f.read()
                return self
        elif self.url == f'{NDBC_MAIN_URL}41001_5day.txt':
            with open('./fixtures/41001_5day.txt') as f:
                self.page_text = f.read()
                self.content.add_text(self.page_text.encode('utf-8'))
                return self
        elif self.url == f'{NDBC_MAIN_URL}42019_5day.txt':
            with open('./fixtures/42019_5day.txt') as f:
                self.page_text = f.read()
                self.content.add_text(self.page_text.encode('utf-8'))
                return self
        elif self.url == f'{NDBC_MAIN_URL}42019_5day.spectral':
            with open('./fixtures/42019_5day.spectral') as f:
                self.page_text = f.read()
                self.content.add_text(self.page_text.encode('utf-8'))
                return self
        elif self.url == f'{NDBC_MAIN_URL}44058_5day.txt':
            with open('./fixtures/44058_5day.txt') as f:
                self.page_text = f.read()
                self.content.add_text(self.page_text.encode('utf-8'))
                return self

    async def release(self):
        pass

    async def text(self):
        return self.page_text

    async def __aexit__(self, exc_type, exc, tb):
        pass


async def test_new_stations_and_data_added(postgresql_fixture):
    global PSQL_CONNECTION_URI
    # HACK: This import is a workaround for compatibility issues between Pandas
    # and Freezegun:
    # https://github.com/spulec/freezegun/issues/98
    import pandas  # noqa

    psql_parameters = postgresql_fixture.get_dsn_parameters()
    PSQL_CONNECTION_URI = (
        f"postgresql://{psql_parameters['user']}@"
        f"{psql_parameters['host']}:{psql_parameters['port']}/"
        f"{psql_parameters['dbname']}"
    )
    with mock.patch(
        'aiohttp.ClientSession.get', side_effect=MockAiohttp
    ), freeze_time('2021-06-24'), mock.patch(
        'job_secrets_helper.get_secret', side_effect=mock_get_secret
    ):
        from main import main

        cur = postgresql_fixture.cursor()
        # Expect no stations before main function is invoked.
        cur.execute("SELECT COUNT(*) FROM stations;")
        assert cur.fetchall()[0][0] == 0

        # Expect no station data before main function is invoked.
        cur.execute("SELECT COUNT(*) FROM station_data;")
        assert cur.fetchall()[0][0] == 0

        await main()
        # Expect 3 new stations to be added.
        cur.execute("SELECT COUNT(*) FROM stations;")
        assert cur.fetchall()[0][0] == 3

        cur.execute("SELECT COUNT(*) FROM station_data;")
        assert cur.fetchall()[0][0] == 230
        cur.close()


async def test_existing_station_valid_latest_timestamp(postgresql_fixture):
    global PSQL_CONNECTION_URI
    # HACK: This import is a workaround for compatibility issues between Pandas
    # and Freezegun:
    # https://github.com/spulec/freezegun/issues/98
    import pandas  # noqa

    psql_parameters = postgresql_fixture.get_dsn_parameters()
    PSQL_CONNECTION_URI = f"""postgresql://{psql_parameters['user']}@"
        "{psql_parameters['host']}:{psql_parameters['port']}/"
        {psql_parameters['dbname']}"""

    cur = postgresql_fixture.cursor()
    cur.execute(
        f"""INSERT INTO stations(source, source_id, status, name, latitude,
        longitude, latest_timestamp) VALUES('NDBC', '41001', 'ONLINE',
        'tester', 42.3, 100.3, '2021-06-24 01:00:00');"""
    )
    postgresql_fixture.commit()

    with mock.patch(
        'aiohttp.ClientSession.get', side_effect=MockAiohttp
    ), freeze_time("2021-06-24"), mock.patch(
        'job_secrets_helper.get_secret', side_effect=mock_get_secret
    ):
        from main import main

        cur.execute(
            "SELECT latest_timestamp FROM stations WHERE source_id =  '41001'"
        )
        latest_timestamp, = cur.fetchall()[0]
        # latest_timestamp is unchanged before running main.
        assert latest_timestamp == datetime(2021, 6, 24, 1)

        await main()

        cur.execute(
            f"""SELECT id, latest_timestamp FROM stations
            WHERE source_id =  '41001';"""
        )
        id, latest_timestamp = cur.fetchall()[0]
        # latest_timestamp is updated because of new data.
        assert latest_timestamp == datetime(2021, 6, 24, 4, 50)

        cur.execute(
            f"""SELECT timestamp FROM station_data WHERE station_id = '{id}'
            ORDER BY timestamp ASC;"""
        )
        timestamps = cur.fetchall()
        # New data added is greater than the previous latest_timestamp and
        # **not** greater than the *new* latest timestamp.
        assert timestamps[0][0] >= datetime(2021, 6, 24, 1) and timestamps[-1][
            0
        ] <= datetime(2021, 6, 24, 4, 50)
        cur.close()


async def test_existing_station_invalid_latest_timestamp(postgresql_fixture):
    global PSQL_CONNECTION_URI
    # HACK: This import is a workaround for compatibility issues between Pandas
    # and Freezegun:
    # https://github.com/spulec/freezegun/issues/98
    import pandas  # noqa

    psql_parameters = postgresql_fixture.get_dsn_parameters()
    PSQL_CONNECTION_URI = (
        f"postgresql://{psql_parameters['user']}@"
        f"{psql_parameters['host']}:{psql_parameters['port']}/"
        f"{psql_parameters['dbname']}"
    )

    cur = postgresql_fixture.cursor()
    cur.execute(
        f"""INSERT INTO stations(source, source_id, status, name, latitude,
        longitude, latest_timestamp) VALUES('NDBC', '41001', 'ONLINE',
        'tester', 42.3, 100.3, '2021-06-22 12:00:00');"""
    )
    postgresql_fixture.commit()

    with mock.patch(
        'aiohttp.ClientSession.get', side_effect=MockAiohttp
    ), freeze_time("2021-06-24"), mock.patch(
        'job_secrets_helper.get_secret', side_effect=mock_get_secret
    ):
        from main import main

        cur.execute(
            "SELECT latest_timestamp FROM stations WHERE source_id =  '41001'"
        )
        latest_timestamp, = cur.fetchall()[0]
        # latest_timestamp is unchanged before running main.
        assert latest_timestamp == datetime(2021, 6, 22, 12)

        await main()

        cur.execute("SELECT id FROM stations WHERE source_id =  '41001';")
        id = cur.fetchall()[0][0]
        cur.execute(
            f"""SELECT timestamp FROM station_data WHERE station_id = '{id}'
            ORDER BY timestamp ASC;"""
        )
        timestamps = cur.fetchall()

        # New data added is no later than 24 hours from the current time:
        # 2021-06-24-00:00
        assert timestamps[0][0] >= datetime(2021, 6, 23)
        cur.close()


async def test_offline_station_is_turned_back_online(postgresql_fixture):
    global PSQL_CONNECTION_URI
    # HACK: This import is a workaround for compatibility issues between Pandas
    # and Freezegun:
    # https://github.com/spulec/freezegun/issues/98
    import pandas  # noqa

    psql_parameters = postgresql_fixture.get_dsn_parameters()
    PSQL_CONNECTION_URI = (
        f"postgresql://{psql_parameters['user']}@"
        f"{psql_parameters['host']}:{psql_parameters['port']}/"
        f"{psql_parameters['dbname']}"
    )

    cur = postgresql_fixture.cursor()
    cur.execute(
        f"""INSERT INTO stations(source, source_id, status, name, latitude,
        longitude, latest_timestamp) VALUES('NDBC', '41001', 'OFFLINE',
        'tester', 42.3, 100.3, '2021-06-20 12:00:00');"""
    )
    postgresql_fixture.commit()

    with mock.patch(
        'aiohttp.ClientSession.get', side_effect=MockAiohttp
    ), freeze_time("2021-06-24"), mock.patch(
        'job_secrets_helper.get_secret', side_effect=mock_get_secret
    ):
        from main import main as main

        cur.execute(
            f"""SELECT status, latest_timestamp FROM stations
            WHERE source_id =  '41001'"""
        )
        status, latest_timestamp = cur.fetchall()[0]
        # latest_timestamp for the buoy is more than 48 hours
        # from the current UTC time, so the buoy is OFFLINE.
        assert (
            latest_timestamp == datetime(2021, 6, 20, 12)
            and status == 'OFFLINE'
        )

        await main()

        cur.execute("SELECT COUNT(*) FROM station_data;")
        cur.execute(
            f"""SELECT status, latest_timestamp FROM stations
            WHERE source_id =  '41001';"""
        )
        status, latest_timestamp = cur.fetchall()[0]
        # Station is turned back ONLINE and latest_timestamp is updated because
        # of new data.
        assert (
            latest_timestamp == datetime(2021, 6, 24, 4, 50)
            and status == 'ONLINE'
        )
        cur.close()
