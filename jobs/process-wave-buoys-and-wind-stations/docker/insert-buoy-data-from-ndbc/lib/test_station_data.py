import uuid
from datetime import datetime, timedelta
from random import uniform
from typing import Dict

import numpy as np  # type:ignore
import pandas as pd  # type:ignore
from science_algorithms import SwellPartition

from lib.station_data import (
    ExistingStation,
    StationDataHour,
    _get_matching_spectra_timestamp_index,
    _get_minimum_timestamp,
    _get_spectra_data_from_file,
    get_all_station_data,
)


def test_create_station_data_hour():
    # Use dominate period over average period when both available.
    row = pd.Series(
        {
            'WVHT': 1.1,
            'DPD': 18,
            'APD': 6,
            'WDIR': np.nan,
            'WSPD': np.nan,
            'GST': np.nan,
            'MWD': 209,
            'PRES': np.nan,
            'ATMP': np.nan,
            'WTMP': 20.1,
            'DEWP': np.nan,
        }
    )
    sdh = StationDataHour(
        "1235-2323-1212-1212",
        "12345",
        datetime.now(),
        row,
        [SwellPartition(0.17, 5.55, 141.8)],
        [1.0],
    )
    assert sdh.peak_period == 18

    # Use average period when dominant period not available.
    row = pd.Series(
        {
            'WVHT': 1.1,
            'DPD': np.nan,
            'APD': 6,
            'WDIR': np.nan,
            'WSPD': np.nan,
            'GST': np.nan,
            'MWD': 209,
            'PRES': np.nan,
            'ATMP': np.nan,
            'WTMP': 20.1,
            'DEWP': np.nan,
        }
    )
    sdh = StationDataHour(
        "1235-2323-1212-1212",
        "12345",
        datetime.now(),
        row,
        [SwellPartition(0.17, 5.55, 141.8)],
        [1.0],
    )
    assert sdh.peak_period == 6


def test_get_spectra_data_from_file():
    with open('./lib/fixtures/ndbc_spectra.txt') as f:
        spectra_dataframe = _get_spectra_data_from_file(f.read())
        assert len(spectra_dataframe.index) == 2
        datetime(2021, 5, 28, 4, 50)
        assert spectra_dataframe['timestamp'].values[0] == np.datetime64(
            '2021-05-29T04:50:00'
        )
        datetime(2021, 5, 28, 3, 50)
        assert spectra_dataframe['timestamp'].values[1] == np.datetime64(
            '2021-05-29T03:50:00'
        )


def test_get_all_station_data():
    now = datetime.strptime('2021-05-27 03:00:00', '%Y-%m-%d %H:%M:%S')
    existing_stations: Dict[str, ExistingStation] = {
        '12345': {
            'station_id': str(uuid.uuid1),
            'latest_timestamp': datetime.strptime(
                '2021-05-27 05:30:00', '%Y-%m-%d %H:%M:%S'
            ),
        }
    }
    station_data = {
        '12345': {
            'wave_data': pd.DataFrame(
                {
                    'timestamp': [
                        datetime.strptime(
                            '2021-05-27 07:30:00', '%Y-%m-%d %H:%M:%S'
                        ),
                        datetime.strptime(
                            '2021-05-27 06:30:00', '%Y-%m-%d %H:%M:%S'
                        ),
                    ],
                    'WVHT': [1.2, 1.0],
                    'DPD': [2.3, 3.4],
                    'APD': [2.1, 3.2],
                    'MWD': [200, 202],
                    'WTMP': [1, 1],
                    'ATMP': [1, 1],
                    'WSPD': [1, 1],
                    'WDIR': [1, 1],
                    'GST': [1, 1],
                    'PRES': [1, 1],
                    'DEWP': [1, 1],
                }
            )
        }
    }
    all_station_data_hours = get_all_station_data(
        station_data, existing_stations, now
    )
    assert len(all_station_data_hours) == 2
    assert all_station_data_hours[0].significant_height == 1.2
    assert all_station_data_hours[0].peak_period == 2.3
    assert np.isnan(all_station_data_hours[0].swell_wave_1_height)

    now = datetime.strptime('2021-05-27 03:00:00', '%Y-%m-%d %H:%M:%S')
    existing_stations = {
        '12345': {
            'station_id': str(uuid.uuid1),
            'latest_timestamp': datetime.strptime(
                '2021-05-27 05:30:00', '%Y-%m-%d %H:%M:%S'
            ),
        }
    }
    station_data = {
        '12345': {
            'wave_data': pd.DataFrame(
                {
                    'timestamp': [
                        datetime.strptime(
                            '2021-05-27 05:50:00', '%Y-%m-%d %H:%M:%S'
                        ),
                        datetime.strptime(
                            '2021-05-26 04:30:00', '%Y-%m-%d %H:%M:%S'
                        ),
                    ],
                    'WVHT': [1.2, 1.0],
                    'DPD': [2.3, 3.4],
                    'APD': [2.1, 3.2],
                    'MWD': [201, 203],
                    'WTMP': [1, 1],
                    'ATMP': [1, 1],
                    'WSPD': [1, 1],
                    'WDIR': [1, 1],
                    'GST': [1, 1],
                    'PRES': [1, 1],
                    'DEWP': [1, 1],
                }
            ),
            'spectra': pd.DataFrame(
                [
                    {
                        'timestamp': datetime.strptime(
                            '2021-05-27 06:00:00', '%Y-%m-%d %H:%M:%S'
                        ),
                        'data': pd.DataFrame(
                            {
                                'freq': [uniform(0.5, 1.0) for _ in range(80)],
                                'bandwidth': [
                                    uniform(0.5, 1.0) for _ in range(80)
                                ],
                                'energy': [
                                    uniform(0.5, 1.0) for _ in range(80)
                                ],
                                'r1': [uniform(0.5, 1.0) for _ in range(80)],
                                'r2': [uniform(0.5, 1.0) for _ in range(80)],
                                'alpha1': [192] * 80,
                                'alpha2': [216] * 80,
                            }
                        ),
                    },
                    {
                        'timestamp': datetime.strptime(
                            '2021-05-27 05:00:00', '%Y-%m-%d %H:%M:%S'
                        ),
                        'data': pd.DataFrame(
                            {
                                'freq': [uniform(0.5, 1.0) for _ in range(40)],
                                'bandwidth': [
                                    uniform(0.5, 1.0) for _ in range(40)
                                ],
                                'energy': [
                                    uniform(0.5, 1.0) for _ in range(40)
                                ],
                                'r1': [uniform(0.5, 1.0) for _ in range(40)],
                                'r2': [uniform(0.5, 1.0) for _ in range(40)],
                                'alpha1': [192] * 40,
                                'alpha2': [216] * 40,
                            }
                        ),
                    },
                ]
            ),
        }
    }
    all_station_data_hours = get_all_station_data(
        station_data, existing_stations, now
    )
    assert len(all_station_data_hours) == 1
    assert all_station_data_hours[0].significant_height == 1.2
    assert all_station_data_hours[0].peak_period == 2.3
    assert not np.isnan(all_station_data_hours[0].swell_wave_1_height)


def test_get_all_station_data_correct_period_used():
    now = datetime.strptime('2021-05-27 03:00:00', '%Y-%m-%d %H:%M:%S')
    existing_stations: Dict[str, ExistingStation] = {}
    station_data = {
        '12345': {
            'wave_data': pd.DataFrame(
                {
                    'timestamp': [
                        datetime.strptime(
                            '2021-05-27 05:50:00', '%Y-%m-%d %H:%M:%S'
                        ),
                        datetime.strptime(
                            '2021-05-26 04:30:00', '%Y-%m-%d %H:%M:%S'
                        ),
                    ],
                    'WVHT': [1.2, 1.0],
                    'DPD': [2.3, np.nan],
                    'APD': [2.1, 1.9],
                    'MWD': [201, 203],
                    'WTMP': [1, 1],
                    'ATMP': [1, 1],
                    'WSPD': [1, 1],
                    'WDIR': [1, 1],
                    'GST': [1, 1],
                    'PRES': [1, 1],
                    'DEWP': [1, 1],
                }
            )
        }
    }
    all_station_data_hours = get_all_station_data(
        station_data, existing_stations, now
    )
    assert len(all_station_data_hours) == 2
    assert (
        all_station_data_hours[0].peak_period == 2.3
    )  # Choose dominant period when available.
    assert (
        all_station_data_hours[1].peak_period == 1.9
    )  # Use average period when not dominant period not available.

    now = datetime.strptime('2021-05-27 03:00:00', '%Y-%m-%d %H:%M:%S')
    existing_stations = {}
    station_data = {
        '12345': {
            'wave_data': pd.DataFrame(
                {
                    'timestamp': [
                        datetime.strptime(
                            '2021-05-27 05:50:00', '%Y-%m-%d %H:%M:%S'
                        ),
                        datetime.strptime(
                            '2021-05-26 04:30:00', '%Y-%m-%d %H:%M:%S'
                        ),
                    ],
                    'WVHT': [1.2, 1.0],
                    'DPD': [np.nan, np.nan],
                    'APD': [np.nan, np.nan],
                    'MWD': [201, 203],
                    'WTMP': [1, 1],
                    'ATMP': [1, 1],
                    'WSPD': [1, 1],
                    'WDIR': [1, 1],
                    'GST': [1, 1],
                    'PRES': [1, 1],
                    'DEWP': [1, 1],
                }
            )
        }
    }
    all_station_data_hours = get_all_station_data(
        station_data, existing_stations, now
    )
    assert (
        not all_station_data_hours
    )  # Expect no data to be returned when dominant period
    # nor average period are available.


def test_get_matching_spectra_timestamp_index():
    row_time = datetime.strptime('2021-05-27 05:30:00', '%Y-%m-%d %H:%M:%S')
    s = pd.DataFrame(
        {
            'timestamp': [
                datetime.strptime('2021-05-27 06:00:00', '%Y-%m-%d %H:%M:%S'),
                datetime.strptime('2021-05-27 05:00:00', '%Y-%m-%d %H:%M:%S'),
            ]
        }
    )
    assert _get_matching_spectra_timestamp_index(s, row_time) is None

    row_time = datetime.strptime('2021-05-27 03:00:00', '%Y-%m-%d %H:%M:%S')
    s = pd.DataFrame(
        {
            'timestamp': [
                datetime.strptime('2021-05-27 05:00:00', '%Y-%m-%d %H:%M:%S'),
                datetime.strptime('2021-05-27 04:00:00', '%Y-%m-%d %H:%M:%S'),
                datetime.strptime('2021-05-27 03:00:00', '%Y-%m-%d %H:%M:%S'),
                datetime.strptime('2021-05-27 02:00:00', '%Y-%m-%d %H:%M:%S'),
            ]
        }
    )
    assert _get_matching_spectra_timestamp_index(s, row_time) == 2

    row_time = datetime.strptime('2021-05-27 02:43:00', '%Y-%m-%d %H:%M:%S')
    s = pd.DataFrame(
        {
            'timestamp': [
                datetime.strptime('2021-05-27 04:00:00', '%Y-%m-%d %H:%M:%S'),
                datetime.strptime('2021-05-27 03:00:00', '%Y-%m-%d %H:%M:%S'),
            ]
        }
    )
    assert _get_matching_spectra_timestamp_index(s, row_time) == 1

    row_time = datetime.strptime('2021-05-27 08:10:00', '%Y-%m-%d %H:%M:%S')
    s = pd.DataFrame(
        {
            'timestamp': [
                datetime.strptime('2021-05-27 08:00:00', '%Y-%m-%d %H:%M:%S'),
                datetime.strptime('2021-05-27 07:00:00', '%Y-%m-%d %H:%M:%S'),
            ]
        }
    )
    assert _get_matching_spectra_timestamp_index(s, row_time) is None


def test_get_minimum_timestamp():
    # New station so get the timestamp 1 day behind the current time.
    current_station_latest_timestamp = None
    now = datetime.strptime('2021-05-27 05:30:00', '%Y-%m-%d %H:%M:%S')
    assert _get_minimum_timestamp(
        current_station_latest_timestamp, now
    ) == now - timedelta(days=1)
    # Existing station whose latest timestamp is *less* then 1 day behind the
    # current utc time.
    current_station_latest_timestamp = datetime.strptime(
        '2021-05-27 02:30:00', '%Y-%m-%d %H:%M:%S'
    )
    now = datetime.strptime('2021-05-27 05:30:00', '%Y-%m-%d %H:%M:%S')
    assert _get_minimum_timestamp(
        current_station_latest_timestamp, now
    ) == datetime.strptime('2021-05-27 02:30:00', '%Y-%m-%d %H:%M:%S')
    # Existing station whose latest timestamp is *more* then 1 day behind the
    # current utc time.
    current_station_latest_timestamp = datetime.strptime(
        '2021-05-26 02:30:00', '%Y-%m-%d %H:%M:%S'
    )
    now = datetime.strptime('2021-05-27 05:30:00', '%Y-%m-%d %H:%M:%S')
    assert _get_minimum_timestamp(
        current_station_latest_timestamp, now
    ) == datetime.strptime('2021-05-26 05:30:00', '%Y-%m-%d %H:%M:%S')
