import asyncio
from contextlib import ExitStack
from datetime import datetime
from functools import partial
from typing import Dict, Union

import psycopg2  # type: ignore


class PSQL:
    """
    PSQL allows copying data from multiple CSVs into Postgres in a single
    transaction.
    Arguments:
        connection_uri: URI to connect to the database.
    """

    def __init__(self, connection_uri: str):
        self.connection_uri = connection_uri

    def __enter__(self):
        self.stack = ExitStack()
        self.stack.__enter__()
        self.conn = self.stack.enter_context(
            psycopg2.connect(self.connection_uri)
        )
        self.cur = self.stack.enter_context(self.conn.cursor())
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stack.__exit__(exc_type, exc_value, traceback)

    def copy_csv(self, csv_path, table):
        """
        Copy CSV file data to a table in Postgres.
        Arguments:
            csv_path: Path to CSV file.
            table: Table to copy data to.
        """
        with open(csv_path) as csv_file:
            columns = csv_file.readline()
            csv_file.seek(0)
            self.cur.copy_expert(
                f'COPY {table} ({columns}) FROM STDIN WITH CSV HEADER',
                csv_file,
            )

    def commit(self):
        """
        Commit transaction to Postgres.
        """
        self.conn.commit()

    def rollback(self):
        """
        Rollback pending transaction.
        """
        self.conn.rollback()

    def get_all_stations(
        self, source
    ) -> Dict[str, Dict[str, Union[str, datetime]]]:
        self.cur.execute(
            (
                f"""
                    SELECT source_id, id, latest_timestamp
                    FROM stations
                    WHERE source = %s;
                    """
            ),
            [source],
        )
        return {
            station[0]: {
                'station_id': station[1],
                'latest_timestamp': station[2],
            }
            for station in self.cur.fetchall()
        }

    async def add_and_update_station(
        self,
        id: str,
        source: str,
        source_id: str,
        name: str,
        latitude: float,
        longitude: float,
        latest_timestamp,
    ):
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(
            None,
            partial(
                self.cur.execute,
                f"""
                    INSERT INTO stations(id, source, source_id, status, name,
                    latitude, longitude, latest_timestamp) VALUES (%s, %s, %s,
                    'ONLINE', %s, %s, %s, %s) ON CONFLICT ON CONSTRAINT
                    stations_source_source_id_key DO UPDATE SET
                    name = %s, latitude = %s, longitude = %s,
                    status = 'ONLINE', latest_timestamp = %s;
                    """,
                (
                    id,
                    source,
                    source_id,
                    name,
                    latitude,
                    longitude,
                    latest_timestamp,
                    name,
                    latitude,
                    longitude,
                    latest_timestamp,
                ),
            ),
        )
