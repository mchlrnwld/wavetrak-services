import os
from distutils.util import strtobool

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
COMMIT = bool(strtobool(os.getenv('COMMIT', 'false')))
SCIENCE_PLATFORM_PSQL_JOB = get_secret(
    SECRETS_PREFIX, 'SCIENCE_PLATFORM_PSQL_JOB'
)
NDBC_MAIN_URL = get_secret(SECRETS_PREFIX, 'NDBC_MAIN_URL')
NDBC_TEXT_URL = get_secret(SECRETS_PREFIX, 'NDBC_TEXT_URL')
DATA_DIR = os.environ['DATA_DIR']
SOURCE = os.environ['SOURCE']
