import pytest  # type: ignore

from lib.buoy_page_parser import (
    _calculate_lat_lon,
    get_station_info,
    parse_ndbc_ids,
)


def test_parse_ndbc_ids():
    with open('./lib/fixtures/ndbc_page.txt') as f:
        ndbc_page = f.read()
        assert parse_ndbc_ids(ndbc_page) == ({'32ST0', '41NT0'}, {'46T29'})


def test_get_station_info():
    with open('./lib/fixtures/ndbc_station_table.txt') as f:
        station_table = f.read()
        df = get_station_info(station_table, set(['00922']))
        assert len(df.index) == 0
        df = get_station_info(station_table, set(['41113']))
        assert len(df.index) == 1
        assert df['station_id'].values[0] == '41113'
        assert df['name'].values[0] == 'Cape Canaveral Nearshore, FL (143)'
        assert df['location'].values[0] == (28.400, -80.534)

        df = get_station_info(station_table, set(['41113']))
        assert len(df.index) == 1
        assert df['station_id'].values[0] == '41113'
        assert df['name'].values[0] == 'Cape Canaveral Nearshore, FL (143)'
        assert df['location'].values[0] == (28.400, -80.534)


def test_calculate_lat_lon():
    with open('./lib/fixtures/ndbc_location.txt') as f:
        assert _calculate_lat_lon(f.readline()) == (28.4, -80.534)
        assert _calculate_lat_lon(f.readline()) == (-4.0, 67.0)
        with pytest.raises(ValueError) as err:
            assert _calculate_lat_lon(f.readline()) == (-4.0, 67.0)

        assert (f'Invalid format for location') in str(err.value)
