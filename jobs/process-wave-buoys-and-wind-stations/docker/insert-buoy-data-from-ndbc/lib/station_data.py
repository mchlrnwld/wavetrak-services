import asyncio
import io
import logging
import uuid
from datetime import datetime, timedelta
from typing import Any, Dict, List, Optional, Set

import aiohttp  # type: ignore
import async_timeout  # type: ignore
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from science_algorithms import (
    SwellPartition,
    calculate_impacts,
    spectra_to_swell_partitions,
)
from typing_extensions import TypedDict

logger = logging.getLogger('insert-buoy-data-from-ndbc')

ExistingStation = TypedDict(
    'ExistingStation', {'latest_timestamp': datetime, 'station_id': str}
)


class StationDataHour:
    """
    Data for a station for a single timestamp.

    Args:
        station_id: station id.
        source_id: source id for the station.
        timestamp: time associated with the data.
        row: Pandas dataframe row for wave data,
        swell_partitions: list of swell partitions if the station has spectra.
        impacts: list of swell impacts if the station has spectra.
    """

    def __init__(
        self,
        station_id: str,
        source_id: str,
        timestamp: datetime,
        row: Any,
        swell_partitions: List[SwellPartition],
        impacts: List[float],
    ):
        self.station_id = station_id
        self.source_id = source_id
        self.timestamp = timestamp
        self.significant_height = row['WVHT']
        self.peak_period = (
            row['DPD'] if ~np.isnan(row['DPD']) else row['APD']
        )  # Use dominant period if possible, otherwise use average period.
        self.mean_wave_direction = row['MWD']
        self.water_temperature = row['WTMP']
        self.air_temperature = row['ATMP']
        self.wind_speed = row['WSPD']
        self.wind_direction = row['WDIR']
        self.wind_gust = row['GST']
        self.pressure = row['PRES']
        self.dew_point = row['DEWP']
        self.elevation = np.nan
        self.swell_wave_1_height = (
            swell_partitions[0].height
            if swell_partitions and len(swell_partitions) > 0
            else np.nan
        )
        self.swell_wave_1_period = (
            swell_partitions[0].period
            if swell_partitions and len(swell_partitions) > 0
            else np.nan
        )
        self.swell_wave_1_direction = (
            swell_partitions[0].direction
            if swell_partitions and len(swell_partitions) > 0
            else np.nan
        )
        self.swell_wave_1_impact = (
            impacts[0] if impacts and len(impacts) > 0 else np.nan
        )
        self.swell_wave_2_height = (
            swell_partitions[1].height
            if swell_partitions and len(swell_partitions) > 1
            else np.nan
        )
        self.swell_wave_2_period = (
            swell_partitions[1].period
            if swell_partitions and len(swell_partitions) > 1
            else np.nan
        )
        self.swell_wave_2_direction = (
            swell_partitions[1].direction
            if swell_partitions and len(swell_partitions) > 1
            else np.nan
        )
        self.swell_wave_2_impact = (
            impacts[1] if impacts and len(impacts) > 1 else np.nan
        )
        self.swell_wave_3_height = (
            swell_partitions[2].height
            if swell_partitions and len(swell_partitions) > 2
            else np.nan
        )
        self.swell_wave_3_period = (
            swell_partitions[2].period
            if swell_partitions and len(swell_partitions) > 2
            else np.nan
        )
        self.swell_wave_3_direction = (
            swell_partitions[2].direction
            if swell_partitions and len(swell_partitions) > 2
            else np.nan
        )
        self.swell_wave_3_impact = (
            impacts[2] if impacts and len(impacts) > 2 else np.nan
        )
        self.swell_wave_4_height = (
            swell_partitions[3].height
            if swell_partitions and len(swell_partitions) > 3
            else np.nan
        )
        self.swell_wave_4_period = (
            swell_partitions[3].period
            if swell_partitions and len(swell_partitions) > 3
            else np.nan
        )
        self.swell_wave_4_direction = (
            swell_partitions[3].direction
            if swell_partitions and len(swell_partitions) > 3
            else np.nan
        )
        self.swell_wave_4_impact = (
            impacts[3] if impacts and len(impacts) > 3 else np.nan
        )
        self.swell_wave_5_height = (
            swell_partitions[4].height
            if swell_partitions and len(swell_partitions) > 4
            else np.nan
        )
        self.swell_wave_5_period = (
            swell_partitions[4].period
            if swell_partitions and len(swell_partitions) > 4
            else np.nan
        )
        self.swell_wave_5_direction = (
            swell_partitions[4].direction
            if swell_partitions and len(swell_partitions) > 4
            else np.nan
        )
        self.swell_wave_5_impact = (
            impacts[4] if impacts and len(impacts) > 4 else np.nan
        )
        self.swell_wave_6_height = (
            swell_partitions[5].height
            if swell_partitions and len(swell_partitions) > 5
            else np.nan
        )
        self.swell_wave_6_period = (
            swell_partitions[5].period
            if swell_partitions and len(swell_partitions) > 5
            else np.nan
        )
        self.swell_wave_6_direction = (
            swell_partitions[5].direction
            if swell_partitions and len(swell_partitions) > 5
            else np.nan
        )
        self.swell_wave_6_impact = (
            impacts[5] if impacts and len(impacts) > 5 else np.nan
        )

    def asdict(self):
        return self.__dict__


def _get_matching_spectra_timestamp_index(
    spectra_df: Any, row_time: datetime
) -> Optional[int]:
    """
    Get the index for the spectra that matches the wave data row.

    Args:
        spectra_timestamps: Pandas series of spectra timestamps.
        row_time: datetime of the current row of wave data.
        spectra_length: Total length of spectra.

    Returns:
        The index if there is a matching spectra otherwise None.
    """
    valid_spectra_timestamps = spectra_df[
        # Valid spectra is data within 20 minutes of the corresponding wave
        # data.
        (spectra_df['timestamp'] - row_time >= timedelta(minutes=0))
        & (spectra_df['timestamp'] - row_time <= timedelta(minutes=20))
    ]

    if not len(valid_spectra_timestamps.index):
        return None
    return list(valid_spectra_timestamps.index)[0]


def _get_minimum_timestamp(
    station_latest_timestamp: Optional[datetime], current_utc_time: datetime
) -> datetime:
    """
    Get the minimum timestamp for station data.

    Args:
        station_latest_timestamp: The current station's latest timestamp if the
        station already exists.
        current_utc_time: The current time in UTC.

    Returns:
        The minimum timestamp for the station.
    """
    return (
        # If the station already exists, grab the latest timestamp for the
        # station if the latest timestamp is no more than 1 day from the
        # current utc time.
        station_latest_timestamp
        if station_latest_timestamp is not None
        and abs(station_latest_timestamp - current_utc_time)
        <= timedelta(days=1)
        else current_utc_time - timedelta(days=1)
    )


def get_all_station_data(
    station_data: Dict[str, Dict[str, Any]],
    existing_stations: Dict[str, ExistingStation],
    current_utc_time: datetime,
) -> List[StationDataHour]:
    """
    Get all valid data for every station.

    Args:
        station_data: Dictionary of station source ids and dataframes
                      containing data.
        existing_stations: Dictionary of stations that already exist in
                           Postgres.

    Returns:
        List of station data hours.
    """
    all_station_data_hours = []
    for station, data in station_data.items():
        wave_df = data['wave_data']
        latest_timestamp: Optional[datetime] = (
            existing_stations[station]['latest_timestamp']
            if station in existing_stations
            else None
        )
        minimum_timestamp = _get_minimum_timestamp(
            latest_timestamp, current_utc_time
        )
        # Filter out stations timestamps that do not have significant wave
        # height data or do not have wave period data (dominant or average
        # period).
        valid_wave_df = wave_df[
            (~np.isnan(wave_df['WVHT']))
            & ((~np.isnan(wave_df['DPD'])) | (~np.isnan(wave_df['APD'])))
            & (wave_df['timestamp'] > minimum_timestamp)
        ]
        if len(valid_wave_df.index):
            station_id = str(uuid.uuid1())
            while station_id in existing_stations:
                station_id = str(uuid.uuid1())
            if station in existing_stations:
                station_id = existing_stations[station]['station_id']
            for _, wave_row in valid_wave_df.iterrows():
                swell_partitions: List[SwellPartition] = []
                impacts: List[float] = []
                row_time = wave_row['timestamp']
                if 'spectra' in data:
                    spectra_df = data['spectra']
                    index = _get_matching_spectra_timestamp_index(
                        spectra_df, row_time
                    )
                    if index is None:
                        continue
                    values = spectra_df.iloc[index]['data']
                    swell_partitions = spectra_to_swell_partitions(
                        np.array(values['freq']),
                        np.array(values['bandwidth']),
                        np.array(values['energy']),
                        np.array(values['r1']),
                        np.array(values['r2']),
                        np.array(values['alpha1']),
                        np.array(values['alpha2']),
                    )
                    impacts = calculate_impacts(swell_partitions)
                all_station_data_hours.append(
                    StationDataHour(
                        station_id,
                        station,
                        row_time,
                        wave_row,
                        swell_partitions,
                        impacts,
                    )
                )
    return all_station_data_hours


async def pull_ndbc_station_data(
    stations_wave_only: Set[str],
    stations_with_spectra: Set[str],
    ndbc_main_url: str,
) -> Dict[str, Dict[str, Any]]:
    """
    Downloads a station data text file for each station id passed, returning
    a DataFrame for each station if available.
    Args:
        station_ids: List of NDBC IDs to download data for.
    Returns:
        List of DataFrames for each station table downloaded.
    """
    async with aiohttp.ClientSession() as session:
        station_wave_only_tasks = [
            download_station_data(ndbc_main_url, station_id, session, False)
            for station_id in stations_wave_only
        ]
        station_with_spectra_tasks = [
            download_station_data(ndbc_main_url, station_id, session, True)
            for station_id in stations_with_spectra
        ]
        all_tasks = station_wave_only_tasks + station_with_spectra_tasks
        results = await asyncio.gather(*all_tasks, return_exceptions=True)
        station_exceptions = [
            result for result in results if isinstance(result, BaseException)
        ]
        # Sometimes stations will go down and the ndbc list isn't
        # updated right away, so this will catch those ids
        for station_exception in station_exceptions:
            logger.error(
                'The following exception occurred while downloading: '
                f'{station_exception}.'
            )
        all_station_data = [
            result
            for result in results
            if not isinstance(result, BaseException)
        ]

    return {
        station_id: data
        for station_data in all_station_data
        for station_id, data in station_data.items()
    }


def _get_spectra_data_from_file(spectral_text: str) -> Any:
    """
    Get spectra from file and create a pandas dataframe.

    Args:
        spectral_text: Spectra file as text to create the dataframe.

    Returns:
        Pandas dataframe for the spectra data.
    """
    spectra_dataframe = pd.read_csv(
        io.StringIO(spectral_text),
        header=[1],
        delim_whitespace=True,
        na_values=['MM', 999.0, 9.999],
    )
    filtered_spectra_dataframe = spectra_dataframe[
        ['freq', 'bandwidth', 'energy', 'r1', 'r2', 'alpha1', 'alpha2']
    ]
    timestamps_dataframe = pd.read_csv(
        io.StringIO(spectral_text),
        delim_whitespace=True,
        skiprows=[1],
        usecols=['YYYY', 'MM', 'DD', 'hh', 'mm'],
    )
    timestamps_dataframe = timestamps_dataframe.rename(
        columns={
            'YYYY': 'year',
            'MM': 'month',
            'DD': 'day',
            'hh': 'hour',
            'mm': 'minute',
        }
    )
    timestamps = pd.to_datetime(
        timestamps_dataframe[
            timestamps_dataframe['year']
            > 2000  # This indicates a timestamp row.
        ]
    )
    all_indices = timestamps.index
    spectra_and_timestamps = [
        {
            'timestamp': row,
            'data': filtered_spectra_dataframe[
                all_indices[index] + 1 :
            ].dropna(),
        }
        if index == len(all_indices) - 1
        else {
            'timestamp': row,
            'data': filtered_spectra_dataframe[
                all_indices[index] + 1 : all_indices[index + 1]
            ].dropna(),
        }
        for index, (_, row) in enumerate(timestamps.iteritems())
    ]
    return pd.DataFrame(spectra_and_timestamps)


async def download_station_data(
    url: str, station_id: str, session: aiohttp.ClientSession, spectra: bool
) -> Dict[str, Dict[str, Any]]:
    """
    Downloads an individual station's wave data, and converts the data from
    text to a dataframe with the columns we are interested in.

    Args:
        station_id: id of the NDBC station to download data for
        session: the aiohttp ClientSession object
        spectra: Bool indicating if the station has spectra data.

    Returns:
        Dictionary of station data and spectra data if the spectra data exists.
    """
    with async_timeout.timeout(100):
        async with session.get(
            f'{url}{station_id}_5day.txt', raise_for_status=True
        ) as response:
            data = await response.content.read()
            text = data.decode('utf8')
            await response.release()
        if spectra:
            async with session.get(
                f'{url}{station_id}_5day.spectral', raise_for_status=True
            ) as response:
                data = await response.content.read()
                spectral_text = data.decode('utf8')
                await response.release()

    station_dataframe = pd.read_csv(
        io.StringIO(text),
        delim_whitespace=True,
        skiprows=[1],
        usecols=[
            '#YY',
            'MM',
            'DD',
            'hh',
            'mm',
            'WDIR',
            'WSPD',
            'GST',
            'WVHT',
            'DPD',
            'APD',
            'MWD',
            'PRES',
            'ATMP',
            'WTMP',
            'DEWP',
        ],
        na_values=['MM'],
        parse_dates={'timestamp': ['#YY', 'MM', 'DD', 'hh', 'mm']},
        date_parser=lambda year, month, day, hour, minute: datetime.strptime(
            year + month + day + hour + minute, '%Y%m%d%H%M'
        ),
    )
    if spectra:
        return {
            station_id: {
                'wave_data': station_dataframe,
                'spectra': _get_spectra_data_from_file(spectral_text),
            }
        }
    return {station_id: {'wave_data': station_dataframe}}
