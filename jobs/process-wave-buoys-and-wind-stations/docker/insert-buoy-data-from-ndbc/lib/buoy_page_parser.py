import asyncio
import io
import re
from contextlib import AsyncExitStack
from typing import Any, Set, Tuple

import aiohttp
import pandas as pd  # type: ignore
from bs4 import BeautifulSoup  # type: ignore

# Matches:
# 28.400 N 80.534 W
# 4.000 S 67.000 E
# Does not match:
# 28.4 N 107.534 W
# 57.000 Y 2.000 L
LOCATION_REGEX = re.compile(
    r'^(\d{1,3}.\d{3})\s([N,S])\s(\d{1,3}.\d{3})\s([W,E])'
)


async def scrape_ndbc_ids(
    ndbc_main_url: str, ndbc_text_page: str
) -> Tuple[Set[str], Set[str], Any]:
    """
    Scrapes NDBC IDs from NDBC website.

    Returns:
        Tuple of ndbc ids with wave data only, ndbc ids with spectra, and a
        dataframe of station info.
    """
    async with AsyncExitStack() as stack:
        session = await stack.enter_async_context(aiohttp.ClientSession())
        responses = await asyncio.gather(
            *[
                stack.enter_async_context(session.get(url))
                for url in [ndbc_text_page, ndbc_main_url]
            ]
        )
        [ndbc_text, ndbc_page] = await asyncio.gather(
            *[response.text() for response in responses]
        )

    ndbc_ids_wave_only, ndbc_ids_with_spectra = parse_ndbc_ids(ndbc_page)
    station_info_df = get_station_info(
        ndbc_text, ndbc_ids_wave_only.union(ndbc_ids_with_spectra)
    )

    return ndbc_ids_wave_only, ndbc_ids_with_spectra, station_info_df


def parse_ndbc_ids(ndbc_page: str) -> Tuple[Set[str], Set[str]]:
    """
    Parses all NDBC IDs from the 5day2 data page on NDBC and information
    specifying if a station has spectra data.

    Args:
        ndbc_page: NDBC HTML page.

    Returns:
        Dictionary of NDBC IDs with information specifying if the station has
        spectra data.
    """
    ndbc_soup = BeautifulSoup(ndbc_page, 'lxml')
    a_elements = ndbc_soup.find_all('a')

    all_station_ids = {
        anchor.text.split('_')[0]
        for anchor in a_elements
        if '_5day.txt' in anchor.text
    }

    station_ids_with_spectra = {
        anchor.text.split('_')[0]
        for anchor in a_elements
        if '_5day.spectral' in anchor.text
    }

    return (
        (all_station_ids - station_ids_with_spectra),
        station_ids_with_spectra,
    )


def _calculate_lat_lon(location_text: str) -> Tuple[float, float]:
    """
    Calculate the latitude and longitude based on cardinal directions.
    """
    result = LOCATION_REGEX.match(location_text)
    if not result:
        raise ValueError('Invalid format for location')

    if result.group(2) == 'N':
        latitude = float(result.group(1))
    elif result.group(2) == 'S':
        latitude = -float(result.group(1))
    else:
        raise ValueError(
            'Invalid direction for latitude. Must be one of: [N, S]'
        )

    if result.group(4) == 'E':
        longitude = float(result.group(3))
    elif result.group(4) == 'W':
        longitude = -float(result.group(3))
    else:
        raise ValueError(
            'Invalid direction for latitude. Must be one of: [W, E]'
        )

    return latitude, longitude


def get_station_info(station_table: str, ndbc_ids: Set[str]) -> Any:
    """
    Gets name, latitude and longitude information for each station.
    Args:
        station_table: String representation of station table scraped from the
                       NDBC site.
        ndbc_ids: Set of NDBCs IDs to search for in the text.
    Returns:
        Pandas dataframe of the name and latitude and longitude values for each
        station.
    """
    station_info_dataframe = pd.read_csv(
        io.StringIO(station_table), sep='|', skiprows=[1]
    )
    station_info_dataframe = station_info_dataframe.rename(
        columns={
            '# STATION_ID ': 'station_id',
            ' NAME ': 'name',
            ' LOCATION ': 'location',
        }
    )
    station_info_dataframe['station_id'] = station_info_dataframe[
        'station_id'
    ].apply(lambda id: str(id).upper())
    station_info_dataframe = station_info_dataframe[
        station_info_dataframe['station_id'].isin(ndbc_ids)
    ]

    station_info_dataframe['location'] = station_info_dataframe[
        'location'
    ].apply(lambda location: _calculate_lat_lon(location))

    return station_info_dataframe
