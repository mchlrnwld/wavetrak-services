<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /data/5day2</title>
 </head>
 <body>
<h1>Index of /data/5day2</h1>
  <table>
   <tr><th valign="top"><img src="/icons/blank.gif" alt="[ICO]"></th><th><a href="?C=N;O=D">Name</a></th><th><a href="?C=M;O=A">Last modified</a></th><th><a href="?C=S;O=A">Size</a></th><th><a href="?C=D;O=A">Description</a></th></tr>
   <tr><th colspan="5"><hr></th></tr>
<tr><td valign="top"><img src="/icons/back.gif" alt="[PARENTDIR]"></td><td><a href="/data/">Parent Directory</a>       </td><td>&nbsp;</td><td align="right">  - </td><td>&nbsp;</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="32ST0_5day.ocean">32ST0_5day.ocean</a>       </td><td align="right">2021-01-25 17:28  </td><td align="right">9.5K</td><td>Oceanographic Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="32ST0_5day.srad">32ST0_5day.srad</a>        </td><td align="right">2021-01-25 17:28  </td><td align="right">4.5K</td><td>Solar Radiation Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="32ST0_5day.txt">32ST0_5day.txt</a>         </td><td align="right">2021-01-25 17:28  </td><td align="right"> 11K</td><td>Standard Meteorological Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="41NT0_5day.ocean">41NT0_5day.ocean</a>       </td><td align="right">2021-01-25 17:28  </td><td align="right">9.4K</td><td>Oceanographic Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="41NT0_5day.srad">41NT0_5day.srad</a>        </td><td align="right">2021-01-25 17:28  </td><td align="right">4.4K</td><td>Solar Radiation Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="41NT0_5day.txt">41NT0_5day.txt</a>         </td><td align="right">2021-01-25 17:28  </td><td align="right"> 11K</td><td>Standard Meteorological Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="46T29_5day.spec">46T29_5day.spec</a>        </td><td align="right">2021-01-25 17:28  </td><td align="right">8.3K</td><td>Spectral Wave Summary Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="46T29_5day.spectral">46T29_5day.spectral</a>    </td><td align="right">2021-01-25 17:28  </td><td align="right">222K</td><td>Combined Spectral Wave Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="46T29_5day.supl">46T29_5day.supl</a>        </td><td align="right">2021-01-25 17:28  </td><td align="right"> 34K</td><td>Supplemental Measurements Data</td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="46T29_5day.txt">46T29_5day.txt</a>         </td><td align="right">2021-01-25 17:28  </td><td align="right"> 66K</td><td>Standard Meteorological Data</td></tr>
</table>
<script id="_fed_an_ua_tag" type="text/javascript" src="/js/federated-analytics.js?agency=DOC&amp;subagency=NOAA&amp;pua=UA-33523145-1"></script>
</body></html>
