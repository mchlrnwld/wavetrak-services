import asyncio
import logging
import os
from datetime import datetime
from tempfile import TemporaryDirectory
from timeit import default_timer

import pandas as pd  # type: ignore

import lib.config as config
from lib.buoy_page_parser import scrape_ndbc_ids
from lib.psql import PSQL
from lib.station_data import get_all_station_data, pull_ndbc_station_data

logger = logging.getLogger('insert-buoy-data-from-ndbc')

STATION_DATA_CSV_FILE_NAME = 'station_data.csv'


async def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    start = default_timer()

    (
        station_ids_wave_only,
        station_ids_with_spectra,
        station_info_df,
    ) = await scrape_ndbc_ids(config.NDBC_MAIN_URL, config.NDBC_TEXT_URL)

    logger.info(
        f'Scraped {len(station_ids_wave_only.union(station_ids_with_spectra))}'
        f' station IDs.'
    )

    all_station_data = await pull_ndbc_station_data(
        station_ids_wave_only, station_ids_with_spectra, config.NDBC_MAIN_URL
    )

    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir, PSQL(
        config.SCIENCE_PLATFORM_PSQL_JOB
    ) as psql:
        existing_stations = psql.get_all_stations(config.SOURCE)
        all_station_data_hours = get_all_station_data(
            all_station_data, existing_stations, datetime.utcnow()
        )

        if all_station_data_hours:
            all_stations_with_new_data = {
                station.source_id: station.station_id
                for station in all_station_data_hours
            }

            all_stations_latest_timestamps = {
                source_id: (
                    sorted(
                        [
                            station.timestamp
                            for station in all_station_data_hours
                            if station.source_id == source_id
                        ],
                        reverse=True,
                    )[0],
                    id,
                    station_info_df[
                        station_info_df['station_id'] == source_id
                    ],
                )
                for source_id, id in all_stations_with_new_data.items()
            }
            results = await asyncio.gather(
                *[
                    psql.add_and_update_station(
                        id,
                        config.SOURCE,
                        station,
                        info['name'].values[0],
                        info['location'].values[0][0],
                        info['location'].values[0][1],
                        latest_timestamp,
                    )
                    for station, (
                        latest_timestamp,
                        id,
                        info,
                    ) in all_stations_latest_timestamps.items()
                ],
                return_exceptions=True,
            )

            station_exceptions = [
                station
                for station, result in zip(
                    all_stations_latest_timestamps.keys(), results
                )
                if isinstance(result, BaseException)
            ]

            for station in station_exceptions:
                logger.error(f'Unable to add or update station: {station}')

            updated_stations = set(all_stations_with_new_data) - set(
                station_exceptions
            )

            for station in updated_stations:
                logger.info(f'Updated station: {station}')

            station_data_csv_file_path = os.path.join(
                temp_dir, STATION_DATA_CSV_FILE_NAME
            )

            all_station_df = pd.DataFrame(
                [
                    station_data.asdict()
                    for station_data in all_station_data_hours
                    if station_data.source_id not in station_exceptions
                ]
            )
            all_station_df.to_csv(
                station_data_csv_file_path,
                compression=None,
                index=False,
                columns=[
                    column
                    for column in all_station_data_hours[0].asdict().keys()
                    if column != 'source_id'
                ],
            )
            logger.info('Writing station data to csv...')

            psql.copy_csv(station_data_csv_file_path, 'station_data')
            if config.COMMIT:
                logger.info('Committing data to Postgres...')
                psql.commit()
            else:
                logger.info('Rolling back changes to Postgres...')
                psql.rollback()

        logger.info(f'Elapsed: {default_timer() - start}')


if __name__ == '__main__':
    asyncio.run(main())
