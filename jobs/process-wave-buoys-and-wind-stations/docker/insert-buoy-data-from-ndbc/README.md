# Insert Buoy Data From NDBC

Docker image to insert NDBC buoy data into Postgres.

## Setup

```sh
$ conda devenv
$ source activate process-wave-buoys-and-wind-stations-insert-buoy-data-from-ndbc
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-wave-buoys-and-wind-stations/insert-buoy-data-from-ndbc .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env process-wave-buoys-and-wind-stations/insert-buoy-data-from-ndbc
```
