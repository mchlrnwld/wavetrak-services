#!/usr/bin/env bash

./wait-for-it.sh database:5432

source activate process-wave-buoys-and-wind-stations-insert-buoy-data-from-ndbc && pytest -s --cov-report term-missing --cov=main test_main.py
