# Turn Buoys Offline

Docker image to turn off buoys that haven't reported data for more than 48 hours.

## Setup

```sh
$ conda devenv
$ source activate process-wave-buoys-and-wind-stations-turn-buoys-offline
$ cp .env.sample .env
$ env $(xargs < .env) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t process-wave-buoys-and-wind-stations/turn-buoys-offline .
$ cp .env.sample .env
$ docker run --rm -it --env-file=.env process-wave-buoys-and-wind-stations/turn-buoys-offline
```
