import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
SCIENCE_PLATFORM_PSQL_JOB = get_secret(
    SECRETS_PREFIX, 'SCIENCE_PLATFORM_PSQL_JOB'
)
