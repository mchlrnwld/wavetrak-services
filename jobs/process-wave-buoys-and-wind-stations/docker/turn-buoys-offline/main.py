import logging
from contextlib import ExitStack

import psycopg2  # type: ignore

import lib.config as config

logger = logging.getLogger('turn-buoys-offline')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    logger.info('Turning outdated buoys offline...')
    with ExitStack() as stack:
        conn = stack.enter_context(
            psycopg2.connect(
                config.SCIENCE_PLATFORM_PSQL_JOB,
            )
        )
        cur = stack.enter_context(conn.cursor())
        cur.execute(
            "SELECT id FROM stations "
            "WHERE latest_timestamp < NOW() - INTERVAL '48 hours';"
        )
        outdated_buoys = [id for (id,) in cur.fetchall()]

        if not outdated_buoys:
            logger.info('No outdated buoys found.')
        else:
            logger.info(f'Outdated buoys: {outdated_buoys}')
            cur.execute(
                "UPDATE stations SET status = 'OFFLINE' WHERE id IN %s;",
                (tuple(outdated_buoys),),
            )

    logger.info('Finished turning outdated buoys offline.')


if __name__ == '__main__':
    main()
