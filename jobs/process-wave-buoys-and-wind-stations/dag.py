from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from dag_helpers.docker import docker_image
from dag_helpers.pager_duty import create_pager_duty_failure_callback

JOB_NAME = 'process-wave-buoys-and-wind-stations'
NDBC_SOURCE = 'NDBC'

AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')
ENV = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
COMMIT = True

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, ENV
)

insert_buoy_data_from_ndbc_environment = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'JOB_NAME': 'wt-{0}-{1}-{2}'.format(JOB_NAME, NDBC_SOURCE.lower(), ENV),
    'JOB_DEFINITION': 'wt-{0}-{1}-{2}'.format(
        JOB_NAME, NDBC_SOURCE.lower(), ENV
    ),
    'JOB_QUEUE': 'wt-jobs-common-high-cpu-local-ssd-job-queue-{0}'.format(ENV),
    'TIMEOUT': str(30 * 60),
    'ENVIRONMENT': ';'.join(
        [
            'DATA_DIR=/mnt/ephemeral/airflow',
            'ENV={0}'.format(ENV),
            'JOB_NAME={0}'.format(JOB_NAME),
            'AWS_DEFAULT_REGION={0}'.format(AWS_DEFAULT_REGION),
            'SOURCE={0}'.format(NDBC_SOURCE),
            'COMMIT={0}'.format(COMMIT),
        ]
    ),
}

turn_buoys_offline_environment = {
    'ENV': ENV,
}

# DAG defintion
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2021, 1, 27),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
    'on_failure_callback': pager_duty_failure_callback,
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=5),
    concurrency=1,
    max_active_runs=1,
    default_args=default_args,
)

insert_buoy_data_from_ndbc = WTDockerOperator(
    task_id='insert-buoy-data-from-ndbc',
    image=docker_image('execute-batch-job'),
    environment=insert_buoy_data_from_ndbc_environment,
    force_pull=True,
    dag=dag,
)
turn_buoys_offline = WTDockerOperator(
    task_id='turn-buoys-offline',
    image=docker_image('turn-buoys-offline', JOB_NAME),
    environment=turn_buoys_offline_environment,
    force_pull=True,
    dag=dag,
)
turn_buoys_offline.set_upstream(insert_buoy_data_from_ndbc)
