import logging
from contextlib import ExitStack
from uuid import uuid4

import lib.config as config
from lib.ec2 import TempInstance, TempKeyPair
from lib.ssh import SSH

log = logging.getLogger('run-lotus-ww3-model')
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler())


def main():
    key_name = f'{config.JOB_NAME}-{uuid4()}'

    tags = [
        {'Key': 'DynamicallyGenerated', 'Value': 'true'},
        {'Key': 'Run', 'Value': config.RUN},
    ]

    with ExitStack() as stack:
        private_key = stack.enter_context(TempKeyPair(key_name))

        slave_instances = [
            stack.enter_context(
                TempInstance(
                    f'{config.JOB_NAME}-{config.RUN}-slave-{i}',
                    [config.SLAVE_LAUNCH_TEMPLATE_NAME],
                    key_name,
                    config.LOTUS_SUBNET_ID,
                    tags,
                )
            )
            for i in range(4)
        ]

        with ExitStack() as slave_init_stack:
            for s in slave_instances:
                slave_ssh = slave_init_stack.enter_context(
                    SSH(s.private_ip_address, 'ubuntu', private_key)
                )
                slave_ssh.sudo(
                    f'mount -t efs {config.FILE_SYSTEM_ID}:/ /export'
                )
                slave_ssh.run(
                    f'aws s3 sync --quiet {config.MODEL_SOURCE_S3_URI}'
                    f' ~/dl/lotus-source'
                )

        master_instance = stack.enter_context(
            TempInstance(
                f'{config.JOB_NAME}-{config.RUN}-master',
                [config.MASTER_LAUNCH_TEMPLATE_NAME],
                key_name,
                config.LOTUS_SUBNET_ID,
                tags,
            )
        )
        master_ssh = stack.enter_context(
            SSH(master_instance.private_ip_address, 'ubuntu', private_key)
        )

        master_ssh.sudo(f'mount -t efs {config.FILE_SYSTEM_ID}:/ /export')
        master_ssh.run(
            f'aws s3 sync --quiet {config.MODEL_SOURCE_S3_URI}'
            ' /export-local/LOTUS'
        )

        master_ssh.sudo(
            f'/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl'
            f' -a fetch-config -m ec2 -s'
            f' -c ssm:{config.CLOUDWATCH_AGENT_CONFIG_PARAMETER_NAME}'
        )

        master_ssh.run(f'echo "{private_key}" > ~/.ssh/id_rsa')
        master_ssh.run('chmod 400 ~/.ssh/id_rsa')
        master_ssh.run(f'echo RUN={config.RUN} > .env')
        for i, s in enumerate(slave_instances):
            master_ssh.run(
                f'ssh-keyscan -H {s.private_ip_address} >> ~/.ssh/known_hosts'
            )
            master_ssh.run(f'echo SLAVE_{i}_IP={s.private_ip_address} >> .env')
            master_ssh.run(
                f'echo SLAVE_{i}_INSTANCE_ID={s.instance_id} >> .env'
            )

        master_ssh.run(
            'env $(xargs < .env) python3 /export-local/LOTUS/run_model.py'
        )


if __name__ == '__main__':
    main()
