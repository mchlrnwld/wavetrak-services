# Run Lotus WW3 Model

Docker image to run the Lotus WW3 model.

## Setup

```sh
conda devenv
source activate run-lotus-ww3-model-run-lotus-ww3-model
cp .env.sample .env
env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
docker build -t run-lotus-ww3-model/run-lotus-ww3-model .
cp .env.sample .env
docker run --env-file=.env --rm run-lotus-ww3-model/run-lotus-ww3-model
```
