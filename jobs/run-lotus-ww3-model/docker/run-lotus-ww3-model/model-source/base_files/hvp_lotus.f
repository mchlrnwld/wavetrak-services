c
c  program to read ww3 spectral data file and output height v period band info
c

        dimension ds(29,36)
        dimension ds2(29,36),f(29),bw(29),rdir(36)
        dimension a0(29),a1(29),b1(29),a2(29),b2(29)
        dimension f2(25),bw2(25),e2(25)
        dimension hpd(15)
        character str1*30,str2*14,ifile*17,ofile*21,str3*72

c------------------------------------------------------
c... begin reading data from wavewatch file
c
c  The spectra are in tri coordinates j=1 is trig. 0 =
c  waves headed towards east, and progresses cclw. The
c  array rdir contains the true compass head from which
c  the waves are arriving
c------------------------------------------------------

c-----------------------------------------
c... calculate frequency bandwidths
c-----------------------------------------

        nf2=25
        open(10,file='hvp_lotus_ww3_freq_bands.dat')
        read(10,*) (f2(j),j=1,nf2)
        close(10)
        xfr=1.1
        do j=1,nf2
          bw2(j)=0.5*(xfr-1./xfr)*f2(j)
        end do
        close(10)

        read(*,*) str1,nf,nd,nloc
        read(*,*) (f(j),j=1,nf)
        read(*,*) (rdir(j),j=1,nd)

c       open(11,file='ncep.bands')
c-----------------------------------------
c  xfr is the ww3 frequency increment setting for the log
c  based freq band spacing. LOLA and MSW use 1.1 where
c  NCEP uses 1.07
        xfr=1.1
        do j=1,nf
           bw(j)=0.5*(xfr-1./xfr)*f(j)
c         write(*,*) f(j),bw(j)
         end do
c       close(11)

c----------------------------------------------
c... beginning of loop through forecast hours
c----------------------------------------------

1       continue

        do ii=1,15
        hpd(ii)=0
        end do

c-----------------------------------------
c... sort out date/time strings
c-----------------------------------------

        read(*,*,err=99,end=99) iymd,ihms

        iy=iymd/10000.
        im=mod(iymd,10000)/100
        id=iymd-iy*10000-im*100
        ih=ihms/10000.

c----------------------------------------------------
c... loop through the number of locations (l now = 1)
c----------------------------------------------------

        read(*,'(a)',err=99,end=99) str3
        read(str3,1001,err=99,end=99) rlat,rlon,depth,wspd,wdir,curs,
     &                                curd
1001    format(14x,f5.2,f7.2,f11.1,f7.2,f6.1,f7.2,f6.1)
        read(*,*) ((ds(i,j),i=1,nf),j=1,nd)

        dbw=360.0/nd
c.. calc freq spectrum m^2/hz
        do i=1,nf
         a0(i)=0
        do j=1,nd
         a0(i)=a0(i)+ds(i,j)*dbw*0.017453
        end do
        end do

c.. reband frequency spectrum into lola ww3 bands
        nbands=29
        call reband(nbands,nf,f,bw,a0,nf2,f2,bw2,e2)

        et=0
        do i=1,25

         if(i.lt.16) then
          j=i
         else
          j=15
         endif

         hpd(j)=hpd(j)+e2(i)*bw2(i)
         et=et+e2(i)*bw2(i)

        end do

c
c        hs=3.28*4*sqrt(et)
c  output in meters
        hs=4*sqrt(et)
c        write(*,'(16x,15f6.1,a6)') ((1/f2(k)),k=15,1,-1),'  Hst '
c        write(*,2000) iy,im,id,ih,0,(3.28*4*sqrt(hpd(k)),k=15,1,-1),hs
        write(*,2000) iy,im,id,ih,0,(4*sqrt(hpd(k)),k=15,1,-1),hs
2000    format(i4,4i3,16f6.2)

        go to 1
99      continue
c        stop
        end
c----------------------------------------------------------------------

        subroutine reband(nbands,nf1,f1,bw1,e1,nf2,f2,bw2,e2)
c
c... subroutine to reband frequency spectrum e1 into spectrum e2
c       (energy densities) with corresponding freqs f1,f2 and bandwidths
c        b1,b2.
c
      real f1(nbands),bw1(nbands),e1(nbands)
      real f2(nf2),bw2(nf2),e2(nf2)
c

c... 4 types of old/new band overlap
c
c 1. new band falls completely within and old band
c 2. new band overlaps low f side of old band
c 3. new band overlaps high f side of old band
c 4. old band falls completely within new band

c...loop through new e2 freq bands
        do 100 i=1,nf2

        e2b=0

c...calculate band edges
        f2s=f2(i)-0.5*bw2(i)
        f2e=f2(i)+0.5*bw2(i)

c... loop through old bands
        do 10 j=1,nf1

c... old band energy density
        e1bd=e1(j)

c... old band edges
        f1s=f1(j)-0.5*bw1(j)
        f1e=f1(j)+0.5*bw1(j)
        if(i.eq.6) then
        endif
c ... skip if no overlap
        if(f2s.gt.f1e.or.f2e.lt.f1s) go to 10

c... figure out type and add to total energy in new band

        if(f2s.ge.f1s) then
          if(f2e.le.f1e) then
c.......... itype=1
            e2b=e2b+e1bd*(f2e-f2s)
            go to 10
          else
c.......... itype=3
            e2b=e2b+e1bd*(f1e-f2s)
            go to 10
          endif
       else
         if(f2e.le.f1e) then
c......... itype=2
            e2b=e2b+e1bd*(f2e-f1s)
           go to 10
         else
c......... itype=4
            e2b=e2b+e1bd*(f1e-f1s)
           go to 10
         endif
       endif

c... end old band loop
10      continue

c... convert to energy density

        e2(i)=e2b/bw2(i)

100     continue
        return
        end
