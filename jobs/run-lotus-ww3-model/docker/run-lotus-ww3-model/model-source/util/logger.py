import logging
import sys

Log_Format = "%(levelname)s %(asctime)s - %(message)s"

logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
    datefmt='%H:%M:%S',
    handlers=[
        logging.FileHandler("/export-local/LOTUS/model_run.log"),
        logging.StreamHandler(),
    ],
)

log = logging.getLogger()


def handler(type, value, tb):
    logging.error("Uncaught exception", exc_info=(type, value, tb))


sys.excepthook = handler
