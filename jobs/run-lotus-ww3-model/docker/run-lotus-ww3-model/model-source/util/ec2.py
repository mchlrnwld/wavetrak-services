import boto3  # type: ignore
from botocore.exceptions import ClientError  # type: ignore
from util.logger import log

session = boto3.Session()
ec2_client = session.client('ec2', region_name='us-west-1')


class LotusInstance:
    def __init__(self, instance_id, private_ip_address):
        self.instance_id = instance_id
        self.private_ip_address = private_ip_address


def terminate_instances_by_id(instance_ids: [str]) -> None:
    log.info(f'terminate_instances_by_id: {instance_ids}')
    response = ec2_client.terminate_instances(InstanceIds=instance_ids)
    log.info(response)
