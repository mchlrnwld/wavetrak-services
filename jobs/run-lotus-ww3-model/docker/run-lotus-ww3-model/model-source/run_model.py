import datetime
import glob
import os
import shutil
import time
from pathlib import Path

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type: ignore
from python_aux_scripts import (
    get_input_files,
    pos_process_boundary,
    pos_process_mechanism,
    pre_process_mechanism,
)
from util.ec2 import terminate_instances_by_id
from util.logger import log

"""

Main script to launch the LOTUS runs.

:parameters:
:return:

Francisco
Mar2019
"""


clean_dirs = 'Yes'
assimilation_mechanism = 'Yes'

# Model paths
# the pos-process tasks are relocated to a local folder on the master,
# so that we can have better writting speeds than at the shared folder
export_shared_dir = '/export/LOTUS'
export_local_dir = '/export-local/LOTUS'
model_source = '/models_source/wwatch_v_607/model/exe'

# Slave configuration

nb_procs_per_slave = 34

# AWS configuration
log.info('Creating AWS session')
session = boto3.Session()
s3 = session.resource('s3')

# Main datetimes used during the run

# If a manual run is planned, there should be create a file (named: "runtime")
# and placed at the s3://msw-lotus/lotus-setup that will be used as the model run time.
# That file should be only one line with the date in a YYYYMMDDHH format,
# with the intended run date time. It will be automatically imported to the $HOME dir at master.
runtime_file = Path(str(Path.home()) + '/dl/runtime')
if runtime_file.is_file():
    log.info('Runtime file provided')
    with open(runtime_file, 'r') as f:
        current_run_date_string = f.read()
        current_run_date = datetime.datetime.strptime(
            current_run_date_string, '%Y%m%d%H'
        )
elif os.environ.get('RUN') != None:
    log.info('Setting model run time from environment variable')
    current_run_date = datetime.datetime.strptime(
        os.environ.get('RUN'), '%Y%m%d%H'
    )
else:
    # The model run time is selected by choosing the nearest run time possibility, minus 6 hours.
    log.info('Determining model run time')
    current_run_date_string = time.strftime('%Y%m%d')
    current_run_date = datetime.datetime.strptime(
        current_run_date_string, '%Y%m%d'
    )
    date_list = [
        current_run_date + datetime.timedelta(hours=6 * x) for x in range(0, 4)
    ]
    alpha = min(date_list, key=lambda x: abs(x - datetime.datetime.now()))
    current_run_date = alpha - datetime.timedelta(hours=6)

log.info(f'Model run time: {current_run_date}')

# Model configurations

model_grids_global_list = ['GLOB_30m', 'GLOB_15m']
model_grids_nested_list = [
    'US_E_3m',
    'UK_3m',
    'CAL_3m',
    'HW_3m',
    'AUS_E_3m',
    'FR_3m',
    'BALI_3m',
    'PT_3m',
    'AUS_SW_3m',
    'AUS_VT_3m',
    'GLAKES_6m',
]
input_files_global_list = ['w_gfs_25', 'w_gfs_50', 'ice_gfs_25', 'ice_gfs_50']
input_files_nested_list = ['w_gfs_25', 'ice_gfs_25']

FIELDS_OUT = 'WND HS FP DP SPR PHS PTP PDIR PSPR PWS'

dt_field = '3600'  # time step for field output
dt_point = '3600'  # time step for point output
dt_restart = '21600'  # time step for restart files
model_length = '16'  # model length in days
restart_output_length = '2'  # restart period length in days
restart_output_length_nested = '2.5'  # restart period length in days for nested grids accounting for the hindcast section

# Clean old folders and create new ones


if clean_dirs in ['y', 'Y', 'yes', 'Yes', 'YES']:
    log.info('Cleaning old folders')
    if os.path.exists(export_shared_dir + '/work'):
        shutil.rmtree(export_shared_dir + '/work')
    if os.path.exists(export_shared_dir + '/model_data'):
        shutil.rmtree(export_shared_dir + '/model_data')
    if os.path.exists(export_local_dir + '/archive'):
        shutil.rmtree(export_local_dir + '/archive')
    if os.path.exists(export_local_dir + '/pos_process'):
        shutil.rmtree(export_local_dir + '/pos_process')
    if os.path.exists(export_local_dir + '/pre_process'):
        shutil.rmtree(export_local_dir + '/pre_process')
    if os.path.exists(export_local_dir + '/log'):
        shutil.rmtree(export_local_dir + '/log')
    if os.path.exists(export_local_dir + '/restarts'):
        shutil.rmtree(export_local_dir + '/restarts')
    if os.path.exists(export_local_dir + '/boundary'):
        shutil.rmtree(export_local_dir + '/boundary')
    if os.path.exists(export_local_dir + '/input_data'):
        shutil.rmtree(export_local_dir + '/input_data')
    if os.path.exists(export_local_dir + '/pre_process_basefiles'):
        shutil.rmtree(export_local_dir + '/pre_process_basefiles')
    if os.path.exists(export_local_dir + '/data_assimilation'):
        shutil.rmtree(export_local_dir + '/data_assimilation')


# Create folders
log.info('Creating folders')
os.system('sudo chmod 777 -R ' + export_shared_dir)
os.makedirs(export_shared_dir + '/work')
os.makedirs(export_shared_dir + '/model_data')

# Create Folders for pos-processing and pre-processing (locally to increase writting speeds)
log.info('Creating folders for post-processing and pre-processing')
os.system('sudo chmod 777 -R ' + export_local_dir)
os.makedirs(export_local_dir + '/archive')
os.makedirs(export_local_dir + '/pos_process')
os.makedirs(export_local_dir + '/pre_process')
os.makedirs(export_local_dir + '/log')
os.makedirs(export_local_dir + '/input_data')
os.makedirs(export_local_dir + '/restarts')
os.makedirs(export_local_dir + '/boundary')
os.makedirs(export_local_dir + '/pre_process_basefiles')
os.makedirs(export_local_dir + '/data_assimilation')

# Get input files from GFS
log.info('Getting input data')
flag_missing_files = get_input_files.create_input_files(
    current_run_date, export_local_dir + '/input_data'
)


if len(flag_missing_files) > 0:
    log.info('Failed to download the input files, trying again')
    flag_missing_files = get_input_files.create_input_files(
        current_run_date, export_local_dir + '/input_data'
    )

if len(flag_missing_files) > 0:
    log.info('It was not possible to download the input files')

    # os.system(
    #     'aws s3 cp '
    #     + export_local_dir
    #     + '/model_run.log s3://msw-lotus/archive/logs/model_run_'
    #     + current_run_date.strftime("%Y%m%d%H")
    #     + '.log'
    # )

    # # Terminate the slaves
    # os.system('sh ~/dl/lotus-source/scripts/lotus/destroy-slaves.sh')
    # # Terminate Master
    # os.system('sudo shutdown -h now')


else:
    log.info('All input files are OK, getting restart files from S3')

    # Get restart files from S3 bucket
    if assimilation_mechanism in ['y', 'Y', 'yes', 'Yes', 'YES']:
        date_restart_str = current_run_date - datetime.timedelta(hours=12)
        date_restart = datetime.datetime.strftime(date_restart_str, '%Y%m%d%H')
    else:
        date_restart = datetime.datetime.strftime(current_run_date, '%Y%m%d%H')

    for model_grid in model_grids_global_list:
        try:
            log.info(
                f'Getting restart for global model grid, date: {model_grid}, {date_restart}'
            )
            s3.Bucket('msw-lotus').download_file(
                'restarts/restart.' + model_grid + '.' + date_restart,
                export_local_dir
                + '/restarts/restart.'
                + model_grid
                + '.'
                + date_restart,
            )
        except ClientError as e:
            restart_download_Error = e

    for model_grid in model_grids_nested_list:
        try:
            log.info(
                f'Getting restart for nested model grid, date: {model_grid}, {date_restart}'
            )
            s3.Bucket('msw-lotus').download_file(
                'restarts/restart.' + model_grid + '.' + date_restart,
                export_local_dir
                + '/restarts/restart.'
                + model_grid
                + '.'
                + date_restart,
            )
        except ClientError as e:
            restart_download_Error = e

    log.info('Running pre-process mechanisms for input files')
    for input_file in input_files_global_list:
        pre_process_mechanism.process_input_files(
            input_file, export_local_dir, model_source
        )

    # Run pre-process mechanisms for the model grids
    pre_process_mechanism.process_model_grids(
        'GLOB_30m', export_local_dir, model_source, current_run_date, 'run'
    )
    pre_process_mechanism.process_model_grids(
        'GLOB_15m', export_local_dir, model_source, current_run_date, 'run'
    )
    pre_process_mechanism.process_model_grids(
        'GLOB_15m', export_local_dir, model_source, current_run_date, 'output'
    )

    for model_grid in model_grids_nested_list:
        pre_process_mechanism.process_model_grids(
            model_grid, export_local_dir, model_source, current_run_date, 'run'
        )
        pre_process_mechanism.process_model_grids(
            model_grid,
            export_local_dir,
            model_source,
            current_run_date,
            'output',
        )

    log.info('Creating hosts file')
    slave_instance_ids = [
        os.environ['SLAVE_0_INSTANCE_ID'],
        os.environ['SLAVE_1_INSTANCE_ID'],
        os.environ['SLAVE_2_INSTANCE_ID'],
        os.environ['SLAVE_3_INSTANCE_ID'],
    ]
    slave_private_ip_addresses = [
        os.environ['SLAVE_0_IP'],
        os.environ['SLAVE_1_IP'],
        os.environ['SLAVE_2_IP'],
        os.environ['SLAVE_3_IP'],
    ]
    OutFile = open(export_local_dir + '/pre_process_basefiles/hosts_file', 'w')
    nb_procs = 0
    for ip in slave_private_ip_addresses:
        OutFile.write(ip + ':' + str(nb_procs_per_slave) + '\n')
        nb_procs += nb_procs_per_slave
    OutFile.close()

    OutFile = open(
        export_local_dir + '/pre_process_basefiles/hosts_file_slot_1', 'w'
    )
    OutFile.write(
        slave_private_ip_addresses[0] + ':' + str(nb_procs_per_slave) + '\n'
    )
    OutFile.close()

    OutFile = open(
        export_local_dir + '/pre_process_basefiles/hosts_file_slot_2', 'w'
    )
    OutFile.write(
        slave_private_ip_addresses[1] + ':' + str(nb_procs_per_slave) + '\n'
    )
    OutFile.close()

    OutFile = open(
        export_local_dir + '/pre_process_basefiles/hosts_file_slot_3', 'w'
    )
    OutFile.write(
        slave_private_ip_addresses[2] + ':' + str(nb_procs_per_slave) + '\n'
    )
    OutFile.close()

    OutFile = open(
        export_local_dir + '/pre_process_basefiles/hosts_file_slot_4', 'w'
    )
    OutFile.write(
        slave_private_ip_addresses[3] + ':' + str(nb_procs_per_slave) + '\n'
    )
    OutFile.close()

    if assimilation_mechanism in ['y', 'Y', 'yes', 'Yes', 'YES']:

        assimilation_process_start_str = current_run_date - datetime.timedelta(
            hours=12
        )

        # Initial run just to create a grid output to serve as adjusmtent base for the assimilation
        #

        # Get satellite data
        log.info('Downloading satellite data')
        get_input_files.create_sat_files(
            assimilation_process_start_str,
            export_local_dir + '/input_data',
            export_local_dir + '/data_assimilation',
        )

        pre_process_mechanism.create_model_run_input_file_assimilation(
            assimilation_process_start_str,
            FIELDS_OUT,
            dt_field,
            dt_point,
            0,
            export_local_dir,
            export_shared_dir,
        )
        for model_grid in model_grids_global_list:
            pre_process_mechanism.pre_process_model_restarts(
                model_grid,
                export_local_dir,
                export_shared_dir,
                'work',
                model_source,
                assimilation_process_start_str,
            )
            shutil.copy2(
                export_local_dir
                + '/pre_process_basefiles/mod_def.'
                + model_grid,
                export_shared_dir + '/work/mod_def.' + model_grid,
            )

        for input_file in input_files_global_list:
            for file in glob.glob(
                export_local_dir + '/pre_process_basefiles/*.' + input_file
            ):
                shutil.copy2(file, export_shared_dir + '/work/.')

        # Execute the model
        shutil.copy2(
            export_local_dir + '/pre_process_basefiles/hosts_file',
            export_shared_dir + '/work/.',
        )
        shutil.copy2(
            model_source + '/ww3_multi', export_shared_dir + '/work/ww3_multi'
        )
        os.chdir(export_shared_dir + '/work')
        os.system(
            'mpiexec -f hosts_file -np '
            + str(nb_procs)
            + ' '
            + export_shared_dir
            + '/work/ww3_multi'
        )

        # Copy Output files for pos-processing
        for model_grid in model_grids_global_list:
            shutil.copy2(
                export_shared_dir + '/work/out_grd.' + model_grid,
                export_shared_dir + '/model_data/.',
            )
            shutil.copy2(
                export_shared_dir + '/work/out_pnt.' + model_grid,
                export_shared_dir + '/model_data/.',
            )

        pos_process_mechanism.create_grid_output_files_hindcast(
            'GLOB_30m',
            'GLOB_30m',
            export_local_dir,
            export_shared_dir,
            current_run_date,
            assimilation_process_start_str,
            FIELDS_OUT,
            dt_field,
            0,
            model_source,
        )
        pos_process_mechanism.read_sigH_nc_file(
            export_local_dir,
            export_local_dir
            + '/archive/grids/GLOB_30m/'
            + current_run_date.strftime("%Y%m%d%H")
            + '/hindcast/ww3_lotus_GLOB_30m.'
            + assimilation_process_start_str.strftime("%Y%m%d%H")
            + '.nc',
        )

        shutil.copy2(
            export_local_dir
            + '/restarts/restart.GLOB_30m.'
            + assimilation_process_start_str.strftime("%Y%m%d%H"),
            export_local_dir + '/data_assimilation/restart.ww3',
        )
        shutil.copy2(
            export_local_dir + '/base_files/GLOB_30m.mask',
            export_local_dir + '/data_assimilation/GLOB_30m.mask_assimilation',
        )
        shutil.copy2(
            export_local_dir + '/base_files/run_assimilation_GLOB_30m.octave',
            export_local_dir
            + '/data_assimilation/run_assimilation_GLOB_30m.octave',
        )
        os.system(
            'chmod +rx '
            + export_local_dir
            + '/data_assimilation/run_assimilation_GLOB_30m.octave'
        )
        os.system(
            export_local_dir
            + '/data_assimilation/run_assimilation_GLOB_30m.octave '
            + assimilation_process_start_str.strftime('%Y%m%d%H')
            + ' '
            + export_local_dir
            + '/data_assimilation'
            + ' '
            + export_local_dir
            + '/archive/grids/GLOB_30m/'
            + current_run_date.strftime("%Y%m%d%H")
            + '/hindcast'
        )

        pre_process_mechanism.update_restart(
            'GLOB_30m',
            export_local_dir,
            model_source,
            assimilation_process_start_str,
        )

        # Run the data assimilation for the first 6 hours of the hindcast period
        # by doing runs with a 3 hours length

        for step in range(12, 6, -3):

            assimilation_process_start_str = (
                current_run_date - datetime.timedelta(hours=step)
            )

            assimilation_process_end_str = (
                assimilation_process_start_str + datetime.timedelta(hours=3)
            )

            # Clean Up the folders
            if os.path.exists(export_shared_dir + '/work'):
                shutil.rmtree(export_shared_dir + '/work')
            os.system('sudo chmod 777 -R ' + export_shared_dir)
            os.makedirs(export_shared_dir + '/work')
            if os.path.exists(export_local_dir + '/data_assimilation'):
                shutil.rmtree(export_local_dir + '/data_assimilation')
            os.makedirs(export_local_dir + '/data_assimilation')

            # Get satellite data
            log.info('Downloading satellite data')
            get_input_files.create_sat_files(
                assimilation_process_end_str,
                export_local_dir + '/input_data',
                export_local_dir + '/data_assimilation',
            )

            pre_process_mechanism.create_model_run_input_file_assimilation(
                assimilation_process_start_str,
                FIELDS_OUT,
                dt_field,
                dt_point,
                3,
                export_local_dir,
                export_shared_dir,
            )
            for model_grid in model_grids_global_list:
                pre_process_mechanism.pre_process_model_restarts(
                    model_grid,
                    export_local_dir,
                    export_shared_dir,
                    'work',
                    model_source,
                    assimilation_process_start_str,
                )
                shutil.copy2(
                    export_local_dir
                    + '/pre_process_basefiles/mod_def.'
                    + model_grid,
                    export_shared_dir + '/work/mod_def.' + model_grid,
                )

            for input_file in input_files_global_list:
                for file in glob.glob(
                    export_local_dir + '/pre_process_basefiles/*.' + input_file
                ):
                    shutil.copy2(file, export_shared_dir + '/work/.')

            # Execute the model
            shutil.copy2(
                export_local_dir + '/pre_process_basefiles/hosts_file',
                export_shared_dir + '/work/.',
            )
            shutil.copy2(
                model_source + '/ww3_multi',
                export_shared_dir + '/work/ww3_multi',
            )
            os.chdir(export_shared_dir + '/work')
            os.system(
                'mpiexec -f hosts_file -np '
                + str(nb_procs)
                + ' '
                + export_shared_dir
                + '/work/ww3_multi'
            )

            # Copy Output files for pos-processing
            for model_grid in model_grids_global_list:
                shutil.copy2(
                    export_shared_dir + '/work/out_grd.' + model_grid,
                    export_shared_dir + '/model_data/.',
                )
                shutil.copy2(
                    export_shared_dir + '/work/out_pnt.' + model_grid,
                    export_shared_dir + '/model_data/.',
                )

            pos_process_mechanism.create_grid_output_files_hindcast(
                'GLOB_30m',
                'GLOB_30m',
                export_local_dir,
                export_shared_dir,
                current_run_date,
                assimilation_process_start_str,
                FIELDS_OUT,
                dt_field,
                3,
                model_source,
            )
            pos_process_mechanism.read_sigH_nc_file(
                export_local_dir,
                export_local_dir
                + '/archive/grids/GLOB_30m/'
                + current_run_date.strftime("%Y%m%d%H")
                + '/hindcast/ww3_lotus_GLOB_30m.'
                + assimilation_process_end_str.strftime("%Y%m%d%H")
                + '.nc',
            )

            shutil.copy2(
                export_shared_dir + '/work/restart001.GLOB_30m',
                export_local_dir + '/data_assimilation/restart.ww3',
            )
            shutil.copy2(
                export_local_dir + '/base_files/GLOB_30m.mask',
                export_local_dir
                + '/data_assimilation/GLOB_30m.mask_assimilation',
            )
            shutil.copy2(
                export_local_dir
                + '/base_files/run_assimilation_GLOB_30m.octave',
                export_local_dir
                + '/data_assimilation/run_assimilation_GLOB_30m.octave',
            )
            os.system(
                'chmod +rx '
                + export_local_dir
                + '/data_assimilation/run_assimilation_GLOB_30m.octave'
            )
            os.system(
                export_local_dir
                + '/data_assimilation/run_assimilation_GLOB_30m.octave '
                + assimilation_process_end_str.strftime('%Y%m%d%H')
                + ' '
                + export_local_dir
                + '/data_assimilation'
                + ' '
                + export_local_dir
                + '/archive/grids/GLOB_30m/'
                + current_run_date.strftime("%Y%m%d%H")
                + '/hindcast'
            )

            pre_process_mechanism.update_restart(
                'GLOB_30m',
                export_local_dir,
                model_source,
                assimilation_process_end_str,
            )
            pos_process_mechanism.create_restarts(
                'GLOB_15m',
                export_local_dir,
                export_shared_dir,
                'work',
                assimilation_process_start_str,
                10800,
                0.125,
            )

            # Create boundary points for nested grids
            for model_grid in model_grids_nested_list:
                pos_process_boundary.create_boundary_point_files_hindcast(
                    'GLOB_15m',
                    model_grid,
                    export_local_dir,
                    export_shared_dir,
                    assimilation_process_start_str,
                    dt_point,
                    0.125,
                    model_source,
                )

        # Final part of hindcast process, without assimilation

        if os.path.exists(export_shared_dir + '/work'):
            shutil.rmtree(export_shared_dir + '/work')
        os.system('sudo chmod 777 -R ' + export_shared_dir)
        os.makedirs(export_shared_dir + '/work')

        assimilation_process_start_str = current_run_date - datetime.timedelta(
            hours=6
        )

        assimilation_process_end_str = current_run_date

        pre_process_mechanism.create_model_run_input_file_assimilation(
            assimilation_process_start_str,
            FIELDS_OUT,
            dt_field,
            dt_point,
            6,
            export_local_dir,
            export_shared_dir,
        )
        for model_grid in model_grids_global_list:
            pre_process_mechanism.pre_process_model_restarts(
                model_grid,
                export_local_dir,
                export_shared_dir,
                'work',
                model_source,
                assimilation_process_start_str,
            )
            shutil.copy2(
                export_local_dir
                + '/pre_process_basefiles/mod_def.'
                + model_grid,
                export_shared_dir + '/work/mod_def.' + model_grid,
            )

        for input_file in input_files_global_list:
            for file in glob.glob(
                export_local_dir + '/pre_process_basefiles/*.' + input_file
            ):
                shutil.copy2(file, export_shared_dir + '/work/.')

        # Execute the model
        shutil.copy2(
            export_local_dir + '/pre_process_basefiles/hosts_file',
            export_shared_dir + '/work/.',
        )
        shutil.copy2(
            model_source + '/ww3_multi', export_shared_dir + '/work/ww3_multi'
        )
        os.chdir(export_shared_dir + '/work')
        os.system(
            'mpiexec -f hosts_file -np '
            + str(nb_procs)
            + ' '
            + export_shared_dir
            + '/work/ww3_multi'
        )

        # Copy Output files for pos-processing
        for model_grid in model_grids_global_list:
            shutil.copy2(
                export_shared_dir + '/work/out_grd.' + model_grid,
                export_shared_dir + '/model_data/.',
            )
            shutil.copy2(
                export_shared_dir + '/work/out_pnt.' + model_grid,
                export_shared_dir + '/model_data/.',
            )

        pos_process_mechanism.create_grid_output_files_hindcast(
            'GLOB_30m',
            'GLOB_30m',
            export_local_dir,
            export_shared_dir,
            current_run_date,
            assimilation_process_start_str,
            FIELDS_OUT,
            dt_field,
            6,
            model_source,
        )

        pos_process_mechanism.create_restarts(
            'GLOB_30m',
            export_local_dir,
            export_shared_dir,
            'work',
            assimilation_process_start_str,
            10800,
            0.25,
        )
        pos_process_mechanism.create_restarts(
            'GLOB_15m',
            export_local_dir,
            export_shared_dir,
            'work',
            assimilation_process_start_str,
            10800,
            0.25,
        )

        # Create boundary points for nested grids
        for model_grid in model_grids_nested_list:
            pos_process_boundary.create_boundary_point_files_hindcast(
                'GLOB_15m',
                model_grid,
                export_local_dir,
                export_shared_dir,
                assimilation_process_start_str,
                dt_point,
                0.25,
                model_source,
            )

    # Start the forecast section of the model run
    if os.path.exists(export_shared_dir + '/work'):
        shutil.rmtree(export_shared_dir + '/work')
    os.system('sudo chmod 777 -R ' + export_shared_dir)
    os.makedirs(export_shared_dir + '/work')

    # Create input_file for model run
    pre_process_mechanism.create_model_run_input_file(
        current_run_date,
        FIELDS_OUT,
        dt_field,
        dt_point,
        dt_restart,
        model_length,
        restart_output_length,
        export_local_dir,
        export_shared_dir,
    )

    for model_grid in model_grids_global_list:
        pre_process_mechanism.pre_process_model_restarts(
            model_grid,
            export_local_dir,
            export_shared_dir,
            'work',
            model_source,
            current_run_date,
        )
        shutil.copy2(
            export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
            export_shared_dir + '/work/mod_def.' + model_grid,
        )

    for input_file in input_files_global_list:
        for file in glob.glob(
            export_local_dir + '/pre_process_basefiles/*.' + input_file
        ):
            shutil.copy2(file, export_shared_dir + '/work/.')

    # Execute the model
    shutil.copy2(
        export_local_dir + '/pre_process_basefiles/hosts_file',
        export_shared_dir + '/work/.',
    )
    shutil.copy2(
        model_source + '/ww3_multi', export_shared_dir + '/work/ww3_multi'
    )
    os.chdir(export_shared_dir + '/work')
    os.system(
        'mpiexec -f hosts_file -np '
        + str(nb_procs)
        + ' '
        + export_shared_dir
        + '/work/ww3_multi'
    )

    # Copy Output files for pos-processing
    for model_grid in model_grids_global_list:
        shutil.copy2(
            export_shared_dir + '/work/out_grd.' + model_grid,
            export_shared_dir + '/model_data/.',
        )
        shutil.copy2(
            export_shared_dir + '/work/out_pnt.' + model_grid,
            export_shared_dir + '/model_data/.',
        )

    # Rename and move restart files to respective folder
    pos_process_mechanism.create_restarts(
        'GLOB_30m',
        export_local_dir,
        export_shared_dir,
        'work',
        current_run_date,
        dt_restart,
        int(restart_output_length),
    )
    pos_process_mechanism.create_restarts(
        'GLOB_15m',
        export_local_dir,
        export_shared_dir,
        'work',
        current_run_date,
        dt_restart,
        int(restart_output_length),
    )

    # Run nested grids

    slot_file = open(export_local_dir + '/slot_1', 'w')
    slot_file.close()
    slot_file = open(export_local_dir + '/slot_2', 'w')
    slot_file.close()
    slot_file = open(export_local_dir + '/slot_3', 'w')
    slot_file.close()
    slot_file = open(export_local_dir + '/slot_4', 'w')
    slot_file.close()

    time_to_wait = 3600
    nested_runs_start_time = current_run_date - datetime.timedelta(hours=12)

    for model_grid in model_grids_nested_list:

        slot_1_open = os.path.exists(export_local_dir + '/slot_1')
        slot_2_open = os.path.exists(export_local_dir + '/slot_2')
        slot_3_open = os.path.exists(export_local_dir + '/slot_3')
        slot_4_open = os.path.exists(export_local_dir + '/slot_4')

        extra_spot = 0

        if model_grid == 'AUS_E_3m':
            extra_spot = 1
        if model_grid == 'AUS_SW_3m':
            extra_spot = 1
        if model_grid == 'AUS_VT_3m':
            extra_spot = 1
        if model_grid == 'HW_3m':
            extra_spot = 1
        if model_grid == 'UK_3m':
            extra_spot = 1
        if model_grid == 'US_E_3m':
            extra_spot = 1

        work_dir = 'work_' + model_grid

        # Clean folders and start nested grids runs
        if os.path.exists(export_shared_dir + '/' + work_dir):
            shutil.rmtree(export_shared_dir + '/' + work_dir)
        os.makedirs(export_shared_dir + '/' + work_dir)
        os.system('sudo chmod 777 -R ' + export_shared_dir + '/' + work_dir)

        pre_process_mechanism.pre_process_model_restarts(
            model_grid,
            export_local_dir,
            export_shared_dir,
            work_dir,
            model_source,
            nested_runs_start_time,
        )
        shutil.copy2(
            export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
            export_shared_dir + '/' + work_dir + '/mod_def.' + model_grid,
        )

        for input_file in input_files_nested_list:
            for file in glob.glob(
                export_local_dir + '/pre_process_basefiles/*.' + input_file
            ):
                shutil.copy2(file, export_shared_dir + '/' + work_dir + '/.')

        if model_grid == 'GLAKES_6m':
            # Create input file for nested grid model run
            pre_process_mechanism.create_model_run_input_file_nested_grids_with_ice(
                model_grid,
                current_run_date,
                FIELDS_OUT,
                dt_field,
                dt_point,
                dt_restart,
                model_length,
                restart_output_length,
                export_local_dir,
                export_shared_dir,
                extra_spot,
            )

        else:

            # Create Boundary files for nested grids
            pos_process_boundary.create_boundary_point_files(
                'GLOB_15m',
                model_grid,
                export_local_dir,
                export_shared_dir,
                current_run_date,
                dt_point,
                model_source,
            )
            pos_process_boundary.merge_individual_files(
                'GLOB_15m', model_grid, export_local_dir
            )
            pos_process_boundary.merge_boundary_files(
                'GLOB_15m', model_grid, export_local_dir, model_source
            )

            # Create input file for nested grid model run
            pre_process_mechanism.create_model_run_input_file_nested_grids(
                model_grid,
                current_run_date,
                FIELDS_OUT,
                dt_field,
                dt_point,
                dt_restart,
                model_length,
                restart_output_length,
                export_local_dir,
                export_shared_dir,
                extra_spot,
            )
            shutil.copy2(
                export_local_dir + '/boundary/nest.' + model_grid,
                export_shared_dir + '/' + work_dir + '/.',
            )

        # Execute the model for the nested grid run

        shutil.copy2(
            model_source + '/ww3_multi',
            export_shared_dir + '/' + work_dir + '/ww3_multi',
        )
        os.chdir(export_shared_dir + '/' + work_dir)

        time_counter = 0

        while (
            slot_1_open == False
            and slot_2_open == False
            and slot_3_open == False
            and slot_4_open == False
        ):

            slot_1_open = os.path.exists(export_local_dir + '/slot_1')
            slot_2_open = os.path.exists(export_local_dir + '/slot_2')
            slot_3_open = os.path.exists(export_local_dir + '/slot_3')
            slot_4_open = os.path.exists(export_local_dir + '/slot_4')

            time.sleep(1)
            time_counter += 1
            if time_counter > time_to_wait:
                break

        if slot_1_open == True:
            os.remove(export_local_dir + '/slot_1')
            shutil.copy2(
                export_local_dir + '/pre_process_basefiles/hosts_file_slot_1',
                export_shared_dir + '/' + work_dir + '/hosts_file',
            )
            shellfile = open(
                export_local_dir + '/run_model_' + model_grid + '.sh', 'w'
            )
            shellfile.write('#! /bin/bash\n')
            shellfile.write('cd ' + export_shared_dir + '/' + work_dir + '\n')
            shellfile.write(
                'mpiexec -f hosts_file -np '
                + str(nb_procs_per_slave)
                + ' '
                + export_shared_dir
                + '/'
                + work_dir
                + '/ww3_multi\n'
            )
            shellfile.write('touch ' + export_local_dir + '/slot_1\n')
            shellfile.close()
            os.system(
                'chmod +rx '
                + export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh'
            )
            os.system(
                export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh > '
                + export_shared_dir
                + '/'
                + work_dir
                + '/model_run_'
                + model_grid
                + '.log 2>&1 &'
            )
        elif slot_2_open == True:
            os.remove(export_local_dir + '/slot_2')
            shutil.copy2(
                export_local_dir + '/pre_process_basefiles/hosts_file_slot_2',
                export_shared_dir + '/' + work_dir + '/hosts_file',
            )
            shellfile = open(
                export_local_dir + '/run_model_' + model_grid + '.sh', 'w'
            )
            shellfile.write('#! /bin/bash\n')
            shellfile.write('cd ' + export_shared_dir + '/' + work_dir + '\n')
            shellfile.write(
                'mpiexec -f hosts_file -np '
                + str(nb_procs_per_slave)
                + ' '
                + export_shared_dir
                + '/'
                + work_dir
                + '/ww3_multi\n'
            )
            shellfile.write('touch ' + export_local_dir + '/slot_2\n')
            shellfile.close()
            os.system(
                'chmod +rx '
                + export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh'
            )
            os.system(
                export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh > '
                + export_shared_dir
                + '/'
                + work_dir
                + '/model_run_'
                + model_grid
                + '.log 2>&1 &'
            )
        elif slot_3_open == True:
            os.remove(export_local_dir + '/slot_3')
            shutil.copy2(
                export_local_dir + '/pre_process_basefiles/hosts_file_slot_3',
                export_shared_dir + '/' + work_dir + '/hosts_file',
            )
            shellfile = open(
                export_local_dir + '/run_model_' + model_grid + '.sh', 'w'
            )
            shellfile.write('#! /bin/bash\n')
            shellfile.write('cd ' + export_shared_dir + '/' + work_dir + '\n')
            shellfile.write(
                'mpiexec -f hosts_file -np '
                + str(nb_procs_per_slave)
                + ' '
                + export_shared_dir
                + '/'
                + work_dir
                + '/ww3_multi\n'
            )
            shellfile.write('touch ' + export_local_dir + '/slot_3\n')
            shellfile.close()
            os.system(
                'chmod +rx '
                + export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh'
            )
            os.system(
                export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh > '
                + export_shared_dir
                + '/'
                + work_dir
                + '/model_run_'
                + model_grid
                + '.log 2>&1 &'
            )
        elif slot_4_open == True:
            os.remove(export_local_dir + '/slot_4')
            shutil.copy2(
                export_local_dir + '/pre_process_basefiles/hosts_file_slot_4',
                export_shared_dir + '/' + work_dir + '/hosts_file',
            )
            shellfile = open(
                export_local_dir + '/run_model_' + model_grid + '.sh', 'w'
            )
            shellfile.write('#! /bin/bash\n')
            shellfile.write('cd ' + export_shared_dir + '/' + work_dir + '\n')
            shellfile.write(
                'mpiexec -f hosts_file -np '
                + str(nb_procs_per_slave)
                + ' '
                + export_shared_dir
                + '/'
                + work_dir
                + '/ww3_multi\n'
            )
            shellfile.write('touch ' + export_local_dir + '/slot_4\n')
            shellfile.close()
            os.system(
                'chmod +rx '
                + export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh'
            )
            os.system(
                export_local_dir
                + '/run_model_'
                + model_grid
                + '.sh > '
                + export_shared_dir
                + '/'
                + work_dir
                + '/model_run_'
                + model_grid
                + '.log 2>&1 &'
            )

    time_to_wait = 3600
    time_counter = 0

    for model_grid in model_grids_nested_list:

        work_dir = 'work_' + model_grid

        model_finished = False

        while model_finished == False:

            if os.path.exists(
                export_shared_dir
                + '/'
                + work_dir
                + '/model_run_'
                + model_grid
                + '.log'
            ):
                with open(
                    export_shared_dir
                    + '/'
                    + work_dir
                    + '/model_run_'
                    + model_grid
                    + '.log',
                    'r',
                ) as InFile:
                    for line in InFile:
                        line = line.rstrip('\n')
                        if line == '  End of program ':
                            model_finished = True
            time.sleep(1)
            time_counter += 1
            if time_counter > time_to_wait:
                break

    filelist = glob.glob(export_local_dir + '/run_model_*.sh')
    for f in filelist:  # type: ignore
        os.remove(f)  # type: ignore
    filelist = glob.glob(export_local_dir + '/slot_*')
    for f in filelist:  # type: ignore
        os.remove(f)  # type: ignore

    # # Terminate the slaves
    # os.system('sh ~/dl/lotus-source/scripts/lotus/destroy-slaves.sh')
    # TODO: remove when post-processing has been extracted to AWS Batch
    terminate_instances_by_id(slave_instance_ids)

    # Create Restart Files for Nested Grids
    for model_grid in model_grids_nested_list:

        work_dir = 'work_' + model_grid
        # Rename and move restart files to respective folder
        pos_process_mechanism.create_restarts(
            model_grid,
            export_local_dir,
            export_shared_dir,
            work_dir,
            nested_runs_start_time,
            dt_restart,
            float(restart_output_length_nested),
        )

    # Upload restarts to AWS S3
    os.system(
        'aws s3 sync --quiet --delete '
        + export_local_dir
        + '/restarts/ s3://surfline-science-s3-dev/wavetrak/lotus-ww3/restarts'
    )

    for model_grid in model_grids_global_list:
        # Create grid output files
        pos_process_mechanism.create_grid_output_files(
            model_grid,
            model_grid,
            export_local_dir,
            export_shared_dir,
            current_run_date,
            FIELDS_OUT,
            dt_field,
            model_length,
            model_source,
        )
        pos_process_mechanism.create_point_output_files(
            model_grid,
            export_local_dir,
            export_shared_dir,
            current_run_date,
            dt_point,
            model_length,
            model_source,
        )

    for model_grid in model_grids_nested_list:

        work_dir = 'work_' + model_grid

        # Copy Output files for pos-processing
        shutil.copy2(
            export_shared_dir + '/' + work_dir + '/out_grd.' + model_grid,
            export_shared_dir + '/model_data/.',
        )
        shutil.copy2(
            export_shared_dir + '/' + work_dir + '/out_pnt.' + model_grid,
            export_shared_dir + '/model_data/.',
        )
        # Create a new grids by merging the nested grid with the coarser grid
        pos_process_mechanism.merge_out_grd_files(
            ['GLOB_30m', 'GLOB_15m', model_grid],
            model_grid + '_out',
            export_local_dir,
            export_shared_dir,
            current_run_date,
            dt_field,
            model_source,
        )
        # Create grid output files
        pos_process_mechanism.create_grid_output_files(
            model_grid + '_out',
            model_grid,
            export_local_dir,
            export_shared_dir,
            current_run_date,
            FIELDS_OUT,
            dt_field,
            model_length,
            model_source,
        )
        # Create point output files
        pos_process_mechanism.create_point_output_files(
            model_grid,
            export_local_dir,
            export_shared_dir,
            current_run_date,
            dt_point,
            model_length,
            model_source,
        )

    # Copy grid data for analysis archive
    for model_grid in model_grids_global_list:
        for step in range(0, 385, 1):
            step_date = current_run_date + datetime.timedelta(hours=step)
            if not os.path.exists(
                export_local_dir
                + '/archive/grids_analysis/'
                + model_grid
                + '/'
                + step_date.strftime("%Y%m")
            ):
                os.makedirs(
                    export_local_dir
                    + '/archive/grids_analysis/'
                    + model_grid
                    + '/'
                    + step_date.strftime("%Y%m")
                )
            shutil.copy2(
                export_local_dir
                + '/archive/grids/'
                + model_grid
                + '/'
                + current_run_date.strftime("%Y%m%d%H")
                + '/ww3_lotus_'
                + model_grid
                + '.'
                + current_run_date.strftime("%Y%m%d%H")
                + '.'
                + ('%03d' % step)
                + '.nc',
                export_local_dir
                + '/archive/grids_analysis/'
                + model_grid
                + '/'
                + step_date.strftime("%Y%m")
                + '/ww3_lotus_'
                + model_grid
                + '.'
                + step_date.strftime("%Y%m%d%H")
                + '.nc',
            )

    for model_grid in model_grids_nested_list:
        for step in range(0, 385, 1):
            step_date = current_run_date + datetime.timedelta(hours=step)
            if not os.path.exists(
                export_local_dir
                + '/archive/grids_analysis/'
                + model_grid
                + '/'
                + step_date.strftime("%Y%m")
            ):
                os.makedirs(
                    export_local_dir
                    + '/archive/grids_analysis/'
                    + model_grid
                    + '/'
                    + step_date.strftime("%Y%m")
                )
            shutil.copy2(
                export_local_dir
                + '/archive/grids/'
                + model_grid
                + '/'
                + current_run_date.strftime("%Y%m%d%H")
                + '/ww3_lotus_'
                + model_grid
                + '.'
                + current_run_date.strftime("%Y%m%d%H")
                + '.'
                + ('%03d' % step)
                + '.nc',
                export_local_dir
                + '/archive/grids_analysis/'
                + model_grid
                + '/'
                + step_date.strftime("%Y%m")
                + '/ww3_lotus_'
                + model_grid
                + '.'
                + step_date.strftime("%Y%m%d%H")
                + '.nc',
            )

    # Upload results to AWS S3
    log.info('Uploading results to s3 quietly')
    os.system(
        'aws s3 cp --quiet --recursive '
        + export_local_dir
        + f'/archive/ s3://surfline-science-s3-dev/wavetrak/lotus-ww3/archive'
    )
    # os.system(
    #     'aws s3 cp --recursive '
    #     + export_local_dir
    #     + '/archive/ s3://surfline-science-s3-prod/lotus/archive --acl bucket-owner-full-control'
    # )
    #
    # # Only upload the 06H run to surfline-dev to save resources
    # # if datetime.datetime.strftime(current_run_date, '%H') == '06':
    # os.system(
    #     'aws s3 cp --recursive '
    #     + export_local_dir
    #     + '/archive/ s3://surfline-science-s3-dev/lotus/archive --acl bucket-owner-full-control'
    # )
    #
    # Upload Log files to AWS S3
    log.info('Uploading logs to s3 quietly')
    os.system(
        'aws s3 cp --quiet '
        + export_local_dir
        + '/model_run.log s3://surfline-science-s3-dev/wavetrak/lotus-ww3/archive/logs/model_run_'
        + current_run_date.strftime("%Y%m%d%H")
        + '.log'
    )

    # # Terminate Master
    # os.system('sudo shutdown -h now')
