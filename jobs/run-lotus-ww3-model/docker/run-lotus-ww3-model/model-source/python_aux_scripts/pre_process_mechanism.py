import csv
import datetime
import os
from shutil import copy2, move

from util.logger import log


def process_input_files(input_file, export_local_dir, model_path):

    log.info(
        f'Process input files: {input_file}, {export_local_dir}, {model_path}'
    )
    os.chdir(export_local_dir + '/pre_process')
    copy2(
        export_local_dir + '/base_files/ww3_grid.inp.' + input_file,
        export_local_dir + '/pre_process/ww3_grid.inp',
    )
    copy2(
        export_local_dir + '/input_data/' + input_file,
        export_local_dir + '/pre_process',
    )

    os.system(
        model_path
        + '/ww3_grid > '
        + export_local_dir
        + '/log/ww3_grid.'
        + input_file
        + '.out'
    )

    InpFile = open('ww3_prep.inp', 'w')
    InpFile.write('$ WAVEWATCH III Field preprocessor input file\n')
    InpFile.write('$ -------------------------------------------\n')
    if input_file == 'w_gfs_50' or input_file == 'w_gfs_25':
        InpFile.write("  'WND' 'LL' T T\n")
    if input_file == 'ice_gfs_50' or input_file == 'ice_gfs_25':
        InpFile.write("  'ICE' 'LL' T T\n")
    if input_file == 'w_gfs_50' or input_file == 'ice_gfs_50':
        InpFile.write("  0. 359.5  720  -90. 90. 361\n")
    if input_file == 'w_gfs_25' or input_file == 'ice_gfs_25':
        InpFile.write("  0 359.75  1440  -90. 90. 721\n")
    InpFile.write("  'NAME' 1 1 '(..T..)' '(..F..)'\n")
    InpFile.write("  20 '%s'\n" % input_file)
    InpFile.write('$\n')
    InpFile.write('$ End of Input File\n')
    InpFile.close()

    log.info('   Running ww3_prep for ' + input_file)
    log.info(
        '   Screen ouput routed to '
        + export_local_dir
        + '/log/ww3_prep.'
        + input_file
        + '.out'
    )
    os.system(
        model_path
        + '/ww3_prep > '
        + export_local_dir
        + '/log/ww3_prep.'
        + input_file
        + '.out'
    )

    if input_file == 'w_gfs_50' or input_file == 'w_gfs_25':
        move(
            export_local_dir + '/pre_process/wind.ww3',
            export_local_dir + '/pre_process_basefiles/wind.' + input_file,
        )
    if input_file == 'ice_gfs_50' or input_file == 'ice_gfs_25':
        move(
            export_local_dir + '/pre_process/ice.ww3',
            export_local_dir + '/pre_process_basefiles/ice.' + input_file,
        )
    move(
        export_local_dir + '/pre_process/mod_def.ww3',
        export_local_dir + '/pre_process_basefiles/mod_def.' + input_file,
    )
    os.remove(export_local_dir + '/pre_process/ww3_grid.inp')
    os.remove(export_local_dir + '/pre_process/ww3_prep.inp')
    os.remove(export_local_dir + '/pre_process/' + input_file)


def process_model_grids(
    model_grid, export_local_dir, model_path, current_run_date, grid_type
):

    log.info(
        f'Process model grids: {model_grid}, {export_local_dir}, {model_path}, {current_run_date}, {grid_type}'
    )
    if grid_type == 'run':

        copy2(
            export_local_dir + '/base_files/ww3_grid.inp.' + model_grid,
            export_local_dir + '/pre_process/ww3_grid.inp',
        )
        copy2(
            export_local_dir + '/base_files/' + model_grid + '.obs',
            export_local_dir + '/pre_process/',
        )
        copy2(
            export_local_dir + '/base_files/' + model_grid + '.mask',
            export_local_dir + '/pre_process/',
        )
        copy2(
            export_local_dir + '/base_files/' + model_grid + '.depth',
            export_local_dir + '/pre_process/',
        )
        os.chdir(export_local_dir + '/pre_process')

        log.info('   Running ww3_prep for ' + model_grid)
        log.info(
            '   Screen ouput routed to '
            + export_local_dir
            + '/log/ww3_grid.'
            + model_grid
            + '.out'
        )

        os.system(
            model_path
            + '/ww3_grid > '
            + export_local_dir
            + '/log/ww3_grid.'
            + model_grid
            + '.out'
        )
        move(
            export_local_dir + '/pre_process/mod_def.ww3',
            export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
        )
        os.remove(export_local_dir + '/pre_process/ww3_grid.inp')
        os.remove(export_local_dir + '/pre_process/' + model_grid + '.obs')
        os.remove(export_local_dir + '/pre_process/' + model_grid + '.mask')
        os.remove(export_local_dir + '/pre_process/' + model_grid + '.depth')
    if grid_type == 'output':
        copy2(
            export_local_dir
            + '/base_files/ww3_grid.inp.'
            + model_grid
            + '_out',
            export_local_dir + '/pre_process/ww3_grid.inp',
        )
        copy2(
            export_local_dir + '/base_files/' + model_grid + '.obs',
            export_local_dir + '/pre_process/',
        )
        copy2(
            export_local_dir + '/base_files/' + model_grid + '_out' + '.mask',
            export_local_dir + '/pre_process/',
        )
        copy2(
            export_local_dir + '/base_files/' + model_grid + '.depth',
            export_local_dir + '/pre_process/',
        )
        os.chdir(export_local_dir + '/pre_process')

        log.info('   Running ww3_prep for ' + model_grid)
        log.info(
            '   Screen ouput routed to '
            + export_local_dir
            + '/log/ww3_grid.'
            + model_grid
            + '_out.out'
        )

        os.system(
            model_path
            + '/ww3_grid > '
            + export_local_dir
            + '/log/ww3_grid.'
            + model_grid
            + '_out.out'
        )
        move(
            export_local_dir + '/pre_process/mod_def.ww3',
            export_local_dir
            + '/pre_process_basefiles/mod_def.'
            + model_grid
            + '_out',
        )
        os.remove(export_local_dir + '/pre_process/ww3_grid.inp')
        os.remove(export_local_dir + '/pre_process/' + model_grid + '.obs')
        os.remove(
            export_local_dir + '/pre_process/' + model_grid + '_out.mask'
        )
        os.remove(export_local_dir + '/pre_process/' + model_grid + '.depth')


def pre_process_model_restarts(
    model_grid,
    export_local_dir,
    export_shared_dir,
    work_dir,
    model_path,
    current_run_date,
):
    log.info(
        f'Pre-process model restarts: {model_grid}, {export_local_dir}, {export_shared_dir}, {work_dir}, {model_path}, {current_run_date}'
    )

    date_restart = datetime.datetime.strftime(current_run_date, '%Y%m%d%H')
    if os.path.isfile(
        export_local_dir
        + '/restarts/restart.'
        + model_grid
        + '.'
        + date_restart
    ):
        log.info(
            '   Initial conditions from restart.'
            + model_grid
            + '.'
            + date_restart
        )
        copy2(
            export_local_dir
            + '/restarts/restart.'
            + model_grid
            + '.'
            + date_restart,
            export_shared_dir + '/' + work_dir + '/restart.' + model_grid,
        )
    else:

        os.chdir(export_local_dir + '/pre_process')
        copy2(
            export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
            export_local_dir + '/pre_process/mod_def.ww3',
        )
        InpFile = open('ww3_strt.inp', 'w')
        InpFile.write('$ WAVEWATCH III Initial conditions input file\n')
        InpFile.write('$ -------------------------------------------\n')
        InpFile.write('3\n')
        InpFile.close()
        log.info('   Running ww3_strt for initial conditions')
        log.info(
            '   Screen ouput routed to '
            + export_local_dir
            + '/log/ww3_strt.'
            + model_grid
            + '.out'
        )
        os.system(
            model_path
            + '/ww3_strt > '
            + export_local_dir
            + '/log/ww3_strt.'
            + model_grid
            + '.out'
        )
        move(
            export_local_dir + '/pre_process/restart.ww3',
            export_shared_dir + '/' + work_dir + '/restart.' + model_grid,
        )
        os.remove(export_local_dir + '/pre_process/ww3_strt.inp')
        os.remove(export_local_dir + '/pre_process/mod_def.ww3')


def create_model_run_input_file(
    current_run_date,
    FIELDS_OUT,
    dt_field,
    dt_point,
    dt_restart,
    model_length,
    restart_output_length,
    export_local_dir,
    export_shared_dir,
):
    log.info(f'Create model run input file')

    os.chdir(export_shared_dir + '/work')
    model_start_date = datetime.datetime.strftime(
        current_run_date, '%Y%m%d %H%M%S'
    )
    model_end_date = current_run_date + datetime.timedelta(
        days=float(model_length)
    )
    model_end_date = model_end_date.strftime("%Y%m%d %H%M%S")

    restart_start_date = current_run_date + datetime.timedelta(
        seconds=int(dt_restart)
    )
    restart_start_date = restart_start_date.strftime("%Y%m%d %H%M%S")
    restart_end_date = current_run_date + datetime.timedelta(
        days=float(restart_output_length)
    )
    restart_end_date = restart_end_date.strftime("%Y%m%d %H%M%S")

    InpFile = open('ww3_multi.inp', 'w')
    InpFile.write('$ WAVEWATCH III multi-grid input file\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(' 2 4 F 3 T T\n')
    InpFile.write("'w_gfs_25'    F F T F F F F\n")
    InpFile.write("'w_gfs_50'    F F T F F F F\n")
    InpFile.write("'ice_gfs_25'  F F F T F F F\n")
    InpFile.write("'ice_gfs_50'  F F F T F F F\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write(
        "'GLOB_30m' 'no'  'no'  'w_gfs_50'  'ice_gfs_50' 'no' 'no' 'no'  1  1   0.00  1.00  F \n"
    )
    InpFile.write(
        "'GLOB_15m' 'no'  'no'  'w_gfs_25'  'ice_gfs_25' 'no' 'no' 'no'  2  10  0.00  1.00  F \n"
    )
    InpFile.write('$\n')
    InpFile.write(model_start_date + '  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('F  F\n')
    InpFile.write('$\n')
    InpFile.write(
        model_start_date + '  ' + dt_field + '  ' + model_end_date + '\n'
    )
    InpFile.write('N\n')
    InpFile.write(FIELDS_OUT + '\n')
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(
        restart_start_date + '  ' + dt_restart + '  ' + restart_end_date + '\n'
    )
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('$\n')

    InpFile.write("'GLOB_30m' 2\n")
    InpFile.write(
        model_start_date + '  ' + dt_point + '  ' + model_end_date + '\n'
    )
    with open(
        export_local_dir + '/base_files/point_outputs_GLOB_30m.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_lon = row[1]
            spot_lat = row[2]
            InpFile.write(spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n')
    InpFile.write(
        spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n'
    )  # Fake spot
    InpFile.write("0 0 'STOPSTRING'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write("'GLOB_15m' 2\n")
    InpFile.write(
        model_start_date + '  ' + dt_point + '  ' + model_end_date + '\n'
    )
    with open(
        export_local_dir + '/base_files/point_outputs_GLOB_15m.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_lon = row[1]
            spot_lat = row[2]
            InpFile.write(spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n')
    InpFile.write("0 0 'STOPSTRING'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write("'the_end'  0\n")
    InpFile.write('$\n')
    InpFile.write("'STP'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()


def create_model_run_input_file_assimilation(
    current_run_date,
    FIELDS_OUT,
    dt_field,
    dt_point,
    model_length,
    export_local_dir,
    export_shared_dir,
):
    log.info(f'Create model run input file assimilation')

    os.chdir(export_shared_dir + '/work')
    model_start_date = datetime.datetime.strftime(
        current_run_date, '%Y%m%d %H%M%S'
    )
    model_end_date = current_run_date + datetime.timedelta(
        hours=int(model_length)
    )
    model_end_date = model_end_date.strftime("%Y%m%d %H%M%S")

    InpFile = open('ww3_multi.inp', 'w')
    InpFile.write('$ WAVEWATCH III multi-grid input file\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(' 2 4 F 3 T T\n')
    InpFile.write("'w_gfs_25'    F F T F F F F\n")
    InpFile.write("'w_gfs_50'    F F T F F F F\n")
    InpFile.write("'ice_gfs_25'  F F F T F F F\n")
    InpFile.write("'ice_gfs_50'  F F F T F F F\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write(
        "'GLOB_30m' 'no'  'no'  'w_gfs_50'  'ice_gfs_50' 'no' 'no' 'no'  1  1   0.00  1.00  F \n"
    )
    InpFile.write(
        "'GLOB_15m' 'no'  'no'  'w_gfs_25'  'ice_gfs_25' 'no' 'no' 'no'  2  10  0.00  1.00  F \n"
    )
    InpFile.write('$\n')
    InpFile.write(model_start_date + '  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('F  F\n')
    InpFile.write('$\n')
    InpFile.write(
        model_start_date + '  ' + dt_field + '  ' + model_end_date + '\n'
    )
    InpFile.write('N\n')
    InpFile.write(FIELDS_OUT + '\n')
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(model_start_date + '  10800  ' + model_end_date + '\n')
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(model_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$\n')

    InpFile.write("'GLOB_30m' 2\n")
    InpFile.write(
        model_start_date + '  ' + dt_point + '  ' + model_end_date + '\n'
    )
    with open(
        export_local_dir + '/base_files/point_outputs_GLOB_30m.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_lon = row[1]
            spot_lat = row[2]
            InpFile.write(spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n')
    InpFile.write(
        spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n'
    )  # Fake spot
    InpFile.write("0 0 'STOPSTRING'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write("'GLOB_15m' 2\n")
    InpFile.write(
        model_start_date + '  ' + dt_point + '  ' + model_end_date + '\n'
    )
    with open(
        export_local_dir + '/base_files/point_outputs_GLOB_15m.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_lon = row[1]
            spot_lat = row[2]
            InpFile.write(spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n')
    InpFile.write("0 0 'STOPSTRING'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write("'the_end'  0\n")
    InpFile.write('$\n')
    InpFile.write("'STP'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()


def create_model_run_input_file_nested_grids(
    model_grid,
    current_run_date,
    FIELDS_OUT,
    dt_field,
    dt_point,
    dt_restart,
    model_length,
    restart_output_length,
    export_local_dir,
    export_shared_dir,
    extra_spot,
):
    log.info(f'Create model run input file nested grids')

    os.chdir(export_shared_dir + '/work_' + model_grid)

    # Change model start to account for the hindcast section
    model_start = current_run_date - datetime.timedelta(hours=int(12))
    model_start_date = datetime.datetime.strftime(model_start, '%Y%m%d %H%M%S')

    output_start_date = datetime.datetime.strftime(
        current_run_date, '%Y%m%d %H%M%S'
    )
    model_end_date = current_run_date + datetime.timedelta(
        days=float(model_length)
    )
    model_end_date = model_end_date.strftime("%Y%m%d %H%M%S")

    restart_start_date = model_start + datetime.timedelta(
        seconds=int(dt_restart)
    )
    restart_start_date = restart_start_date.strftime("%Y%m%d %H%M%S")
    restart_end_date = current_run_date + datetime.timedelta(
        days=float(restart_output_length)
    )
    restart_end_date = restart_end_date.strftime("%Y%m%d %H%M%S")

    InpFile = open('ww3_multi.inp', 'w')
    InpFile.write('$ WAVEWATCH III multi-grid input file\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(' 1 1 F 3 T T\n')
    InpFile.write("'w_gfs_25'    F F T F F F F\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write(
        "'"
        + model_grid
        + "' 'no' 'no' 'w_gfs_25' 'no' 'no' 'no' 'no'  1  1  0.00 1.00  F \n"
    )
    InpFile.write('$\n')
    InpFile.write(model_start_date + '  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('F  F\n')
    InpFile.write('$\n')
    InpFile.write(
        output_start_date + '  ' + dt_field + '  ' + model_end_date + '\n'
    )
    InpFile.write('N\n')
    InpFile.write(FIELDS_OUT + '\n')
    InpFile.write(
        output_start_date + '  ' + dt_point + '  ' + model_end_date + '\n'
    )
    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_lon = row[1]
            spot_lat = row[2]
            InpFile.write(spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n')
    for line in range(0, extra_spot, 1):
        InpFile.write(
            spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n'
        )  # Fake spot
    InpFile.write("0 0 'STOPSTRING'\n")
    InpFile.write(output_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(
        restart_start_date + '  ' + dt_restart + '  ' + restart_end_date + '\n'
    )
    InpFile.write(output_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(output_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write("'the_end'  0\n")
    InpFile.write('$\n')
    InpFile.write("'STP'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()


def create_model_run_input_file_nested_grids_with_ice(
    model_grid,
    current_run_date,
    FIELDS_OUT,
    dt_field,
    dt_point,
    dt_restart,
    model_length,
    restart_output_length,
    export_local_dir,
    export_shared_dir,
    extra_spot,
):
    log.info(f'Create model run input file nested grids with ice')

    os.chdir(export_shared_dir + '/work_' + model_grid)

    # Change model start to account for the hindcast section
    model_start = current_run_date - datetime.timedelta(hours=int(12))
    model_start_date = datetime.datetime.strftime(model_start, '%Y%m%d %H%M%S')

    output_start_date = datetime.datetime.strftime(
        current_run_date, '%Y%m%d %H%M%S'
    )
    model_end_date = current_run_date + datetime.timedelta(
        days=float(model_length)
    )
    model_end_date = model_end_date.strftime("%Y%m%d %H%M%S")

    restart_start_date = model_start + datetime.timedelta(
        seconds=int(dt_restart)
    )
    restart_start_date = restart_start_date.strftime("%Y%m%d %H%M%S")
    restart_end_date = current_run_date + datetime.timedelta(
        days=float(restart_output_length)
    )
    restart_end_date = restart_end_date.strftime("%Y%m%d %H%M%S")

    InpFile = open('ww3_multi.inp', 'w')
    InpFile.write('$ WAVEWATCH III multi-grid input file\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(' 1 2 F 3 T T\n')
    InpFile.write("'w_gfs_25'    F F T F F F F\n")
    InpFile.write("'ice_gfs_25'  F F F T F F F\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write(
        "'"
        + model_grid
        + "' 'no' 'no' 'w_gfs_25' 'ice_gfs_25' 'no' 'no' 'no'  1  1  0.00 1.00  F \n"
    )
    InpFile.write('$\n')
    InpFile.write(model_start_date + '  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('F  F\n')
    InpFile.write('$\n')
    InpFile.write(
        output_start_date + '  ' + dt_field + '  ' + model_end_date + '\n'
    )
    InpFile.write('N\n')
    InpFile.write(FIELDS_OUT + '\n')
    InpFile.write(
        output_start_date + '  ' + dt_point + '  ' + model_end_date + '\n'
    )
    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_lon = row[1]
            spot_lat = row[2]
            InpFile.write(spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n')
    for line in range(0, extra_spot, 1):
        InpFile.write(
            spot_lon + '  ' + spot_lat + '  ' + spot_id + '\n'
        )  # Fake spot
    InpFile.write("0 0 'STOPSTRING'\n")
    InpFile.write(output_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(
        restart_start_date + '  ' + dt_restart + '  ' + restart_end_date + '\n'
    )
    InpFile.write(output_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write(output_start_date + '  0  ' + model_end_date + '\n')
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write("'the_end'  0\n")
    InpFile.write('$\n')
    InpFile.write("'STP'\n")
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()


def update_restart(model_grid, export_local_dir, model_path, target_date):
    log.info(
        f'Update restart: {model_grid}, {export_local_dir}, {model_path}, {target_date}'
    )

    os.chdir(export_local_dir + '/data_assimilation')
    copy2(
        export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
        export_local_dir + '/data_assimilation/mod_def.ww3',
    )
    copy2(
        model_path + '/ww3_uprstr',
        export_local_dir + '/data_assimilation/ww3_uprstr',
    )

    InpFile = open('ww3_uprstr.inp', 'w')
    InpFile.write('$ WAVEWATCH III Update Restart input file\n')
    InpFile.write(target_date.strftime('%Y%m%d %H%M%S') + '\n')
    InpFile.write('$ \n')
    InpFile.write(' UPD2\n')
    InpFile.write('$ \n')
    InpFile.write(' 0.6754 \n')
    InpFile.write(' 10\n')  # only valid with version 6_07
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(' anl.grbtxt\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()

    os.system(model_path + '/ww3_uprstr_serial')

    move(
        export_local_dir + '/data_assimilation/restart001.ww3',
        export_local_dir
        + '/restarts/restart.'
        + model_grid
        + '.'
        + target_date.strftime("%Y%m%d%H"),
    )
