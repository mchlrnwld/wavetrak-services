import datetime
import ftplib
import itertools
import os
import re
import time
from itertools import chain
from pathlib import Path
from threading import Thread
from urllib.request import urlretrieve

import boto3  # type: ignore
import netCDF4  # type: ignore
import numpy as np  # type: ignore
from botocore.exceptions import ClientError  # type: ignore
from util.logger import log


def downloadGrib_NomadsFilter(
    step, current_run_date, output_date, working_dir
):
    """
    Download GFS gribs from nomads filter page.
    We repeate the same ice coverage for the entire run, so we only download it at the 000 step.

    :parameters: step
    :parameters: current_run_date
    :return:
    :output: grib_files

    Francisco
    Mar2019
    """

    log.info(
        f'Download grib from NOMADS filter: {step}, {current_run_date}, {output_date}, {working_dir}'
    )
    current_daterun_str = datetime.datetime.strftime(
        current_run_date, '%Y%m%d'
    )
    current_run_str = datetime.datetime.strftime(current_run_date, '%H')
    output_date_str = datetime.datetime.strftime(output_date, '%Y%m%d%H')
    if step == '000':
        url_string = (
            'https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t'
            + current_run_str
            + 'z.pgrb2.0p25.f'
            + step
            + '&lev_10_m_above_ground=on&lev_surface=on&var_ICEC=on&var_UGRD=on&var_VGRD=on&leftlon=0&rightlon=360&toplat=90&bottomlat=-90&dir=%2Fgfs.'
            + current_daterun_str
            + '%2F'
            + current_run_str
            + '%2Fatmos'
        )
    else:
        url_string = (
            'https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t'
            + current_run_str
            + 'z.pgrb2.0p25.f'
            + step
            + '&lev_10_m_above_ground=on&lev_surface=on&var_UGRD=on&var_VGRD=on&leftlon=0&rightlon=360&toplat=90&bottomlat=-90&dir=%2Fgfs.'
            + current_daterun_str
            + '%2F'
            + current_run_str
            + '%2Fatmos'
        )
    urlretrieve(url_string, 'gfs.' + step + '.grb2')

    if step == '000':
        os.system(
            '/usr/local/bin/wgrib2 '
            + working_dir
            + '/gfs.'
            + step
            + '.grb2'
            + ' -match "ICEC" -netcdf '
            + working_dir
            + '/ice.nc'
        )

    os.system(
        '/usr/local/bin/wgrib2 '
        + working_dir
        + '/gfs.'
        + step
        + '.grb2'
        + ' -match "UGRD" -netcdf '
        + working_dir
        + '/data_UGRD_'
        + output_date_str
        + '.nc'
    )
    os.system(
        '/usr/local/bin/wgrib2 '
        + working_dir
        + '/gfs.'
        + step
        + '.grb2'
        + ' -match "VGRD" -netcdf '
        + working_dir
        + '/data_VGRD_'
        + output_date_str
        + '.nc'
    )

    os.remove(working_dir + '/gfs.' + step + '.grb2')


def downloadGrib_hindcastsection_NomadsFilter(
    step, current_run_date, output_date, working_dir
):
    """
    Download GFS gribs from nomads filter page.
    We repeate the same ice coverage for the entire run, so we only download it at the 000 step.

    :parameters: step
    :parameters: current_run_date
    :parameters: output_date
    :return:
    :output: grib_files

    Francisco
    Mar2019
    """

    log.info(
        f'Download grib hindcast section from NOMADS filter: {step}, {current_run_date}, {output_date}, {working_dir}'
    )
    current_daterun_str = datetime.datetime.strftime(
        current_run_date, '%Y%m%d'
    )
    current_run_str = datetime.datetime.strftime(current_run_date, '%H')
    output_date_str = datetime.datetime.strftime(output_date, '%Y%m%d%H')
    url_string = (
        'https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t'
        + current_run_str
        + 'z.pgrb2.0p25.f'
        + step
        + '&lev_10_m_above_ground=on&lev_surface=on&var_UGRD=on&var_VGRD=on&leftlon=0&rightlon=360&toplat=90&bottomlat=-90&dir=%2Fgfs.'
        + current_daterun_str
        + '%2F'
        + current_run_str
        + '%2Fatmos'
    )
    urlretrieve(url_string, 'gfs.' + output_date_str + '.grb2')

    os.system(
        '/usr/local/bin/wgrib2 '
        + working_dir
        + '/gfs.'
        + output_date_str
        + '.grb2'
        + ' -match "UGRD" -netcdf '
        + working_dir
        + '/data_UGRD_'
        + output_date_str
        + '.nc'
    )
    os.system(
        '/usr/local/bin/wgrib2 '
        + working_dir
        + '/gfs.'
        + output_date_str
        + '.grb2'
        + ' -match "VGRD" -netcdf '
        + working_dir
        + '/data_VGRD_'
        + output_date_str
        + '.nc'
    )

    os.remove(working_dir + '/gfs.' + output_date_str + '.grb2')


def download_hindcastsection_NomadsFilter(current_run_date, working_dir):
    log.info(
        f'Download hindcast section from NOMADS filter: {current_run_date}, {working_dir}'
    )
    # Hindcast part - download from previous GFS runs
    list_threads = []
    list_output_files = []
    for day_step in np.arange(0.5, 0, -0.25):
        folder_rundate = current_run_date - datetime.timedelta(
            hours=int(day_step * 24)
        )
        for step in range(0, 6, 1):
            step = f"{step:03d}"
            output_date = folder_rundate + datetime.timedelta(hours=int(step))
            getfile = Thread(
                target=downloadGrib_hindcastsection_NomadsFilter,
                args=(step, folder_rundate, output_date, working_dir),
            )
            list_threads.append(getfile)
            list_output_files.append(output_date)
            getfile.start()

    for t in list_threads:
        t.join()  # Wait until thread terminates its task
    time.sleep(10)  # to avoid the block from abusive download


def download_NomadsFilter(current_run_date, step_list, working_dir):
    log.info(
        f'Downloading from NOMADS filter page: {current_run_date}, {step_list}, {working_dir}'
    )
    # parellize the download process
    list_threads = []
    counter = 0
    for step in step_list:
        step = f"{step:03d}"
        output_date = current_run_date + datetime.timedelta(hours=int(step))
        getfile = Thread(
            target=downloadGrib_NomadsFilter,
            args=(step, current_run_date, output_date, working_dir),
        )
        list_threads.append(getfile)
        getfile.start()
        counter += 1

        if counter == 10:
            for t in list_threads:
                t.join()  # Wait until thread terminates its task
            counter = 0
            list_threads = []
            time.sleep(5)  # to avoid the block from abusive download


def check_gfs_ready_OpenDataAWS(bucket, current_run_date, run_horizon):
    """
    Checks if the given run is ready for processing. This is accomplished by
    checking that the run has all of the files needed in the given s3 bucket.

    :parameters: bucket: S3 bucket to check for file objects.
    :parameters: run_horizon: FORECAST | HINDCAST
    :parameters: run: Model run time (YYYYMMDDHH) to check.

    Returns:
        True if all files exist in the s3 bucket for the model_run.
        False if the model_run was already processed, if the some files exist
        in s3, but not all, or if there are no files in s3.
    """

    log.info(
        f'Checking GFS ready on OpenData: {bucket}, {current_run_date}, {run_horizon}'
    )
    GRIB_REGEX = re.compile(r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f(\d{3})$')
    IDX_REGEX = re.compile(
        r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f(\d{3})\.idx$'
    )

    if run_horizon == 'FORECAST':
        expected_timestamps = list(chain(range(121), range(123, 385, 3)))
    else:
        expected_timestamps = list(range(6))

    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')
    run_date = datetime.datetime.strftime(current_run_date, '%Y%m%d')
    run_hour = datetime.datetime.strftime(current_run_date, '%H')
    object_keys = [
        item['Key']
        for item in chain.from_iterable(
            response['Contents']
            for response in paginator.paginate(
                Bucket=bucket, Prefix=f'gfs.{run_date}/{run_hour}/atmos'
            )
            if 'Contents' in response
        )
    ]
    grib_matches = [GRIB_REGEX.search(key) for key in object_keys]
    grib_timestamps = [int(match.group(2)) for match in grib_matches if match]
    idx_matches = [IDX_REGEX.search(key) for key in object_keys]
    idx_timestamps = [int(match.group(2)) for match in idx_matches if match]

    if run_horizon == 'FORECAST':
        pass
    else:
        grib_timestamps = grib_timestamps[0:6]
        idx_timestamps = idx_timestamps[0:6]

    return grib_timestamps == idx_timestamps == expected_timestamps


def downloadGrib_OpenData(current_run_date, working_dir, step_list):
    """
    Download GFS gribs from NOAA Open Data on AWS.

    :parameters: step
    :parameters: current_run_date
    :parameters: working directory
    :return:
    :output: grib_files

    Francisco
    Apr2021
    """

    log.info(
        f'Downloading gribs from NOAA OpenData bucket: {current_run_date}, {working_dir}, {step_list}'
    )

    # AWS configuration
    region = 'us-east-1'
    bucketname = 'noaa-gfs-bdp-pds'
    session = boto3.Session()
    s3 = session.resource('s3', region_name=region)

    current_daterun_str = datetime.datetime.strftime(
        current_run_date, '%Y%m%d'
    )
    current_run_str = datetime.datetime.strftime(current_run_date, '%H')

    for step in step_list:

        output_date = current_run_date + datetime.timedelta(hours=int(step))
        output_date_str = datetime.datetime.strftime(output_date, '%Y%m%d%H')

        step = f"{step:03d}"

        try:
            s3.Bucket(bucketname).download_file(
                'gfs.'
                + current_daterun_str
                + '/'
                + current_run_str
                + '/atmos/gfs.t'
                + current_run_str
                + 'z.pgrb2.0p25.f'
                + step
                + '.idx',
                working_dir + '/gfs.' + step + '.idx',
            )

            gfs_indx = [('UGRD', '10 m above ground')]
            idxfile = open(working_dir + '/gfs.' + step + '.idx', mode='r')
            raw_inventory = idxfile.read()
            idxfile.close()
            parsed_inventory = [
                line.split(':') for line in raw_inventory.split('\n')[:-1]
            ]
            for i, params in enumerate(parsed_inventory):
                if (params[3], params[4]) in gfs_indx:
                    byte_start = int(params[1])
                    byte_end = (
                        int(parsed_inventory[i + 1][1]) - 1
                        if i < len(parsed_inventory) - 1
                        else None
                    )

                    key = (
                        'gfs.'
                        + current_daterun_str
                        + '/'
                        + current_run_str
                        + '/atmos/gfs.t'
                        + current_run_str
                        + 'z.pgrb2.0p25.f'
                        + step
                    )
                    Bytes_range = (
                        'bytes=' + str(byte_start) + '-' + str(byte_end)
                    )

                    client = boto3.client('s3', region_name=region)
                    resp = client.get_object(
                        Bucket=bucketname, Key=key, Range=Bytes_range
                    )

                    file_string = params[3] + '_' + output_date_str

                    tempfile = open(file_string + '.grb2', 'wb')
                    tempfile.write(resp['Body'].read())
                    tempfile.close()

                    os.system(
                        '/usr/local/bin/wgrib2 '
                        + working_dir
                        + '/'
                        + file_string
                        + '.grb2'
                        + ' -match "UGRD" -netcdf '
                        + working_dir
                        + '/data_'
                        + file_string
                        + '.nc'
                    )
                    os.remove(working_dir + '/' + file_string + '.grb2')

            gfs_indx = [('VGRD', '10 m above ground')]
            idxfile = open(working_dir + '/gfs.' + step + '.idx', mode='r')
            raw_inventory = idxfile.read()
            idxfile.close()
            parsed_inventory = [
                line.split(':') for line in raw_inventory.split('\n')[:-1]
            ]
            for i, params in enumerate(parsed_inventory):
                if (params[3], params[4]) in gfs_indx:
                    byte_start = int(params[1])
                    byte_end = (
                        int(parsed_inventory[i + 1][1]) - 1
                        if i < len(parsed_inventory) - 1
                        else None
                    )

                    key = (
                        'gfs.'
                        + current_daterun_str
                        + '/'
                        + current_run_str
                        + '/atmos/gfs.t'
                        + current_run_str
                        + 'z.pgrb2.0p25.f'
                        + step
                    )
                    Bytes_range = (
                        'bytes=' + str(byte_start) + '-' + str(byte_end)
                    )

                    client = boto3.client('s3', region_name=region)
                    resp = client.get_object(
                        Bucket=bucketname, Key=key, Range=Bytes_range
                    )

                    file_string = params[3] + '_' + output_date_str

                    tempfile = open(file_string + '.grb2', 'wb')
                    tempfile.write(resp['Body'].read())
                    tempfile.close()

                    os.system(
                        '/usr/local/bin/wgrib2 '
                        + working_dir
                        + '/'
                        + file_string
                        + '.grb2'
                        + ' -match "VGRD" -netcdf '
                        + working_dir
                        + '/data_'
                        + file_string
                        + '.nc'
                    )
                    os.remove(working_dir + '/' + file_string + '.grb2')

            if step == '000':

                gfs_indx = [('ICEC', 'surface')]
                idxfile = open(working_dir + '/gfs.' + step + '.idx', mode='r')
                raw_inventory = idxfile.read()
                idxfile.close()
                parsed_inventory = [
                    line.split(':') for line in raw_inventory.split('\n')[:-1]
                ]
                for i, params in enumerate(parsed_inventory):
                    if (params[3], params[4]) in gfs_indx:
                        byte_start = int(params[1])
                        byte_end = (
                            int(parsed_inventory[i + 1][1]) - 1
                            if i < len(parsed_inventory) - 1
                            else None
                        )

                        key = (
                            'gfs.'
                            + current_daterun_str
                            + '/'
                            + current_run_str
                            + '/atmos/gfs.t'
                            + current_run_str
                            + 'z.pgrb2.0p25.f'
                            + step
                        )
                        Bytes_range = (
                            'bytes=' + str(byte_start) + '-' + str(byte_end)
                        )

                        client = boto3.client('s3', region_name=region)
                        resp = client.get_object(
                            Bucket=bucketname, Key=key, Range=Bytes_range
                        )

                        file_string = params[3]

                        tempfile = open(file_string + '.grb2', 'wb')
                        tempfile.write(resp['Body'].read())
                        tempfile.close()

                        os.system(
                            '/usr/local/bin/wgrib2 '
                            + working_dir
                            + '/'
                            + file_string
                            + '.grb2'
                            + ' -match "ICEC" -netcdf '
                            + working_dir
                            + '/ice.nc'
                        )
                        os.remove(working_dir + '/' + file_string + '.grb2')

            os.remove(working_dir + '/gfs.' + step + '.idx')

        except:
            pass


def downloadGrib_hindcastsection_OpenData(current_run_date, working_dir):
    """
    Download GFS gribs from NOAA Open Data on AWS.

    :parameters: current_run_date
    :parameters: working directory
    :return:
    :output: grib_files

    Francisco
    Apr2021
    """

    log.info(
        f'Downloading hindcast section from OpenData: {current_run_date}, {working_dir}'
    )
    # AWS configuration
    region = 'us-east-1'
    bucketname = 'noaa-gfs-bdp-pds'
    session = boto3.Session()
    s3 = session.resource('s3', region_name=region)

    for step in range(0, 6, 1):
        step = f"{step:03d}"
        output_date = current_run_date + datetime.timedelta(hours=int(step))

        current_daterun_str = datetime.datetime.strftime(
            current_run_date, '%Y%m%d'
        )
        current_run_str = datetime.datetime.strftime(current_run_date, '%H')
        output_date_str = datetime.datetime.strftime(output_date, '%Y%m%d%H')

        try:
            s3.Bucket(bucketname).download_file(
                'gfs.'
                + current_daterun_str
                + '/'
                + current_run_str
                + '/atmos/gfs.t'
                + current_run_str
                + 'z.pgrb2.0p25.f'
                + step
                + '.idx',
                working_dir + '/gfs.' + output_date_str + '.idx',
            )

            gfs_indx = [('UGRD', '10 m above ground')]
            idxfile = open(
                working_dir + '/gfs.' + output_date_str + '.idx', mode='r'
            )
            raw_inventory = idxfile.read()
            idxfile.close()
            parsed_inventory = [
                line.split(':') for line in raw_inventory.split('\n')[:-1]
            ]
            for i, params in enumerate(parsed_inventory):
                if (params[3], params[4]) in gfs_indx:
                    byte_start = int(params[1])
                    byte_end = (
                        int(parsed_inventory[i + 1][1]) - 1
                        if i < len(parsed_inventory) - 1
                        else None
                    )

                    key = (
                        'gfs.'
                        + current_daterun_str
                        + '/'
                        + current_run_str
                        + '/atmos/gfs.t'
                        + current_run_str
                        + 'z.pgrb2.0p25.f'
                        + step
                    )
                    log.info(key)
                    Bytes_range = (
                        'bytes=' + str(byte_start) + '-' + str(byte_end)
                    )

                    client = boto3.client('s3', region_name=region)
                    resp = client.get_object(
                        Bucket=bucketname, Key=key, Range=Bytes_range
                    )

                    file_string = params[3] + '_' + output_date_str

                    tempfile = open(file_string + '.grb2', 'wb')
                    tempfile.write(resp['Body'].read())
                    tempfile.close()

                    os.system(
                        '/usr/local/bin/wgrib2 '
                        + working_dir
                        + '/'
                        + file_string
                        + '.grb2'
                        + ' -match "UGRD" -netcdf '
                        + working_dir
                        + '/data_'
                        + file_string
                        + '.nc'
                    )
                    os.remove(working_dir + '/' + file_string + '.grb2')

            gfs_indx = [('VGRD', '10 m above ground')]
            idxfile = open(
                working_dir + '/gfs.' + output_date_str + '.idx', mode='r'
            )
            raw_inventory = idxfile.read()
            idxfile.close()
            parsed_inventory = [
                line.split(':') for line in raw_inventory.split('\n')[:-1]
            ]
            for i, params in enumerate(parsed_inventory):
                if (params[3], params[4]) in gfs_indx:
                    byte_start = int(params[1])
                    byte_end = (
                        int(parsed_inventory[i + 1][1]) - 1
                        if i < len(parsed_inventory) - 1
                        else None
                    )

                    key = (
                        'gfs.'
                        + current_daterun_str
                        + '/'
                        + current_run_str
                        + '/atmos/gfs.t'
                        + current_run_str
                        + 'z.pgrb2.0p25.f'
                        + step
                    )
                    log.info(key)
                    Bytes_range = (
                        'bytes=' + str(byte_start) + '-' + str(byte_end)
                    )

                    client = boto3.client('s3', region_name=region)
                    resp = client.get_object(
                        Bucket=bucketname, Key=key, Range=Bytes_range
                    )

                    file_string = params[3] + '_' + output_date_str

                    tempfile = open(file_string + '.grb2', 'wb')
                    tempfile.write(resp['Body'].read())
                    tempfile.close()

                    os.system(
                        '/usr/local/bin/wgrib2 '
                        + working_dir
                        + '/'
                        + file_string
                        + '.grb2'
                        + ' -match "VGRD" -netcdf '
                        + working_dir
                        + '/data_'
                        + file_string
                        + '.nc'
                    )
                    os.remove(working_dir + '/' + file_string + '.grb2')

            os.remove(working_dir + '/gfs.' + output_date_str + '.idx')

        except:
            pass


def get_grid_wind_data(netcdf_file_U, netcdf_file_V, working_dir):
    """
    Export data from netcdf and read the U anc V wind data.

    :parameters: netcdf_file_U
    :parameters: netcdf_file_V
    :parameters: current_run_date
    :return: ugrd, vgrd
    :output:

    Francisco
    Apr2021
    """

    log.info(
        f'Getting grid wind data: {netcdf_file_U}, {netcdf_file_V}, {working_dir}'
    )
    f = netCDF4.Dataset(
        working_dir + '/' + netcdf_file_U, 'r', format='NETCDF4'
    )
    ugrd = f.variables['UGRD_10maboveground'][0][:][:]
    f.close()
    os.remove(working_dir + '/' + netcdf_file_U)

    h = netCDF4.Dataset(
        working_dir + '/' + netcdf_file_V, 'r', format='NETCDF4'
    )
    vgrd = h.variables['VGRD_10maboveground'][0][:][:]
    h.close()
    os.remove(working_dir + '/' + netcdf_file_V)
    return ugrd, vgrd


def get_grid_ice_data(working_dir):
    """
    Export data from netcdf.

    :parameters: netcdf_file
    :return: ice
    :output:

    Francisco
    Apr2021
    """
    log.info(f'Getting grid ice data: {working_dir}')
    f = netCDF4.Dataset(working_dir + '/ice.nc', 'r', format='NETCDF4')
    ice = f.variables['ICEC_surface'][0][:][:]
    f.close()
    os.remove(working_dir + '/ice.nc')
    return ice


def rescale_wind_to_halfdegree(
    wind_x_25, wind_y_25, lon_25, lat_25, lon_50, lat_50
):
    """
    WW3 does not allow for the input data hato have a higher resolution than the model grid
    resolution, so it is necessary to rescale the input data to meet the coarser model resolution.
    We could download the half degree data but it is simpler to just rescale the quarter degree.

    The flag_error will be used as a quality control for the input data, later used in the warning
    system, preventing runs with bad input data.

    :parameters: wind_x_25, wind_y_25, lon_25, lat_25, lon_50, lat_50
    :return: wind_x_50, wind_y_50, flag_error
    :output:

    Francisco
    Apr2021
    """
    index_lat = []
    for value in lat_50:
        index = np.where(lat_25 == value)
        index_lat.append(index[0][0])
    index_lon = []
    for value in lon_50:
        index = np.where(lon_25 == value)
        index_lon.append(index[0][0])
    wind_x_50 = np.zeros((lat_50.shape[0], lon_50.shape[0]))
    wind_y_50 = np.zeros((lat_50.shape[0], lon_50.shape[0]))
    count_lat = 0
    for idx_lat in index_lat:
        count_lon = 0
        for idx_lon in index_lon:
            wind_x_50[count_lat, count_lon] = wind_x_25[idx_lat, idx_lon]
            wind_y_50[count_lat, count_lon] = wind_y_25[idx_lat, idx_lon]
            count_lon += 1
        count_lat += 1
    wind_global = np.concatenate((wind_x_25, wind_y_25), axis=0)
    check_for_nan = np.isnan(np.min(wind_global))
    flag_error = list()
    if check_for_nan:
        flag_error.append('NaN value identified on wind file')
    check_for_zeros = np.where(wind_global == 0)[0]
    if len(check_for_zeros) > 50000:
        flag_error.append('Abnormal number of zeros')
    return wind_x_50, wind_y_50, flag_error


def rescale_ice_to_halfdegree(ice_25, lon_25, lat_25, lon_50, lat_50):
    """
    WW3 does not allow for the input data hato have a higher resolution than the model grid
    resolution, so it is necessary to rescale the input data to meet the coarser model resolution.
    We could download the half degree data but it is simpler to just rescale the quarter degree.

    The flag_error will be used as a quality control for the input data, later used in the warning
    system, preventing runs with bad input data.

    :parameters: ice_25, lon_25, lat_25, lon_50, lat_50
    :return: ice_50, flag_error
    :output:

    Francisco
    Apr2021
    """
    index_lat = []
    for value in lat_50:
        index = np.where(lat_25 == value)
        index_lat.append(index[0][0])
    index_lon = []
    for value in lon_50:
        index = np.where(lon_25 == value)
        index_lon.append(index[0][0])
    ice_50 = np.zeros((lat_50.shape[0], lon_50.shape[0]))
    count_lat = 0
    for idx_lat in index_lat:
        count_lon = 0
        for idx_lon in index_lon:
            ice_50[count_lat, count_lon] = ice_25[idx_lat, idx_lon]
            count_lon += 1
        count_lat += 1
    check_for_nan = np.isnan(np.min(ice_25))
    flag_error = list()
    if check_for_nan:
        flag_error.append('NaN value identified on ice file')
    return ice_50, flag_error


def create_input_files(current_run_date, working_dir):
    """
    Main routine to control the input data download process and creating the input files.

    Mechanism:
        - Download individual grib files
        - Export data from grib files to netcdf
        - Rescale quarter degree data to half degree
        - Write output files

    The wind files have the following structure

    Datetime step 000 [yyyymmdd HHMMSS]
    Wind_x step 000
    Wind_y step 000
    Datetime step 001 [yyyymmdd HHMMSS]
    Wind_x step 001
    Wind_y step 001 ...

    The ice files have the following structure:

    Ice_coverage (unique field of data)

    :parameters: current_run_date, working_dir
    :return: flag_error_wind, flag_error_ice
    :output: w_gfs_50, w_gfs_25, ice_gfs_50, ice_gfs_25, flag_missing_files

    Francisco
    Apr2021
    """

    log.info(f'Creating input files: {current_run_date}, {working_dir}')
    os.chdir(working_dir)

    flag_missing_files = list()

    lon_25 = np.arange(0, 360, 0.25)
    lat_25 = np.arange(-90, 90.25, 0.25)
    lon_50 = np.arange(0, 360, 0.5)
    lat_50 = np.arange(-90, 90.5, 0.5)

    # included one extra step because range does not includes the last value
    step_list = list(itertools.chain(range(0, 120, 1), range(120, 387, 3)))
    # step_list = list(range(0, 5, 1))

    # initialize the output files
    log.info('Initializing output files')
    f_handle = open(working_dir + '/w_gfs_50', 'wb')
    f_handle = open(working_dir + '/w_gfs_25', 'wb')

    # Hindcast part - download from previous GFS runs
    log.info('Hindcast - download from previous GFS runs')
    for day_step in np.arange(0.5, 0, -0.25):
        folder_rundate = current_run_date - datetime.timedelta(
            hours=int(day_step * 24)
        )

        gfs_flag = check_gfs_ready_OpenDataAWS(
            'noaa-gfs-bdp-pds', folder_rundate, 'HINDCAST'
        )
        if gfs_flag:
            downloadGrib_hindcastsection_OpenData(folder_rundate, working_dir)
        else:
            download_hindcastsection_NomadsFilter(
                current_run_date, working_dir
            )

    # Forecast part - download from current GFS run
    gfs_flag = check_gfs_ready_OpenDataAWS(
        'noaa-gfs-bdp-pds', current_run_date, 'FORECAST'
    )
    if gfs_flag:
        downloadGrib_OpenData(current_run_date, working_dir, step_list)
    else:
        download_NomadsFilter(current_run_date, step_list, working_dir)

    # Read Hindcast files:
    log.info('Reading hindcast files')
    for step in range(-12, 0, 1):
        try:
            date_string = current_run_date + datetime.timedelta(
                hours=int(step)
            )
            step_file = date_string.strftime("%Y%m%d%H")
            date_string = date_string.strftime("%Y%m%d %H%M%S")
            log.info(step_file)
            wind_x_25, wind_y_25 = get_grid_wind_data(
                'data_UGRD_' + step_file + '.nc',
                'data_VGRD_' + step_file + '.nc',
                working_dir,
            )
            wind_x_50, wind_y_50, flag_error_wind = rescale_wind_to_halfdegree(
                wind_x_25, wind_y_25, lon_25, lat_25, lon_50, lat_50
            )

            f_handle = open(working_dir + '/w_gfs_50', 'ab')
            np.savetxt(f_handle, np.array(date_string).reshape(1), fmt='%s')
            np.savetxt(f_handle, wind_x_50, fmt='%1.2f')
            np.savetxt(f_handle, wind_y_50, fmt='%1.2f')
            f_handle.close()
            f_handle = open(working_dir + '/w_gfs_25', 'ab')
            np.savetxt(f_handle, np.array(date_string).reshape(1), fmt='%s')
            np.savetxt(f_handle, wind_x_25, fmt='%1.2f')
            np.savetxt(f_handle, wind_y_25, fmt='%1.2f')
            f_handle.close()
        except:
            flag_missing_files.append(
                'File is missing: gfs.' + step_file + '.grb2'
            )

    # Read Forecast files:
    for step in step_list:
        try:
            date_string = current_run_date + datetime.timedelta(
                hours=int(step)
            )
            step_file = date_string.strftime("%Y%m%d%H")
            date_string = date_string.strftime("%Y%m%d %H%M%S")
            log.info(step_file)

            wind_x_25, wind_y_25 = get_grid_wind_data(
                'data_UGRD_' + step_file + '.nc',
                'data_VGRD_' + step_file + '.nc',
                working_dir,
            )
            wind_x_50, wind_y_50, flag_error_wind = rescale_wind_to_halfdegree(
                wind_x_25, wind_y_25, lon_25, lat_25, lon_50, lat_50
            )
            f_handle = open(working_dir + '/w_gfs_50', 'ab')
            np.savetxt(f_handle, np.array(date_string).reshape(1), fmt='%s')
            np.savetxt(f_handle, wind_x_50, fmt='%1.2f')
            np.savetxt(f_handle, wind_y_50, fmt='%1.2f')
            f_handle.close()
            f_handle = open(working_dir + '/w_gfs_25', 'ab')
            np.savetxt(f_handle, np.array(date_string).reshape(1), fmt='%s')
            np.savetxt(f_handle, wind_x_25, fmt='%1.2f')
            np.savetxt(f_handle, wind_y_25, fmt='%1.2f')
            f_handle.close()
        except:
            flag_missing_files.append(
                'File is missing: gfs.' + step_file + '.grb2'
            )

    # Read Ice Data
    try:
        date_string = datetime.datetime.strftime(
            current_run_date, '%Y%m%d %H%M%S'
        )
        ice_25 = get_grid_ice_data(working_dir)
        ice_50, flag_error_ice = rescale_ice_to_halfdegree(
            ice_25, lon_25, lat_25, lon_50, lat_50
        )
        f_handle = open(working_dir + '/ice_gfs_50', 'wb')
        np.savetxt(f_handle, np.array(date_string).reshape(1), fmt='%s')
        np.savetxt(f_handle, ice_50, fmt='%1.2f')
        f_handle.close()
        f_handle = open(working_dir + '/ice_gfs_25', 'wb')
        np.savetxt(f_handle, np.array(date_string).reshape(1), fmt='%s')
        np.savetxt(f_handle, ice_25, fmt='%1.2f')
        f_handle.close()
    except:
        flag_missing_files.append('File is missing: ICE')

    return flag_missing_files


def create_sat_files(current_run_date, working_dir, assimilation_dir):
    """
    Main routine to control the satellite input data download process and creating the input files.
    Data is downloaded from Copernicus ftp.

    Information about the data at:

    http://marine.copernicus.eu/new-satellite-based-wave-product-released/

    Mechanism:
        - Evaluate what files are within the limits we are looking for
        - Download individual netcdf files
        - Export data from netcdf files
        - Remove data that is not within the limits or is flagged as wrong
        - Write output files

    The sat files have the following structure:

    Timestep in Epoch, lon, lat, sigH [m]


    :parameters: current_run_date, working_dir
    :return:
    :output: sat_data.dat

    Francisco
    Aug2019
    """

    log.info(
        f'Creating satellite files: {current_run_date}, {working_dir}, {assimilation_dir}'
    )

    os.chdir(working_dir)

    username_ftp = 'fsilva'
    password_ftp = 'Oceano$1'

    low_limite_date = current_run_date - datetime.timedelta(hours=2.5)
    upper_limite_date = current_run_date + datetime.timedelta(hours=2.5)

    low_limite_date_download = current_run_date - datetime.timedelta(hours=3)
    upper_limite_date_download = current_run_date + datetime.timedelta(hours=3)

    file_list = []

    datasets_list = ['al', 'c2', 'cfo', 'h2b', 'j3', 's3a', 's3b']

    for dataset in datasets_list:

        dir_lower_limit = (
            'Core/WAVE_GLO_WAV_L3_SWH_NRT_OBSERVATIONS_014_001/dataset-wav-alti-l3-swh-rt-global-'
            + dataset
            + '/'
            + low_limite_date_download.strftime('%Y')
            + '/'
            + low_limite_date_download.strftime('%m')
        )
        dir_upper_limit = (
            'Core/WAVE_GLO_WAV_L3_SWH_NRT_OBSERVATIONS_014_001/dataset-wav-alti-l3-swh-rt-global-'
            + dataset
            + '/'
            + upper_limite_date_download.strftime('%Y')
            + '/'
            + upper_limite_date_download.strftime('%m')
        )

        ftp_error = []
        if dir_lower_limit == dir_upper_limit:
            try:
                ftpcon = ftplib.FTP()
                ftpcon.connect('nrt.cmems-du.eu')
                ftpcon.login(user=username_ftp, passwd=password_ftp)
                ftpcon.cwd(dir_lower_limit)
                files = ftpcon.nlst()
                for sat_file in files:
                    alpha = sat_file.find('_' + dataset + '_')
                    file_dates_string = sat_file[
                        alpha + len(dataset) + 2 : alpha + len(dataset) + 33
                    ]
                    start_file_date = datetime.datetime.strptime(
                        file_dates_string[0:14], '%Y%m%dT%H%M%S'
                    )
                    end_file_date = datetime.datetime.strptime(
                        file_dates_string[16:], '%Y%m%dT%H%M%S'
                    )
                    if (
                        start_file_date >= low_limite_date_download
                        and end_file_date <= upper_limite_date_download
                    ):
                        log.info(
                            f'Downloading satellite data file: {sat_file}'
                        )
                        file_list.append(sat_file)
                        ftpcon.retrbinary(
                            "RETR " + sat_file, open(sat_file, 'wb').write
                        )
                ftpcon.quit()
            except ftplib.all_errors as e:
                ftp_error = e
        else:
            try:
                ftpcon = ftplib.FTP()
                ftpcon.connect('nrt.cmems-du.eu')
                ftpcon.login(user=username_ftp, passwd=password_ftp)
                ftpcon.cwd(dir_lower_limit)
                files = ftpcon.nlst()
                for sat_file in files:
                    alpha = sat_file.find('_' + dataset + '_')
                    file_dates_string = sat_file[
                        alpha + len(dataset) + 2 : alpha + len(dataset) + 33
                    ]
                    start_file_date = datetime.datetime.strptime(
                        file_dates_string[0:14], '%Y%m%dT%H%M%S'
                    )
                    end_file_date = datetime.datetime.strptime(
                        file_dates_string[16:], '%Y%m%dT%H%M%S'
                    )
                    if (
                        start_file_date >= low_limite_date_download
                        and end_file_date <= upper_limite_date_download
                    ):
                        log.info(
                            f'Downloading satellite data file: {sat_file}'
                        )
                        file_list.append(sat_file)
                        ftpcon.retrbinary(
                            "RETR " + sat_file, open(sat_file, 'wb').write
                        )
                ftpcon.quit()
            except ftplib.all_errors as e:
                ftp_error = e
            try:
                ftpcon = ftplib.FTP()
                ftpcon.connect('nrt.cmems-du.eu')
                ftpcon.login(user=username_ftp, passwd=password_ftp)
                ftpcon.cwd(dir_upper_limit)
                files = ftpcon.nlst()
                for sat_file in files:
                    alpha = sat_file.find('_' + dataset + '_')
                    file_dates_string = sat_file[
                        alpha + len(dataset) + 2 : alpha + len(dataset) + 33
                    ]
                    start_file_date = datetime.datetime.strptime(
                        file_dates_string[0:14], '%Y%m%dT%H%M%S'
                    )
                    end_file_date = datetime.datetime.strptime(
                        file_dates_string[16:], '%Y%m%dT%H%M%S'
                    )
                    if (
                        start_file_date >= low_limite_date_download
                        and end_file_date <= upper_limite_date_download
                    ):
                        log.info(
                            f'Downloading satellite data file: {sat_file}'
                        )
                        file_list.append(sat_file)
                        ftpcon.retrbinary(
                            "RETR " + sat_file, open(sat_file, 'wb').write
                        )
                ftpcon.quit()
            except ftplib.all_errors as e:
                ftp_error = e
        if ftp_error:
            log.info(' ')
            log.info(
                '##################################################################'
            )
            log.info(
                '##################################################################'
            )
            log.info(' ')
            log.info(ftp_error)
            log.info(' ')
            log.info(
                '##################################################################'
            )
            log.info(
                '##################################################################'
            )
            log.info(' Running The download process again')
            log.info(' ')

            ftp_error = []
            if dir_lower_limit == dir_upper_limit:
                try:
                    ftpcon = ftplib.FTP()
                    ftpcon.connect('nrt.cmems-du.eu')
                    ftpcon.login(user=username_ftp, passwd=password_ftp)
                    ftpcon.cwd(dir_lower_limit)
                    files = ftpcon.nlst()
                    for sat_file in files:
                        alpha = sat_file.find('_' + dataset + '_')
                        file_dates_string = sat_file[
                            alpha
                            + len(dataset)
                            + 2 : alpha
                            + len(dataset)
                            + 33
                        ]
                        start_file_date = datetime.datetime.strptime(
                            file_dates_string[0:14], '%Y%m%dT%H%M%S'
                        )
                        end_file_date = datetime.datetime.strptime(
                            file_dates_string[16:], '%Y%m%dT%H%M%S'
                        )
                        if (
                            start_file_date >= low_limite_date_download
                            and end_file_date <= upper_limite_date_download
                        ):
                            log.info(
                                f'Downloading satellite data file: {sat_file}'
                            )
                            file_list.append(sat_file)
                            ftpcon.retrbinary(
                                "RETR " + sat_file, open(sat_file, 'wb').write
                            )
                    ftpcon.quit()
                except ftplib.all_errors as e:
                    ftp_error = e
            else:
                try:
                    ftpcon = ftplib.FTP()
                    ftpcon.connect('nrt.cmems-du.eu')
                    ftpcon.login(user=username_ftp, passwd=password_ftp)
                    ftpcon.cwd(dir_lower_limit)
                    files = ftpcon.nlst()
                    for sat_file in files:
                        alpha = sat_file.find('_' + dataset + '_')
                        file_dates_string = sat_file[
                            alpha
                            + len(dataset)
                            + 2 : alpha
                            + len(dataset)
                            + 33
                        ]
                        start_file_date = datetime.datetime.strptime(
                            file_dates_string[0:14], '%Y%m%dT%H%M%S'
                        )
                        end_file_date = datetime.datetime.strptime(
                            file_dates_string[16:], '%Y%m%dT%H%M%S'
                        )
                        if (
                            start_file_date >= low_limite_date_download
                            and end_file_date <= upper_limite_date_download
                        ):
                            log.info(
                                f'Downloading satellite data file: {sat_file}'
                            )
                            file_list.append(sat_file)
                            ftpcon.retrbinary(
                                "RETR " + sat_file, open(sat_file, 'wb').write
                            )
                    ftpcon.quit()
                except ftplib.all_errors as e:
                    ftp_error = e
                try:
                    ftpcon = ftplib.FTP()
                    ftpcon.connect('nrt.cmems-du.eu')
                    ftpcon.login(user=username_ftp, passwd=password_ftp)
                    ftpcon.cwd(dir_upper_limit)
                    files = ftpcon.nlst()
                    for sat_file in files:
                        alpha = sat_file.find('_' + dataset + '_')
                        file_dates_string = sat_file[
                            alpha
                            + len(dataset)
                            + 2 : alpha
                            + len(dataset)
                            + 33
                        ]
                        start_file_date = datetime.datetime.strptime(
                            file_dates_string[0:14], '%Y%m%dT%H%M%S'
                        )
                        end_file_date = datetime.datetime.strptime(
                            file_dates_string[16:], '%Y%m%dT%H%M%S'
                        )
                        if (
                            start_file_date >= low_limite_date_download
                            and end_file_date <= upper_limite_date_download
                        ):
                            log.info(
                                f'Downloading satellite data file: {sat_file}'
                            )
                            file_list.append(sat_file)
                            ftpcon.retrbinary(
                                "RETR " + sat_file, open(sat_file, 'wb').write
                            )
                    ftpcon.quit()
                except ftplib.all_errors as e:
                    ftp_error = e
            if ftp_error:
                log.info(' ')
                log.info(
                    '##########################################################'
                )
                log.info(
                    '##########################################################'
                )
                log.info(' ')
                log.info(ftp_error)
                log.info(' ')
                log.info(
                    '##########################################################'
                )
                log.info(
                    '##########################################################'
                )
                log.info(' Failled for the second time - FILE NOT DOWNLOADED')
                log.info(' ')
                log.info(' ')

    lon = []
    lat = []
    hs = []
    timesteps = []

    for file_sat in file_list:

        check4file = Path(working_dir + '/' + file_sat)
        if check4file.is_file():
            if os.stat(check4file).st_size == 0:
                log.info(f'{file_sat} is empty')
            else:
                netcdf_file_id = netCDF4.Dataset(file_sat)
                time = netcdf_file_id.variables['time'][:]
                t_unit = netcdf_file_id.variables['time'].units
                t_cal = netcdf_file_id.variables['time'].calendar
                lon_temp = []
                lat_temp = []
                hs_temp = []
                timesteps_temp = []
                timesteps_temp[:] = netCDF4.num2date(
                    time,
                    units=t_unit,
                    calendar=t_cal,
                    only_use_cftime_datetimes=False,
                    only_use_python_datetimes=True,
                )
                lon_temp = netcdf_file_id.variables['longitude'][:]  # /1000000
                lat_temp = netcdf_file_id.variables['latitude'][:]  # /1000000
                hs_temp = netcdf_file_id.variables['VAVH'][:]  # /1000
                if len(hs_temp) > 0:
                    pos_remove = np.nonzero(hs_temp > 100)
                    hs_temp = np.delete(hs_temp, pos_remove)
                    timesteps_temp = np.delete(timesteps_temp, pos_remove)
                    lon_temp = np.delete(lon_temp, pos_remove)
                    lat_temp = np.delete(lat_temp, pos_remove)
                if len(hs_temp) > 0:
                    pos_remove = np.nonzero(hs_temp <= 0)
                    hs_temp = np.delete(hs_temp, pos_remove)
                    timesteps_temp = np.delete(timesteps_temp, pos_remove)
                    lon_temp = np.delete(lon_temp, pos_remove)
                    lat_temp = np.delete(lat_temp, pos_remove)
                netcdf_file_id.close()

                os.remove(file_sat)

                lon.extend(lon_temp)
                lat.extend(lat_temp)
                hs.extend(hs_temp)
                timesteps.extend(timesteps_temp)

    pos_remove = []
    counter = 0
    for timestep in timesteps:
        if timestep < low_limite_date or timestep > upper_limite_date:
            pos_remove.append(counter)
        counter += 1

    timesteps = np.delete(timesteps, pos_remove)
    lon = np.delete(lon, pos_remove)
    lat = np.delete(lat, pos_remove)
    hs = np.delete(hs, pos_remove)

    if len(hs) > 0:
        output_file = open(
            assimilation_dir
            + '/sat_data_'
            + current_run_date.strftime('%Y%m%d%H')
            + '.dat',
            'w',
        )
        for k in range(0, len(timesteps)):
            output_file.write(
                "%f %.5f %.5f %.1f\n"
                % (timesteps[k].timestamp(), lon[k], lat[k], hs[k])
            )
        output_file.close()
