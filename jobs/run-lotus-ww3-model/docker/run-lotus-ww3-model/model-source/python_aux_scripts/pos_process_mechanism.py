import csv
import datetime
import glob
import os
import time
from multiprocessing.dummy import Pool as ThreadPool
from shutil import copy2, move, rmtree

import netCDF4  # type: ignore
import numpy as np  # type: ignore
from util.logger import log


def create_restarts(
    model_grid,
    export_local_dir,
    export_shared_dir,
    work_dir,
    current_run_date,
    dt_restart,
    restart_output_length,
):

    restart_start_date = current_run_date + datetime.timedelta(
        seconds=int(dt_restart)
    )
    restart_end_date = current_run_date + datetime.timedelta(
        days=restart_output_length
    )
    step_date = restart_start_date

    log.info('   Creating restart files for ' + model_grid + ' grid')
    log.info(
        '   Screen ouput routed to '
        + export_local_dir
        + '/log/restarts.'
        + model_grid
        + '.out'
    )
    InpFile = open(
        export_local_dir + '/log/restarts.' + model_grid + '.out', 'w'
    )
    nb_rest = 1
    while step_date <= restart_end_date:
        step_date_string = step_date.strftime("%Y%m%d%H")
        nb_rest_string = '%03d' % nb_rest
        InpFile.write(
            'Moving restart'
            + nb_rest_string
            + '.'
            + model_grid
            + ' to restarts/restart.'
            + model_grid
            + '.'
            + step_date_string
            + '\n'
        )
        move(
            export_shared_dir
            + '/'
            + work_dir
            + '/restart'
            + nb_rest_string
            + '.'
            + model_grid,
            export_local_dir
            + '/restarts/restart.'
            + model_grid
            + '.'
            + step_date_string,
        )
        step_date = step_date + datetime.timedelta(seconds=int(dt_restart))
        nb_rest += 1
    InpFile.close()


def read_sigH_nc_file(export_local_dir, netcdf_file):

    os.chdir(export_local_dir + '/data_assimilation')
    sigH_raw_file = netCDF4.Dataset(netcdf_file, 'r', mmap=False)
    sigH_update = sigH_raw_file.variables['hs']

    sigH_update = sigH_update[0][:][:]
    sigH_update[sigH_update > 100] = -99

    f_handle = open('sigH_raw.dat', 'w')
    np.savetxt(f_handle, sigH_update, fmt='%1.2f')
    f_handle.close()
    sigH_raw_file.close()


def merge_out_grd_files(
    input_grids,
    output_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_field,
    model_path,
):

    os.chdir(export_shared_dir + '/model_data')
    model_start_date = datetime.datetime.strftime(
        current_run_date, '%Y%m%d %H%M%S'
    )

    nb_grids = len(input_grids) + 1
    InpFile = open('ww3_gint.inp', 'w')
    InpFile.write('$ WAVEWATCH III Grid Integration input file\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(model_start_date + '  ' + dt_field + '  9999999\n')
    InpFile.write('$\n')
    InpFile.write(str(nb_grids) + '\n')
    InpFile.write('$\n')
    for grid in input_grids:
        InpFile.write("'" + grid + "'\n")
    InpFile.write("'" + output_grid + "'\n")
    InpFile.write('$\n')
    InpFile.write('0\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()
    log.info('   Running ww3_gint for grid ' + output_grid)
    log.info(
        '   Screen ouput routed to '
        + export_local_dir
        + '/log/ww3_gint.'
        + output_grid
        + '.out'
    )

    for grid in input_grids:
        os.symlink(
            export_local_dir + '/pre_process_basefiles/mod_def.' + grid,
            export_shared_dir + '/model_data/mod_def.' + grid,
        )
    os.symlink(
        export_local_dir + '/pre_process_basefiles/mod_def.' + output_grid,
        export_shared_dir + '/model_data/mod_def.' + output_grid,
    )
    os.system(
        model_path
        + '/ww3_gint > '
        + export_local_dir
        + '/log/ww3_gint.'
        + output_grid
        + '.out'
    )
    os.remove('ww3_gint.inp')
    filelist = glob.glob('mod_def.*')
    for f in filelist:
        os.remove(f)
    filelist = glob.glob('*.bin')
    for f in filelist:
        os.remove(f)


def prepare2create_grid_netcdf(
    model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    step,
    dt_field,
    FIELDS_OUT,
    model_path,
):

    step_date = current_run_date + datetime.timedelta(
        seconds=int(dt_field) * step
    )
    step_date_string = step_date.strftime("%Y%m%d %H%M%S")
    step_string = '%03d' % step

    target_work_dir = (
        export_local_dir
        + '/pos_process/grids/'
        + model_grid
        + '/'
        + step_string
    )

    if os.path.exists(target_work_dir):
        rmtree(target_work_dir)
    os.makedirs(target_work_dir)
    os.chdir(target_work_dir)

    InpFile = open('ww3_ounf.inp', 'w')
    InpFile.write('$ WAVEWATCH III Grid output post-processing\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(step_date_string + '  ' + dt_field + '  1\n')
    InpFile.write('$\n')
    InpFile.write('N\n')
    InpFile.write(FIELDS_OUT + '\n')
    InpFile.write('$\n')
    InpFile.write('4 4\n')
    InpFile.write('0 1 2 3 4 5\n')
    InpFile.write('T\n')
    InpFile.write('ww3.' + model_grid + '.\n')
    InpFile.write('10\n')
    InpFile.write('1 1000000000 1 1000000000\n')
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()

    os.symlink(
        export_shared_dir + '/model_data/out_grd.' + model_grid,
        target_work_dir + '/out_grd.ww3',
    )
    os.symlink(
        export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
        target_work_dir + '/mod_def.ww3',
    )
    copy2(model_path + '/ww3_ounf', target_work_dir + '/ww3_ounf')


def create_grid_netcdf(target_work_dir):
    os.system(
        'cd '
        + target_work_dir
        + '; '
        + target_work_dir
        + '/ww3_ounf > '
        + target_work_dir
        + '/log.txt'
    )


def create_grid_output_files(
    model_grid,
    output_name,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    FIELDS_OUT,
    dt_field,
    model_length,
    model_path,
):

    log.info('  Creating grid output files for ' + model_grid + ' grid')

    target_archive_dir = (
        export_local_dir
        + '/archive/grids/'
        + output_name
        + '/'
        + current_run_date.strftime("%Y%m%d%H")
    )
    if not os.path.exists(target_archive_dir):
        os.makedirs(target_archive_dir)

    output_end_date = current_run_date + datetime.timedelta(
        days=int(model_length)
    )
    step_date = current_run_date
    step = 0
    target_work_dir_map = []
    while step_date <= output_end_date:
        prepare2create_grid_netcdf(
            model_grid,
            export_local_dir,
            export_shared_dir,
            current_run_date,
            step,
            dt_field,
            FIELDS_OUT,
            model_path,
        )
        target_work_dir = (
            export_local_dir
            + '/pos_process/grids/'
            + model_grid
            + '/'
            + ('%03d' % step)
        )
        target_work_dir_map.append(target_work_dir)
        step_date = step_date + datetime.timedelta(seconds=int(dt_field))
        step += 1

    pool = ThreadPool(8)
    pool.map(create_grid_netcdf, target_work_dir_map)
    pool.close()
    pool.join()

    for target_work_dir in target_work_dir_map:
        step = target_work_dir[-3:]
        file_date = current_run_date + datetime.timedelta(
            seconds=int(dt_field) * int(step)
        )
        move(
            export_local_dir
            + '/pos_process/grids/'
            + model_grid
            + '/'
            + step
            + '/ww3.'
            + model_grid
            + '.'
            + file_date.strftime("%Y%m%dT%HZ")
            + '.nc',
            target_archive_dir
            + '/ww3_lotus_'
            + output_name
            + '.'
            + current_run_date.strftime("%Y%m%d%H")
            + '.'
            + step
            + '.nc',
        )


def create_grid_output_files_hindcast(
    model_grid,
    output_name,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    target_run_date,
    FIELDS_OUT,
    dt_field,
    model_length,
    model_path,
):

    log.info('  Creating grid output files for ' + model_grid + ' grid')
    target_archive_dir = (
        export_local_dir
        + '/archive/grids/'
        + output_name
        + '/'
        + current_run_date.strftime("%Y%m%d%H")
        + '/hindcast'
    )
    if not os.path.exists(target_archive_dir):
        os.makedirs(target_archive_dir)

    output_end_date = target_run_date + datetime.timedelta(
        hours=int(model_length)
    )
    step_date = target_run_date
    step = 0
    target_work_dir_map = []
    while step_date <= output_end_date:
        prepare2create_grid_netcdf(
            model_grid,
            export_local_dir,
            export_shared_dir,
            target_run_date,
            step,
            dt_field,
            FIELDS_OUT,
            model_path,
        )
        target_work_dir = (
            export_local_dir
            + '/pos_process/grids/'
            + model_grid
            + '/'
            + ('%03d' % step)
        )
        target_work_dir_map.append(target_work_dir)
        step_date = step_date + datetime.timedelta(seconds=int(dt_field))
        step += 1

    pool = ThreadPool(8)
    pool.map(create_grid_netcdf, target_work_dir_map)
    pool.close()
    pool.join()

    for target_work_dir in target_work_dir_map:
        step = target_work_dir[-3:]
        file_date = target_run_date + datetime.timedelta(
            seconds=int(dt_field) * int(step)
        )
        move(
            export_local_dir
            + '/pos_process/grids/'
            + model_grid
            + '/'
            + step
            + '/ww3.'
            + model_grid
            + '.'
            + file_date.strftime("%Y%m%dT%HZ")
            + '.nc',
            target_archive_dir
            + '/ww3_lotus_'
            + output_name
            + '.'
            + file_date.strftime("%Y%m%d%H")
            + '.nc',
        )


def prepare2create_point(
    model_grid,
    export_local_dir,
    export_shared_dir,
    step_date,
    step,
    dt_point,
    point_filetype,
    model_path,
):

    date_string = step_date.strftime("%Y%m%d %H%M%S")
    step = '%03d' % step
    target_work_dir = (
        export_local_dir + '/pos_process/points/' + point_filetype + '/' + step
    )

    if os.path.exists(target_work_dir):
        rmtree(target_work_dir)
    os.makedirs(target_work_dir)
    os.chdir(target_work_dir)

    if point_filetype == 'part':

        InpFile = open('ww3_outp.inp', 'w')
        InpFile.write('$ WAVEWATCH III NETCDF Point output post-processing\n')
        InpFile.write('$ -------------------------------------------\n')
        InpFile.write(date_string + '  ' + dt_point + '  1\n')
        InpFile.write('$\n')
        with open(
            export_local_dir
            + '/base_files/point_outputs_'
            + model_grid
            + '.dat',
            "rt",
            encoding="utf8",
        ) as infile:
            spot_list_file = csv.reader(infile)
            next(spot_list_file)  # skip the header line
            for row in spot_list_file:
                spot_id = row[0]
                spot_bound_flag = row[3]
                if spot_bound_flag == '0':
                    InpFile.write(spot_id + '\n')
        InpFile.write('-1\n')
        InpFile.write('4\n')
        InpFile.write('1 0 0 33 F\n')
        InpFile.write('$\n')
        InpFile.write('$ End of input file\n')
        InpFile.close()

        os.symlink(
            export_shared_dir + '/model_data/out_pnt.' + model_grid,
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/out_pnt.ww3',
        )
        os.symlink(
            export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/mod_def.ww3',
        )
        copy2(
            model_path + '/ww3_outp',
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/ww3_outpoint',
        )

    if point_filetype == 'spec1d':

        InpFile = open('ww3_ounp.inp', 'w')
        InpFile.write('$ WAVEWATCH III NETCDF Point output post-processing\n')
        InpFile.write('$ -------------------------------------------\n')
        InpFile.write(date_string + '  ' + dt_point + '  1\n')
        InpFile.write('$\n')
        with open(
            export_local_dir
            + '/base_files/point_outputs_'
            + model_grid
            + '.dat',
            "rt",
            encoding="utf8",
        ) as infile:
            spot_list_file = csv.reader(infile)
            next(spot_list_file)  # skip the header line
            for row in spot_list_file:
                spot_id = row[0]
                spot_bound_flag = row[3]
                if spot_bound_flag == '0':
                    InpFile.write(spot_id + '\n')
        InpFile.write('-1\n')
        InpFile.write('ww3.' + model_grid + '.\n')
        InpFile.write('10\n')
        InpFile.write('3\n')
        InpFile.write('T 100000\n')
        InpFile.write('1\n')
        InpFile.write('0\n')
        InpFile.write('T\n')
        InpFile.write('$\n')
        InpFile.write('2 1 0\n')
        InpFile.write('$\n')
        InpFile.write('$ End of input file\n')
        InpFile.close()

        os.symlink(
            export_shared_dir + '/model_data/out_pnt.' + model_grid,
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/out_pnt.ww3',
        )
        os.symlink(
            export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/mod_def.ww3',
        )
        copy2(
            model_path + '/ww3_ounp',
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/ww3_outpoint',
        )

    if point_filetype == 'spec2d':

        InpFile = open('ww3_ounp.inp', 'w')
        InpFile.write('$ WAVEWATCH III NETCDF Point output post-processing\n')
        InpFile.write('$ -------------------------------------------\n')
        InpFile.write(date_string + '  ' + dt_point + '  1\n')
        InpFile.write('$\n')
        with open(
            export_local_dir
            + '/base_files/point_outputs_'
            + model_grid
            + '.dat',
            "rt",
            encoding="utf8",
        ) as infile:
            spot_list_file = csv.reader(infile)
            next(spot_list_file)  # skip the header line
            for row in spot_list_file:
                spot_id = row[0]
                spot_bound_flag = row[3]
                if spot_bound_flag == '0':
                    InpFile.write(spot_id + '\n')
        InpFile.write('-1\n')
        InpFile.write('ww3.' + model_grid + '.\n')
        InpFile.write('10\n')
        InpFile.write('3\n')
        InpFile.write('T 100000\n')
        InpFile.write('1\n')
        InpFile.write('0\n')
        InpFile.write('T\n')
        InpFile.write('$\n')
        InpFile.write('3 1 0\n')
        InpFile.write('$\n')
        InpFile.write('$ End of input file\n')
        InpFile.close()

        os.symlink(
            export_shared_dir + '/model_data/out_pnt.' + model_grid,
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/out_pnt.ww3',
        )
        os.symlink(
            export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/mod_def.ww3',
        )
        copy2(
            model_path + '/ww3_ounp',
            export_local_dir
            + '/pos_process/points/'
            + point_filetype
            + '/'
            + step
            + '/ww3_outpoint',
        )


def run_create_point(target_work_dir):
    os.system(
        'cd '
        + target_work_dir
        + '; '
        + target_work_dir
        + '/ww3_outpoint > '
        + target_work_dir
        + '/log_part.txt'
    )


def merge_partition_files(
    model_grid, export_local_dir, current_run_date, dt_point, model_length
):

    max_nb_part = 6  # max number of partitions to read

    number_of_spots = 0
    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            row[0]
            spot_bound_flag = row[3]
            if spot_bound_flag == '0':
                number_of_spots += 1

    output_end_date = (
        current_run_date
        + datetime.timedelta(days=int(model_length))
        + datetime.timedelta(seconds=int(dt_point))
    )  # To also include the last step

    duration_of_the_run = output_end_date - current_run_date
    duration_of_the_run = duration_of_the_run.total_seconds()
    number_of_steps = int(divmod(duration_of_the_run, int(dt_point))[0])

    time_output = np.zeros(int(number_of_steps)) * np.nan

    longitude_output = np.zeros(int(number_of_spots)) * np.nan
    latitude_output = np.zeros(int(number_of_spots)) * np.nan
    depth_output = np.zeros(int(number_of_spots)) * np.nan

    sigh_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )
    tp_output = np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    wvelen_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )
    pdir_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )
    spread_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )
    wind_frac_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )
    wind_speed_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )
    wind_dir_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )
    nb_parts_output = (
        np.zeros((int(number_of_spots), int(number_of_steps))) * np.nan
    )

    sigh_parts_output = (
        np.zeros(
            (int(number_of_spots), int(number_of_steps), int(max_nb_part))
        )
        * np.nan
    )
    tp_parts_output = (
        np.zeros(
            (int(number_of_spots), int(number_of_steps), int(max_nb_part))
        )
        * np.nan
    )
    pdir_parts_output = (
        np.zeros(
            (int(number_of_spots), int(number_of_steps), int(max_nb_part))
        )
        * np.nan
    )
    wvelen_parts_output = (
        np.zeros(
            (int(number_of_spots), int(number_of_steps), int(max_nb_part))
        )
        * np.nan
    )
    spread_parts_output = (
        np.zeros(
            (int(number_of_spots), int(number_of_steps), int(max_nb_part))
        )
        * np.nan
    )
    wind_frac_parts_output = (
        np.zeros(
            (int(number_of_spots), int(number_of_steps), int(max_nb_part))
        )
        * np.nan
    )

    for step in range(number_of_steps):

        step_date = current_run_date + datetime.timedelta(
            seconds=int(dt_point) * step
        )

        time_output[step] = time.mktime(step_date.utctimetuple())

        # Read Part data

        inputfile = open(
            export_local_dir
            + '/pos_process/points/part/ww3.'
            + model_grid
            + '.'
            + step_date.strftime("%Y%m%d%H")
            + '.part'
        )

        for spot_number in range(number_of_spots):

            file_line_header = inputfile.readline()
            file_line_header = file_line_header.replace("'", "")
            file_line_header = file_line_header.split()
            longitude_slice = float(file_line_header[3])
            latitude_slice = float(file_line_header[2])
            spot_number_slice = int(file_line_header[4])
            spot_number_slice = '%06d' % spot_number_slice
            depth_slice = float(file_line_header[6])
            nb_parts_slice = int(file_line_header[5])
            if nb_parts_slice > max_nb_part:
                nb_parts_slice = nb_parts_slice
            wind_speed_slice = float(file_line_header[7])
            wind_dir_slice = float(file_line_header[8])

            sigh_part_slice = np.zeros(int(max_nb_part)) * np.nan
            tp_part_slice = np.zeros(int(max_nb_part)) * np.nan
            pdir_part_slice = np.zeros(int(max_nb_part)) * np.nan
            wvelen_part_slice = np.zeros(int(max_nb_part)) * np.nan
            spread_part_slice = np.zeros(int(max_nb_part)) * np.nan
            wind_frac_part_slice = np.zeros(int(max_nb_part)) * np.nan

            if nb_parts_slice < 0:
                sigh_slice = np.nan
                tp_slice = np.nan
                wvelen_slice = np.nan
                pdir_slice = np.nan
                spread_slice = np.nan
                wind_frac_slice = np.nan

            if nb_parts_slice >= 0:
                # Read overall statistical parameters
                file_line = inputfile.readline()
                file_line = file_line.split()
                sigh_slice = float(file_line[1])
                tp_slice = float(file_line[2])
                wvelen_slice = float(file_line[3])
                pdir_slice = float(file_line[4])
                spread_slice = float(file_line[5])
                wind_frac_slice = float(file_line[6])

                # Read individual swell partition parameters
                for nb_part in range(nb_parts_slice):
                    file_line = inputfile.readline()
                    file_line = file_line.split()
                    if nb_part < max_nb_part:
                        sigh_part_slice[nb_part] = float(file_line[1])
                        tp_part_slice[nb_part] = float(file_line[2])
                        wvelen_part_slice[nb_part] = float(file_line[3])
                        pdir_part_slice[nb_part] = float(file_line[4])
                        spread_part_slice[nb_part] = float(file_line[5])
                        wind_frac_part_slice[nb_part] = float(file_line[6])

            if step == 0:
                longitude_output[spot_number] = longitude_slice
                latitude_output[spot_number] = latitude_slice
                depth_output[spot_number] = depth_slice

            sigh_output[spot_number][step] = sigh_slice
            tp_output[spot_number][step] = tp_slice
            wvelen_output[spot_number][step] = wvelen_slice
            pdir_output[spot_number][step] = pdir_slice
            spread_output[spot_number][step] = spread_slice
            wind_frac_output[spot_number][step] = wind_frac_slice
            wind_speed_output[spot_number][step] = wind_speed_slice
            wind_dir_output[spot_number][step] = wind_dir_slice
            nb_parts_output[spot_number][step] = nb_parts_slice

            sigh_parts_output[spot_number][step][:] = sigh_part_slice
            tp_parts_output[spot_number][step][:] = tp_part_slice
            wvelen_parts_output[spot_number][step][:] = wvelen_part_slice
            pdir_parts_output[spot_number][step][:] = pdir_part_slice
            spread_parts_output[spot_number][step][:] = spread_part_slice
            wind_frac_parts_output[spot_number][step][:] = wind_frac_part_slice

    netcdf_output_file = (
        export_local_dir
        + '/pos_process/points/part/ww3.'
        + model_grid
        + '.part.nc'
    )

    output_netcdf = netCDF4.Dataset(
        netcdf_output_file, 'w', format='NETCDF4_CLASSIC'
    )
    output_netcdf.description = 'Wavewatch III Point Output'

    output_netcdf.createDimension('time', int(number_of_steps))
    output_netcdf.createDimension('nb_of_spots', int(number_of_spots))
    output_netcdf.createDimension('nb_part', int(max_nb_part))

    variable = output_netcdf.createVariable('time', 'i', ('time',))
    variable[:] = time_output
    variable.units = 'Seconds since 1970-01-01 00:00:00'

    variable = output_netcdf.createVariable(
        'longitude', float, ('nb_of_spots',)
    )
    variable[:] = longitude_output
    variable.units = 'Degrees East'

    variable = output_netcdf.createVariable(
        'latitude', float, ('nb_of_spots',)
    )
    variable[:] = latitude_output
    variable.units = 'Degrees North'

    variable = output_netcdf.createVariable('depth', float, ('nb_of_spots',))
    variable[:] = depth_output
    variable.units = 'Meters'

    variable = output_netcdf.createVariable(
        'sigH', float, ('nb_of_spots', 'time')
    )
    variable[:] = sigh_output
    variable.units = 'Meters'
    variable.description = 'Significant Wave Height'

    variable = output_netcdf.createVariable(
        'tp', float, ('nb_of_spots', 'time')
    )
    variable[:] = tp_output
    variable.units = 'Seconds'
    variable.description = 'Peak Period'

    variable = output_netcdf.createVariable(
        'pdir', float, ('nb_of_spots', 'time')
    )
    variable[:] = pdir_output
    variable.units = 'Degrees'
    variable.description = 'Peak Direction'

    variable = output_netcdf.createVariable(
        'wavelength', float, ('nb_of_spots', 'time')
    )
    variable[:] = wvelen_output
    variable.units = 'Meters'
    variable.description = 'Wave Length'

    variable = output_netcdf.createVariable(
        'wave_spread', float, ('nb_of_spots', 'time')
    )
    variable[:] = spread_output
    variable.units = 'Degrees'
    variable.description = 'Directional Wave Spread'

    variable = output_netcdf.createVariable(
        'wind_fraction', float, ('nb_of_spots', 'time')
    )
    variable[:] = wind_frac_output
    variable.units = 'Value between 0 and 1'
    variable.description = 'Wind Fraction'

    variable = output_netcdf.createVariable(
        'wind_speed', float, ('nb_of_spots', 'time')
    )
    variable[:] = wind_speed_output
    variable.units = 'Meters per second'
    variable.description = 'Wind Speed'

    variable = output_netcdf.createVariable(
        'wind_dir', float, ('nb_of_spots', 'time')
    )
    variable[:] = wind_dir_output
    variable.units = 'Degrees'
    variable.description = 'Wind direction (nautical convention)'

    variable = output_netcdf.createVariable(
        'sigH_partition', float, ('nb_of_spots', 'time', 'nb_part')
    )
    variable[:] = sigh_parts_output
    variable.units = 'Meters'
    variable.description = 'Significant Wave Height of Partition'

    variable = output_netcdf.createVariable(
        'tp_partition', float, ('nb_of_spots', 'time', 'nb_part')
    )
    variable[:] = tp_parts_output
    variable.units = 'Seconds'
    variable.description = 'Peak Period of Partition'

    variable = output_netcdf.createVariable(
        'pdir_partition', float, ('nb_of_spots', 'time', 'nb_part')
    )
    variable[:] = pdir_parts_output
    variable.units = 'Degrees'
    variable.description = 'Peak Direction of Partition'

    variable = output_netcdf.createVariable(
        'wavelength_partition', float, ('nb_of_spots', 'time', 'nb_part')
    )
    variable[:] = wvelen_parts_output
    variable.units = 'Meters'
    variable.description = 'Wavelength of Partition'

    variable = output_netcdf.createVariable(
        'wave_spread_partition', float, ('nb_of_spots', 'time', 'nb_part')
    )
    variable[:] = spread_parts_output
    variable.units = 'Degrees'
    variable.description = 'Directional Wave Spread of Partition'

    variable = output_netcdf.createVariable(
        'wind_fraction_partition', float, ('nb_of_spots', 'time', 'nb_part')
    )
    variable[:] = wind_frac_parts_output
    variable.units = 'Value between 0 and 1'
    variable.description = 'Wind Fraction of Partition'

    output_netcdf.close()


def merge_point_output_files(
    model_grid, export_local_dir, current_run_date, dt_point, model_length
):

    target_archive_dir = (
        export_local_dir
        + '/archive/points/'
        + model_grid
        + '/'
        + current_run_date.strftime("%Y%m%d%H")
    )
    if os.path.exists(target_archive_dir):
        rmtree(target_archive_dir)
    os.makedirs(target_archive_dir)

    point_out_file_list = open(
        target_archive_dir + '/point_outputs_' + model_grid + '.dat', 'w'
    )
    number_of_spots = 0
    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            row[0]
            longitude_spot = float(row[1])
            latitude_spot = float(row[2])
            spot_bound_flag = row[3]
            if spot_bound_flag == '0':
                number_of_spots += 1
                point_out_file_list.write(
                    'ww3_lotus_'
                    + model_grid
                    + '.spot_'
                    + ('%07.3f' % longitude_spot)
                    + '_'
                    + ('%07.3f' % latitude_spot)
                    + '.nc\n'
                )

    point_out_file_list.close()

    output_end_date = (
        current_run_date
        + datetime.timedelta(days=int(model_length))
        + datetime.timedelta(seconds=int(dt_point))
    )  # To also include the last step

    duration_of_the_run = output_end_date - current_run_date
    duration_of_the_run = duration_of_the_run.total_seconds()
    number_of_steps = int(divmod(duration_of_the_run, int(dt_point))[0])

    os.chdir(export_local_dir + '/pos_process/points/spec1d')
    input_netcdf_spec1 = netCDF4.MFDataset('ww3.' + model_grid + '*_spec1d.nc')
    energy_1d_spot_raw = input_netcdf_spec1.variables['f']
    mean_wave_dir_1d_spot_raw = input_netcdf_spec1.variables['th1m']
    mean_wave_spread_1d_spot_raw = input_netcdf_spec1.variables['sth1m']
    frequency_list = input_netcdf_spec1.variables['frequency'][:]

    os.chdir(export_local_dir + '/pos_process/points/spec2d')
    input_netcdf_spec2 = netCDF4.MFDataset('ww3.' + model_grid + '*_spec2d.nc')
    energy_2d_raw = input_netcdf_spec2.variables['efth']
    frequency_list_top = input_netcdf_spec2.variables['frequency1'][:]
    frequency_list_bottom = input_netcdf_spec2.variables['frequency2'][:]
    dir_list_raw = input_netcdf_spec2.variables['direction'][:] - 180
    dir_list_raw = dir_list_raw % 360
    turning_point = np.where(np.diff(dir_list_raw) > 100)
    turning_point = int(turning_point[0] + 1)
    dir_list = np.hstack(
        (dir_list_raw[turning_point:], dir_list_raw[:turning_point])
    )
    dir_list = np.flip(dir_list)

    input_netcdf_part = netCDF4.Dataset(
        export_local_dir
        + '/pos_process/points/part/ww3.'
        + model_grid
        + '.part.nc'
    )

    for spot_number in range(number_of_spots):

        depth_spot = input_netcdf_part.variables['depth'][spot_number]
        longitude_spot = input_netcdf_part.variables['longitude'][spot_number]
        latitude_spot = input_netcdf_part.variables['latitude'][spot_number]
        time_spot = input_netcdf_part.variables['time'][:]

        wind_speed_spot = input_netcdf_part.variables['wind_speed'][
            spot_number
        ][:]
        wind_dir_spot = input_netcdf_part.variables['wind_dir'][spot_number][:]

        sigh_spot = input_netcdf_part.variables['sigH'][spot_number][:]
        tp_spot = input_netcdf_part.variables['tp'][spot_number][:]
        pdir_spot = input_netcdf_part.variables['pdir'][spot_number][:]
        wvelen_spot = input_netcdf_part.variables['wavelength'][spot_number][:]
        spread_spot = input_netcdf_part.variables['wave_spread'][spot_number][
            :
        ]
        wind_frac_spot = input_netcdf_part.variables['wind_fraction'][
            spot_number
        ][:]

        sigh_part_spot = input_netcdf_part.variables['sigH_partition'][
            spot_number
        ][:][:]
        tp_part_spot = input_netcdf_part.variables['tp_partition'][
            spot_number
        ][:][:]
        pdir_part_spot = input_netcdf_part.variables['pdir_partition'][
            spot_number
        ][:][:]
        wvelen_part_spot = input_netcdf_part.variables['wavelength_partition'][
            spot_number
        ][:][:]
        spread_part_spot = input_netcdf_part.variables[
            'wave_spread_partition'
        ][spot_number][:][:]
        wind_frac_part_spot = input_netcdf_part.variables[
            'wind_fraction_partition'
        ][spot_number][:][:]

        energy_1d_spot = energy_1d_spot_raw[:, spot_number, :]
        mean_wave_dir_1d_spot = mean_wave_dir_1d_spot_raw[:, spot_number, :]
        mean_wave_spread_1d_spot = mean_wave_spread_1d_spot_raw[
            :, spot_number, :
        ]

        energy_2d_spot = (
            np.zeros(
                (len(frequency_list), len(dir_list), int(number_of_steps))
            )
            * np.nan
        )
        for step in range(number_of_steps):
            energy_2d_spot_slice = energy_2d_raw[step, spot_number, :, :]
            energy_2d_spot_slice = np.flip(
                np.hstack(
                    (
                        energy_2d_spot_slice[:, turning_point:],
                        energy_2d_spot_slice[:, :turning_point],
                    )
                ),
                1,
            )
            energy_2d_spot[:, :, step] = energy_2d_spot_slice

        netcdf_output_file = (
            export_local_dir
            + '/archive/points/'
            + model_grid
            + '/'
            + current_run_date.strftime("%Y%m%d%H")
            + '/ww3_lotus_'
            + model_grid
            + '.spot_'
            + ('%07.3f' % longitude_spot)
            + '_'
            + ('%07.3f' % latitude_spot)
            + '.nc'
        )

        output_netcdf = netCDF4.Dataset(
            netcdf_output_file, 'w', format='NETCDF4_CLASSIC'
        )
        output_netcdf.description = 'Wavewatch III Point Output'

        output_netcdf.createDimension('time', int(number_of_steps))
        output_netcdf.createDimension('single', 1)
        output_netcdf.createDimension('nb_part', sigh_part_spot.shape[1])
        output_netcdf.createDimension('directions', int(len(dir_list)))
        output_netcdf.createDimension('frequency', int(len(frequency_list)))

        variable = output_netcdf.createVariable('time', 'i', ('time',))
        variable[:] = time_spot
        variable.units = 'Seconds since 1970-01-01 00:00:00'

        variable = output_netcdf.createVariable(
            'longitude', float, ('single',)
        )
        variable[:] = longitude_spot
        variable.units = 'Degrees East'

        variable = output_netcdf.createVariable('latitude', float, ('single',))
        variable[:] = latitude_spot
        variable.units = 'Degrees North'

        variable = output_netcdf.createVariable('depth', float, ('single',))
        variable[:] = depth_spot
        variable.units = 'Meters'

        variable = output_netcdf.createVariable(
            'frequency', float, ('frequency',)
        )
        variable[:] = frequency_list
        variable.units = 'Hz'
        variable.description = 'Frequency list - Center of bin'

        variable = output_netcdf.createVariable(
            'frequency_top', float, ('frequency',)
        )
        variable[:] = frequency_list_bottom
        variable.units = 'Hz'
        variable.description = 'Frequency list - Bottom of bin'

        variable = output_netcdf.createVariable(
            'frequency_bottom', float, ('frequency',)
        )
        variable[:] = frequency_list_top
        variable.units = 'Hz'
        variable.description = 'Frequency list - Top of bin'

        variable = output_netcdf.createVariable(
            'directions', float, ('directions',)
        )
        variable[:] = dir_list
        variable.units = 'Degrees'
        variable.description = 'Direction list'

        variable = output_netcdf.createVariable('sigH', float, ('time',))
        variable[:] = sigh_spot
        variable.units = 'Meters'
        variable.description = 'Significant Wave Height'

        variable = output_netcdf.createVariable('tp', float, ('time',))
        variable[:] = tp_spot
        variable.units = 'Seconds'
        variable.description = 'Peak Period'

        variable = output_netcdf.createVariable('pdir', float, ('time',))
        variable[:] = pdir_spot
        variable.units = 'Degrees'
        variable.description = 'Peak Direction'

        variable = output_netcdf.createVariable('wavelength', float, ('time',))
        variable[:] = wvelen_spot
        variable.units = 'Meters'
        variable.description = 'Wave Length'

        variable = output_netcdf.createVariable(
            'wave_spread', float, ('time',)
        )
        variable[:] = spread_spot
        variable.units = 'Degrees'
        variable.description = 'Directional Wave Spread'

        variable = output_netcdf.createVariable(
            'wind_fraction', float, ('time',)
        )
        variable[:] = wind_frac_spot
        variable.units = 'Value between 0 and 1'
        variable.description = 'Wind Fraction'

        variable = output_netcdf.createVariable('wind_speed', float, ('time',))
        variable[:] = wind_speed_spot
        variable.units = 'Meters per second'
        variable.description = 'Wind Speed'

        variable = output_netcdf.createVariable('wind_dir', float, ('time',))
        variable[:] = wind_dir_spot
        variable.units = 'Degrees'
        variable.description = 'Wind direction (nautical convention)'

        variable = output_netcdf.createVariable(
            'sigH_partition', float, ('time', 'nb_part')
        )
        variable[:] = sigh_part_spot
        variable.units = 'Meters'
        variable.description = 'Significant Wave Height of Partition'

        variable = output_netcdf.createVariable(
            'tp_partition', float, ('time', 'nb_part')
        )
        variable[:] = tp_part_spot
        variable.units = 'Seconds'
        variable.description = 'Peak Period of Partition'

        variable = output_netcdf.createVariable(
            'pdir_partition', float, ('time', 'nb_part')
        )
        variable[:] = pdir_part_spot
        variable.units = 'Degrees'
        variable.description = 'Peak Direction of Partition'

        variable = output_netcdf.createVariable(
            'wavelength_partition', float, ('time', 'nb_part')
        )
        variable[:] = wvelen_part_spot
        variable.units = 'Meters'
        variable.description = 'Wavelength of Partition'

        variable = output_netcdf.createVariable(
            'wave_spread_partition', float, ('time', 'nb_part')
        )
        variable[:] = spread_part_spot
        variable.units = 'Degrees'
        variable.description = 'Directional Wave Spread of Partition'

        variable = output_netcdf.createVariable(
            'wind_fraction_partition', float, ('time', 'nb_part')
        )
        variable[:] = wind_frac_part_spot
        variable.units = 'Value between 0 and 1'
        variable.description = 'Wind Fraction of Partition'

        variable = output_netcdf.createVariable(
            'spectral_wave_density_1D', float, ('time', 'frequency')
        )
        variable[:] = energy_1d_spot
        variable.units = 'm^2/Hz'
        variable.description = 'Energy for each frequency'

        variable = output_netcdf.createVariable(
            'mean_wave_direction_1D',
            float,
            ('time', 'frequency'),
            fill_value=np.nan,
        )
        variable[:] = mean_wave_dir_1d_spot
        variable.units = 'Degrees'
        variable.description = (
            'Mean Wave Direction from spectral moments for each frequency'
        )

        variable = output_netcdf.createVariable(
            'directional_spread_1D',
            float,
            ('time', 'frequency'),
            fill_value=np.nan,
        )
        variable[:] = mean_wave_spread_1d_spot
        variable.units = 'Degrees'
        variable.description = (
            'Directional Spread from spectral moments for each frequency'
        )

        variable = output_netcdf.createVariable(
            'spectral_wave_density_2D',
            float,
            ('frequency', 'directions', 'time'),
        )
        variable[:] = energy_2d_spot
        variable.units = 'm^2/Hz/rad'
        variable.description = 'Energy for each frequency and direction'

        output_netcdf.close()

    input_netcdf_spec1.close()
    input_netcdf_spec2.close()
    input_netcdf_part.close()


def prepare2create_hvp_files(
    model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_point,
    spot_id,
    model_path,
):

    date_string = current_run_date.strftime("%Y%m%d %H%M%S")
    target_work_dir = export_local_dir + '/pos_process/hvp/' + spot_id

    if os.path.exists(target_work_dir):
        rmtree(target_work_dir)
    os.makedirs(target_work_dir)
    os.chdir(target_work_dir)

    InpFile = open('ww3_outp.inp', 'w')
    InpFile.write('$ WAVEWATCH III NETCDF Point output post-processing\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(date_string + '  ' + dt_point + '  9999999\n')
    InpFile.write('$\n')
    InpFile.write(spot_id + '\n')
    InpFile.write('-1\n')
    InpFile.write('$\n')
    InpFile.write('1\n')
    InpFile.write('3 0 0 33 F\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()

    os.symlink(
        export_shared_dir + '/model_data/out_pnt.' + model_grid,
        target_work_dir + '/out_pnt.ww3',
    )
    os.symlink(
        export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
        target_work_dir + '/mod_def.ww3',
    )
    copy2(model_path + '/ww3_outp', target_work_dir + '/ww3_outpoint')


def run_create_hvp_files(target_work_dir):
    os.system(
        'cd '
        + target_work_dir
        + '; '
        + target_work_dir
        + '/ww3_outpoint > '
        + target_work_dir
        + '/log_hvp.txt'
    )


def create_hvp_files(
    model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_point,
    model_path,
):

    flag_hvp = 0
    target_work_dir_map = []
    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="ISO-8859-1",
    ) as points_list, open(
        export_local_dir
        + '/base_files/point_outputs_'
        + model_grid
        + '_names.dat',
        "rt",
        encoding="ISO-8859-1",
    ) as points_list_names:
        points_list = csv.reader(points_list)
        points_list_names = csv.reader(points_list_names)
        next(points_list)  # Skip header
        next(points_list_names)  # Skip header
        for line_spots, line_spots_names in zip(
            points_list, points_list_names
        ):
            spot_output_id_list = line_spots[4:]
            spot_output_names_list = line_spots_names[4:]
            for spot_output_id, spot_output_name in zip(
                spot_output_id_list, spot_output_names_list
            ):
                if int(spot_output_id) > 9999 and int(spot_output_id) < 19999:
                    spot_id = line_spots[0]
                    flag_hvp = 1
                    prepare2create_hvp_files(
                        model_grid,
                        export_local_dir,
                        export_shared_dir,
                        current_run_date,
                        dt_point,
                        spot_id,
                        model_path,
                    )
                    target_work_dir = (
                        export_local_dir + '/pos_process/hvp/' + spot_id
                    )
                    target_work_dir_map.append(target_work_dir)

    pool = ThreadPool(8)
    pool.map(run_create_hvp_files, target_work_dir_map)
    pool.close()
    pool.join()

    if flag_hvp == 1:
        target_archive_dir = (
            export_local_dir
            + '/archive/hvp/'
            + model_grid
            + '/'
            + current_run_date.strftime("%Y%m%d%H")
        )
        if os.path.exists(target_archive_dir):
            rmtree(target_archive_dir)
        os.makedirs(target_archive_dir)

        os.chdir(export_local_dir + '/pos_process/hvp')

        copy2(
            export_local_dir + '/base_files/hvp_lotus_ww3_freq_bands.dat',
            export_local_dir + '/pos_process/hvp/hvp_lotus_ww3_freq_bands.dat',
        )
        copy2(
            export_local_dir + '/base_files/hvp_lotus.f',
            export_local_dir + '/pos_process/hvp/hvp_lotus.f',
        )

        os.system('gfortran hvp_lotus.f -o hvp_lotus.exe')

        with open(
            export_local_dir
            + '/base_files/point_outputs_'
            + model_grid
            + '.dat',
            "rt",
            encoding="ISO-8859-1",
        ) as points_list, open(
            export_local_dir
            + '/base_files/point_outputs_'
            + model_grid
            + '_names.dat',
            "rt",
            encoding="ISO-8859-1",
        ) as points_list_names:
            points_list = csv.reader(points_list)
            points_list_names = csv.reader(points_list_names)
            next(points_list)  # Skip header
            next(points_list_names)  # Skip header
            for line_spots, line_spots_names in zip(
                points_list, points_list_names
            ):
                spot_output_id_list = line_spots[4:]
                spot_output_names_list = line_spots_names[4:]
                for spot_output_id, spot_output_name in zip(
                    spot_output_id_list, spot_output_names_list
                ):
                    if (
                        int(spot_output_id) > 9999
                        and int(spot_output_id) < 19999
                    ):
                        spot_id = line_spots[0]
                        target_work_dir = (
                            export_local_dir + '/pos_process/hvp/' + spot_id
                        )
                        os.system(
                            'cat '
                            + target_work_dir
                            + '/ww3.'
                            + current_run_date.strftime("%y%m%d%H")
                            + '.spc | '
                            + export_local_dir
                            + '/pos_process/hvp/hvp_lotus.exe >> '
                            + target_archive_dir
                            + '/'
                            + spot_output_name
                            + '.hvp'
                        )


def create_point_output_files(
    model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_point,
    model_length,
    model_path,
):

    output_end_date = current_run_date + datetime.timedelta(
        days=int(model_length)
    )

    point_filetype_list = ['part', 'spec1d', 'spec2d']

    for point_filetype in point_filetype_list:

        log.info(
            '  Creating point '
            + point_filetype
            + ' output files for '
            + model_grid
            + ' grid'
        )

        step_date = current_run_date
        step = 0
        target_work_dir_map = []
        while step_date <= output_end_date:
            prepare2create_point(
                model_grid,
                export_local_dir,
                export_shared_dir,
                step_date,
                step,
                dt_point,
                point_filetype,
                model_path,
            )
            target_work_dir = (
                export_local_dir
                + '/pos_process/points/'
                + point_filetype
                + '/'
                + ('%03d' % step)
            )
            target_work_dir_map.append(target_work_dir)
            step_date = step_date + datetime.timedelta(seconds=int(dt_point))
            step += 1

        pool = ThreadPool(8)
        pool.map(run_create_point, target_work_dir_map)
        pool.close()
        pool.join()

    log.info('  Cleaning the folders and renaming files')

    for point_filetype in point_filetype_list:

        target_work_dir = (
            export_local_dir + '/pos_process/points/' + point_filetype
        )
        step_date = current_run_date
        step = 0
        while step_date <= output_end_date:

            if point_filetype == 'part':
                move(
                    target_work_dir + '/' + ('%03d' % step) + '/tab51.ww3',
                    target_work_dir
                    + '/ww3.'
                    + model_grid
                    + '.'
                    + step_date.strftime("%Y%m%d%H")
                    + '.part',
                )
                rmtree(target_work_dir + '/' + ('%03d' % step))

            if point_filetype == 'spec1d':
                move(
                    target_work_dir
                    + '/'
                    + ('%03d' % step)
                    + '/ww3.'
                    + model_grid
                    + '.'
                    + step_date.strftime("%Y%m%dT%HZ")
                    + '_tab.nc',
                    target_work_dir
                    + '/ww3.'
                    + model_grid
                    + '.'
                    + step_date.strftime("%Y%m%d%H")
                    + '_spec1d.nc',
                )
                rmtree(target_work_dir + '/' + ('%03d' % step))

            if point_filetype == 'spec2d':
                move(
                    target_work_dir
                    + '/'
                    + ('%03d' % step)
                    + '/ww3.'
                    + model_grid
                    + '.'
                    + step_date.strftime("%Y%m%dT%HZ")
                    + '_spec.nc',
                    target_work_dir
                    + '/ww3.'
                    + model_grid
                    + '.'
                    + step_date.strftime("%Y%m%d%H")
                    + '_spec2d.nc',
                )
                rmtree(target_work_dir + '/' + ('%03d' % step))

            step_date = step_date + datetime.timedelta(seconds=int(dt_point))
            step += 1

    log.info('  Merging partition files to one single file')

    merge_partition_files(
        model_grid, export_local_dir, current_run_date, dt_point, model_length
    )

    log.info('  Creating final point output files')

    merge_point_output_files(
        model_grid, export_local_dir, current_run_date, dt_point, model_length
    )

    log.info('  Creating hvp files')

    create_hvp_files(
        model_grid,
        export_local_dir,
        export_shared_dir,
        current_run_date,
        dt_point,
        model_path,
    )
