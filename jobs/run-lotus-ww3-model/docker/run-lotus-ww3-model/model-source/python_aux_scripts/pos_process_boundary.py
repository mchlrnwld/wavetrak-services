import csv
import datetime
import glob
import os
from multiprocessing.dummy import Pool as ThreadPool
from shutil import copy2, move, rmtree

import numpy as np  # type: ignore
from util.logger import log


def prepare2create_boundary_point(
    model_grid,
    nested_model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_point,
    spot_id,
    model_path,
):

    date_string = current_run_date.strftime("%Y%m%d %H%M%S")
    target_work_dir = (
        export_local_dir
        + '/pos_process/points/'
        + nested_model_grid
        + '/'
        + spot_id
    )

    if os.path.exists(target_work_dir):
        rmtree(target_work_dir)
    os.makedirs(target_work_dir)
    os.chdir(target_work_dir)

    InpFile = open('ww3_outp.inp', 'w')
    InpFile.write('$ WAVEWATCH III NETCDF Point output post-processing\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(date_string + '  ' + dt_point + '  9999999\n')
    InpFile.write('$\n')
    InpFile.write(spot_id + '\n')
    InpFile.write('-1\n')
    InpFile.write('$\n')
    InpFile.write('1\n')
    InpFile.write('3 0 0 33 F\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()

    os.symlink(
        export_shared_dir + '/model_data/out_pnt.' + model_grid,
        target_work_dir + '/out_pnt.ww3',
    )
    os.symlink(
        export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
        target_work_dir + '/mod_def.ww3',
    )
    copy2(model_path + '/ww3_outp', target_work_dir + '/ww3_outpoint')


def prepare2create_boundary_point_hindcast(
    model_grid,
    nested_model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_point,
    spot_id,
    step_folder,
    model_path,
):

    date_string = current_run_date.strftime("%Y%m%d %H%M%S")
    target_work_dir = (
        export_local_dir
        + '/pos_process/points/'
        + nested_model_grid
        + '/'
        + spot_id
        + '/'
        + step_folder
    )

    if os.path.exists(target_work_dir):
        rmtree(target_work_dir)
    os.makedirs(target_work_dir)
    os.chdir(target_work_dir)

    InpFile = open('ww3_outp.inp', 'w')
    InpFile.write('$ WAVEWATCH III NETCDF Point output post-processing\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write(date_string + '  ' + dt_point + '  1\n')
    InpFile.write('$\n')
    InpFile.write(spot_id + '\n')
    InpFile.write('-1\n')
    InpFile.write('$\n')
    InpFile.write('1\n')
    InpFile.write('3 0 0 33 F\n')
    InpFile.write('$\n')
    InpFile.write('$ End of input file\n')
    InpFile.close()

    os.symlink(
        export_shared_dir + '/model_data/out_pnt.' + model_grid,
        target_work_dir + '/out_pnt.ww3',
    )
    os.symlink(
        export_local_dir + '/pre_process_basefiles/mod_def.' + model_grid,
        target_work_dir + '/mod_def.ww3',
    )
    copy2(model_path + '/ww3_outp', target_work_dir + '/ww3_outpoint')


def run_create_boundary_point(target_work_dir):
    os.system(
        'cd '
        + target_work_dir
        + '; '
        + target_work_dir
        + '/ww3_outpoint > '
        + target_work_dir
        + '/log_bound.txt'
    )


def create_boundary_point_files(
    model_grid,
    nested_model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_point,
    model_path,
):

    log.info(
        '  Creating Boundary point files from ' + nested_model_grid + ' grid'
    )

    target_work_dir_map = []
    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_bound_flag = row[3]
            if spot_bound_flag == nested_model_grid:
                prepare2create_boundary_point(
                    model_grid,
                    nested_model_grid,
                    export_local_dir,
                    export_shared_dir,
                    current_run_date,
                    dt_point,
                    spot_id,
                    model_path,
                )
                target_work_dir = (
                    export_local_dir
                    + '/pos_process/points/'
                    + nested_model_grid
                    + '/'
                    + spot_id
                )
                target_work_dir_map.append(target_work_dir)

    pool = ThreadPool(8)
    pool.map(run_create_boundary_point, target_work_dir_map)
    pool.close()
    pool.join()

    log.info('  Cleaning the folders and renaming files')

    target_work_dir = (
        export_local_dir + '/pos_process/points/' + nested_model_grid
    )
    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_bound_flag = row[3]
            if spot_bound_flag == nested_model_grid:
                move(
                    target_work_dir
                    + '/'
                    + spot_id
                    + '/ww3.'
                    + current_run_date.strftime("%y%m%d%H")
                    + '.spc',
                    export_local_dir
                    + '/boundary/'
                    + nested_model_grid
                    + '/ww3.'
                    + nested_model_grid
                    + '.'
                    + spot_id
                    + '_'
                    + current_run_date.strftime("%Y%m%d%H")
                    + '.spec2d',
                )
                rmtree(target_work_dir + '/' + spot_id)


def create_boundary_point_files_hindcast(
    model_grid,
    nested_model_grid,
    export_local_dir,
    export_shared_dir,
    current_run_date,
    dt_point,
    model_length,
    model_path,
):

    output_end_date = current_run_date + datetime.timedelta(days=model_length)

    log.info(
        '  Creating Boundary point files from ' + nested_model_grid + ' grid'
    )

    step_date = current_run_date
    target_work_dir_map = []
    step = 0
    while step_date <= output_end_date:
        step_folder = '%03d' % step
        with open(
            export_local_dir
            + '/base_files/point_outputs_'
            + model_grid
            + '.dat',
            "rt",
            encoding="utf8",
        ) as infile:
            spot_list_file = csv.reader(infile)
            next(spot_list_file)  # skip the header line
            for row in spot_list_file:
                spot_id = row[0]
                spot_bound_flag = row[3]
                if spot_bound_flag == nested_model_grid:
                    prepare2create_boundary_point_hindcast(
                        model_grid,
                        nested_model_grid,
                        export_local_dir,
                        export_shared_dir,
                        step_date,
                        dt_point,
                        spot_id,
                        step_folder,
                        model_path,
                    )
                    target_work_dir = (
                        export_local_dir
                        + '/pos_process/points/'
                        + nested_model_grid
                        + '/'
                        + spot_id
                        + '/'
                        + step_folder
                    )
                    target_work_dir_map.append(target_work_dir)
        step_date = step_date + datetime.timedelta(seconds=int(dt_point))
        step += 1

    pool = ThreadPool(8)
    pool.map(run_create_boundary_point, target_work_dir_map)
    pool.close()
    pool.join()

    log.info('  Cleaning the folders and renaming files')

    if not os.path.exists(export_local_dir + '/boundary/' + nested_model_grid):
        os.makedirs(export_local_dir + '/boundary/' + nested_model_grid)

    step_date = current_run_date
    step = 0
    target_work_dir = (
        export_local_dir + '/pos_process/points/' + nested_model_grid
    )
    while step_date <= output_end_date:
        step_folder = '%03d' % step
        with open(
            export_local_dir
            + '/base_files/point_outputs_'
            + model_grid
            + '.dat',
            "rt",
            encoding="utf8",
        ) as infile:
            spot_list_file = csv.reader(infile)
            next(spot_list_file)  # skip the header line
            for row in spot_list_file:
                spot_id = row[0]
                spot_bound_flag = row[3]
                if spot_bound_flag == nested_model_grid:
                    move(
                        target_work_dir
                        + '/'
                        + spot_id
                        + '/'
                        + step_folder
                        + '/ww3.'
                        + step_date.strftime("%y%m%d%H")
                        + '.spc',
                        export_local_dir
                        + '/boundary/'
                        + nested_model_grid
                        + '/ww3.'
                        + nested_model_grid
                        + '.'
                        + spot_id
                        + '_'
                        + step_date.strftime("%Y%m%d%H")
                        + '.spec2d',
                    )
        step_date = step_date + datetime.timedelta(seconds=int(dt_point))
        step += 1

    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_bound_flag = row[3]
            if spot_bound_flag == nested_model_grid:
                rmtree(target_work_dir + '/' + spot_id)


def merge_individual_files(model_grid, nested_model_grid, export_local_dir):

    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_bound_flag = row[3]
            if spot_bound_flag == nested_model_grid:
                filelist = sorted(
                    glob.glob(
                        export_local_dir
                        + '/boundary/'
                        + nested_model_grid
                        + '/ww3.*'
                        + nested_model_grid
                        + '.'
                        + spot_id
                        + '_*'
                    )
                )
                outFile_name = (
                    export_local_dir
                    + '/boundary/'
                    + nested_model_grid
                    + '/ww3.'
                    + nested_model_grid
                    + '.'
                    + spot_id
                    + '.spec2d'
                )
                flag_first = 0
                for individual_file in filelist:
                    if flag_first == 0:
                        move(individual_file, outFile_name)
                        flag_first = 1
                    else:
                        outFile = open(outFile_name, 'a')
                        with open(individual_file) as inFile:
                            step = 0
                            for line_spec in inFile:
                                line_spec = line_spec.rstrip('\n')
                                if step < 11:
                                    step += 1  # skip initial heading lines
                                else:
                                    outFile.write(line_spec + '\n')
                        inFile.close()
                        # os.remove(individual_file)
                # outFile.close()


def merge_boundary_files(
    model_grid, nested_model_grid, export_local_dir, model_path
):

    os.chdir(export_local_dir + '/boundary/' + nested_model_grid)

    os.symlink(
        export_local_dir
        + '/pre_process_basefiles/mod_def.'
        + nested_model_grid,
        export_local_dir + '/boundary/' + nested_model_grid + '/mod_def.ww3',
    )

    log.info(
        '  Merging Boundary point files for ' + nested_model_grid + ' grid'
    )

    InpFile = open('ww3_bound.inp', 'w')
    InpFile.write('$ WAVEWATCH III NetCDF boundary input processing\n')
    InpFile.write('$ -------------------------------------------\n')
    InpFile.write('WRITE\n')
    InpFile.write('1\n')
    InpFile.write('0\n')

    with open(
        export_local_dir + '/base_files/point_outputs_' + model_grid + '.dat',
        "rt",
        encoding="utf8",
    ) as infile:
        spot_list_file = csv.reader(infile)
        next(spot_list_file)  # skip the header line
        for row in spot_list_file:
            spot_id = row[0]
            spot_bound_flag = row[3]
            if spot_bound_flag == nested_model_grid:
                InpFile.write(
                    'ww3.' + nested_model_grid + '.' + spot_id + '.spec2d\n'
                )

    InpFile.write("'" + 'STOPSTRING' + "'\n")
    InpFile.write('$ End of input file\n')
    InpFile.close()

    os.system(
        model_path
        + '/ww3_bound > '
        + export_local_dir
        + '/log/ww3_bound.'
        + nested_model_grid
        + '.out'
    )

    move(
        export_local_dir + '/boundary/' + nested_model_grid + '/nest.ww3',
        export_local_dir + '/boundary/nest.' + nested_model_grid,
    )
    os.remove('ww3_bound.inp')
    os.remove('mod_def.ww3')

    # filelist = glob.glob('*.spec2d')
    # for f in filelist:
    #    os.remove(f)
