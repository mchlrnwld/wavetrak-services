import boto3  # type: ignore
import pytest  # type: ignore
from moto import mock_ec2  # type: ignore


@pytest.fixture(autouse=True)
def aws_credentials(monkeypatch):
    """
    Mocked AWS Credentials for moto.
    This is a temporary workaround,
    otherwise moto will throw 'NoCredentialsError'.
    """
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')


@mock_ec2
def test_temp_key_pair_creates_key_pair():
    from lib.ec2 import TempKeyPair

    with TempKeyPair('test-key-pair'):
        ec2 = boto3.client('ec2', 'us-west-1')
        response = ec2.describe_key_pairs()
        assert len(response.get('KeyPairs', [])) == 1
