import logging
import sys
from typing import List

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type: ignore

EC2_CLIENT = boto3.client('ec2')
EC2_RESOURCE = boto3.resource('ec2')

logger = logging.getLogger('run-lotus-ww3-model')


class TempKeyPair:
    """
    Create a temporary EC2 Key Pair.
    On enter a Key Pair is created. The private key is returned.
    On exit the Key Pair is deleted.
    Arguments:
        key_name: Name of Key Pair to create.
    """

    def __init__(self, key_name: str):
        self.key_name = key_name

    def __enter__(self):
        logger.info(f'Creating key pair {self.key_name}...')
        key_pair = EC2_CLIENT.create_key_pair(KeyName=self.key_name)
        pem = key_pair['KeyMaterial']
        return pem

    def __exit__(self, type, value, traceback):
        logger.info(f'Deleting key pair {self.key_name}...')
        EC2_CLIENT.delete_key_pair(KeyName=self.key_name)


class TempInstance:
    """
    Create a temporary EC2 Instance from a list of launch templates.
    If creating an EC2 instance with a launch template fails,
    then the next launch template given in the list will be used.
    If creating an EC2 instance with every launch templates fails,
    then the program will exit.
    On enter an Instance is created and tagged with a name. The Instance is
    returned after it is successfully running.
    On exit the Instance is terminated.
    Arguments:
        name: Name to tag Instance with.
        launch_template_names: A list of launch templates to be
                               used to create EC2 instances.
                               The launch templates will be used
                               in the order they are passed in.
        key_name: Name of Key Pair to associate Instance with.
        subnet_id: ID for subnet to launch Instance in.
        tags: list of tags to apply to the instance
    """

    def __init__(
        self,
        name: str,
        launch_template_names: List[str],
        key_name: str,
        subnet_id: str,
        tags: List[dict],
    ):
        self.name = name
        self.launch_template_names = launch_template_names
        self.key_name = key_name
        self.tags = tags
        self.subnet_id = subnet_id
        self.instance = None

    def __launch_instance(self, index: int = 0):
        if index == len(self.launch_template_names):
            raise Exception('No instance can be launched.')

        try:
            logger.info(
                f'Launching instance {self.name} using template: '
                f'{self.launch_template_names[index]}'
            )
            new_instance = EC2_RESOURCE.create_instances(
                LaunchTemplate={
                    'LaunchTemplateName': self.launch_template_names[index]
                },
                KeyName=self.key_name,
                MinCount=1,
                MaxCount=1,
                SubnetId=self.subnet_id,
                TagSpecifications=[
                    {
                        'ResourceType': 'instance',
                        'Tags': [
                            {'Key': 'Name', 'Value': self.name},
                            *self.tags,
                        ],
                    }
                ],
            )[0]

            logger.info(
                f'Waiting for instance {new_instance.instance_id} to be ready'
            )

            new_instance.wait_until_running()

            new_instance.reload()

            self.instance = new_instance

        except ClientError as e:
            logger.error(e)
            error_code = e.response['Error']['Code']
            if (
                error_code == 'InsufficientInstanceCapacity'
                or error_code == 'SpotMaxPriceTooLow'
            ):
                logger.error(
                    f'The use of launch template: '
                    f'{self.launch_template_names[index]} failed.'
                )
                return self.__launch_instance(index + 1)
            else:
                raise

        return self.instance

    def __enter__(self):
        try:
            return self.__launch_instance()
        except Exception:
            self.__exit__(*sys.exc_info())

    def __exit__(self, type, value, traceback):
        # TODO:
        # Lotus currently terminates its own slaves in the middle of the
        # process because they are not used during the final 2.5 hours
        # (post-processing).
        #
        # Airflow attempts to terminate slaves fail with UnauthorizedException
        # because the required tag 'DynamicallyGenerated':'true' can not be
        # found on a non-existent instance, so IAM assumes Airflow is
        # terminating a typical instance which is not permitted.
        #
        # This verification was added to stop the UnauthorizedException but
        # should be removed when Lotus post-processing is separated from the
        # model run process and it is not responsible for performing
        # infrastructure operations anymore.
        instance_verify_response = EC2_CLIENT.describe_instances(
            Filters=[
                {'Name': 'instance-id', 'Values': [self.instance.instance_id]}
            ]
        )
        if (
            self.instance
            and len(instance_verify_response.get('Reservations')) > 0
        ):
            logger.info(f'Terminating instance {self.instance.instance_id}...')
            self.instance.terminate()
