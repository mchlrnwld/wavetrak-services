#!/usr/bin/env bash
aws --profile="${ENVIRONMENT}" s3 sync --exact-timestamps model-source "s3://surfline-science-s3-${ENVIRONMENT}/jobs/run-lotus-ww3-model/model-source"
