import logging
from functools import partial

import boto3  # type: ignore
import lib.config as config
from check_model_ready import check_sqs_messages
from lib.check_gfs_ready import check_gfs_ready
from lib.helpers import model_run_from_key

logger = logging.getLogger('check-run-ready')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

s3 = boto3.client('s3')


def main():

    logger.info(f'Polling queue: {config.SQS_QUEUE_NAME}...')

    result = None
    total_messages = 0
    while True:
        model_run, messages_found = check_sqs_messages(  # type: ignore
            config.GFS_BUCKET,
            'gfs.',
            config.SQS_QUEUE_NAME,
            partial(
                check_gfs_ready,
                config.GFS_BUCKET,
                config.JOBS_BUCKET,
                f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}',
                config.AGENCY,
                config.MODEL,
            ),
            model_run_from_key,
        )
        result = model_run
        total_messages += messages_found
        if result is not None or messages_found == 0:
            break

    logger.info(f'Checked {total_messages} messages.')
    logger.debug(f'Resulting model run: {result}')

    # Write to stdout for xcoms_push
    if result is not None:
        working_folder = (
            f'{config.JOBS_KEY_PREFIX}/{config.JOB_NAME}/{result}/'
        )
        logger.info(f'Creating s3 working folder: {working_folder}')
        s3.put_object(Bucket=config.JOBS_BUCKET, Key=working_folder)
        print(result)
    else:
        print('')


if __name__ == '__main__':
    main()
