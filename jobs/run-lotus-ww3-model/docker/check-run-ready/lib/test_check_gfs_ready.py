import itertools
import json
import unittest.mock as mock

import boto3  # type: ignore
import pytest  # type: ignore
from boto3_type_annotations.s3 import Client as s3_client  # type: ignore
from lib.check_gfs_ready import check_gfs_ready
from moto import mock_s3  # type: ignore

BUCKET = 'test-bucket'
SCIENCE_BUCKET = 'test-science-bucket'
JOB_PREFIX = 'jobs/run-lotus-ww3-model'
AGENCY = 'NOAA'
MODEL = 'GFS'
RUN_DATE = '20190502'
RUN_HOUR = '00'
RUN = int(f'{RUN_DATE}{RUN_HOUR}')


@pytest.fixture(autouse=True)
def aws_credentials(monkeypatch):
    """
    Mocked AWS Credentials for moto.
    This is a temporary workaround,
    otherwise moto will throw 'NoCredentialsError'.
    """
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3', region_name='us-east-1')
        s3_client.create_bucket(Bucket=BUCKET)
        s3_client.create_bucket(Bucket=SCIENCE_BUCKET)
        yield s3_client


def create_message_body(object_keys: list):
    return json.dumps(
        {
            'Message': json.dumps(
                {
                    'Records': [
                        {'s3': {'object': {'key': key}}} for key in object_keys
                    ]
                }
            )
        }
    )


def put_items_in_bucket(
    s3_client: s3_client, grib_timestamps: list, idx_timestamps: list
):
    for time in grib_timestamps:
        s3_client.put_object(
            Bucket=BUCKET,
            Key=(
                f'gfs.{RUN_DATE}/{RUN_HOUR}/atmos/gfs.t00z.pgrb2.0p25.f'
                f'{str(time).zfill(3)}'
            ),
        )

    for time in grib_timestamps:
        s3_client.put_object(
            Bucket=BUCKET,
            Key=(
                f'gfs.{RUN_DATE}/{RUN_HOUR}/atmos/gfs.t00z.pgrb2.0p25.f'
                f'{str(time).zfill(3)}.idx'
            ),
        )


def test_check_gfs_ready(create_s3_bucket):
    with mock.patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        expected_timestamps = list(
            itertools.chain(range(121), range(123, 385, 3))
        )
        put_items_in_bucket(
            create_s3_bucket, expected_timestamps, expected_timestamps
        )

        assert check_gfs_ready(
            BUCKET, SCIENCE_BUCKET, JOB_PREFIX, AGENCY, MODEL, RUN
        )


def test_check_gfs_ready_model_not_ready(create_s3_bucket):
    with mock.patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        expected_timestamps = list(itertools.chain(range(5)))
        put_items_in_bucket(
            create_s3_bucket, expected_timestamps, expected_timestamps
        )

        assert not check_gfs_ready(
            BUCKET, SCIENCE_BUCKET, JOB_PREFIX, AGENCY, MODEL, RUN
        )


def test_check_gfs_ready_missing_idx(create_s3_bucket):
    with mock.patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        expected_timestamps = list(itertools.chain(range(5)))
        put_items_in_bucket(
            create_s3_bucket, expected_timestamps, expected_timestamps[:-1]
        )

        assert not check_gfs_ready(
            BUCKET, SCIENCE_BUCKET, JOB_PREFIX, AGENCY, MODEL, RUN
        )


def test_check_gfs_ready_missing_grib(create_s3_bucket):
    with mock.patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        expected_timestamps = list(itertools.chain(range(5)))
        put_items_in_bucket(
            create_s3_bucket, expected_timestamps[:-1], expected_timestamps
        )

        assert not check_gfs_ready(
            BUCKET, SCIENCE_BUCKET, JOB_PREFIX, AGENCY, MODEL, RUN
        )


def test_check_gfs_ready_no_file_in_s3(create_s3_bucket):
    with mock.patch(
        'check_model_ready.check_model_run_already_exists', return_value=False
    ):
        assert not check_gfs_ready(
            BUCKET, SCIENCE_BUCKET, JOB_PREFIX, AGENCY, MODEL, RUN
        )
