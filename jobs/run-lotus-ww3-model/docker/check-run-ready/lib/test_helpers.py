from lib.helpers import model_run_from_key


def test_model_run_from_key():
    assert (
        model_run_from_key('gfs.20190502/00/atmos/gfs.t00z.pgrb2.0p25.f000')
        == 2019050200
    )
    assert (
        model_run_from_key(
            'gfs.20190502/00/atmos/gfs.t00z.pgrb2.0p25.f000.idx'
        )
        == 2019050200
    )


def test_model_run_from_key_handles_irrelevant_file_keys():
    assert model_run_from_key('gfsmos.20210217/mdl_gfsmex.t12z') is None
