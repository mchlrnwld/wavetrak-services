import logging
import re
from itertools import chain

import boto3  # type: ignore

GRIB_REGEX = re.compile(r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f(\d{3})$')
IDX_REGEX = re.compile(r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f(\d{3})\.idx$')

logger = logging.getLogger('check-run-ready')


def check_gfs_ready(
    gfs_bucket: str,
    job_bucket: str,
    job_prefix: str,
    agency: str,
    model: str,
    run: int,
) -> bool:
    """
    Checks if the given run is ready for processing. This is accomplished by
    checking that the run has all of the files needed in the given s3 bucket.

    Args:
        gfs_bucket: S3 bucket to check for gfs file objects.
        job_bucket: S3 bucket to check for existing model run folders
        job_prefix: Science bucket prefix for state of this job
        agency: Model agency to check.
        model: Name of model to check.
        run: Model run time (YYYYMMDDHH) to check.

    Returns:
        True if all files exist in the s3 bucket for the model_run.
        False if the Lotus model run has already started, if the some files
        exist in s3, but not all, or if there are no files in s3.
    """
    logger.info(f'Checking if model run {run} is already in progress...')
    s3_client = boto3.client('s3')
    response = s3_client.list_objects_v2(
        Bucket=job_bucket, Prefix=f'{job_prefix}/{run}/'
    )
    logger.info(response)
    if response.get('KeyCount') > 0:
        logger.info('Model run is already in progress.')
        return False

    logger.info(f'Checking {run} model run ready for processing...')
    expected_timestamps = list(chain(range(121), range(123, 385, 3)))
    paginator = s3_client.get_paginator('list_objects_v2')
    run_date = str(run)[:8]
    run_hour = str(run)[8:]
    object_keys = [
        item['Key']
        for item in chain.from_iterable(
            response['Contents']
            for response in paginator.paginate(
                Bucket=gfs_bucket, Prefix=f'gfs.{run_date}/{run_hour}/atmos'
            )
            if 'Contents' in response
        )
    ]
    grib_matches = [GRIB_REGEX.search(key) for key in object_keys]
    grib_timestamps = [int(match.group(2)) for match in grib_matches if match]
    idx_matches = [IDX_REGEX.search(key) for key in object_keys]
    idx_timestamps = [int(match.group(2)) for match in idx_matches if match]
    logger.debug(f'Expected timestamps: {expected_timestamps}')
    logger.debug(f'Found GRIB timestamps: {grib_timestamps}')
    logger.debug(f'Found IDX timestamps: {idx_timestamps}')
    return grib_timestamps == idx_timestamps == expected_timestamps
