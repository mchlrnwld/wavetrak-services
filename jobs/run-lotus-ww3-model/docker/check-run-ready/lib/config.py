import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
SECRETS_PREFIX = f'{ENV}/common/'
AGENCY = os.environ['AGENCY']
JOB_NAME = os.environ['JOB_NAME']
MODEL = os.environ['MODEL']
SQS_QUEUE_NAME = os.environ['SQS_QUEUE_NAME']

JOBS_BUCKET = get_secret(SECRETS_PREFIX, 'JOBS_BUCKET')
GFS_BUCKET = get_secret(SECRETS_PREFIX, 'GFS_BUCKET')
JOBS_KEY_PREFIX = get_secret(SECRETS_PREFIX, 'JOBS_KEY_PREFIX')
