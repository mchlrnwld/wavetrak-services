import re
from typing import Optional

# Matches GRIB and IDX S3 object keys.
# Examples:
#     gfs.20190502/00/gfs.t00z.pgrb2.0p25.f000
#     gfs.20190502/00/gfs.t00z.pgrb2.0p25.f000.idx
KEY_REGEX = re.compile(
    r'gfs\.(\d{8})/(00|06|12|18)/atmos/'
    r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f\d{3}'
)


# TODO: Extract these helper functions into modules.
def model_run_from_key(key: str) -> Optional[int]:
    """
    Extracts a model run time from the given key string

    Args:
        key: The key string to extract data from

    Returns:
        A string representing the model run time
    """
    match = KEY_REGEX.match(key)
    if not match:
        return None

    return int(f'{match.group(1)}{match.group(2)}')
