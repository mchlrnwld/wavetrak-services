# Check Model Ready

Docker image to check if latest GFS model is ready for pre-processing. 

## Setup

```sh
conda devenv
source activate run-lotus-ww3-model-check-run-ready
cp .env.sample .env
env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
docker build -t run-lotus-ww3-model/check-run-ready .
cp .env.sample .env
docker run --env-file=.env --rm run-lotus-ww3-model/check-run-ready
```
