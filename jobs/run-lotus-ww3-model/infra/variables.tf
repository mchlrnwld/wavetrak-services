variable "environment" {
  type = string
}

variable "account_id" {
  type = string
}

variable "topic_arns" {
  type = list(string)
}

variable "queue_name" {
  default = "s3-events"
}

variable "company" {
  default = "wt"
}

variable "application" {
  default = "run-lotus-ww3-model"
}

variable "master_instance_ami_id" {
  description = "AMI of the Lotus cluster master instance"
  type        = string
}

variable "slave_instance_ami_id" {
  description = "AMI of the Lotus cluster slave instances"
  type        = string
}

variable "science_bucket" {
  type = string
}

variable "subnet_id" {
  description = "VPC subnet for Lotus cluster placement"
  type        = string
}
