{
  "agent": {
    "run_as_user": "root"
  },
  "logs": {
    "logs_collected": {
      "files": {
        "collect_list": [
          {
            "file_path": "${file_path}",
            "log_group_name": "${log_group_name}",
            "log_stream_name": "${log_stream_name}"
          }
        ]
      }
    }
  }
}
