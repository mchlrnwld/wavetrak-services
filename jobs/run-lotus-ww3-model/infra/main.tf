provider "aws" {
  alias = "noaa"
}

locals {
  common_tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}

data "aws_region" "current" {}

data "aws_subnet" "lotus_cluster" {
  id = var.subnet_id
}

data "aws_vpc" "lotus_cluster" {
  id = data.aws_subnet.lotus_cluster.vpc_id
}

module "sqs_with_sns_subscription" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs-with-sns-subscription"

  providers = {
    aws.topic_provider = aws.noaa
  }

  application = var.application
  company     = var.company
  environment = var.environment
  queue_name  = var.queue_name
  topic_arns  = var.topic_arns
}

data "template_file" "cloudwatch_agent_config" {
  template = file("${path.module}/resources/cloudwatch_agent_config.tpl")
  vars = {
    file_path       = "/export-local/LOTUS/model_run.log"
    log_group_name  = "${var.company}-${var.application}-${var.environment}"
    log_stream_name = "${var.company}-${var.application}-${var.environment}"
  }
}

resource "aws_ssm_parameter" "cloudwatch_agent_config" {
  name  = "/${var.environment}/jobs/run-lotus-ww3-model/CLOUDWATCH_AGENT_CONFIG"
  type  = "String"
  value = data.template_file.cloudwatch_agent_config.rendered
}

data "aws_iam_policy_document" "lotus_assumed_role" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "lotus_instance" {
  name               = "${var.company}-${var.application}-${var.environment}"
  assume_role_policy = data.aws_iam_policy_document.lotus_assumed_role.json
}

resource "aws_iam_role_policy_attachment" "lotus_cloudwatch" {
  role       = aws_iam_role.lotus_instance.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

data "aws_iam_policy_document" "lotus_ssm" {
  statement {
    actions   = ["ssm:GetParameter"]
    resources = [aws_ssm_parameter.cloudwatch_agent_config.arn]
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "lotus_ssm" {
  name        = "wt-lotus-ssm-access"
  description = "Allow Lotus master instance to access SSM parameter with CloudWatch config"
  policy      = data.aws_iam_policy_document.lotus_ssm.json
}

resource "aws_iam_role_policy_attachment" "lotus_ssm" {
  role       = aws_iam_role.lotus_instance.name
  policy_arn = aws_iam_policy.lotus_ssm.arn
}

data "aws_iam_policy_document" "lotus_ec2" {
  statement {
    actions   = ["ec2:TerminateInstances"]
    resources = ["*"]
    effect    = "Allow"
    condition {
      test     = "StringEquals"
      variable = "ec2:ResourceTag/Template"
      values   = ["LOTUS - Slave"]
    }
  }
}

resource "aws_iam_policy" "lotus_ec2" {
  name        = "wt-lotus-ec2-access"
  description = "Allow Lotus master instance to terminate Lotus slave instances"
  policy      = data.aws_iam_policy_document.lotus_ec2.json
}

resource "aws_iam_role_policy_attachment" "lotus_ec2" {
  role       = aws_iam_role.lotus_instance.name
  policy_arn = aws_iam_policy.lotus_ec2.arn
}

data "aws_iam_policy_document" "lotus_s3" {
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::noaa-gfs-bdp-pds",
      "arn:aws:s3:::noaa-gfs-bdp-pds/*"
    ]
  }
  statement {
    actions = [
      "s3:ListBucket"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:s3:::${var.science_bucket}"
    ]
  }
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:s3:::${var.science_bucket}/wavetrak/lotus-ww3/*",
      "arn:aws:s3:::${var.science_bucket}/jobs/run-lotus-ww3-model/*"
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::msw-lotus",
      "arn:aws:s3:::msw-lotus/*"
    ]
  }
}

resource "aws_iam_policy" "lotus_s3" {
  name        = "wt-lotus-s3-access"
  description = "Allow Lotus instances to access s3"
  policy      = data.aws_iam_policy_document.lotus_s3.json
}

resource "aws_iam_role_policy_attachment" "lotus_s3" {
  role       = aws_iam_role.lotus_instance.name
  policy_arn = aws_iam_policy.lotus_s3.arn
}

resource "aws_iam_instance_profile" "lotus_instance" {
  name = "${var.company}-${var.application}-${var.environment}"
  role = aws_iam_role.lotus_instance.name
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
resource "aws_security_group" "lotus_cluster" {
  name        = "${var.company}-${var.application}-${var.environment}"
  description = "Allow access within the Lotus cluster"
  vpc_id      = data.aws_vpc.lotus_cluster.id

  # Allow any inbound from this security group
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = -1
    self      = true
  }

  # Allow any inbound from VPC because airflow instances are in multiple subnets
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.lotus_cluster.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_efs_file_system" "lotus_export" {
  provisioned_throughput_in_mibps = 200
  throughput_mode                 = "provisioned"
  tags = {
    Name = "${var.company}-${var.application}-${var.environment}"
  }
}

resource "aws_efs_mount_target" "lotus_export" {
  file_system_id  = aws_efs_file_system.lotus_export.id
  subnet_id       = data.aws_subnet.lotus_cluster.id
  security_groups = [aws_security_group.lotus_cluster.id]
}

resource "aws_cloudwatch_log_group" "lotus_master" {
  name              = "${var.company}-${var.application}-${var.environment}"
  retention_in_days = 7
  tags              = local.common_tags
}

resource "aws_cloudwatch_log_stream" "lotus_master" {
  name           = "${var.company}-${var.application}-${var.environment}"
  log_group_name = aws_cloudwatch_log_group.lotus_master.name
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template
resource "aws_launch_template" "lotus_master" {
  name                   = "${var.company}-${var.application}-lotus-master-${var.environment}"
  image_id               = var.master_instance_ami_id
  instance_type          = "m5.2xlarge"
  vpc_security_group_ids = [aws_security_group.lotus_cluster.id]
  placement {
    availability_zone = data.aws_subnet.lotus_cluster.availability_zone
  }
  iam_instance_profile {
    arn = aws_iam_instance_profile.lotus_instance.arn
  }
  tag_specifications {
    resource_type = "instance"
    tags          = merge(local.common_tags, { Template = "LOTUS - Master" })
  }
  tag_specifications {
    resource_type = "volume"
    tags          = merge(local.common_tags, { Template = "LOTUS - Master" })
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template
resource "aws_launch_template" "lotus_slave" {
  name                   = "${var.company}-${var.application}-lotus-slave-${var.environment}"
  image_id               = var.slave_instance_ami_id
  instance_type          = "c5.9xlarge"
  vpc_security_group_ids = [aws_security_group.lotus_cluster.id]
  placement {
    availability_zone = data.aws_subnet.lotus_cluster.availability_zone
  }
  iam_instance_profile {
    arn = aws_iam_instance_profile.lotus_instance.arn
  }
  tag_specifications {
    resource_type = "instance"
    tags          = merge(local.common_tags, { Template = "LOTUS - Slave" })
  }
  tag_specifications {
    resource_type = "volume"
    tags          = merge(local.common_tags, { Template = "LOTUS - Slave" })
  }
}
