# run-lotus-ww3-model Airflow DAG infrastructure

## Naming Conventions

See the [AWS naming conventions guide](https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions).

## Terraform

_Uses terraform version `0.12.28`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See [here](https://www.terraform.io/docs/modules/usage.html) for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sandbox make plan
ENV=sandbox make apply
```

Valid environments are `sandbox` and `prod`.
