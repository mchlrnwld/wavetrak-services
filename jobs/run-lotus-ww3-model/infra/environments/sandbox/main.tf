provider "aws" {
  region = "us-west-1"
}

provider "aws" {
  alias  = "noaa"
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/run-lotus-ww3-model/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "run_lotus_ww3_model" {
  source = "../../"

  providers = {
    aws      = aws
    aws.noaa = aws.noaa
  }

  environment            = "sandbox"
  account_id             = "665294954271"
  topic_arns             = ["arn:aws:sns:us-east-1:123901341784:NewGFSObject"]
  subnet_id              = "subnet-0b09466e"
  master_instance_ami_id = "ami-0930655254f9aa858"
  slave_instance_ami_id  = "ami-074a24a8b30cc18dc"
  science_bucket         = "surfline-science-s3-dev"
}
