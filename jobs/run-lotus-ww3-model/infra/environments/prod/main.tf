provider "aws" {
  region = "us-west-1"
}

provider "aws" {
  alias  = "noaa"
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/run-lotus-ww3-model/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "run_lotus_ww3_model" {
  source = "../../"

  providers = {
    aws      = aws
    aws.noaa = aws.noaa
  }

  environment            = "prod"
  account_id             = "833713747344"
  topic_arns             = ["arn:aws:sns:us-east-1:123901341784:NewGFSObject"]
  subnet_id              = "subnet-baab36df"
  master_instance_ami_id = "ami-09731255aad9da5fb"
  slave_instance_ami_id  = "ami-0c1bbf62d3cdaa660"
  science_bucket         = "surfline-science-s3-prod"
}
