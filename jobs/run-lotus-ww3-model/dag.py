import itertools
import re
from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators import WTDockerOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from dag_helpers.docker import docker_image
from dag_helpers.pager_duty import create_pager_duty_failure_callback

AWS_DEFAULT_REGION = Variable.get('AWS_DEFAULT_REGION')
ENVIRONMENT = Variable.get('ENVIRONMENT')

JOB_NAME = 'run-lotus-ww3-model'
APP_ENVIRONMENT = 'sandbox' if ENVIRONMENT == 'dev' else ENVIRONMENT
SQS_QUEUE_NAME = 'wt-{0}-s3-events-{1}'.format(JOB_NAME, APP_ENVIRONMENT)
AGENCY = 'NOAA'
MODEL = 'GFS'
RUN = '{{ task_instance.xcom_pull(task_ids=\'check-run-ready\') }}'

pager_duty_failure_callback = create_pager_duty_failure_callback(
    'pagerduty_forecast_squad', JOB_NAME, APP_ENVIRONMENT
)

common_environment_values = {
    'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
    'ENV': APP_ENVIRONMENT,
    'JOB_NAME': JOB_NAME,
    'AGENCY': AGENCY,
    'MODEL': MODEL,
    'RUN': RUN,
}


def task_environment(*keys, **keyvalues):
    environment = keyvalues.copy()
    for key in keys:
        environment[key] = common_environment_values[key]
    return environment


# DAG definition
default_args = {
    'owner': 'surfline',
    'start_date': datetime(2022, 1, 19, 19, 30),
    'email': 'platformsquad@surfline.com',
    'email_on_failure': True,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True,
    'execution_timeout': timedelta(minutes=30),
    'on_failure_callback': pager_duty_failure_callback,
}

# TODO: remove 'max_active_runs' when this is deployed in prod
# and the correct behavior of DAG runs with all tasks complete
# is observed
dag = DAG(
    '{0}-v2'.format(JOB_NAME),
    schedule_interval=timedelta(minutes=5),
    default_args=default_args,
    max_active_runs=100
)

check_run_ready = WTDockerOperator(
    task_id='check-run-ready',
    image=docker_image('check-run-ready', JOB_NAME),
    environment=task_environment(
        'ENV',
        'AWS_DEFAULT_REGION',
        'AGENCY',
        'JOB_NAME',
        'MODEL',
        SQS_QUEUE_NAME=SQS_QUEUE_NAME,
    ),
    force_pull=True,
    xcom_push=True,
    depends_on_past=True,
    dag=dag,
)

check_run_ready_branch = BranchPythonOperator(
    task_id='check-run-ready-branch',
    templates_dict={'run': RUN},
    python_callable=lambda **kwargs: (
        'begin-running-model'
        if kwargs['templates_dict']['run']
        # HACK: Check model run to prevent 'None' from triggering processing.
        and re.match(r'\d{10}', kwargs['templates_dict']['run'])
        else 'no-run-ready'
    ),
    dag=dag,
)

no_run_ready = DummyOperator(task_id='no-run-ready', dag=dag)

begin_running_model = DummyOperator(task_id='begin-running-model', dag=dag)

run_lotus_ww3_model = WTDockerOperator(
    task_id='run-lotus-ww3-model',
    image=docker_image('run-lotus-ww3-model', JOB_NAME),
    execution_timeout=timedelta(minutes=360),
    environment=task_environment(
        'AWS_DEFAULT_REGION', 'ENV', 'RUN', 'JOB_NAME'
    ),
    force_pull=True,
    depends_on_past=False,
    dag=dag,
    retries=0,
)

check_run_ready_branch.set_upstream(check_run_ready)
no_run_ready.set_upstream(check_run_ready_branch)
begin_running_model.set_upstream(check_run_ready_branch)
run_lotus_ww3_model.set_upstream(begin_running_model)
