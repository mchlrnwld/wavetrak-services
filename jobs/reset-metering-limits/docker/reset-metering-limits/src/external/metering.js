/* istanbul ignore file */
import axios from 'axios';
import { getConfig } from '../config';

export const getUniqueMeteringTimezones = async () => {
  const response = await axios.get(`${getConfig().METERING_SERVICE}/admin/timezones`);

  return response.data;
};

/**
 * @description Makes an external call to the metering service admin endpoint
 * to retrieve meters based on a user query and meter query.
 * @param {{}} userQuery User properties to query meters with
 * @param {{}} meterQuery Meter properties to query meters with
 * @param {{}} projection Projection properties for MongoDB aggregate
 * @param {number} skip Skip integer for MongoDB aggregate
 * @param {number} limit Limit integer for MongoDB aggregate
 */
export const getMeters = async (userQuery, meterQuery, projection, limit, skip) => {
  const body = {
    user: userQuery,
    meter: meterQuery,
    projection,
    skip,
    limit,
  };
  const response = await axios.get(`${getConfig().METERING_SERVICE}/admin/meters`, { data: body });

  return response.data;
};

export const refreshMeter = async (meterId) => {
  const response = await axios.patch(`${getConfig().METERING_SERVICE}/admin/refresh/${meterId}`);

  return response.data;
};
