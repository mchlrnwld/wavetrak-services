/* istanbul ignore file */
/* eslint-disable no-console */
import newrelic from 'newrelic';
import resetMeteringLimits from './handlers/resetMeteringLimits';
import { createConfig } from './config';
import { setupLogger } from './utils/logger';

const JOB_NAME = 'reset-metering-limits';
let config;
let log;

const initialize = async () => {
  try {
    config = await createConfig();
    log = setupLogger(config);
  } catch (err) {
    const message = `reset-metering-limits job initialization failed: ${err.message}`;
    if (log) log.error({ message, stack: err });
    console.log(message);
    throw err;
  }
};

newrelic.startBackgroundTransaction(JOB_NAME, async () => {
  const transaction = newrelic.getTransaction();
  try {
    await initialize();
    log.info(`Starting: ${JOB_NAME} job on ${Date.now()}`);
    await resetMeteringLimits({ debugTimezones: config.DEBUG_TIMEZONES, log });
    log.info(`Completed: ${JOB_NAME} job completed at ${Date.now()}`);
    transaction.end();

    // To guaranteed at least 1 logsene batch
    await new Promise((resolve) => setTimeout(resolve, 20000));

    process.exit(0);
  } catch (err) {
    newrelic.noticeError(err);
    log.error(err, `Failed: ${JOB_NAME} failed at ${Date.now()}`);
    transaction.end();
    process.exit(1);
  }
});
