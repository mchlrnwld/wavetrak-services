/* istanbul ignore file */
import { getSecret } from '@surfline/services-common';

const ENV = process.env.NODE_ENV === 'prod' ? 'prod' : 'staging';

let config;

export const createConfig = async () => {
  if (!config) {
    config = {
      DEBUG_TIMEZONES: process.env.DEBUG_TIMEZONES ? process.env.DEBUG_TIMEZONES.split(',') : null,
      LOGSENE_KEY: await getSecret(`${ENV}/common/`, 'LOGSENE_KEY'),
      LOGSENE_LEVEL: await getSecret(`${ENV}/common/`, 'LOGSENE_LEVEL'),
      METERING_SERVICE: await getSecret(`${ENV}/common/`, 'METERING_SERVICE'),
    };
  }
  return config;
};

export const getConfig = () => {
  if (!config) throw new Error('Config not set, you must call `createConfig`.');

  return config;
};
