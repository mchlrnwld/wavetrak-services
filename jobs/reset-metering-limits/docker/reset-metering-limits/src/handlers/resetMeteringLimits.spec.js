import { expect } from 'chai';
import sinon from 'sinon';
import newrelic from 'newrelic';
import * as meteringApi from '../external/metering';
import resetMeteringLimits from './resetMeteringLimits';

describe('resetMeteringLimits', () => {
  let clock;
  const defaultTimestamp = 1588849200000; // Thursday May 7 2020 11:00:00 GMT

  const params = {
    debugTimezones: null,
    log: {
      info: () => null,
      error: () => null,
    },
  };

  const simulateMaxMeters = () => {
    const meters = [];
    for (let i = 0; i < 10000; i += 1) {
      meters.push({ _id: `${i}` });
    }
    return { meters };
  };

  beforeEach(() => {
    sinon.stub(meteringApi, 'getMeters').resolves({ meters: [] });
    sinon.stub(meteringApi, 'refreshMeter');
    sinon.stub(meteringApi, 'getUniqueMeteringTimezones');
    sinon.stub(newrelic, 'noticeError');
    clock = sinon.useFakeTimers(defaultTimestamp);
  });
  afterEach(() => {
    meteringApi.getUniqueMeteringTimezones.restore();
    meteringApi.refreshMeter.restore();
    meteringApi.getMeters.restore();
    newrelic.noticeError.restore();
    clock.restore();
  });

  it('should default unknown timezones to UTC', async () => {
    const time = 1588564800000; // Mon May 4 2020 04:00:00 GMT
    clock = sinon.useFakeTimers(time);
    meteringApi.getUniqueMeteringTimezones.resolves({
      timezones: ['America/Los_Angeles', 'America/UNKNOWN'],
    });
    meteringApi.getMeters.resolves({ meters: [{ _id: '12345' }] });
    meteringApi.refreshMeter.resolves();

    await resetMeteringLimits(params);

    expect(meteringApi.getUniqueMeteringTimezones).to.have.been.calledOnce();
    expect(meteringApi.getMeters).to.have.been.calledWith(
      { timezone: 'America/UNKNOWN' },
      { active: true },
      { 'user._id': 1 },
      10000,
      0,
    );
    expect(meteringApi.refreshMeter).to.have.been.calledOnceWithExactly('12345');
  });

  it('should handle resetting a timezone that is currently at Monday 4am', async () => {
    const time = 1588590000000; // Mon May 4 2020 04:00:00 PDT
    clock = sinon.useFakeTimers(time);
    meteringApi.getUniqueMeteringTimezones.resolves({
      timezones: ['America/Los_Angeles', 'UTC', 'America/New_York'],
    });
    meteringApi.getMeters.resolves({ meters: [{ _id: '12345' }] });
    meteringApi.refreshMeter.resolves();

    await resetMeteringLimits(params);

    expect(meteringApi.getUniqueMeteringTimezones).to.have.been.calledOnce();
    expect(meteringApi.getMeters).to.have.been.calledOnceWithExactly(
      { timezone: 'America/Los_Angeles' },
      { active: true },
      { 'user._id': 1 },
      10000,
      0,
    );
    expect(meteringApi.refreshMeter).to.have.been.calledOnceWithExactly('12345');
  });

  it('should skip processing if no timezones are currently at Monday 4am', async () => {
    clock = sinon.useFakeTimers(defaultTimestamp);
    meteringApi.getUniqueMeteringTimezones.resolves({
      timezones: ['America/Los_Angeles', 'UTC', 'America/New_York'],
    });

    await resetMeteringLimits(params);

    expect(meteringApi.getUniqueMeteringTimezones).to.have.been.calledOnce();
    expect(meteringApi.getMeters).to.not.have.been.called();
    expect(meteringApi.refreshMeter).to.not.have.been.called();
  });

  it('should use DEBUG_TIMEZONES instead of the current timezone if available', async () => {
    meteringApi.getMeters.resolves({ meters: [{ _id: '12345' }] });
    meteringApi.refreshMeter.resolves();

    await resetMeteringLimits({ debugTimezones: ['America/Los_Angeles'], log: params.log });

    expect(meteringApi.getMeters).to.have.been.calledWith(
      { timezone: 'America/Los_Angeles' },
      { active: true },
      { 'user._id': 1 },
      10000,
      0,
    );
    expect(meteringApi.refreshMeter).to.have.been.calledOnceWithExactly('12345');
  });

  it('should use paging when the meter returned is equal to the limit', async () => {
    clock = sinon.useFakeTimers(defaultTimestamp);
    meteringApi.getMeters.onFirstCall().resolves(simulateMaxMeters());
    meteringApi.refreshMeter.resolves();

    await resetMeteringLimits({ debugTimezones: ['America/Los_Angeles'], log: params.log });

    expect(meteringApi.getMeters).to.have.been.calledTwice();
    expect(meteringApi.getMeters).to.have.been.calledWith(
      { timezone: 'America/Los_Angeles' },
      { active: true },
      { 'user._id': 1 },
      10000,
      0,
    );
    expect(meteringApi.getMeters).to.have.been.calledWith(
      { timezone: 'America/Los_Angeles' },
      { active: true },
      { 'user._id': 1 },
      10000,
      10000,
    );
  });

  it('should not throw if refreshing a meter fails', async () => {
    const time = 1588564800000; // Mon May 4 2020 04:00:00 GMT
    clock = sinon.useFakeTimers(time);
    meteringApi.getUniqueMeteringTimezones.resolves({
      timezones: ['America/Los_Angeles', 'UTC'],
    });
    meteringApi.getMeters.resolves({
      meters: [{ _id: '12345' }, { _id: '2345' }, { _id: '235233' }],
    });
    meteringApi.refreshMeter.resolves();
    meteringApi.refreshMeter.onFirstCall().throws();

    await resetMeteringLimits(params);

    expect(meteringApi.refreshMeter).to.have.been.calledThrice();
  });

  it('should throw a fatal error if one occurs', async () => {
    const error = { message: 'Something went wrong' };
    meteringApi.getUniqueMeteringTimezones.rejects(error);

    try {
      await resetMeteringLimits(params);
    } catch (err) {
      expect(err).to.equal(error);
    }
    expect(newrelic.noticeError).to.have.been.calledOnceWithExactly(error);
  });
});
