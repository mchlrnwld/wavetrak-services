/* eslint-disable no-await-in-loop */
/* eslint-disable no-console */
import newrelic from 'newrelic';
import moment from 'moment-timezone';
import { getUniqueMeteringTimezones, getMeters, refreshMeter } from '../external/metering';

const METER_LIMIT = 10000;

const getTimezonesToProcess = (timezones) =>
  timezones?.filter((timezone) => {
    const currentMomentInTimezone = moment.tz(timezone);

    const currentHourInTimezone = currentMomentInTimezone.get('hour');
    const currentDayInTimezone = currentMomentInTimezone.format('dddd');

    const is4AMInTimezone = currentHourInTimezone === 4;
    const isMondayInTimezone = currentDayInTimezone === 'Monday';

    return isMondayInTimezone && is4AMInTimezone;
  });

/**
 * Iterates through metersToReset and attempts to refresh the meter
 * @param {[{ _id }]} metersToReset Array of meters with an _id.
 * @returns The count of successes and failures.
 */
const refreshMeters = async (metersToReset, log) => {
  let success = 0;
  let fail = 0;
  for (let i = 0; i < metersToReset?.length; i += 1) {
    const meter = metersToReset[i];

    const meterId = meter._id;
    try {
      await refreshMeter(meterId);
      log.info(`Refreshed meter: ${meterId}`);
      success += 1;
    } catch (error) {
      log.error(error, 'Failed to refresh meter.');
      newrelic.noticeError(error);
      fail += 1;
    }
  }
  return { success, fail };
};

const resetMeteringLimits = async ({ debugTimezones, log }) => {
  try {
    const timezonesToProcess =
      debugTimezones ?? getTimezonesToProcess((await getUniqueMeteringTimezones())?.timezones);

    if (!timezonesToProcess?.length) {
      log.info('No timezones are currently at 4am on Monday. Skipping resetting meter limits.');
      return;
    }
    log.info(`The following timezones need to be reset: ${timezonesToProcess.toString()}`);

    const meterQuery = {
      active: true,
    };

    const projection = {
      'user._id': 1,
    };

    for (let index = 0; index < timezonesToProcess.length; index += 1) {
      const timezone = timezonesToProcess[index];
      const userQuery = {
        timezone,
      };
      log.info(`Resetting meters for: ${timezone}`);

      let successCount = 0;
      let failCount = 0;
      // Paging loop
      for (let skip = 0; ; skip += METER_LIMIT) {
        const { meters: metersToReset } = await getMeters(
          userQuery,
          meterQuery,
          projection,
          METER_LIMIT,
          skip,
        );
        if (metersToReset) {
          const { success, fail } = await refreshMeters(metersToReset, log);
          successCount += success;
          failCount += fail;
        }
        if (!metersToReset || metersToReset.length < METER_LIMIT) break;
      }

      log.info(
        `Job complete for Timezone: ${timezone}. Success: ${successCount},` +
          ` Failure: ${failCount}, Total: ${successCount + failCount}`,
      );
    }
  } catch (err) {
    log.error({ message: `Fatal Error: ${err.message}`, stack: err });
    newrelic.noticeError(err);
    throw err;
  }
};

export default resetMeteringLimits;
