#cloud-config
bootcmd:
  - mkfs.ext4 -E nodiscard /dev/nvme1n1
  - mkdir -p /mnt/ephemeral
  - mount /dev/nvme1n1 /mnt/ephemeral
