variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "job_name" {
  default = "build-full-grid-db"
}

variable "launch_template_vpc_security_groups" {
  default = []
}

variable "science_s3_bucket" {
}

variable "smdb_autoscaling_group" {
}

variable "smdb_stream_name" {
}
