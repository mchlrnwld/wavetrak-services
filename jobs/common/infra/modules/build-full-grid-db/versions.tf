terraform {
  required_version = ">= 0.12"
  required_providers {
    aws      = "~> 3.19"
    template = "~> 2.1"
  }
}
