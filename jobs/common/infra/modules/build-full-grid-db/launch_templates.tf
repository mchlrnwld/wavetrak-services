data "aws_ami" "airflow_task_runner" {
  most_recent      = true
  executable_users = ["self"]
  owners           = ["833713747344"]
  name_regex       = "surfline-airflow-task-runner"
}

data "template_file" "build_full_grid_db_user_data" {
  template = file("${path.module}/resources/build_full_grid_db_user_data.tpl")
}

locals {
  common_tags = {
    Company     = var.company
    Service     = "airflow"
    Application = var.application
    Environment = var.environment
    Terraform   = "true"
  }

  resource_tags = merge(
    local.common_tags,
    {
      "DynamicallyGenerated" = "true"
    },
  )
}

resource "aws_launch_template" "build_full_grid_db_spot" {
  name                                 = "${var.company}-${var.application}-${var.job_name}-spot-${var.environment}"
  image_id                             = data.aws_ami.airflow_task_runner.id
  instance_type                        = "c5d.2xlarge"
  instance_initiated_shutdown_behavior = "terminate"
  vpc_security_group_ids               = var.launch_template_vpc_security_groups
  tags                                 = local.common_tags
  user_data                            = base64encode(file("${path.module}/resources/build_full_grid_db_user_data.tpl"))

  iam_instance_profile {
    arn = aws_iam_instance_profile.build_full_grid_db.arn
  }

  monitoring {
    enabled = true
  }

  instance_market_options {
    market_type = "spot"

    spot_options {
      block_duration_minutes = 240
    }
  }

  tag_specifications {
    resource_type = "instance"
    tags          = local.resource_tags
  }
}

resource "aws_launch_template" "build_full_grid_db_on_demand" {
  name                                 = "${var.company}-${var.application}-${var.job_name}-on-demand-${var.environment}"
  image_id                             = data.aws_ami.airflow_task_runner.id
  instance_type                        = "c5d.2xlarge"
  instance_initiated_shutdown_behavior = "terminate"
  vpc_security_group_ids               = var.launch_template_vpc_security_groups
  tags                                 = local.common_tags
  user_data                            = base64encode(file("${path.module}/resources/build_full_grid_db_user_data.tpl"))

  iam_instance_profile {
    arn = aws_iam_instance_profile.build_full_grid_db.arn
  }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"
    tags          = local.resource_tags
  }
}
