data "aws_autoscaling_group" "smdb" {
  name = var.smdb_autoscaling_group
}

data "aws_kinesis_stream" "smdb_stream" {
  name = var.smdb_stream_name
}

resource "aws_iam_role" "build_full_grid_db" {
  name               = "${var.company}-${var.application}-${var.job_name}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/assume-role-policy.json")
}

data "template_file" "build_full_grid_db_role_policy" {
  template = file("${path.module}/resources/build-full-grid-db-role-policy.tpl")

  vars = {
    science_s3_bucket = var.science_s3_bucket
    smdb_stream       = data.aws_kinesis_stream.smdb_stream.arn
  }
}

resource "aws_iam_role_policy" "build_full_grid_db" {
  name = "${var.company}-${var.application}-${var.job_name}-${var.environment}"
  role = aws_iam_role.build_full_grid_db.id

  policy = data.template_file.build_full_grid_db_role_policy.rendered
}

resource "aws_iam_role_policy_attachment" "build_full_grid_db" {
  role       = aws_iam_role.build_full_grid_db.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "build_full_grid_db" {
  name = "${var.company}-${var.application}-${var.job_name}-${var.environment}"
  role = aws_iam_role.build_full_grid_db.name
}
