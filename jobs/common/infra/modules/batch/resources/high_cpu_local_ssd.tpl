MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="==MYBOUNDARY=="

--==MYBOUNDARY==
Content-Type: text/cloud-config; charset="us-ascii"

packages:
- nfs-utils

bootcmd:
- mkfs.ext4 -E nodiscard /dev/nvme2n1
- mkdir -p /mnt/ephemeral
- mount /dev/nvme2n1 /mnt/ephemeral

runcmd:
- echo ECS_IMAGE_PULL_BEHAVIOR=always >> /etc/ecs/ecs.config
- mkdir /ocean
- mount -t nfs4 -o ro prod-nfs-primary-1.aws.surfline.com:/ocean_aws_prod /ocean

--==MYBOUNDARY==--
