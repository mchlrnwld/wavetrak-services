variable "environment" {
  description = "Environment name: sandbox or prod."
}

variable "public_security_groups" {
  type        = list(string)
  description = "List of security group IDs in the public subnet."
}

variable "public_subnets" {
  type        = list(string)
  description = "List of public subnet IDs."
}

variable "private_security_groups" {
  type        = list(string)
  description = "List of security group IDs in the private subnet."
}

variable "private_subnets" {
  type        = list(string)
  description = "List of private subnet IDs."
}

variable "download_files_public_key" {
  description = "Public key for SSH access to download-files compute environment instances."
}

variable "high_cpu_local_ssd_public_key" {
  description = "Public key for SSH access to high-cpu-local-ssd compute environment instances."
}

variable "general_purpose_gpu_key" {
  description = "Public key for SSH access to cam-highlights compute environment instances."
}
