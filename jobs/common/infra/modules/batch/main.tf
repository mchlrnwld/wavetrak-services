# 1Password > Surfline > Engineering > wt-jobs-common-download-files-keypair-[environment]
resource "aws_key_pair" "download_files" {
  key_name   = "wt-jobs-common-download-files-${var.environment}"
  public_key = var.download_files_public_key
}

module "download_files" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-compute-environment-with-queue"

  company                     = "wt"
  application                 = "jobs-common"
  service                     = "airflow"
  environment                 = var.environment
  job_name                    = "download-files"
  min_on_demand_vcpus         = 0
  max_on_demand_vcpus         = 32
  root_volume_size            = 30
  instance_type               = ["m5"]
  ec2_key_pair                = aws_key_pair.download_files.key_name
  vpc_security_groups         = var.public_security_groups
  vpc_subnets                 = var.public_subnets
  user_data                   = file("${path.module}/resources/download_files.tpl")
  associate_public_ip_address = true
}

# 1Password > Surfline > Engineering > wt-jobs-common-compute-optimized-keypair-[environment]
resource "aws_key_pair" "high_cpu_local_ssd" {
  key_name   = "wt-jobs-common-high-cpu-local-ssd-${var.environment}"
  public_key = var.high_cpu_local_ssd_public_key
}

module "high_cpu_local_ssd" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-compute-environment-with-queue"

  company                     = "wt"
  application                 = "jobs-common"
  service                     = "airflow"
  environment                 = var.environment
  job_name                    = "high-cpu-local-ssd"
  min_on_demand_vcpus         = 0
  max_on_demand_vcpus         = 1024
  instance_type               = ["c5d.large", "c5d.xlarge", "c5d.2xlarge", "c5d.4xlarge"]
  root_volume_size            = 500
  ec2_key_pair                = aws_key_pair.high_cpu_local_ssd.key_name
  vpc_security_groups         = var.private_security_groups
  vpc_subnets                 = var.private_subnets
  user_data                   = file("${path.module}/resources/high_cpu_local_ssd.tpl")
  associate_public_ip_address = false
}

# 1Password > Surfline > Engineering > wt-jobs-common-compute-cam-highlights-keypair-[environment]
resource "aws_key_pair" "general_purpose_gpu_key" {
  key_name   = "wt-jobs-common-cam-highlights-${var.environment}"
  public_key = var.general_purpose_gpu_key
}

module "general_purpose_gpu" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/batch-compute-environment-with-queue"

  company                     = "wt"
  application                 = "jobs-common"
  service                     = "general-purpose-gpu"
  environment                 = var.environment
  job_name                    = "general-purpose-gpu"
  min_on_demand_vcpus         = 0
  # 10 instances, jobs can't share a GPU so they have to have an entire instance each
  max_on_demand_vcpus         = 60
  root_volume_size            = 100
  instance_type               = ["g4dn.xlarge"]
  user_data                   = file("${path.module}/resources/general_purpose_gpu.tpl")
  ec2_key_pair                = aws_key_pair.general_purpose_gpu_key.key_name
  vpc_security_groups         = var.private_security_groups
  vpc_subnets                 = var.private_subnets
  associate_public_ip_address = false
}
