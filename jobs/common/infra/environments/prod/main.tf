provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "jobs/common/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "build-full-grid-db" {
  source = "../../modules/build-full-grid-db"

  company                             = "wt"
  application                         = "jobs-common"
  environment                         = "prod"
  launch_template_vpc_security_groups = ["sg-1094ad77", "sg-a4a0e5c1", "sg-a5a0e5c0"]
  science_s3_bucket                   = "surfline-science-s3-prod"
  smdb_autoscaling_group              = "wt-science-models-db-ecs-prod"
  smdb_stream_name                    = "wt-science-models-db-prod"
}

module "batch" {
  source = "../../modules/batch"

  environment             = "prod"
  public_security_groups  = ["sg-a4a0e5c1", "sg-a5a0e5c0"]
  public_subnets          = ["subnet-baab36df", "subnet-debb6987"]
  private_security_groups = ["sg-1094ad77", "sg-a5a0e5c0", "sg-a4a0e5c1"]
  private_subnets         = ["subnet-d1bb6988", "subnet-85ab36e0"]

  # 1Password > Surfline > Engineering > wt-jobs-common-download-files-keypair-prod
  download_files_public_key = file("${path.module}/key-pairs/wt-jobs-common-download-files-keypair-prod.pub")
  # 1Password > Surfline > Engineering > wt-jobs-common-high-cpu-local-ssd-keypair-prod
  high_cpu_local_ssd_public_key = file("${path.module}/key-pairs/wt-jobs-common-high-cpu-local-ssd-keypair-prod.pub")
  # 1Password > Surfline > Engineering > wt-jobs-common-general-purpose-gpu-keypair-sandbox
  general_purpose_gpu_key = file("${path.module}/key-pairs/wt-jobs-common-general-purpose-gpu-keypair-prod.pub")
}
