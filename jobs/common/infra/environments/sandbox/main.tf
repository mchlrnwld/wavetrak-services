provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "jobs/common/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "build-full-grid-db" {
  source = "../../modules/build-full-grid-db"

  company                             = "wt"
  application                         = "jobs-common"
  environment                         = "dev"
  launch_template_vpc_security_groups = ["sg-91aeaef4", "sg-90aeaef5", "sg-2aafbf4e"]
  science_s3_bucket                   = "surfline-science-s3-dev"
  smdb_autoscaling_group              = "wt-science-models-db-ecs-sandbox"
  smdb_stream_name                    = "wt-science-models-db-sandbox"
}

module "batch" {
  source = "../../modules/batch"

  environment             = "sandbox"
  public_security_groups  = ["sg-91aeaef4", "sg-90aeaef5"]
  public_subnets          = ["subnet-0b09466e", "subnet-f4d458ad"]
  private_security_groups = ["sg-90aeaef5", "sg-2aafbf4e", "sg-91aeaef4"]
  private_subnets         = ["subnet-f2d458ab", "subnet-0909466c"]

  # 1Password > Surfline > Engineering > wt-jobs-common-download-files-keypair-sandbox
  download_files_public_key = file("${path.module}/key-pairs/wt-jobs-common-download-files-keypair-sandbox.pub")
  # 1Password > Surfline > Engineering > wt-jobs-common-high-cpu-local-ssd-keypair-sandbox
  high_cpu_local_ssd_public_key = file("${path.module}/key-pairs/wt-jobs-common-high-cpu-local-ssd-keypair-sandbox.pub")
  # 1Password > Surfline > Engineering > wt-jobs-common-general-purpose-gpu-keypair-sandbox
  general_purpose_gpu_key = file("${path.module}/key-pairs/wt-jobs-common-general-purpose-gpu-keypair-sandbox.pub")
}
