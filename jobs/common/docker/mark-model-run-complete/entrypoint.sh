#!/usr/bin/env bash
#
# Set the model run status to ONLINE. Environment variables expected:
#
# AIRFLOW_TMP_DIR: A temporary folder to write an empty file.
# AWS_DEFAULT_REGION: AWS region, for accessing secrets manager
# ENV: "sandbox" or "prod".
# JOB_NAME: The Airflow job name.
# MODEL_AGENCY: E.g. NOAA or Wavetrak
# MODEL_NAME: E.g Lotus-WW3 or GFS.
# MODEL_RUN: Run timestamp in "yyyymmddhh" format.
# MODEL_RUN_TYPE: Run type to set status for; LEGACY, POINT_OF_INTEREST, or
#   FULL_GRID. Optional, defaults to LEGACY.
#
# And for local development only (in `prod` and `sandbox` rely on AWS Secrets):
#
# SCIENCE_BUCKET: Bucket to write the job completed status file. Optional;
#   retrieves from AWS secrets manager (for ENV) if not provided.
# SCIENCE_DATA_SERVICE: Optional; retrieves from AWS secrets manager (for
#   ENV) if not provided.
#

set -ex

get_secret() {
  local NAME="$1"
  aws secretsmanager get-secret-value \
    --secret-id "${ENV}/common/${NAME}" \
    --query "SecretString" \
    --output text
}

# Read from AWS Secrets Manager only if value not provided in environment
if [ -z "${SCIENCE_DATA_SERVICE}" ]; then
  SCIENCE_DATA_SERVICE=$(get_secret "SCIENCE_DATA_SERVICE")
fi

if [ -z "${SCIENCE_BUCKET}" ]; then
  SCIENCE_BUCKET=$(get_secret "SCIENCE_BUCKET")
fi

if [ -z "${MODEL_RUN_TYPE}" ]; then
  echo "MODEL_RUN_TYPE variable required."
  exit 1
fi

echo "Setting model run ${MODEL_RUN} (${MODEL_RUN_TYPE}) online..."

GRAPH_QL_MUTATION=$(awk '{printf "%s\\n", $0}' - << EOF
mutation {
  set_model_run(runs: [{
    agency: \"${MODEL_AGENCY}\"
    model: \"${MODEL_NAME}\"
    run: ${MODEL_RUN}
    status: ONLINE
    type: ${MODEL_RUN_TYPE}
  }])
}
EOF
)

curl "${SCIENCE_DATA_SERVICE}/graphql/" \
  -H "Accept-Encoding: gzip, deflate, br" \
  -H "Content-Type: application/json" \
  -H "Cache-Control: no-cache" \
  -H "Accept: application/json" \
  -H "Connection: keep-alive" \
  --data-binary "{\"query\":\"${GRAPH_QL_MUTATION}\"}" \
  --compressed

if [ "${MODEL_RUN_TYPE}" == "LEGACY" ]; then
  S3_FILE_LOCATION="s3://${SCIENCE_BUCKET}/jobs/completed/${JOB_NAME}/${MODEL_RUN}"
  echo "Uploading file object to ${S3_FILE_LOCATION}..."
  touch "${AIRFLOW_TMP_DIR}/${MODEL_RUN}"
  aws s3 mv "${AIRFLOW_TMP_DIR}/${MODEL_RUN}" "${S3_FILE_LOCATION}"
fi
