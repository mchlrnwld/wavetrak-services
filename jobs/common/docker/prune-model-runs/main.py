import logging

from lib import config, helpers, kinesis_smdb, sds

logger = logging.getLogger('prune-model-runs')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    sds_client = sds.SDS(
        config.SCIENCE_DATA_SERVICE, config.MODEL_AGENCY, config.MODEL_NAME
    )
    kinesis_smdb_client = (
        kinesis_smdb.SMDB(
            config.STREAM_NAME, config.MODEL_AGENCY, config.MODEL_NAME
        )
        if config.STREAM_NAME is not None
        else None
    )

    available_model_runs = sds_client.list_available_model_runs(
        config.MODEL_RUN_TYPE
    )
    logger.info(
        f'Available {config.MODEL_RUN_TYPE} model runs: {available_model_runs}.'
    )

    model_runs_to_remove = helpers.find_model_runs_to_remove(
        available_model_runs, config.MODEL_RUN, config.MODEL_LIMIT
    )

    if model_runs_to_remove:
        logger.info(
            f'Taking {config.MODEL_RUN_TYPE} model runs offline: '
            f'{model_runs_to_remove}...'
        )
        sds_client.set_model_runs_status(
            model_runs_to_remove, 'OFFLINE', config.MODEL_RUN_TYPE
        )

        if kinesis_smdb_client is not None:
            oldest_model_run_to_keep = sorted(
                available_model_runs - model_runs_to_remove
            )[0]
            logger.info(
                f'Removing runs older than {oldest_model_run_to_keep} from Science Models DB...'
            )
            kinesis_smdb_client.prune_databases(oldest_model_run_to_keep)
    else:
        logger.info('No model runs to take offline.')


if __name__ == '__main__':
    main()
