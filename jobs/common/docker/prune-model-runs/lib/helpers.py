from typing import Collection, Set


def find_model_runs_to_remove(
    all_runs: Collection[int], new_run: int, model_limit: int
) -> Set[int]:
    """
    Determines which model runs need to be taken offline.

    Args:
        all_runs: List of model runs (YYYYMMDDHH) available.
        new_run: New model run (YYYYMMDDHH) to be added. Model
                 runs older than this model run will be considered for
                 removal.
        model_limit: Number of model runs to keep.
    Returns:
        Set of model runs (YYYYMMDDHH) to remove.
    """
    prior_runs = sorted(
        [run for run in all_runs if run < new_run], reverse=True
    )
    if len(prior_runs) >= model_limit:
        return set(prior_runs[model_limit - 1 :])

    return set()
