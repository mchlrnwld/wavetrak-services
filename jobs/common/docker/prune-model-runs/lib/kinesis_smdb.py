import json

import boto3  # type: ignore


class SMDB:
    """
    Client for managing Science Models DB via Kinesis Stream records.

    Args:
        stream_name: Name of AWS Kinesis Data Stream to put records to.
                     Science Models DB updates its inventory based on these
                     records.
        agency: Agency of model to update.
        model: Name of model to update.

    Attributes:
        stream_name: Name of AWS Kinesis Data Stream to put records to.
                     Science Models DB updates its inventory based on these
                     records.
        agency: Agency of model to update.
        model: Name of model to update.
    """

    def __init__(self, stream_name: str, agency: str, model: str):
        self.stream_name = stream_name
        self.agency = agency
        self.model = model
        self.kinesis_client = boto3.client('kinesis')

    def prune_databases(self, run: int):
        """
        Puts JSON encoded Kinesis record to update databases in Science Models
        DB instances.

        Args:
            run: Oldest model run to keep. Model runs older than this run are
                 subject to be deleted.
        """
        self.kinesis_client.put_record(
            StreamName=self.stream_name,
            PartitionKey='Prune Older Databases',
            Data=json.dumps(
                {'agency': self.agency, 'model': self.model, 'run': run}
            ).encode(),
        )
