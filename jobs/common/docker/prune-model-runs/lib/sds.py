import itertools
from typing import Any, Dict, Iterable, Set

import json
import requests


def create_model_run_json(
    agency: str, model: str, run: int, status: str, run_type: str
) -> Dict[str, Any]:
    """
    Create a dict representing a model run's JSON payload.

    Args:
        agency: Agency of model run.
        model: Model of model run.
        run: Model run time YYYYMMDDHH.
        status: Status of 'ONLINE', 'PENDING', or 'OFFLINE'

    Returns:
        Dict with model run meta data.
    """
    return {
        'agency': agency,
        'model': model,
        'run': run,
        'status': status,
        'type': run_type,
    }


class SDS:
    """
    Client for managing model runs through Science Data Service.

    Args:
        host_url: URL for Science Data Service host.

    Attributes:
        model_runs_url (str): URL for managing model runs.
    """

    def __init__(self, host_url: str, agency: str, model: str):
        self.agency = agency
        self.model = model
        self.model_runs_url = f'{host_url}/model_runs'

    def list_available_model_runs(self, model_run_type: str) -> Set[int]:
        """
        List available (PENDING, ONLINE) model runs.

        Args:
            model_run_type: One of LEGACY, FULL_GRID, or POINT_OF_INTEREST.

        Returns:
            Set of available (PENDING, ONLINE) model runs (YYYYMMDDHH).
        """
        response = requests.get(
            self.model_runs_url,
            params={
                'agency': self.agency,
                'model': self.model,
                'status': 'PENDING,ONLINE',
                'types': model_run_type,
            },
        )
        response.raise_for_status()
        return set(model_run['run'] for model_run in response.json())

    def set_model_runs_status(
        self, runs: Iterable[int], status: str, model_run_type: str
    ):
        set_model_runs_response = requests.put(
            self.model_runs_url,
            headers={'Content-Type': 'application/json'},
            data=json.dumps(
                [
                    create_model_run_json(
                        self.agency, self.model, run, status, model_run_type
                    )
                    for run in runs
                ]
            ),
        )
        set_model_runs_response.raise_for_status()
