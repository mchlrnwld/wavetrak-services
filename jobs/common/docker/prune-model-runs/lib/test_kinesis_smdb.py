import json

import boto3  # type: ignore
from moto import mock_kinesis  # type: ignore
import pytest  # type: ignore

from lib.kinesis_smdb import SMDB

STREAM_NAME = 'science-models-db-stream'
MODEL_AGENCY = 'Wavetrak'
MODEL_NAME = 'Proteus-WW3'
MODEL_RUN = 2019081906
MODEL_LIMIT = 2


@pytest.fixture(autouse=True)
def environment_variables(monkeypatch):
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'test-aws-access-key-id')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'test-secret-access-key')
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')


@pytest.fixture
def create_kinesis_stream():
    with mock_kinesis():
        kinesis_client = boto3.client('kinesis')
        kinesis_client.create_stream(StreamName=STREAM_NAME, ShardCount=1)
        stream = kinesis_client.describe_stream(StreamName=STREAM_NAME)
        shard_id = stream['StreamDescription']['Shards'][0]['ShardId']
        shard_iterator = kinesis_client.get_shard_iterator(
            StreamName=STREAM_NAME,
            ShardId=shard_id,
            ShardIteratorType='LATEST',
        )['ShardIterator']
        yield kinesis_client, shard_iterator


def test_complete_database(create_kinesis_stream):
    kinesis_client, shard_iterator = create_kinesis_stream
    smdb_client = SMDB(STREAM_NAME, MODEL_AGENCY, MODEL_NAME)
    smdb_client.prune_databases(MODEL_RUN)
    records = kinesis_client.get_records(ShardIterator=shard_iterator)[
        'Records'
    ]
    assert len(records) == 1
    assert records[0]['PartitionKey'] == 'Prune Older Databases'
    assert (
        records[0]['Data']
        == json.dumps(
            {'agency': MODEL_AGENCY, 'model': MODEL_NAME, 'run': MODEL_RUN}
        ).encode()
    )
