from lib.helpers import find_model_runs_to_remove


def test_find_model_runs_to_remove_new_run_is_the_latest():
    new_run = 2018111618
    all_runs = [
        new_run,  # New model run about to come online.
        2018111606,
        2018111518,  # Last model run to keep.
        2018111512,
        2018111506,
        2018111500,
        2018111612,  # Current model run. (out of order)
    ]
    model_limit = 4
    assert find_model_runs_to_remove(all_runs, new_run, model_limit) == {
        2018111512,
        2018111506,
        2018111500,
    }


def test_find_model_runs_to_remove_new_run_not_latest():
    new_run = 2018111612
    model_limit = 4
    all_runs = [
        2018111618,  # Future model run already exists
        new_run,  # New run to be added is not the latest run.
        2018111606,
        2018111518,
        2018111512,  # Last model run to keep
        2018111506,
        2018111500,
        2018111418,
    ]
    model_limit = 4
    assert find_model_runs_to_remove(all_runs, new_run, model_limit) == {
        2018111506,
        2018111500,
        2018111418,
    }

    new_run = 2018111518
    model_limit = 1
    all_runs = [
        2018111618,  # Future model runs already exist.
        2018111612,
        2018111606,
        new_run,  # New run to be added is not the latest run.
        # All runs after this should be removed.
        2018111512,
        2018111506,
        2018111500,
        2018111418,
    ]
    model_limit = 1
    assert find_model_runs_to_remove(all_runs, new_run, model_limit) == {
        2018111512,
        2018111506,
        2018111500,
        2018111418,
    }


def test_find_model_runs_to_remove_new_run_is_the_last():
    new_run = 2018111518
    all_runs = [2018111618, 2018111612, 2018111606, 2018111600, new_run]
    model_limit = 4
    assert find_model_runs_to_remove(all_runs, new_run, model_limit) == set()


def test_find_model_runs_to_remove_all_runs_equal_model_limit():
    # New run is the latest run available.
    # No runs should be removed.
    new_run = 2018111618
    all_runs = [
        new_run,  # New model run about to come online.
        2018111612,  # Current model run.
        2018111606,
    ]
    model_limit = 3
    assert find_model_runs_to_remove(all_runs, new_run, model_limit) == set()

    # New run is not the latest run available.
    # No runs should be removed.
    new_run = 2018111612
    all_runs = [
        2018111618,  # Current model run.
        new_run,  # New model run about to come online.
        2018111606,
    ]
    model_limit = 3
    assert find_model_runs_to_remove(all_runs, new_run, model_limit) == set()


def test_find_model_runs_to_remove_all_runs_less_than_model_limit():
    new_run = 2018111618
    all_runs = [
        new_run,  # New model run about to come online.
        2018111612,  # Current model run.
        2018111606,
    ]
    model_limit = 4
    assert find_model_runs_to_remove(all_runs, new_run, model_limit) == set()
