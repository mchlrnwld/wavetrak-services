# Prune Model Runs

Docker image to remove old model runs and clean Science Models DB database files.

## Setup

```sh
$ conda devenv
$ source activate common-prune-model-runs
$ cp .env.sample .env
$ env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t common/prune-model-runs .
$ docker run --env-file=.env --volume $(pwd)/data:/opt/app/data --rm -it common/prune-model-runs
```
