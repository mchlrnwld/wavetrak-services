import os
from unittest import mock

import pytest  # type:ignore

from lib.batch import CannotStartContainerError


@mock.patch.dict(
    os.environ,
    {
        'JOB_NAME': 'test-job',
        'JOB_QUEUE': 'test-queue',
        'JOB_DEFINITION': 'test-definition',
        'TIMEOUT': '0',
        'ENVIRONMENT': '',
    },
)
def test_execute_batch_job():
    # NOTE: main module needs to be imported here
    # to mock the environment variables.
    from main import execute_batch_job

    with mock.patch('lib.batch.boto3.client'), mock.patch(
        'lib.log_stream.LogStream'
    ), mock.patch('lib.batch.BatchJob.start'):
        attempts = 0

        def wait_with_retries():
            nonlocal attempts
            attempts += 1
            raise CannotStartContainerError

        # Error is listed in AUTO_RETRY_EXCEPTIONS. Batch Job will be retried.
        with mock.patch(
            'lib.batch.BatchJob.wait', side_effect=wait_with_retries
        ), pytest.raises(CannotStartContainerError) as container_error:
            execute_batch_job(
                'test', 'test-queue', 'test-definition', 1, {'ENV': 'sandbox'}
            )
            assert attempts == 4
            assert isinstance(container_error.value, CannotStartContainerError)

        attempts = 0

        def wait_no_retries():
            nonlocal attempts
            attempts += 1
            raise Exception('do not retry')

        # Error is *not* listed in AUTO_RETRY_EXCEPTIONS.
        # Batch job will *not* be retried.
        with mock.patch(
            'lib.batch.BatchJob.wait', side_effect=wait_no_retries
        ), pytest.raises(Exception) as container_error:
            execute_batch_job(
                'test', 'test-queue', 'test-definition', 1, {'ENV': 'sandbox'}
            )
            assert attempts == 1

        attempts = 0

        def wait_suceeded():
            nonlocal attempts
            attempts += 1
            yield 'SUCCEEDED'

        with mock.patch('lib.batch.BatchJob.wait', side_effect=wait_suceeded):
            outcome = execute_batch_job(
                'test', 'test-queue', 'test-definition', 1, {'ENV': 'sandbox'}
            )
            assert outcome == 'SUCCEEDED'
            assert attempts == 1
