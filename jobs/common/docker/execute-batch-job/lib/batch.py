import logging
from datetime import datetime
from time import sleep
from typing import Any, Dict, Generator, Optional

import boto3  # type: ignore
from backoff import full_jitter  # type: ignore
from botocore.config import Config  # type: ignore

logger = logging.getLogger('batch')


class CannotStartContainerError(BaseException):
    def __str__(self):
        return 'CannotStartContainerError'


AUTO_RETRY_EXCEPTIONS = [CannotStartContainerError]


class BatchJob:
    """
    BatchJob manages executing an AWS Batch Job.

    Args:
        job_name: Name of job to submit to Batch.
        job_queue: Name of queue to submit job to.
        job_definition: Name of job definition for compute environment to
                        execute.
        timeout: Execution timeout for job.
        environment: Environment variables to set for job.
        wait_interval: Polling interval (in seconds) when waiting for job to
                       start or finish.
        boto_config: Boto3 Config options.

    Attributes:
        job_id: Job ID to reference the submitted job.
        log_stream_name: Name of CloudWatchLogs LogStream for the submitted
                         job.
    """

    job_id: str
    log_stream_name: Optional[str]

    def __init__(
        self,
        job_name: str,
        job_queue: str,
        job_definition: str,
        timeout: int,
        environment: Dict[str, str],
        wait_interval: int = 15,
        boto_config: Optional[Config] = None,
    ):
        self.client = boto3.client('batch', config=boto_config)
        self.job_name = job_name
        self.job_queue = job_queue
        self.job_definition = job_definition
        self.timeout = timeout
        self.environment = [
            {"name": key, "value": value} for key, value in environment.items()
        ]
        self.wait_interval = wait_interval
        self.log_stream_name = None

    def _describe(self) -> Dict[str, Any]:
        return self.client.describe_jobs(jobs=[self.job_id])['jobs'][0]

    def start(self):
        """
        Submits Batch job and waits for job to start running. Sets the
        job_id and log_stream_name properties once job has started.
        """
        logger.info(f'Submitting job at {datetime.utcnow()}...')
        self.job_id = self.client.submit_job(
            jobName=self.job_name,
            jobQueue=self.job_queue,
            jobDefinition=self.job_definition,
            containerOverrides={'environment': self.environment},
            timeout={'attemptDurationSeconds': self.timeout},
        )['jobId']

        logger.info(f'Job ID: {self.job_id}')

        while True:
            sleep(full_jitter(self.wait_interval))

            description = self._describe()
            status = description['status']
            started_at = description.get('startedAt', None)

            if status == 'FAILED':
                break
            elif not started_at:
                logger.info(
                    f'Waiting for job to start... Current status: {status}'
                )
            else:
                started_at_dt = datetime.fromtimestamp(started_at / 1000.0)
                logger.info(f'Job started at {started_at_dt}.')

                self.log_stream_name = description['container'][
                    'logStreamName'
                ]
                logger.info(f'Log Stream: {self.log_stream_name}')
                break

    def wait(self) -> Generator[str, None, None]:
        """
        Polls job status until execution has finished.

        Returns:
            Generator that yields that job's status on each status check until
            the job is no longer running.
        """
        while True:
            sleep(full_jitter(self.wait_interval))

            description = self._describe()

            yield description['status']

            if 'stoppedAt' in description:
                stopped_at = datetime.fromtimestamp(
                    description['stoppedAt'] / 1000.0
                )
                job_reason = description['statusReason']
                container_reason = description['container'].get('reason', None)

                logger.info(
                    f'Job stopped at {stopped_at} for reason: {job_reason}.'
                )

                if container_reason:
                    logger.info(
                        f'Container exited for reason: {container_reason}'
                    )
                    container_exceptions = [
                        container_exception
                        for container_exception in AUTO_RETRY_EXCEPTIONS
                        if str(container_exception()) in container_reason
                    ]
                    if container_exceptions:
                        raise container_exceptions[0]

                return
