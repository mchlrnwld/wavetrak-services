import os

JOB_NAME = os.environ['JOB_NAME']
JOB_QUEUE = os.environ['JOB_QUEUE']
JOB_DEFINITION = os.environ['JOB_DEFINITION']
TIMEOUT = int(os.environ['TIMEOUT'])
ENVIRONMENT = {
    key: value
    for key, value in [
        tuple(variable.split('='))
        for variable in os.getenv('ENVIRONMENT', '').split(';')
        if variable
    ]
}
