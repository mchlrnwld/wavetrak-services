from datetime import datetime
from time import sleep
from typing import Generator, Optional, Tuple

import boto3  # type: ignore
from backoff import full_jitter  # type: ignore
from botocore.config import Config  # type: ignore


class LogStream:
    """
    LogStream allows streaming log events from AWS CloudWatch Logs.

    Args:
        name: Name of Log Stream to stream log events from.
        wait_interval: Polling interval (in seconds) when waiting for job to
                       start or finish.
        boto_config: Boto3 Config options.
    """

    def __init__(
        self,
        name: str,
        wait_interval: int = 15,
        boto_config: Optional[Config] = None,
    ):
        self.client = boto3.client('logs', config=boto_config)
        self.name = name
        self.wait_interval = wait_interval
        self.next_token = None

    def more(self) -> Generator[Tuple[datetime, str], None, None]:
        """
        Gets log events' timestamp and message from Log Stream.

        Returns:
            Generator that yields (timestamp, message) tuple until no new logs
            are found.
        """
        while True:
            sleep(full_jitter(self.wait_interval))
            args = {
                'logGroupName': '/aws/batch/job',
                'logStreamName': self.name,
                'startFromHead': True,
            }
            if self.next_token:
                args['nextToken'] = self.next_token

            log_events = self.client.get_log_events(**args)

            for event in log_events['events']:
                yield (
                    datetime.utcfromtimestamp(event['timestamp'] / 1000),
                    event['message'],
                )

            next_forward_token = log_events['nextForwardToken']
            if next_forward_token == self.next_token:
                return

            self.next_token = next_forward_token
