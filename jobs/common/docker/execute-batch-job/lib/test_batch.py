from unittest import mock

import pytest  # type: ignore

from lib.batch import BatchJob, CannotStartContainerError

retry_failed_exception = {
    'status': 'FAILED',
    'stoppedAt': 1610398001597,
    'statusReason': 'Test failure',
    'container': {'reason': 'CannotStartContainerError'},
}

do_not_retry_failed_exception = {
    'status': 'FAILED',
    'stoppedAt': 1610398001597,
    'statusReason': 'Test failure',
    'container': {'reason': 'CannotInspectContainerError'},
}

job_succeeded = {
    'status': 'SUCCEEDED',
    'stoppedAt': 1610398001597,
    'statusReason': 'Essential container in task exited',
    'container': {},
}


def test_wait():
    with mock.patch('lib.batch.boto3.client'):
        batch = BatchJob(
            'test', 'test-queue', 'test-definition', 1, {'ENV': 'sandbox'}, 0
        )
        # describe() returns an exception that should be retried
        with mock.patch(
            'lib.batch.BatchJob._describe', return_value=retry_failed_exception
        ), pytest.raises(CannotStartContainerError) as container_error:
            results = [result for result in batch.wait()]
            assert results[0] == 'FAILED'
            assert isinstance(container_error.value, CannotStartContainerError)

        # describe() returns an exception that should not be retried
        with mock.patch(
            'lib.batch.BatchJob._describe',
            return_value=do_not_retry_failed_exception,
        ):
            results = [result for result in batch.wait()]
            assert results[0] == 'FAILED'

        # # describe() returns an exception that should not be retried
        with mock.patch(
            'lib.batch.BatchJob._describe', return_value=job_succeeded
        ):
            results = [result for result in batch.wait()]
            assert results[0] == 'SUCCEEDED'
