import logging
from typing import Dict, Optional

import backoff  # type: ignore
from botocore.config import Config  # type: ignore

import lib.config as config
from lib.batch import AUTO_RETRY_EXCEPTIONS, BatchJob
from lib.log_stream import LogStream

logger = logging.getLogger('batch')


@backoff.on_exception(
    backoff.expo, (*AUTO_RETRY_EXCEPTIONS), max_tries=4,
)
def execute_batch_job(
    job_name: str,
    job_queue: str,
    job_definition: str,
    timeout: int,
    environment: Dict[str, str],
) -> Optional[str]:
    """
    Executes batch job until the job succeeds or fails.

    Args:
        job_name: Name of job to submit to Batch.
        job_queue: Name of queue to submit job to.
        job_definition: Name of job definition for compute environment to
                        execute.
        timeout: Job duration limit in seconds.
        environment: Environment variables to use for the Batch job.

    Returns:
        The status of the job and exception if the job failed.
    """
    wait_interval = 15
    boto_config = Config(retries={'mode': 'standard', 'max_attempts': 5})
    batch_job = BatchJob(
        job_name,
        job_queue,
        job_definition,
        timeout,
        environment,
        wait_interval,
        boto_config,
    )

    batch_job.start()

    log_stream = (
        LogStream(batch_job.log_stream_name, wait_interval, boto_config)
        if batch_job.log_stream_name
        else None
    )
    outcome: Optional[str] = None
    for status in batch_job.wait():
        outcome = status
        if log_stream:
            for timestamp, message in log_stream.more():
                logger.info(f'[{timestamp}] - {message}')

    logger.info(f'Job {outcome}.')
    return outcome


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    outcome = execute_batch_job(
        config.JOB_NAME,
        config.JOB_QUEUE,
        config.JOB_DEFINITION,
        config.TIMEOUT,
        config.ENVIRONMENT,
    )

    if outcome == 'FAILED':
        raise Exception('Job FAILED.')


if __name__ == '__main__':
    main()
