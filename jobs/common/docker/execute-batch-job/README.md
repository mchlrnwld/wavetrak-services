# Execute Batch Job

Docker image to execute and output logs from a AWS Batch Job.

## Environment

Environment variables can be seen in [.env.sample](.env.sample).

- `JOB_NAME` - Name of job to submit to Batch.
- `JOB_QUEUE` - Name of queue to submit job to.
- `JOB_DEFINITION` - Name of job definition for compute environment to execute.
- `TIMEOUT` - Execution timeout in seconds.
- `ENVIRONMENT` - Comma separated list of environment variables. For example, `NAME1=VALUE1,NAME2=VALUE2`

## Setup

```sh
$ conda env create -f environment.yml
$ source activate common-execute-batch-job
$ cp .env.sample .env
$ env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t common/execute-batch-job .
$ docker run --env-file=.env --volume ~/.aws:/root/.aws --rm -it common/execute-batch-job
```
