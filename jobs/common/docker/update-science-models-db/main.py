import logging

from lib import config, kinesis_smdb, sds, smdb

logger = logging.getLogger('update-science-models-db')


def main():
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    sds_client = sds.SDS(
        config.SCIENCE_DATA_SERVICE, config.MODEL_AGENCY, config.MODEL_NAME
    )
    smdb_client = smdb.SMDB(config.MODEL_AGENCY, config.MODEL_NAME, config.ENV)
    kinesis_smdb_client = kinesis_smdb.SMDB(
        config.STREAM_NAME, config.MODEL_AGENCY, config.MODEL_NAME
    )

    online_model_runs = sds_client.list_online_model_runs(
        config.MODEL_RUN_TYPE
    )

    if all(model_run != config.MODEL_RUN for model_run in online_model_runs):
        logger.info(
            f'Completing Science Models DB model run: {config.MODEL_RUN}...'
        )
        kinesis_smdb_client.complete_database(config.MODEL_RUN)
        smdb_client.wait_for_inventory_updated(config.MODEL_RUN)
    else:
        logger.info(f'Model run {config.MODEL_RUN} already online.')


if __name__ == '__main__':
    main()
