from typing import Set

import requests


class SDS:
    """
    Client for managing model runs through Science Data Service.

    Args:
        host_url: URL for Science Data Service host.

    Attributes:
        model_runs_url (str): URL for managing model runs.
    """

    def __init__(self, host_url: str, agency: str, model: str):
        self.agency = agency
        self.model = model
        self.model_runs_url = f'{host_url}/model_runs'

    def list_online_model_runs(self, model_run_type: str) -> Set[int]:
        """
        List ONLINE model runs.

        Returns:
            Set of ONLINE model runs (YYYYMMDDHH).
        """
        response = requests.get(
            self.model_runs_url,
            params={
                'agency': self.agency,
                'model': self.model,
                'status': 'ONLINE',
                'types': model_run_type,
            },
        )
        response.raise_for_status()
        return set(model_run['run'] for model_run in response.json())
