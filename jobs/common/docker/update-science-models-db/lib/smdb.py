import logging

import boto3  # type: ignore
import requests

from lib.helpers import wait

logger = logging.getLogger('update-science-models-db')

ec2_client = boto3.client('ec2')
ecs_client = boto3.client('ecs')


class SMDB:
    """
    Client for managing Science Models DB.

    Args:
        agency: Agency of model run.
        model: Model of model run.
        environment: Application environment of Science Models DB to
            manage.

    Attributes:
        instance_ids ([str]): List of instance IDs in Science Models DB
            cluster.
        task_urls ([str]): List of task URLs in Science Models DB cluster.
    """

    def __init__(self, agency: str, model: str, environment: str):
        self.agency = agency
        self.model = model
        service_name = f'wt-science-models-db-{environment}'
        logger.info(
            'Looking up Science Models DB instances and tasks to ' 'update...'
        )

        # Get container instance ARNs
        ci_arns = ecs_client.list_container_instances(
            cluster=service_name, status='ACTIVE'
        )['containerInstanceArns']

        # Get EC2 instance IPs for container instance ARNs
        cis = ecs_client.describe_container_instances(
            cluster=service_name, containerInstances=ci_arns
        )['containerInstances']

        ci_ec2_ids = {
            ci['containerInstanceArn']: ci['ec2InstanceId'] for ci in cis
        }

        ec2_instances = ec2_client.describe_instances(
            InstanceIds=list(ci_ec2_ids.values())
        )['Reservations'][0]['Instances']

        self.instance_ids = [
            instance['InstanceId'] for instance in ec2_instances
        ]

        ec2_ips = {
            instance['InstanceId']: instance['PrivateIpAddress']
            for instance in ec2_instances
        }

        # Map container instance ARNs to EC2 instance IPs
        ci_arns = [ci['containerInstanceArn'] for ci in cis]
        ci_ips = {ci_arn: ec2_ips[ci_ec2_ids[ci_arn]] for ci_arn in ci_arns}

        # Get task ARNs
        task_arns = ecs_client.list_tasks(
            cluster=service_name,
            serviceName=service_name,
            desiredStatus='RUNNING',
        )['taskArns']

        tasks = ecs_client.describe_tasks(
            cluster=service_name, tasks=task_arns
        )['tasks']

        # Build task URLs using container instance IPs and ports
        ci_arns_and_ports = [
            (
                task['containerInstanceArn'],
                task['containers'][0]['networkBindings'][0]['hostPort'],
            )
            for task in tasks
        ]

        self.task_urls = [
            f'http://{ci_ips[ci_arn]}:{port}'
            for ci_arn, port in ci_arns_and_ports
        ]

    def wait_for_inventory_updated(self, run: int):
        """
        Waits for database inventory for each Science Models DB task to be
        updated. Raises exception if update not successful within 10 minutes.

        Args:
            run: Model run to check.
        """
        for task_url in self.task_urls:

            def check_inventory_updated() -> bool:
                inventory_response = requests.get(f'{task_url}/inventory')
                inventory_response.raise_for_status()
                inventory = inventory_response.json()

                inventory_runs = set(
                    item['run']
                    for item in inventory
                    if item['agency'] == self.agency
                    if item['model'] == self.model
                )

                return run in inventory_runs

            wait(check_inventory_updated)
