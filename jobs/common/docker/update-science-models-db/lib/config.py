import os

from job_secrets_helper import get_secret  # type: ignore

ENV = os.environ['ENV']
MODEL_AGENCY = os.environ['MODEL_AGENCY']
MODEL_NAME = os.environ['MODEL_NAME']
MODEL_RUN = int(os.environ['MODEL_RUN'])
MODEL_RUN_TYPE = os.environ['MODEL_RUN_TYPE']

SCIENCE_DATA_SERVICE = get_secret(f'{ENV}/common/', 'SCIENCE_DATA_SERVICE')
STREAM_NAME = os.environ['STREAM_NAME']
