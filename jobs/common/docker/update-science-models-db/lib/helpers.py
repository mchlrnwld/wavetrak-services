from time import sleep, time
from typing import Callable


class TimeoutError(Exception):
    pass


def wait(attempt: Callable[[], bool], interval: int = 15, timeout: int = 600):
    """
    Wait for attempt to succeed. Executes attempt until succeeds or times out.

    Args:
        attempt: Function to execute on each attempt. Returns boolean to
            indicate success or failure.
        interval: Interval in seconds to wait between each attempt.
            Defaults to 15.
        timeout: Execution timeout in seconds. Defaults to 600.
    """
    start = time()
    while True:
        if timeout is not None and time() - start > timeout:
            raise TimeoutError(f'Wait timed out after {timeout} seconds.')

        success = attempt()
        if success:
            break

        sleep(interval)
