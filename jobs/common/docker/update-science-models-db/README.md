# Update Science Models DB

Docker image to update Science Models DB and clean database files.

## Setup

```sh
$ conda devenv
$ source activate common-update-science-models-db
$ cp .env.sample .env
$ env $(cat .env | xargs) python main.py
```

## Docker

### Build and Run

```sh
$ docker build -t common/update-science-models-db .
$ docker run --env-file=.env --volume $(pwd)/data:/opt/app/data --rm -it common/update-science-models-db
```
