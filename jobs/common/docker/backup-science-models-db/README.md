# Backup Science Models DB

Docker image for backing up a Science Models DB model run to S3. Uses AWS CLI to send a run command to a healthy Science Models DB instance to run `aws s3 sync`.

## Setup

```
$ cp .env.sample .env
$ env $(xargs < .env) ./entrypoint.sh
```

## Docker

### Build and Run

```sh
$ docker build -t common/backup-science-models-db .
$ docker run --env-file=.env --rm -it common/backup-science-models-db
```
