#!/usr/bin/env bash

set -e

echo "Getting variables..."
if [ -z "${SCIENCE_BUCKET}" ]; then
  SCIENCE_BUCKET=$(aws secretsmanager get-secret-value --secret-id="${ENV}/common/SCIENCE_BUCKET" | jq -r '.SecretString')
fi

MODEL_PATH="${MODEL_AGENCY}/${MODEL_NAME}/${MODEL_RUN}"
echo "Backing up ${MODEL_PATH}..."

echo "Looking up a healthy ${SERVICE_NAME} instance..."

TARGET_GROUP_ARN=$(aws elbv2 describe-target-groups \
  --names="${SERVICE_NAME}" \
  | jq -r '.TargetGroups[0].TargetGroupArn')

HEALTHY_INSTANCE_ID=$(aws elbv2 describe-target-health \
  --target-group-arn="${TARGET_GROUP_ARN}" \
  | jq -r '.TargetHealthDescriptions[] | [select(.TargetHealth.State == "healthy")][0].Target.Id')

echo "Creating backup from instance ${HEALTHY_INSTANCE_ID}..."

LOCAL_PATH="/mnt/storage/data/${MODEL_PATH}/"
S3_LOCATION="s3://${SCIENCE_BUCKET}/science-models-db/backups/${MODEL_PATH}/"

COMMAND="time aws s3 sync --only-show-errors --delete ${LOCAL_PATH} ${S3_LOCATION} && echo ${MODEL_PATH} backed up!"
COMMAND_ID=$(aws ssm send-command \
  --instance-ids="${HEALTHY_INSTANCE_ID}" \
  --document-name=AWS-RunShellScript \
  --parameters=commands="${COMMAND}" \
  | jq -r '.Command.CommandId')

echo "Waiting for backup command ${COMMAND_ID} on ${HEALTHY_INSTANCE_ID} to complete..."
while true; do
  COMMAND_INVOCATION=$(aws ssm get-command-invocation \
    --command-id="${COMMAND_ID}" \
    --instance-id="${HEALTHY_INSTANCE_ID}")

  STATUS=$(echo "${COMMAND_INVOCATION}" | jq -r '.Status')
  STDOUT=$(echo "${COMMAND_INVOCATION}" | jq -r '.StandardOutputContent')
  STDERR=$(echo "${COMMAND_INVOCATION}" | jq -r '.StandardErrorContent')

  echo "Backup command status: ${STATUS}"

  if [[ -n "${STDOUT}" ]]; then
    printf "Standard Output:\n%s\n" "${STDOUT}"
  fi

  if [[ -n "${STDERR}" ]]; then
    printf "Standard Error:\n%s\n" "${STDERR}"
  fi

  case "${STATUS}" in
    Success)
      exit 0
      ;;
    Delayed | Pending | "InProgress")
      continue
      ;;
    *)
      exit 1
  esac

  sleep 20
done
