import { getSecret } from '@surfline/services-common/dist/utils/secretsHelper';

export default async () => ({
  MONGO_CONNECTION_STRING_KBYG_PROD: await getSecret('sandbox/mongo-env-sync/', 'MONGO_CONNECTION_STRING_KBYG_PROD'),
  MONGO_CONNECTION_STRING_KBYG: await getSecret('sandbox/common/', 'MONGO_CONNECTION_STRING_KBYG'),
  TMP_DIR: process.env.AIRFLOW_TMP_DIR,
});
