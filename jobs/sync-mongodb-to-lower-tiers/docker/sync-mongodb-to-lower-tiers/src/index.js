import backup from 'mongodb-backup';
import restore from 'mongodb-restore';
import getConfigs from './config';

const backupDB = (config) => new Promise((resolve, reject) => {
  console.log('Dumping prod KBYG')
  backup({
    uri: config.MONGO_CONNECTION_STRING_KBYG_PROD,
    root: `${config.TMP_DIR}`,
    callback: err => {
      if (err) {
        return reject(err);
      }
      return resolve();
    },
  });
});

const restoreDB = (config) => new Promise((resolve, reject) => {
  console.log('Restoring to dev KBYG')
  restore({
    uri: config.MONGO_CONNECTION_STRING_KBYG,
    root: `${config.TMP_DIR}`,
    callback: err => {
      if (err) {
        return reject(err);
      }
      console.log('Finished syncing KBYG DBs');
      return resolve();
    }
  })
});

const main = async (config) => {
  await backupDB(config);
  await restoreDB(config);
}

getConfigs()
  .then(config => main(config))
  .catch((err) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    process.exit(0);
  });
