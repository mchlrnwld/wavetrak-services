from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.operators import WTDockerOperator

from dag_helpers.docker import docker_image

JOB_NAME = 'sync-mongodb-to-lower-tiers'

# DAG definition
default_args = {
    'owner': 'wavetrak',
    'start_date': datetime(2020, 2, 18, 0),
    'email': 'kbygsquad@surfline.com',
    'email_on_failure': True,
    'retries': 0,
}

dag = DAG(
    '{0}-v1'.format(JOB_NAME),
    schedule_interval=timedelta(weeks=1),
    max_active_runs=1,
    default_args=default_args,
)

WTDockerOperator(
    task_id='sync-mongodb-to-lower-tiers',
    image=docker_image('sync-mongodb-to-lower-tiers', JOB_NAME),
    force_pull=True,
    dag=dag,
)
